  $(function() {
  
    // Setup form validation on the #register-form element
    $("#register-form").validate({
    
        // Specify the validation rules
        rules: {
           
            Year: "required",
            dob: "required",
            gender: "required",
            city: "required",
            mob_no: {
                required:true,
               
            },
            seeking: "required",

            name:{
                required:true,
                maxlength: 50
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                maxlength: 8
            }
            
        },
        
        // Specify the validation error messages
        messages: {
            name:{
                required:"Please Enter Your Name",
                maxlength: "Please enter no more than 50 characters."
            },
            dob : "Please Enter Birth Date",
            gender: "Please Select Gender",
            city: "Please Enter Your City",
            Year: "Please Enter Your date",
            mob_no: {
                required:"Please Enter Your Mobile No",
                
            },
            seeking: "Please Select Seeking Option",
            password: {
                required: "Please provide a password",
                maxlength: "Please enter no more than 8 digit password."
            },
            email: "Please Enter a valid Email Address e.g abc@gmail.com"
            
        },
        
        // submitHandler: function(form) {
        //     form.submit();
        // }
    });

    $("#selfprofile-form").validate({
    
        // Specify the validation rules
        rules: {
           
            name: "required",
            dob: "required",
            gender: "required",
            city: "required",
            height:"required",
            mob_no: {
                required:true,
              //  maxlength:12
            },
            seeking: "required",

            name:{
                required:true,
                maxlength: 50
            },
            email: {
                required: true,
                email: true
            },
            
            
        },
        
        // Specify the validation error messages
        messages: {
            name:{
                required:"Please Enter Your Name",
                maxlength: "Please enter no more than 50 characters."
            },
            dob : "Please Enter Birth Date",
            gender: "Please Select Gender",
            city: "Please Enter Your City",
            height: "Please Enter Your height",
            mob_no: {
                required:"Please Enter Your Mobile No",
               // maxlength:"Please enter no more than 12 digit mobile number"
            },
            seeking: "Please Select Seeking Option",
            email: "Please Enter a valid Email Address e.g abc@gmail.com"
            
        },
        
        // submitHandler: function(form) {
        //     form.submit();
        // }
    });

    $("#cnt-form").validate({
    
        // Specify the validation rules
        rules: {
            name: "required",
            phone: "required",   
            message: "required",
            email: {
                required: true,
                email: true
            },
          
            
        },
        
        // Specify the validation error messages
        messages: {
            name: "Please Enter Your Name",         
            phone: "Please Enter Your Phone Number",
            message: "Please Enter Message",       
            email: "Please Enter a valid Email Address"
            
        },
        
        // submitHandler: function(form) {
        //     form.submit();
        // }
    });
    $("#testimonial-form").validate({
    
        // Specify the validation rules
        rules: {
            name: "required",
            story: "required",
            email: {
                required: true,
                email: true
            },
          
            
        },
        
        // Specify the validation error messages
        messages: {
            name: "Please Enter Your Name",         
            story: "Please Enter Your Story",       
            email: "Please Enter a valid Email Address"
            
        },
        
        // submitHandler: function(form) {
        //     form.submit();
        // }
    });
  });
 
   
 // $( function() {
 //    $( "#datepicker" ).datepicker({
 //      changeMonth: true,
 //      changeYear: true,
 //       maxDate:'0'
 //    });
 //  });


 $("#planned_checked").change(function() {

        if($(this).prop('checked') == true) {
           $('#submit_form').removeAttr('disabled');
        } else {
           $('#submit_form').attr('disabled',true);
        }
    });

// 100 words limit for caption area
  $(document).ready(function() {
    $("#caption").keyup(function(){
    var content = $("#caption").val(); //content is now the value of the text box
    var words = content.split(/\s+/); //words is an array of words, split by space
    var num_words = words.length; //num_words is the number of words in the array
    var max_limit=100;
    if(num_words>max_limit){
        alert("Exceeding the max limit");
    var lastIndex = content.lastIndexOf(" ");
        $("#caption").val(content.substring(0, lastIndex));
    
   $('#remainingChars').text('Limit Exceeding!');
   document.getElementById("aboutMe").disabled = true;

    return false;

    }
    else 
    {
    document.getElementById("aboutMe").disabled = false;

    $('#remainingChars').text(max_limit+1-num_words +" Words Remaining");
    }
});
});

















   $(function() {
  
    // Setup form validation on the #register-form element
    $("#contact-form").validate({
    
        // Specify the validation rules
        rules: {
           
            Year: "required",
            dob: "required",
            gender: "required",
            city: "required",
            mob_no: {
                required:true,
                maxlength:12
            },
            seeking: "required",

            name:{
                required:true,
                maxlength: 10
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                maxlength: 8
            }
            
        },
        
        // Specify the validation error messages
        messages: {
            name:{
                required:"Please Enter Your Name",
                maxlength: "Please enter no more than 10 characters."
            },
            dob : "Please Enter Birth Date",
            gender: "Please Select Gender",
            city: "Please Enter Your City",
            Year: "Please Enter Your date",
            mob_no: {
                required:"Please Enter Your Mobile No",
                maxlength:"Please enter no more than 12 digit mobile number"
            },
            seeking: "Please Select Seeking Option",
            password: {
                required: "Please provide a password",
                maxlength: "Please enter no more than 8 digit password."
            },
            email: "Please Enter a valid Email Address e.g abc@gmail.com"
            
        },
        
        // submitHandler: function(form) {
        //     form.submit();
        // }
    });


  });