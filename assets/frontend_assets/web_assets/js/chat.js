  // Initialize Firebase
  var global_user_id = 0;
   var config = {
        apiKey: "AIzaSyBQnUSBzGId8TwJGhYcTlcfXCKVVDAxs7M",
        authDomain: "love-undivided.firebaseapp.com",
        databaseURL: "https://love-undivided.firebaseio.com",
        projectId: "love-undivided",
        storageBucket: "love-undivided.appspot.com",
        messagingSenderId: "494611323583"
      };
      firebase.initializeApp(config);


      var sender_user_id = $('#sender_user_id').val()

    
    // var dbref = firebase.database().ref().child('users');
    // dbref.on('value', snap);
    function snap(data)
    {
    	//console.log('1');
    	var users = data.val();
    	//console.log(users);
    	var keys=Object.keys(users);
      var html ='';
    	for(var i=0;i<keys.length;i++)
    	{
    		var k = keys[i];
        var status = 'offline';
        if(users[k].online==1)
        {
            var status = 'online';
        }
        html += '<li class="clearfix getid" id="'+users[k].user_id+'#'+users[k].name+'" style="cursor:pointer">';
        html+='<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_01.jpg" alt="avatar" />';
        html+='<div class="about"><div class="name">'+users[k].name+'</div><div class="status" ><i class="fa fa-circle '+status+'" id="status"'+users[k].user_id+'"></i> <span id="statuss"'+users[k].user_id+'">offline</span></div></div>';
        html+='</li>'; 
        //console.log(html);             
      }
     // html+="</ul>";
      $(".list").html(html);

      $(".list li").click(function(){    
       $("#message-to-send").removeAttr("disabled");       
          var chatwith = this.id;                 
          $('#chatwith').val(chatwith); 
          var id = chatwith.split('#'); 
          var user_id=id[0];
          //get chat history
          console.log(user_id);
          var ref = firebase.database().ref().child('chats');
           ref.orderByChild('user_id').equalTo(parseInt(user_id)).on("value", function(snapshot) {
              // var username = '';
              // var userref = firebase.database().ref().child('users');
              // userref.orderByChild('user_id').equalTo(parseInt(user_id)).on("value", function(result) {
              //   var user = result.val();
              //   var keys=Object.keys(user);
              //   var k = keys[0];
              //   username = user[k].name;

              // });
              var chats = snapshot.val();

              console.log(chats);
              $('#chatwith').val(id[0]+'#'+id[1]);

              
              $(".chat-history").empty();
              $('.chat-with').html(id[1]);
              var html ='';
              var html ="<ul id='ul_"+id[0]+"'>";
              if(chats!=undefined)
              {
                var keys=Object.keys(chats);
                for(var i=0;i<keys.length;i++)
                {
                  var k = keys[i];
                  var a = chats[k].message;
                  var b = chats[k].time;
                  var c = chats[k].sender;
                  var c = chats[k].receiver;
                  console.log(chats[k]);

                  if(c==0){
                    html += '<li class="clearfix"><div class="message-data align-right"><span message-data-time" >'+moment(b).format('MM/DD/YYYY h:mm a')+'</span> &nbsp; &nbsp;</div><div class="message other-message float-right">'+a+'</div></li>';
                  }
                  else 
                  {
                   html+='<li><div class="message-data"> <span class="message-data-time">'+moment(b).format('MM/DD/YYYY h:mm a')+'</span></div><div class="message my-message">'+a+'</div></li>'; 
                  }
                }
              }
               html+="</ul>";
               $(".chat-history").html(html);

                
           });

          //********************
      });
          var ref = firebase.database().ref().child('chats');
          // ref.orderByChild('user_id').equalTo(parseInt(user_id)).on("value", function(snapshot) {
          //ref.on("value", function(snapshot) {
            ref.orderByChild('time').startAt(Date.now()).on('child_added', function(snapshot) {
              console.log('new record', snap.key());
           // });
            Push.create('Hello World!', {
                    body: 'This is some body content!',
                    icon: {
                        x16: 'images/icon-x16.png',
                        x32: 'images/icon-x32.png'
                    },
                    timeout: 5000
                });
           });
    	
    }
   // console.log(dbref);
    const auth = firebase.auth();
    function login()
    {
    	var email = document.getElementById('username').value;
    	var password = document.getElementById('password').value;
    	const promise = auth.signInWithEmailAndPassword(email, password);    			
		  promise.catch(function(error) {
  		  // Handle Errors here.
  		  var errorCode = error.code;		  
  		  var errorMessage = error.message;
  		  if (errorCode === 'auth/wrong-password') {
  		    console.log(errorCode);
  		    return;
  		  } else {
  		    console.log(errorCode);
  		    return;
  		  }
  		 // console.log(errorCode);
  		  return;
		  });
	    promise.then(function(result){
		    	firebase.auth().onAuthStateChanged(firebaseUser =>{
	    	if(firebaseUser){	    		
	    		window.location="chat.html";
	    	}
	    	else
	    	{
	    		console.log('firebaseUser');
	    	}
	   		});
	    });	    
		  return false;
    }

    function get_chats(user_id,username) {
       var ref = firebase.database().ref("chats");
          console.log(user_id); 
           ref.orderByChild('user_id').equalTo(parseInt(sender_user_id)).on("value", function(arr_chats) {

              ref.orderByChild('receiver_id').equalTo(sender_user_id).on("value", function(arr_chats2) {
              //var username = '';

              $('#chat_user_pic').attr('src',$('#li_user_pic_'+user_id).attr('src'));
              $('#chat_user_name').html($('#li_user_name_'+user_id).html());
              
              $('.chat-history').html('<div class="text-center" style="height: 400px" ><img  style="margin-top: 200px;" src="'+web_assets+'images/ajax-loader.gif" ></div>');         
              var chats = arr_chats.val();
              var chats2 = arr_chats2.val();
              //console.log(chats); 
              if(chats != undefined && chats2 != undefined)
              {
                 
                var arr_chats_comb = Object.assign(chats, chats2); 
              }
              else if(chats != undefined && chats2 == undefined)
              {
               
                var arr_chats_comb = chats;
              }
              else if(chats == undefined && chats2 != undefined)
              {
                
                var arr_chats_comb = chats2; 
              }


              if(arr_chats_comb == undefined)
              {
                var html ='';
                html += '<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>';
                html += '<div class="viewport"><div class="overview"><ul class="chat-arae">';
                html += "<ul id='ul_"+user_id+"'>";
                 html+="</ul></div></div></div>";
               // $('#loading').hide();
               // $('.chat-history').show();
               $(".chat-history").html(html);


              }

              console.log(arr_chats_comb);
              var arr_new_chats = [];
               var keys2=Object.keys(arr_chats_comb);
              for (var i = 0; i < keys2.length; i++) {
                var j = keys2[i];
                if(arr_chats_comb[j].sender_id == user_id || arr_chats_comb[j].receiver_id == user_id)
                {
                  arr_new_chats.push(arr_chats_comb[j]);
                }
      
              }
              console.log(arr_new_chats);
              var byDate = arr_new_chats.slice(0);
              byDate.sort(function(a,b) {
                  var myDate1 = new Date(a.created_on);
                  var myDate2 = new Date(b.created_on);
                  return myDate1.getTime() - myDate2.getTime();
              });
              console.log('by date:');
              console.log(byDate);

              $('#chatwith').val(user_id+'#'+username);

              
              $(".chat-history").empty();
              $('.chat-with').html(username);
              var html ='';
              html += '<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>';
              html += '<div class="viewport"><div class="overview"><ul class="chat-arae">';
              html += "<ul id='ul_"+user_id+"'>";
              if(byDate!=undefined)
              {
                //var keys=Object.keys(chats);
                for(var i=0;i<byDate.length;i++)
                {
                  
                  let msg = byDate[i].message;
                  var b = byDate[i].created_on;
                  let sender_id = byDate[i].sender_id;
                  let receiver_id = byDate[i].receiver_id;
                  let u_id = byDate[i].user_id;
                  console.log(byDate[i]);

                  if(sender_id==user_id){
                    html += ' <li>'+msg+'</li>';
                  }
                  else 
                  {
                   html+=' <li class="right">'+msg+'</li>'; 
                  }
                }
              }
               html+="</ul></div></div></div>";
               // $('#loading').hide();
               // $('.chat-history').show();
               $(".chat-history").html(html);

                
           });
          });
    }

    $(".list li").click(function(){    
        //alert('in');
      // $("#message-to-send").removeAttr("disabled");       
          var chatwith = this.id;                 
          $('#chatwith').val(chatwith); 
          var id = chatwith.split('#'); 
          var user_id=id[0];
          var user_name=id[1];

          get_chats(user_id,user_name);
          //get chat history
          //var ref = firebase.database().ref().child('chats');
         
          //********************
      });

/*----------------fetch latest record--------------------*/
  //  var ref = firebase.database().ref().child('chats');
 //   ref.orderByChild('receiver').equalTo(1).on("value", function(snapshot) {
      
      // var chats = snapshot.val();
      // //alert(chats);
      // $('#chatwith').val('1#shital');
      // var keys=Object.keys(chats);
      // $(".chat-history").empty();
      // var html ='';
      // var html ="<ul id='ul_1'>";
      // for(var i=0;i<keys.length;i++)
      // {
      //   var k = keys[i];
      //   var a = chats[k].message;
      //   var b = chats[k].time;
      //   var c = chats[k].sender;
      //   var c = chats[k].receiver;

      //   if(c==global_user_id+'0'){
      //     html += '<li class="clearfix"><div class="message-data align-right"><span message-data-time" >'+moment(b).format('MM/DD/YYYY h:mm a')+'</span> &nbsp; &nbsp;</div><div class="message other-message float-right">'+a+'</div></li>';
      //   }
      //   else 
      //   {
      //    html+='<li><div class="message-data"> <span class="message-data-time">'+moment(b).format('MM/DD/YYYY h:mm a')+'</span></div><div class="message my-message">'+a+'</div></li>'; 
      //   }
      // }
      //  html+="</ul>";
      //  $(".chat-history").html(html);
   // });

/*----------------save message--------------------*/
(function(){
  
  var chat = {
    messageToSend: '',
    init: function() {
      this.cacheDOM();
      this.bindEvents();
      this.render();
    },
    cacheDOM: function() {
      // this.$chatHistory = $('.chat-history');
      // this.$button = $('button');
      // this.$textarea = $('#message-to-send');
      // this.$chatHistoryList = $('.chat-history ul');
      //this.$chatHistoryList1 = $('.chat-history ul');
      this.$chatHistory = $('.chat-history');

      this.$divscroll = $('.div-scroll');
    //  this.$button = $('button');
      this.$textarea = $('#message-to-send');
      this.$chatHistoryList =  this.$chatHistory.find('ul');
    },
    bindEvents: function() {
     // this.$button.on('click', this.addMessage.bind(this));
      this.$textarea.on('keyup', this.addMessageEnter.bind(this));
      //this.$textarea.on('keyup', this.stopIsTyping.bind(this));
    },
    render: function() {
      this.scrollToBottom();
      if (this.messageToSend.trim() !== '') {

        // template = Handlebars.compile( $("#message-template").html());
        var context = { 
          messageOutput: this.messageToSend,
          time: this.getCurrentTime()
        };
        //emit messsage to other person
        var chatwith = $('#chatwith').val();
        var chatperson =chatwith.split('#');
        var chatid = chatperson[0];
        var chatname = chatperson[1];

       $(".chat-history #ul_"+chatid).append('<li class="right">'+context.messageOutput+'</li>');

        var cur_date = new Date().toISOString().slice(0, 19).replace('T', ' ');
        var chatsref = firebase.database().ref("chats/");
        chatsref.push({
              message: this.messageToSend,
              created_on: cur_date,
              sender_id:sender_user_id,
              receiver_id:chatid,
              user_id:parseInt(sender_user_id)           
        });

       

        this.scrollToBottom();
        this.$textarea.val('');       
      }
      
    },
    
    addMessage: function() {
      this.messageToSend = this.$textarea.val()
      this.render();         
    },
    addMessageEnter: function(event) {
        var chatwith = $('#chatwith').val();
        
        var chatperson =chatwith.split('#');
        var chatid = chatperson[0];
        var chatname = chatperson[1];
       
        // enter was pressed
        if (event.keyCode === 13) {
          this.addMessage();
        }
        else
        {
          
          
        }
    },   
    stopIsTyping:function(event){          
          $('#showistyping').html('');
    },
    scrollToBottom: function() {
      console.log(this.$chatHistory[0].scrollHeight);
       this.$chatHistory.scrollTop(this.$divscroll[0].scrollHeight);
    },
    getCurrentTime: function() {
      return new Date().toLocaleTimeString().
              replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
    },
    getRandomItem: function(arr) {
      return arr[Math.floor(Math.random()*arr.length)];
    }
  };
  chat.init();  
  // var searchFilter = {
  //   options: { valueNames: ['name'] },
  //   init: function() {
  //     var userList = new List('people-list', this.options);
  //     var noItems = $('<li id="no-items-found">No items found</li>');
      
  //     userList.on('updated', function(list) {
  //       if (list.matchingItems.length === 0) {
  //         $(list.list).append(noItems);
  //       } else {
  //         noItems.detach();
  //       }
  //     });
  //   }
  // };
  
  //  searchFilter.init();

 //$("#message-to-send").attr("disabled", "disabled"); 
})();
