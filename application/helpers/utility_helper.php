<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



/* Function check for password hash
 * @param string
 * return TRUE on success FALSE if fails
 */

function verifyPasswordHash($password, $hash_and_salt) {
  
    if (password_verify($password, $hash_and_salt)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/* Function create hash and salt password
 * @param string
 * return string 
 */

function createHashAndSalt($user_provided_password) {
    
    $options = [
        'cost' => 12,
    ];
    $hash = password_hash($user_provided_password, PASSWORD_BCRYPT, $options);
    return $hash;
}

/* Function To Send An Email To User
 */

function sendMail($to, $subject, $message) {
   // print_r($to);die();
    $from = 'no-reply@LoveUndivided.com';
    include 'email_library.php'; // include the library file
    include "email_classes/class.phpmailer.php"; // include the class name
    $mail = new PHPMailer; // call the class 
    $mail->IsSMTP();
    $mail->Host = SMTP_HOST; //Hostname of the mail server
    $mail->Port = SMTP_PORT; //Port of the SMTP like to be 25, 80, 465 or 587
    $mail->SMTPAuth = true; //Whether to use SMTP authentication
    $mail->Username = SMTP_UNAME; //Username for SMTP authentication any valid email created in your domain
    $mail->Password = SMTP_PWORD; //Password for SMTP authentication
    $mail->AddReplyTo($from); //reply-to address
    $mail->SetFrom($from, "LoveUndivided"); //From address of the mail
    // put your while loop here like below,
    $mail->Subject = $subject; //Subject od your mail

    // echo count($to);die;
    if(count($to)==1)
    {
        //echo "Manu";
           
            if(gettype($to)=="array"){
                foreach ($to as $value) {
                    $mail->AddAddress($value, "");
                }
            }else{
                $mail->AddAddress($to, "");
            }

            //$mail->AddAddress($to, ""); //To address who will receive this email
            $mail->MsgHTML($message); //Put your body of the message you can place html code here
            //$mail->AddAttachment("images/asif18-logo.png"); //Attach a file here if any or comment this line,
          $send = $mail->Send();

    } 
    else
    {
       // print_r($to);

        if(is_array($message))
        {
        
            foreach ($to as $key1 => $value1) {
               $mail->ClearAddresses();        //It removes adress history 
               $mail->AddAddress($value1, "");
               $mail->MsgHTML($message[$key1]);
               $send = $mail->Send();
            }
        }
        else
        {
             foreach ($to as $key1 => $value1) {
               $mail->ClearAddresses();        //It removes adress history 
               $mail->AddAddress($value1, "");
               $mail->MsgHTML($message);
               $send = $mail->Send();
            }

        }


    }
//die;

    
    if ($send) {
        return true;
    } else {
        return false;
    }
}

function random_password($cnt = 8) {
    $alphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $cnt; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

/*
 * funciton for upload image without compress
 * created on : 
 * modified on: 
 * created by :Akshay Patil
 *
 */

function upload_image($destination, $fieldName, $prefix) {
    $CI = & get_instance();
    $file_ext = substr(strrchr($_FILES[$fieldName]['name'], '.'), 1);
    $name = $prefix . "_" . time() . "_" . random_password();
    $config = array(
        'upload_path' => "./$destination",
        'allowed_types' => "jpeg|jpg|png|gif",
        'overwrite' => TRUE,
        'file_name' => $name . "." . $file_ext
    );
    $CI->load->helper('file');
    $CI->load->library('upload', $config);
    $CI->upload->initialize($config);
    if ($CI->upload->do_upload($fieldName)) {
        $data = array('upload_data' => $CI->upload->data());
        return $data['upload_data']['file_name'];
    } else {
        $error = array('error' => $CI->upload->display_errors());
        return 0; //$error;
    }
}

/*
 * funciton for upload image with compress
 * created on : 
 * modified on: 
 * created by :Akshay Patil
 *
 */

function image_compress($source, $destination, $quality) {
    $info = getimagesize($source);
    if ($info['mime'] == 'image/jpeg')
        $image = imagecreatefromjpeg($source);
    elseif ($info['mime'] == 'image/gif')
        $image = imagecreatefromgif($source);
    elseif ($info['mime'] == 'image/png')
        $image = imagecreatefrompng($source);
    imagejpeg($image, $destination, $quality);
    imagedestroy($image);
    return $destination;
}

/*
 * funciton for thumb image
 * created on : 
 * modified on: 
 * created by :Akshay Patil
 *
 */

function image_thumb($source, $destination, $new_width = 150, $new_height = 150) {
    list($old_width, $old_height) = getimagesize($source);
    $new_image = imagecreatetruecolor($new_width, $new_height);
    $old_image = imagecreatefromjpeg($source);
    imagecopyresampled($new_image, $old_image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);
    // save thumbnail into a file
    imagejpeg($new_image, "{$destination}");
    imagedestroy($old_image);
    imagedestroy($new_image);
    return true;
}

/*
 * funciton for upload video files
 * created on : 
 * modified on: 
 * created by :Akshay Patil
 *
 */

function upload_video($destination, $fieldName, $prefix) {
    $CI = & get_instance();
    $file_ext = substr(strrchr($_FILES[$fieldName]['name'], '.'), 1);
    $name = $prefix . "_" . time() . "_" . random_password();
    $config = array(
        'upload_path' => "./$destination",
        'allowed_types' => "*",
        'overwrite' => TRUE,
        'file_name' => $name . "." . $file_ext
    );
    //$config['max_size']     = '100';
    $CI->load->helper('file');
    $CI->load->library('upload', $config);
    $CI->upload->initialize($config); // Make sure it has been initialized
    if ($CI->upload->do_upload($fieldName)) {
        $data = array('upload_data' => $CI->upload->data());
        return $data['upload_data']['file_name'];
    } else {
        $error = array('error' => $CI->upload->display_errors());
        return $error;
    }
}


/*
    function for generating Slug
    created on : 22 Aug 2017
    created by : Pratik Guthalkar
*/
function create_slug($text,$tbl_name,$col_name,$user_id="")
{
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
    return 'n-a';
    }

    $slug = $text;

    $i = 1; 
    if(empty($user_id))
    {
        if(slug_exist($text,$tbl_name,$col_name)){
            $slug = $text . "-" . $i++;        
        }
    }
    else{
        if(slug_exist($text,$tbl_name,$col_name,$user_id)){
            $slug = $text . "-" . $i++;        
        }
    }

     return $slug;
}

function check_subscription($user_id)
{
    if(!empty($user_id))
    {
        $CI = & get_instance();

       // $sql = "SELECT user_id,CURRENT_DATE FROM user_subscription where user_id = $user_id AND CURRENT_DATE BETWEEN DATE(start_date) AND DATE(end_date)";
        $sql = "SELECT user_id,(IF(end_date='0000-00-00 00:00:00', (SELECT user_id FROM user_subscription WHERE user_id = $user_id ),(SELECT CURRENT_DATE FROM user_subscription WHERE user_id = $user_id AND CURRENT_DATE BETWEEN DATE(start_date) AND DATE(end_date)) ) ) as data FROM user_subscription where user_id=$user_id";
        
        // echo $sql ; die;
        $query = $CI->db->query($sql);
         
        $res = $query->num_rows();
        //print_r($res); die;
        if($res > 0)
            return false;
        else
            return true;
    }
    else
        return false;
}

function slug_exist($slug,$tbl_name,$col_name,$user_id="")
{
    $CI = & get_instance();

    $sql = "SELECT $col_name FROM $tbl_name where $col_name = '$slug'";
    if(!empty($user_id))
        $sql .= " AND user_id != $user_id ";

    //echo $sql ; die;
    $query = $CI->db->query($sql);


    $res = $query->num_rows();

    if($res > 0)
        return true;
    else
        return false;
}

function check_expiration($user_id)
{
   if(!empty($user_id))
    {
        $CI = & get_instance();

        $sql = "SELECT user_subscription.* , TIMESTAMPDIFF(DAY , CURRENT_DATE, end_date ) AS days
                FROM user_subscription WHERE user_id = $user_id";
        
        // echo $sql ; die;
        $query = $CI->db->query($sql);
         
        $res = $query->result_array();
       // print_r($res); die;
        if(!empty($res))
            return $res;
        else
            return false;
    }
    else
        return false;
}
