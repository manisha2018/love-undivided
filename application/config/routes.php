<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['admin'] = '/admin/Login';

$route['login'] = '/api/User_Login/LoginView';

$route['register'] = '/api/User_Login/RegisterView';
$route['update-password'] = '/api/User_Login/update_password';
$route['change-password'] = 'api/User_Login/change_pass';
$route['question-category'] = '/api/User_Questions/que_category';

$route['questions/(:any)'] = 'api/User_Questions/getCategoryQue/$1';

$route['activities'] = 'api/User_Activity/user_activity';
$route['activities/(:any)'] = 'api/User_Activity/user_activity/$1';
$route['activities/visitors'] = 'api/User_Activity/user_activity';
$route['activities/profile-update'] = 'api/User_Activity/user_activity';
$route['activities/photo-update'] = 'api/User_Activity/user_activity';

$route['profile'] = 'api/User_Login/View_Profile';

$route['dashboard'] = 'api/User_Activity/user_activity';
$route['profile-pic'] = 'api/User_Login/profile_pic';
$route['upload-pic'] = 'api/User_Login/profile_pic';
$route['gallery'] = 'api/User_Login/uploadGallery';

$route['caption'] = 'api/User_Login/AddCaption';
$route['how-we-match'] = 'api/User_Login/how_we_match';
$route['pricing-plan'] = 'api/User_Activity/Pricing_Plan';
$route['pay'] = 'api/User_Activity/payment_view';
$route['About-us'] = 'Home/about_us';
$route['Privacy-policy'] = 'Home/privacy_policy';
$route['Testimonials'] = 'api/Testimonial/getstories';
$route['Frequently-asked-questions'] = 'api/FAQs/getFaqs';
//$route['Frequently-asked-questions/(:any)'] = 'api/FAQs/getFaqs/$1';
$route['Contact-us'] = 'Home/contact_us';
$route['matched-profile/(:any)'] = 'api/User_Activity/other_user_profile/$1';

$route['matches'] = 'api/User_matches';
$route['matches/(:any)'] = 'api/User_matches/get_matches/$1';
$route['submit_plan'] = 'api/User_Activity/submit_plan';
$route['featured-profile/(:any)'] = 'api/User_Activity/featuredProfile/$1';
$route['chats'] = 'api/User_Chat';
$route['general-settings'] = 'api/User_Activity/general_settings';
$route['billing-settings'] = 'api/User_Activity/billing_settings';
$route['contact-settings'] = 'api/User_Activity/contact_settings';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
