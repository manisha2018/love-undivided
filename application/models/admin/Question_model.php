<?php
class Question_model extends CI_Model
{

	  public function getOptionGroup()
	  {
	  	$query = $this->db->query("SELECT * FROM `option_group`");
		$option_group = $query->result_array();
		return $option_group;
	  }  
	  public function insert_question($que_data)
	  {
	  	$this->db->insert('questions',$que_data);
	    return $this->db->affected_rows();
	  }	

	  public function getQuestions()
		{
			$query = $this->db->query("SELECT questions.question_id,
					questions.question, categories.category_name, 
					option_group.option_group_name
					FROM questions 
					LEFT JOIN categories ON questions.category_id = categories.category_id 
					LEFT JOIN option_group ON questions.option_group_id = option_group.option_group_id");
			$questions = $query->result_array();
			return $questions;

		}

	  public function delete_question($id)
		{
			$this->db->where('question_id', $id);
        	$this->db->delete('questions');
        	return $this->db->affected_rows();
		}
		public function get_que_data($id)
		{
			$query = $this->db->query("SELECT questions.question_id,
					questions.question, categories.category_id,
					categories.category_name, 
					option_group.option_group_id,
					option_group.option_group_name
					FROM questions 
					LEFT JOIN categories ON questions.category_id = categories.category_id 
					LEFT JOIN option_group ON questions.option_group_id = option_group.option_group_id WHERE questions.question_id=$id");
			$que_res = $query->result_array();
			return $que_res;
		}

		public function edit_que_data($id,$edit_que_data)
		{
			$this->db->where('question_id', $id);
        	$this->db->update('questions',$edit_que_data);
        	return $this->db->affected_rows();
		}


}
