<?php
class Contact_us_model extends CI_Model
{
	public function insert_category($cat)
	{
		$this->db->insert('categories',$cat);
        return $this->db->affected_rows();
	}

	public function getContacts()
	{
		$query = $this->db->query("SELECT * FROM `contact_us`");
		$contact_us = $query->result_array();
		return $contact_us;
	}

	public function delete_contact($id)
	{
		$this->db->where('contact_us_id', $id);
        $this->db->delete('contact_us');
        return $this->db->affected_rows();
	}

}