<?php
class Users_model extends CI_Model
{
	function getUsers()
	{
		$query=$this->db->query("SELECT `user_id`,`name`, `email`, `password`, `gender`, `city`, `mob_no`, `dob`, `seeking`, `user_caption`, `user_status`, `user_image`, `height`, `religion_name`, `ethnicity`, `education`, `created_on`,`featured_user` FROM `user` LEFT JOIN religion ON religion.religion_id=user.religion_id ORDER BY user_id DESC");

		$users=$query->result_array();
		return $users;

	}
	public function delete_user($id)
	{
		$this->db->where('user_id', $id);
        $this->db->delete('user');
        return $this->db->affected_rows();
	}
	public function get_usersplan_data($id)
	{
		$query = $this->db->query("SELECT plan.plan_name,plan.amount,plan.duration,plan.plan_id,users_plan.plan_id FROM users_plan LEFT JOIN plan ON plan.plan_id = users_plan.plan_id WHERE users_plan.user_id='$id'");

		$users_plan = $query->result_array();
		return $users_plan;
	}

 	public function getQueryResult($query)
    {
        $result = $this->db->query($query);
        return $result->result_array();
     
    }
    function feature_user($user_id,$val)
	{
		$query = $this->db->query("UPDATE `user` SET `featured_user`= '$val' WHERE user_id=$user_id");
		return $this->db->affected_rows();
	}
}