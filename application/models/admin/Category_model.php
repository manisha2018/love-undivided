<?php
class Category_model extends CI_Model
{
	public function insert_category($cat)
	{
		$this->db->insert('categories',$cat);
        return $this->db->affected_rows();
	}

	public function getCategory()
	{
		$query = $this->db->query("SELECT * FROM `categories`");
		$category = $query->result_array();
		return $category;
	}

	public function delete_category($id)
	{
		$this->db->where('category_id', $id);
        $this->db->delete('categories');
        return $this->db->affected_rows();
	}

	public function get_cat_data($id)
	{
		$query = $this->db->query("SELECT category_name,category_id FROM `categories` WHERE category_id=$id");
		$category_res = $query->result_array();
		return $category_res;

	}

	public function edit_cat_data($id,$category_name)
	{
		$query = $this->db->query("UPDATE `categories` SET `category_name`= '$category_name' WHERE category_id=$id");
		 return $this->db->affected_rows();
	}

}