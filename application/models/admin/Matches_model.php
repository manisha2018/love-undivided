<?php
class Matches_model extends CI_Model
{
	
	public function getUserList()
	{
		$query = $this->db->query("SELECT * FROM `user`");
		$users = $query->result_array();
		return $users;
	}
	public function delete_user($id)
	{
		$this->db->where('user_id', $id);
        $this->db->delete('user');
        return $this->db->affected_rows();
	}


}