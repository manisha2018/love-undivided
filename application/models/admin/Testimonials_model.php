<?php
class Testimonials_model extends CI_Model
{
	function getstories()
	{
		$query = $this->db->query("SELECT * FROM `testimonial`");
		$res = $query->result_array();
		return $res;
	}

	function delete_story($id)
	{
		$this->db->where('testimonial_id', $id);
        $this->db->delete('testimonial');
        return $this->db->affected_rows();
	}
	function updateStory($testimonial_id,$val)
	{
		$query = $this->db->query("UPDATE `testimonial` SET `is_approve`= '$val' WHERE testimonial_id=$testimonial_id");
		return $this->db->affected_rows();
	}
	function get_testimonial_data($id)
	{
		$query = $this->db->query("SELECT * FROM `testimonial` WHERE testimonial_id=$id");
		$res = $query->result_array();
		return $res;
	}
	function edit_story($id,$edit_testimonial_data)
		{
			$this->db->where('testimonial_id', $id);
        	$this->db->update('testimonial',$edit_testimonial_data);
        	return $this->db->affected_rows();
		}

}