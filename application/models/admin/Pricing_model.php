<?php
class Pricing_model extends CI_Model
{
	public function getPricingPlan()
	{
		$query = $this->db->query("SELECT `plan_id`, `plan_name`, `duration`, `price_per_month`,`amount`, `created_on` FROM `plan`");
		$plan = $query->result_array();
		return $plan;
	}

	function insert_plan($plan)
	{
		$this->db->insert('plan',$plan);
        return $this->db->affected_rows();
	}

	public function delete_plan($id)
	{
		$this->db->where('plan_id', $id);
        $this->db->delete('plan');
        return $this->db->affected_rows();
	}

	public function edit_plan_data($id,$plan_data)
	{
		$this->db->where('plan_id', $id);
        $this->db->update('plan',$plan_data);
        return $this->db->affected_rows();
	}

	public function get_plan_data($id)
	{
		$query = $this->db->query("SELECT * FROM `plan` WHERE plan_id=$id");
		$plan_res = $query->result_array();
		return $plan_res;
	}
}