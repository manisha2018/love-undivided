<?php
class Login_model extends CI_Model
{

	public function get_user_details($user_email='',$user_id='')
	{
		$this->db->select('*');
		$this->db->from('master_user');
		if($user_id!=''){
			$this->db->where('userid',$user_id);
		}elseif ($user_email!='') {
			$this->db->where('user_email',$user_email);
		}
		
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getRecords($admin_id)
	{
		$query = $this->db->query("SELECT * FROM master_user where userid='$admin_id'");
		$records=$query->result_array();
		return $records;
	}

	public function updateRecord($admin_data,$admin_id)
	{
		$this->db->where('userid', $admin_id);
        $this->db->update('master_user', $admin_data);
        return $this->db->affected_rows();
	}
    public function getAdminRecords($user_email)
    {
    	$query = $this->db->query("SELECT * FROM master_user where user_email='$user_email'");
		$records=$query->result_array();
		
		return $records;
    }
    public function update_admin_pass($user_email,$user_password)
    {
    	$query = $this->db->query("UPDATE `master_user` SET `user_password`='$user_password' WHERE `user_email`='$user_email'");
    	return $this->db->affected_rows();
    }
}