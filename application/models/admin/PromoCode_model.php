<?php
class PromoCode_model extends CI_Model
{
	function getpromocode()
	{
		$query = $this->db->query("SELECT * FROM `promo_code`");
		$res = $query->result_array();
		return $res;
	}

	public function insert_code($code_data)
  	{
  		$this->db->insert('promo_code',$code_data);
    	return $this->db->affected_rows();
  	}	
  	public function deletepromocode($id)
	{
		$this->db->where('promo_id', $id);
    	$this->db->delete('promo_code');
    	return $this->db->affected_rows();
	}
	public function get_promo_data($id)
	{
		$query = $this->db->query("SELECT * FROM `promo_code` WHERE promo_id=$id");
		$res = $query->result_array();
		return $res;
	}
	public function edit_code($id,$edit_code_data)
		{
			$this->db->where('promo_id', $id);
        	$this->db->update('promo_code',$edit_code_data);
        	return $this->db->affected_rows();
		}


}