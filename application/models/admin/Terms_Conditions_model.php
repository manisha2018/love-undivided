<?php
class Terms_Conditions_model extends CI_Model
{
	public function getTermsConditions()
	{
		$query = $this->db->query("SELECT * FROM `terms_condition`");
		$terms_condition = $query->result_array();
		return $terms_condition;
	}
  	public function insert_terms($terms_data)
  	{
  		$this->db->insert('terms_condition',$terms_data);
    	return $this->db->affected_rows();
  	}	
  	public function delete_terms($id)
	{
		$this->db->where('term_cond_id', $id);
    	$this->db->delete('terms_condition');
    	return $this->db->affected_rows();
	}
	public function get_terms_data($id)
	{
		$query = $this->db->query("SELECT * FROM `terms_condition` WHERE term_cond_id=$id");
		$res = $query->result_array();
		return $res;
	}
	public function edit_terms($id,$edit_term_data)
		{
			$this->db->where('term_cond_id', $id);
        	$this->db->update('terms_condition',$edit_term_data);
        	return $this->db->affected_rows();
		}


}