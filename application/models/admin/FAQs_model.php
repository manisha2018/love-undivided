<?php
class FAQs_model extends CI_Model
{
	public function getFaqs()
	{
		$query = $this->db->query("SELECT * FROM `faqs`");
		$faq = $query->result_array();
		return $faq;
	}
	function insert_faqs($faqs)
	{
		$this->db->insert('faqs',$faqs);
        return $this->db->affected_rows();
	}

	function get_faqs_data($id)
	{
		$query = $this->db->query("SELECT * FROM `faqs` WHERE faq_id=$id");
		$faq_res = $query->result_array();
		return $faq_res;
	}
	function edit_faqs($id,$faq_data)
	{
		$this->db->where('faq_id', $id);
        $this->db->update('faqs',$faq_data);
        return $this->db->affected_rows();
	}
	function delete_faqs($id)
	{
		$this->db->where('faq_id', $id);
        $this->db->delete('faqs');
        return $this->db->affected_rows();
	}



}