<?php
class User_Login_model extends CI_Model
{
	function insert_users($userdata)
	{
		$this->db->insert('user',$userdata);
		return $this->db->insert_id();
	}
	function send_feedback($contactdata)
	{
		$this->db->insert('contact_us',$contactdata);
		return $this->db->insert_id();
		
	}


	function get_user_details($email,$user_id='')
	{
		$this->db->select('*');
		$this->db->from('user');
		if($user_id!=''){
			$this->db->where('user_id',$user_id);
		}elseif ($email!='') {
			$this->db->where('email',$email);
		}
		
		$query = $this->db->get();
		return $query->result_array();
	}

	function getAllRecords()

	{
		$query = $this->db->query("SELECT user_id,gender,seeking,email,name FROM `user` where user_status = '1'");
		$res = $query->result_array();
		return $res;
	}
	function getRecords($email)

	{
		$query = $this->db->query("SELECT * FROM `user` where email = '$email'");
		$res = $query->result_array();
		return $res;
	}
	function checkuseravailable($email)
	{
		$query = $this->db->query("SELECT email FROM `user` where email = '$email'");
		$res = $query->num_rows();
		return $res;
	}

	
	function getProfile($slug,$user_id = 0)
	{
		$sql = "SELECT *,(select count(*) FROM questions ) as cnt_all_question ";
			if($user_id > 0)
			{
				$sql .= " ,
					IF(user.is_unblur_active= '1','4',match_msg_profile.status) as status ";
			}

			$sql .= "
			 FROM `user` LEFT JOIN religion ON user.religion_id = religion.religion_id
			 ";
			if($user_id > 0)
			{
			 	$sql .="
			 		LEFT JOIN match_msg_profile on match_msg_profile.user_match_id = user.user_id AND match_msg_profile.user_id = $user_id ";
			}

			 $sql .="
			  where slug_name = '$slug'";

		$query = $this->db->query($sql);
		//echo $this->db->last_query();die;
		$res = $query->result_array();
		return $res;

	}

	function update_user($user_id,$data_arr)
	{
		$this->db->where('user_id', $user_id);
        $this->db->update('user',$data_arr);
        //echo $this->db->last_query();die;
        return $this->db->affected_rows();
	}

	function update_caption_data($user_id,$data_arr)
	{
		$this->db->where('user_id', $user_id);
        $this->db->update('user',$data_arr);
        return $this->db->affected_rows();
	}

	function updateProfilePic($user_data,$user_id)
	{
		$this->db->where('user_id', $user_id);
        $this->db->update('user',$user_data);
        //echo $this->db->last_query();die;
        return $this->db->affected_rows();
	}

	function addImages($data,$user_id)
	{
		//print_r($data);die;
		 $inputarray =array();

		 foreach ($data as $key => $value) {
		 		$temp['upload_type'] = $value['file_type'];
                $temp['gallery_image'] = $value['file_name'];
                $temp['user_id'] = $user_id;
                array_push($inputarray, $temp);
            } 
          //  print_r($inputarray);die;
            if(!empty($inputarray))
            {
                 $this->db->insert_batch('gallery',$inputarray);

            }
          
            return true;
	}

	function getImages($user_id)
	{
	$query = $this->db->query("SELECT * FROM `gallery` where user_id = '$user_id'");
	$res = $query->result_array();
	return $res;
	}
	function getOnlyvideos($user_id)
	{
	$query = $this->db->query("SELECT * FROM `gallery` where user_id = '$user_id' AND upload_type ='1'");
	$res = $query->result_array();
	return $res;
	}
	function getOnlyimages($user_id)
	{
	$query = $this->db->query("SELECT * FROM `gallery` where user_id = '$user_id' AND upload_type ='0'");
	$res = $query->result_array();
	return $res;
	}
	function updateCaption($id,$data_arr)
	{
		$this->db->where('user_id', $id);
        $this->db->update('user',$data_arr);
        return $this->db->affected_rows();
	}

	function getPricingPlans()
	{
		$query = $this->db->query("SELECT * FROM `plan`");
		$res = $query->result_array();
		return $res;
	}
	function GalleryPic($user_data)
	{
		//print_r($user_data);
		$this->db->insert('gallery',$user_data);
		return $this->db->affected_rows();
	}

	function getReligion()
	{
		$query = $this->db->query("SELECT * FROM `religion`");
		$res = $query->result_array();
		return $res;
	}

	function update_user_password($email,$data_arr)
	{
		$this->db->where('email', $email);
        $this->db->update('user',$data_arr);
        //echo $this->db->last_query();die;
        return $this->db->affected_rows();
	}
	function getTermsConditions()
	{
		$query = $this->db->query("SELECT * FROM `terms_condition`");
		$res = $query->result_array();
		return $res;
	}

	function delete_images($gallery_id)
	{
		$this->db->where('gallery_id', $gallery_id);
        $this->db->delete('gallery');
        return true;
	}

	function check_user($field,$where)
	{
		$this->db->select($field);
		$this->db->where($where);
		$query = $this->db->get('user');
		$res = $query->result_array();
		if(count($res)> 0 && $res[0][$field] != "")
			return true;
		else
			return false;
	}


	function getUserActivePlan($user_id)
	{
		$sql = "SELECT * FROM  user_subscription  JOIN plan on plan.plan_id = user_subscription.plan_id WHERE user_id = $user_id AND CURRENT_DATE BETWEEN DATE(start_date) AND DATE(end_date) ORDER BY start_date DESC limit 1";
		//echo $sql;die;
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
	function getUserActivePromo($user_id)
	{
		//$sql = "SELECT * FROM  user_subscription JOIN promo_code on promo_code.promo_id = user_subscription.promo_id WHERE user_id = $user_id AND CURRENT_DATE BETWEEN DATE(start_date) AND DATE(end_date)  AND user_subscription.is_free = '1' ORDER BY start_date DESC limit 1";

		//$sql ="SELECT * FROM user_subscription LEFT JOIN promo_code ON user_subscription.promo_id = promo_code.promo_id WHERE user_subscription.user_id =$user_id AND CURRENT_DATE BETWEEN DATE( start_date )  AND DATE( end_date )";
		$sql ="SELECT user_subscription.*,promo_code.*, (IF(end_date='0000-00-00 00:00:00',
         (SELECT promo_id FROM user_subscription WHERE user_id = $user_id ),(SELECT CURRENT_DATE FROM user_subscription WHERE user_id = $user_id AND CURRENT_DATE BETWEEN DATE(start_date) AND DATE(end_date))) ) as date_or_promoid FROM user_subscription LEFT JOIN promo_code ON user_subscription.promo_id = promo_code.promo_id where user_id=$user_id";

		//echo $sql;die;
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

}