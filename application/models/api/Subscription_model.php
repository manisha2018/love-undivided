<?php

class Subscription_model extends CI_Model
{
	public function add_subscription($user_id,$plan_id)	
	{
		$sql = "INSERT INTO `user_subscription`(
					 `user_id`, `plan_id`, 
					 `end_date`, `status`
				)
				(
				    SELECT
				    	$user_id,
				    	plan_id,
				    	DATE_ADD(CURRENT_DATE, INTERVAL plan.duration_month MONTH),
				    	'1'
				    FROM 
				    	plan
				    WHERE
				    	plan_id = $plan_id
				    	
				)";

		$query = $this->db->query($sql);
		return $this->db->affected_rows();		
	}

	public function check_promo($user_id,$promo_code)
	{
		$sql = "INSERT INTO `user_subscription`(
					 `user_id`,  `promo_id`,
					 `end_date`, `status`,`is_free`
				)(
				    SELECT
				    	$user_id,
				    	promo_id,
				    	DATE_ADD(CURRENT_DATE, INTERVAL promo_code.duration MONTH),
				    	'1',
				    	If(promo_code.is_free = '1','1','0') 
				    FROM 
				    	promo_code
				    WHERE
				    	code = '$promo_code'
				    	
				)";
		//echo $sql;  die;

		$query = $this->db->query($sql);
		return $this->db->affected_rows();
				
	}
	public function getPlans($plan_id)
	{
		$query = $this->db->query("SELECT * FROM `plan` where plan_id = '$plan_id'");
		echo $this->db->last_query();
		//$res = $query->result_array();
		//return $res;
	}

	public function checkUserExist($query)
	{
		$query = $this->db->query($query);
		$res = $query->num_rows();
		// print_r($res);
		// die;
		return $res;
	}

	public function updateSubscription($user_id,$plan_id)
	{
		$sql = $this->db->query("UPDATE user_subscription SET plan_id = $plan_id,status = '1',start_date = CURRENT_DATE,end_date = (SELECT DATE_ADD(CURRENT_DATE, INTERVAL plan.duration_month MONTH) FROM plan WHERE plan_id = $plan_id) WHERE user_id = $user_id");
		//echo $this->db->last_query();

		$query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function updatePromo($user_id,$promo)
	{
		//$sql = $this->db->query("UPDATE user_subscription SET promo_id = $promo,status = '1', is_free = (SELECT IF(is_free = '1','1','0') FROM promo_code WHERE code = $promo),start_date = CURRENT_DATE,end_date = (SELECT DATE_ADD(CURRENT_DATE, INTERVAL promo_code.duration MONTH) FROM promo_code WHERE code = $promo) WHERE user_id = $user_id");
		$sql="UPDATE user_subscription SET promo_id = (select promo_id FROM promo_code where code = '$promo') ,plan_id = 0,status = '1', is_free = (SELECT IF(is_free = '1','1','0') FROM promo_code WHERE code = '$promo'),start_date = CURRENT_DATE,
			end_date = (SELECT (IF(user_subscription.is_free!='0',user_subscription.end_date = '',(SELECT DATE_ADD(CURRENT_DATE, INTERVAL promo_code.duration MONTH) FROM promo_code WHERE code = '$promo')))) WHERE user_id = $user_id";

		$query = $this->db->query($sql);
		return $this->db->affected_rows();
	}
}

?>

