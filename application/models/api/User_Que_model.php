<?php
class User_Que_model extends CI_Model
{
    function get_que_id($id)
    {
        $query = $this->db->query("SELECT `question_id` FROM `questions` where category_id = '$id'");
        $res = $query->result_array();
        // echo $this->db->last_query();
        // die;
        return $res;
    }
    public function record_count($id)
        {
            $result = $this->db->where('category_id',$id)->count_all_results("questions");
            // $q="SELECT count(*) from questions where category_id = '$id'";
            // $query=$this->db->query($q);
            // return $query->result_array();
            //echo $q;die;
            return $result;
        }


   public function get_que_cat()
    {
        //$sql = "SELECT category_id,category_name,created_on FROM categories";
        $sql="SELECT categories.category_id,categories.category_name,categories.created_on
              FROM categories
              LEFT JOIN questions ON questions.category_id = categories.category_id
              WHERE questions.category_id IS NOT NULL group by categories.category_id";
        $query=$this->db->query($sql);
        return $query->result_array();
    }
    public function count_que_cat($user_id)    
    {
        $sql = "SELECT 
                    COUNT(answers.answer_id) as ans_cnt,
                    categories.category_id,
                    COUNT(categories.category_id) as cat_cnt
                FROM 
                    questions 
                    JOIN categories ON questions.category_id = categories.category_id
                    left JOIN answers on questions.question_id = answers.question_id  AND answers.user_id = $user_id
                    
                    GROUP BY categories.category_id
                    
                    ";

        $query=$this->db->query($sql);
        return $query->result_array();
    }

    public function check_answered_question($user_id)
    {
        $sql = "SELECT 
                    IFNULL(answers.answer_id,0) as ans_cnt
                FROM 
                    questions 
                    left JOIN answers on questions.question_id = answers.question_id  AND answers.user_id = $user_id
                    
                    HAVING ans_cnt = 0";

        $query=$this->db->query($sql);
        return $query->num_rows();

    }


    public function add_que_rating($user_id)
    {
        $sql ="INSERT INTO que_cat_rating (cat_id, user_id, avg_rating)  
            (
                SELECT 
                    questions.category_id, 
                    answers.user_id, 
                    AVG(answers.que_rating) 
                FROM 
                    `answers` 
                    JOIN questions ON questions.question_id = answers.question_id 
                WHERE 
                    answers.user_id = $user_id 
                GROUP BY 
                    questions.category_id
            )";
        $this->db->simple_query($sql);
        return $this->db->affected_rows();
    }
    
    public function Get_Questions($start,$id,$user_id)
    {
        $q="SELECT 
                DISTINCT questions.question_id, 
                questions.question, 
                categories.category_name, 
                categories.category_id, 
                option_group.option_group_name, 
                option_group.option_group_id, 
                (
                    SELECT 
                        GROUP_CONCAT(options.options) 
                    FROM 
                        options 
                    WHERE 
                        questions.option_group_id = options.option_group_id
                ) as options,
                (
                    SELECT 
                        GROUP_CONCAT(options.option_id) 
                    FROM 
                        options 
                    WHERE 
                        questions.option_group_id = options.option_group_id
                ) as options_id ,
                IFNULL(
                    answers.user_id,
                    0
                ) as user_id
            FROM 
                questions 
                JOIN categories ON questions.category_id = categories.category_id 
                RIGHT JOIN options ON questions.option_group_id = options.option_group_id 
                JOIN option_group ON questions.option_group_id = option_group.option_group_id 
                LEFT JOIN answers ON answers.question_id = questions.question_id AND answers.user_id = $user_id
              ";

            if($id != "")
                $q .= " WHERE categories.category_id=$id ";

            $q .= "

             HAVING user_id = 0 ORDER BY questions.created_on DESC LIMIT 0,1";
       //echo $q;
        $query=$this->db->query($q);
        return $query->result_array();
        
    }

    public function submit_answer($input_data)
    {
        $sql = $this->db->set($input_data)->get_compiled_insert('answers');
        //echo $sql; die;
        $this->db->simple_query($sql);
        return $this->db->affected_rows();
    }

}
