<?php
class Testimonial_model extends CI_Model
{
	function submit_story($story_data)
	{
		$this->db->insert('testimonial',$story_data);
		return $this->db->affected_rows();
	}

	function getStories($limit,$Start)
	{
		$query = $this->db->query("SELECT * FROM `testimonial` where is_approve='1' limit $Start,$limit");
		$res = $query->result_array();
		return $res;
	}
    function record_count()
        {
            return $this->db->count_all("testimonial");
        }
}