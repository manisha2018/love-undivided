<?php
class User_Match_model extends CI_Model
{

	function get_match_count($user_id,$seeking)
	{
		// $sql = "SELECT count(*) as match_cnt FROM match_msg_profile WHERE user_id = $user_id";
		$sql = "SELECT 
					a1.user_id as u1_user_id, 
					a2.user_id as u2_user_id,
					 (SELECT ceil((COUNT(a1.question_id)/COUNT(*))*100)  FROM questions)  as que_per

				FROM 
					answers as a1, 
					answers as a2 
					JOIN options AS op1 ON op1.option_id = a2.option_id 
					JOIN user ON user.user_id = a2.user_id 
					AND user.gender = $seeking
					AND user.is_que_answered = '1' 
					JOIN user as u2 ON u2.user_id = $user_id
					AND u2.is_que_answered = '1' 
					LEFT JOIN match_msg_profile on match_msg_profile.user_match_id = user.user_id AND match_msg_profile.user_id = u2.user_id 
					LEFT JOIN favourite ON  favourite.to_user_id = user.user_id AND favourite.frm_user_id = u2.user_id
					
					
				WHERE 
					((a1.question_id = a2.question_id 
					AND FIND_IN_SET(
						a1.option_id, op1.match_option_ids
					)) 
					OR FIND_IN_SET(
						a2.user_id, (SELECT group_concat(DISTINCT(temp.Q2_user) ORDER BY temp.Q2_user) as users FROM (SELECT 
											q1.user_id as Q1_user, 
											q2.user_id as Q2_user, 
											(q1.avg_rating - q2.avg_rating) as diff, 
											q1.cat_id as Q1_cat, 
											q2.cat_id as Q2_cat 
										FROM 
											`que_cat_rating` as q1 
											INNER JOIN que_cat_rating as q2 ON q1.cat_id = q2.cat_id 
											AND q1.user_id != q2.user_id 
											AND (q1.avg_rating - q2.avg_rating) BETWEEN -1 AND 1 
										WHERE 
											q1.user_id = $user_id 
										                                                                                                                        
											HAVING 
												(SELECT CEIL(COUNT(*)/2) FROM `categories` as cat) <= (SELECT COUNT(DISTINCT(Q1_cat)))
										ORDER BY 
											`Q1_user` DESC) AS temp)
					)) 
					AND a1.user_id <> a2.user_id 
					AND a1.user_id = $user_id ";


					$sql .="
					
				GROUP BY
				    
				    u2_user_id
				    
				   HAVING
				   que_per >= 70";

				

		$query = $this->db->query($sql);
		return count($query->result_array());
	}
	function get_matches($user_id,$seeking,$is_messaged=1,$age1='',$age2='',$search_key="",$distance="",$page="",$limit="")
	{
		$sql = "SELECT 
					a1.question_id, 
					a1.user_id as u1_user_id, 
					a2.user_id as u2_user_id, 
					user.name, 
					user.email, 
					user.slug_name, 
					user.age,
					IF(
						user.user_image IS NULL 
						OR user.user_image = '' 
						OR user.user_image = '0', 
						'', 
						IF(
							user.user_image like 'http%', 
							user.user_image, 
							CONCAT(
								'".USER_IMAGES."', 
								user.user_image
							)
						)
					) AS user_image,
					( 3959 * acos( cos( radians(user.lat) ) * cos( radians( u2.lat ) ) * cos( radians( u2.lng ) - radians(user.lng) ) + sin( radians(user.lat) ) * sin( radians( u2.lat ) ) ) ) AS distance,
				     (SELECT ceil((COUNT(a1.question_id)/COUNT(*))*100)  FROM questions)  as que_per,
				     IF(u2.is_unblur_active= '1','4',match_msg_profile.status) as status,
				     match_msg_profile.read_count,
				     IFNULL(favourite.frm_user_id,0) as fav_user_id

				FROM 
					answers as a1, 
					answers as a2 
					JOIN options AS op1 ON op1.option_id = a2.option_id 
					JOIN user ON user.user_id = a2.user_id 
					AND user.gender = $seeking
					AND user.is_que_answered = '1' 
					JOIN user as u2 ON u2.user_id = $user_id
					AND u2.is_que_answered = '1' 
					LEFT JOIN match_msg_profile on match_msg_profile.user_match_id = user.user_id AND match_msg_profile.user_id = u2.user_id 
					LEFT JOIN favourite ON  favourite.to_user_id = user.user_id AND favourite.frm_user_id = u2.user_id
					
					
				WHERE 
					((a1.question_id = a2.question_id 
					AND FIND_IN_SET(
						a1.option_id, op1.match_option_ids
					)) 
					OR FIND_IN_SET(
						a2.user_id, (SELECT group_concat(DISTINCT(temp.Q2_user) ORDER BY temp.Q2_user) as users FROM (SELECT 
											q1.user_id as Q1_user, 
											q2.user_id as Q2_user, 
											(q1.avg_rating - q2.avg_rating) as diff, 
											q1.cat_id as Q1_cat, 
											q2.cat_id as Q2_cat 
										FROM 
											`que_cat_rating` as q1 
											INNER JOIN que_cat_rating as q2 ON q1.cat_id = q2.cat_id 
											AND q1.user_id != q2.user_id 
											AND (q1.avg_rating - q2.avg_rating) BETWEEN -1 AND 1 
										WHERE 
											q1.user_id = $user_id 
										                                                                                                                        
											HAVING 
												(SELECT CEIL(COUNT(*)/2) FROM `categories` as cat) <= (SELECT COUNT(DISTINCT(Q1_cat)))
										ORDER BY 
											`Q1_user` DESC) AS temp)
					)) 
					AND a1.user_id <> a2.user_id 
					AND a1.user_id = $user_id ";

					if($age1 != '' && $age2 != '')
						$sql .= " AND user.age BETWEEN $age1 AND $age2 ";

					if($search_key != "")
					{
						$sql .= " AND (user.name like '%$search_key%' OR user.city like '%$search_key%') ";
					}

					$sql .="
					
				GROUP BY
				    
				    u2_user_id
				    
				   HAVING
				   que_per >= 70";

				   	if($is_messaged != "")
				   	{
				   		if($is_messaged == 0)
							$sql .=" AND status = '0'";
						
					}
					if($distance != "")
					{
						$sql .= " AND distance <= $distance ";
					}
					if($page!="" && $limit!="")
						$sql .= " LIMIT $page,$limit ";

				// echo $sql; die;
		$query = $this->db->query($sql);
		$res = $query->result_array();
		// echo $this->db->last_query();
		// die;

		// $sql = "SELECT * FROM match_msg_profile WHERE user_id = $user_id";
		// $query2 = $this->db->query($sql);
		// $arr_res = $query2->result_array();

		
		// echo '<pre>';
		// print_r($res); die;
		

		$input_data = [];
		// foreach ($res as $key => $value) {
		// 	$temp['user_id'] = $value['u1_user_id'];
		// 	$temp['user_match_id'] = $value['u2_user_id'];

		// 	foreach ($arr_res as $key2 => $value2) {
		// 		if($value2['user_match_id'] != $value['u2_user_id'])
		// 			array_push($input_data, $temp);
		// 	}

			
		// }


		
		foreach ($res as $key => $value2) {

			$temp['user_id'] = $value2['u1_user_id'];
			$temp['user_match_id'] = $value2['u2_user_id'];

			if($value2['status'] == '')
				array_push($input_data, $temp);
		}
		

		//print_r($input_data ); die;
		if(count($input_data) > 0)
		{



			$sql = "INSERT INTO match_msg_profile(user_id,user_match_id) VALUES ";
			foreach ($input_data as $key => $value) {
				$sql.="
						('".$value['user_id']."','".$value['user_match_id']."'),";
			}
			$sql=rtrim($sql,',');

			//echo $sql; die;
			$this->db->simple_query($sql);
		}


		return $res;
	}

	public function update_match_msg($where,$input_data)
	{


		$this->db->where($where)
			->update('match_msg_profile',$input_data);

		//echo $this->db->last_query(); die;

		return $this->db->affected_rows();
	}


	public function make_read_count($user_id,$user_match_id)
	{
		$sql = "UPDATE match_msg_profile 
				    SET read_count = read_count + 1
				    WHERE user_id = $user_id AND user_match_id = $user_match_id ";
		// echo $sql ; die;				    
		$this->db->simple_query($sql);
		return $this->db->affected_rows();
	}
	public function insert_match_msg($input_data)
	{
		

		$sql = $this->db->set($input_data)->get_compiled_insert('match_msg_profile');
		$this->db->simple_query($sql);
		return $this->db->affected_rows();
	}

	public function create_activity($input_data)
	{
		$this->db->insert('activity',$input_data);
		return $this->db->affected_rows();
	}

	public function filter_matches($type,$data,$age1,$age2)
	{
		 $user_id = $this->session->userdata('session_data')['user_id'];
    	$seeking = $this->session->userdata('session_data')['seeking'];
    	//print_r($data); die;
    	if($data == '1')
			return $this->get_matches($user_id,$seeking);
		else if($data == '2')
		{
			return $this->get_matches($user_id,$seeking,'1');
		}
		else if($data == '3')
		{
			return $this->get_matches($user_id,$seeking,'0');
		}

		else if(!empty($age1) && !empty($age2) && $type == '2')
		{
			return $this->get_matches($user_id,$seeking,'',$age1,$age2);
		}

		else if($type == '3')
		{
			return $this->get_matches($user_id,$seeking,'',$age1,$age2,trim($data));
		
		}
		else if($type == '4')
		{
			return $this->get_matches($user_id,$seeking,'',$age1,$age2,"",trim($data));
		
		}
	}


	public function find_match()
	{
		$sql = "SELECT 
					a1.question_id, 
					a1.user_id as u1_user_id, 
					a2.user_id as u2_user_id, 
					user.name, 
					user.slug_name, 
					user.age
				

				FROM 
					answers as a1, 
					answers as a2 
					JOIN options AS op1 ON op1.option_id = a2.option_id 
					JOIN user ON user.user_id = a2.user_id 
					AND user.gender = $seeking
					AND user.is_que_answered = '1' 
					JOIN user as u2 ON u2.user_id = $user_id
					AND u2.is_que_answered = '1' 
					LEFT JOIN match_msg_profile on match_msg_profile.user_match_id = u2.user_id AND match_msg_profile.user_id = user.user_id 
					
					
				WHERE 
					((a1.question_id = a2.question_id 
					AND FIND_IN_SET(
						a1.option_id, op1.match_option_ids
					)) 
					OR FIND_IN_SET(
						a2.user_id, (SELECT group_concat(DISTINCT(temp.Q2_user) ORDER BY temp.Q2_user) as users FROM (SELECT 
											q1.user_id as Q1_user, 
											q2.user_id as Q2_user, 
											(q1.avg_rating - q2.avg_rating) as diff, 
											q1.cat_id as Q1_cat, 
											q2.cat_id as Q2_cat 
										FROM 
											`que_cat_rating` as q1 
											INNER JOIN que_cat_rating as q2 ON q1.cat_id = q2.cat_id 
											AND q1.user_id != q2.user_id 
											AND (q1.avg_rating - q2.avg_rating) BETWEEN -1 AND 1 
										WHERE 
											q1.user_id = $user_id 
										                                                                                                                        
											HAVING 
												(SELECT CEIL(COUNT(*)/2) FROM `categories` as cat) <= (SELECT COUNT(DISTINCT(Q1_cat)))
										ORDER BY 
											`Q1_user` DESC) AS temp)
					)) 
					AND a1.user_id <> a2.user_id 
					AND a1.user_id = $user_id
					
				GROUP BY
				    
				    u2_user_id
				    
				   HAVING
				   que_per >= 70";

				 //   	if($is_messaged != "")
				 //   	{
				 //   		if($is_messaged == 0)
					// 		$sql .=" AND status = '0'";
						
					// }
					// if($distance != "")
					// {
					// 	$sql .= " AND distance <= $distance ";
					// }
	}

	public function add_favourite($input_data)
	{
		$sql = $this->db->set($input_data)->get_compiled_insert('favourite'); 
		// echo $sql; die;
		$this->db->simple_query($sql);

		if($this->db->affected_rows() == '-1')
		{
			$this->db->delete('favourite',['frm_user_id'=>$input_data['frm_user_id'],'to_user_id'=>$input_data['to_user_id']]);
		}

   		return $this->db->affected_rows();

	}

	public function get_favourites($user_id,$is_fav_me=0)
	{

		if($is_fav_me == 0)
		{
			$sql = "SELECT 
					user.user_id as u2_user_id,
				    user.slug_name,
				    IF(
						user.user_image IS NULL 
						OR user.user_image = '' 
						OR user.user_image = '0', 
						'', 
						IF(
							user.user_image like 'http%', 
							user.user_image, 
							CONCAT(
								'".USER_IMAGES."', 
								user.user_image
							)
						)
					) AS user_image,
				    
				    user.name,
				    IF(user.is_unblur_active= '1','4',match_msg_profile.status) as status,
				    match_msg_profile.read_count,
				    IFNULL(fav2.frm_user_id,0) as fav_user_id

				FROM 
					`favourite` 
					JOIN user on user.user_id = favourite.to_user_id 
					
					LEFT JOIN match_msg_profile on match_msg_profile.user_match_id = user.user_id AND match_msg_profile.user_id = $user_id 
					LEFT JOIN favourite as fav2 ON  fav2.to_user_id = user.user_id AND fav2.frm_user_id = $user_id
				WHERE  favourite.frm_user_id = $user_id";
		}
		else
		{
			$sql = "SELECT 
					user.user_id as u2_user_id,
				    user.slug_name,
				    IF(
						user.user_image IS NULL 
						OR user.user_image = '' 
						OR user.user_image = '0', 
						'', 
						IF(
							user.user_image like 'http%', 
							user.user_image, 
							CONCAT(
								'".USER_IMAGES."', 
								user.user_image
							)
						)
					) AS user_image,
				    
				    user.name,
				    IF(user.is_unblur_active= '1','4',match_msg_profile.status) as status,
				    match_msg_profile.read_count,
				    IFNULL(fav2.frm_user_id,0) as fav_user_id

				FROM 
					`favourite` 
					JOIN user   on user.user_id = favourite.frm_user_id 
					LEFT JOIN match_msg_profile on match_msg_profile.user_match_id = user.user_id AND match_msg_profile.user_id = $user_id 
					LEFT JOIN favourite as fav2 ON  fav2.to_user_id = user.user_id AND fav2.frm_user_id = $user_id
				WHERE 
				favourite.to_user_id = $user_id";
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->result_array();

	}



	function get_new_matches($user_id,$seeking)
	{
		$sql = "SELECT 
					a1.question_id, 
					a1.user_id as u1_user_id, 
					a2.user_id as u2_user_id, 
					user.name, 
					user.email, 
					user.slug_name, 
					user.age, 
					IF(
						user.user_image IS NULL 
						OR user.user_image = '' 
						OR user.user_image = '0', 
						'', 
						IF(
							user.user_image like 'http%', 
							user.user_image, 
							CONCAT(
								'http://bizmoapps.com/love-undivided/uploads/user_images/', 
								user.user_image
							)
						)
					) AS user_image, 
					(
						3959 * acos(
							cos(
								radians(user.lat)
							) * cos(
								radians(u2.lat)
							) * cos(
								radians(u2.lng) - radians(user.lng)
							) + sin(
								radians(user.lat)
							) * sin(
								radians(u2.lat)
							)
						)
					) AS distance, 
					(
						SELECT 
							ceil(
								(
									COUNT(a1.question_id)/ COUNT(*)
								)* 100
							) 
						FROM 
							questions
					) as que_per, 
					IF(
						u2.is_unblur_active = '1', '4', match_msg_profile.status
					) as status, 
					match_msg_profile.read_count, 
					IFNULL(favourite.frm_user_id, 0) as fav_user_id 
				FROM 
					answers as a1, 
					answers as a2 
					JOIN options AS op1 ON op1.option_id = a2.option_id 
					JOIN user ON user.user_id = a2.user_id 
					AND user.gender = $seeking
					AND user.is_que_answered = '1' 
					JOIN user as u2 ON u2.user_id = $user_id 
					AND u2.is_que_answered = '1' 
					LEFT JOIN match_msg_profile on match_msg_profile.user_match_id = user.user_id 
					AND match_msg_profile.user_id = u2.user_id 
					LEFT JOIN favourite ON favourite.to_user_id = user.user_id 
					AND favourite.frm_user_id = u2.user_id 
				WHERE 
					(
						(
							a1.question_id = a2.question_id 
							AND FIND_IN_SET(
								a1.option_id, op1.match_option_ids
							)
						) 
						OR FIND_IN_SET(
							a2.user_id, 
							(
								SELECT 
									group_concat(
										DISTINCT(temp.Q2_user) 
										ORDER BY 
											temp.Q2_user
									) as users 
								FROM 
									(
										SELECT 
											q1.user_id as Q1_user, 
											q2.user_id as Q2_user, 
											(q1.avg_rating - q2.avg_rating) as diff, 
											q1.cat_id as Q1_cat, 
											q2.cat_id as Q2_cat 
										FROM 
											`que_cat_rating` as q1 
											INNER JOIN que_cat_rating as q2 ON q1.cat_id = q2.cat_id 
											AND q1.user_id != q2.user_id 
											AND (q1.avg_rating - q2.avg_rating) BETWEEN -1 
											AND 1 
										WHERE 
											q1.user_id = $user_id 
										HAVING 
											(
												SELECT 
													CEIL(
														COUNT(*)/ 2
													) 
												FROM 
													`categories` as cat
											) <= (
												SELECT 
													COUNT(
														DISTINCT(Q1_cat)
													)
											) 
										ORDER BY 
											`Q1_user` DESC
									) AS temp
							)
						)
					) 
					AND a1.user_id <> a2.user_id 
					AND a1.user_id = $user_id 
					AND a2.user_id NOT IN (SELECT user_match_id 
				FROM  `match_msg_profile` 
				WHERE  `user_id` =$user_id)
					 
				GROUP BY 
					u2_user_id 
				HAVING 
					que_per >= 70";

				 //   	if($is_messaged != "")
				 //   	{
				 //   		if($is_messaged == 0)
					// 		$sql .=" AND status = '0'";
						
					// }

					//echo $sql; die;
					

		$query = $this->db->query($sql);
		$res = $query->result_array();


		return $res;
	}
}