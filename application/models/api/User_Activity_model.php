<?php
class User_Activity_model extends CI_Model
{
	function getQueryResult($query)
    {
        $result = $this->db->query($query);
        return $result->result_array();
     
    }
    function getQueryAffectedRows($query)
    {
        $result = $this->db->query($query);
		return $this->db->affected_rows();     
    }
    function get_activity_count($user_id,$type)
    {
    	$sql = "SELECT activity_id  FROM activity WHERE activity.match_id = $user_id "; 

    	if($type!='')
    		$sql .= " AND activity.activity_type = '$type'";

    	$sql .= " OR (activity.match_id = 0 AND $user_id IN ( SELECT user_match_id FROM match_msg_profile as mp2 where mp2.user_id = activity.user_id ) ) GROUP BY
    
    			activity.user_id ,
    			activity.activity_type ";
    			// echo $sql; die;

    	$query = $this->db->query($sql);

		return  count($query->result_array());
    }
	function get_user_activities($user_id,$type,$page,$limit)
	{
		$sql = "SELECT * FROM (SELECT 
				activity.*,
				user.name,
				user.gender,
				IF(
					user.user_image IS NULL 
					OR user.user_image = '' 
					OR user.user_image = '0', 
					'', 
					IF(
						user.user_image like 'http%', 
						user.user_image, 
						CONCAT(
							'".USER_IMAGES."', 
							user.user_image
						)
					)
				) AS user_image,
				count(activity.user_id) as cnt_activity,
				match_msg_profile.status
				FROM activity 
				JOIN user ON activity.user_id = user.user_id
				 LEFT JOIN match_msg_profile ON match_msg_profile.user_match_id = user.user_id AND match_msg_profile.user_id = activity.match_id
				 WHERE activity.match_id = $user_id
				 ";
		if($type != '')
		{
			$sql .= " AND activity.activity_type = '$type'
				 ";
		}



		$sql .= "
				GROUP BY
    
    			activity.user_id ,
    			activity.activity_type

				 UNION 
				SELECT 
				activity.*,
				user.name,
				user.gender,
				IF(
					user.user_image IS NULL 
					OR user.user_image = '' 
					OR user.user_image = '0', 
					'', 
					IF(
						user.user_image like 'http%', 
						user.user_image, 
						CONCAT(
							'".USER_IMAGES."', 
							user.user_image
						)
					)
				) AS user_image,
				count(activity.user_id) as cnt_activity,
				match_msg_profile.status
				FROM activity 
				JOIN user ON activity.user_id = user.user_id
				 LEFT JOIN match_msg_profile ON match_msg_profile.user_match_id = user.user_id AND match_msg_profile.user_id = activity.match_id
				 WHERE activity.match_id = 0
				 AND $user_id IN (SELECT user_match_id FROM match_msg_profile as mp2 where mp2.user_id = user.user_id)

				 ";
		if($type != '')
		{
			$sql .= " AND activity.activity_type = '$type'
				 ";
		}
		$sql .= "  GROUP BY
    
    			activity.user_id ,
    			activity.activity_type
    			) as temp_activity limit $page,$limit
    			
    			";
		// echo $sql; die;
		$query = $this->db->query($sql);

		return  $query->result_array();

	}

	function get_user_update_activities($user_id,$type)
	{
		$sql = "SELECT 
				activity.*,
				user.name,
				user.gender,
				IF(
					user.user_image IS NULL 
					OR user.user_image = '' 
					OR user.user_image = '0', 
					'', 
					IF(
						user.user_image like 'http%', 
						user.user_image, 
						CONCAT(
							'".USER_IMAGES."', 
							user.user_image
						)
					)
				) AS user_image,
				count(activity.user_id) as cnt_activity,
				match_msg_profile.status
				FROM activity 
				JOIN user ON activity.user_id = user.user_id
				 LEFT JOIN match_msg_profile ON match_msg_profile.user_match_id = user.user_id AND match_msg_profile.user_id = activity.match_id
				 WHERE activity.match_id = 0
				 AND $user_id IN (SELECT user_match_id FROM match_msg_profile as mp2 where mp2.user_id = user.user_id)

				 ";
		if($type != '')
		{
			$sql .= " AND activity.activity_type = '$type'
				 ";
		}
		$sql .= "  GROUP BY
    
    			activity.user_id,
    			activity.activity_type ";
		// echo $sql; die;
		$query = $this->db->query($sql);

		return  $query->result_array();

	}

	function get_user_visits($user_id=0)
	{
		$sql = "SELECT 
					activity.*, 
					user.email, 
					user.name, 
					user.city, 
					u2.email as visit_by_email, 
					u2.name as visit_by_name, 
					u2.city as visit_by_city, 
					COUNT(match_id) as cnt 
				FROM 
					`activity` 
					JOIN user ON user.user_id = activity.match_id 
					JOIN user as u2  ON u2.user_id = activity.user_id 
				WHERE 
					DATE(activity.created_on) = CURDATE() ";

		if(!empty($user_id))
		{
			$sql .= "user.user_id = $user_id ";
		}	

		$sql .= "
				GROUP BY 
					match_id";

		$query = $this->db->query($sql);

		return  $query->result_array();
	}

	function add_contact_setting($user_id)
	{
		$inputdata = [];

		$sql = " INSERT INTO user_email_alert (user_id,alert_type,status) VALUES ";
	    for ($i=1; $i <= 4; $i++) { 
	      // $temp = [];
	      // $temp['user_id'] = $this->session->userdata('session_data')['user_id'];
	      // $temp['alert_type'] = $i;
	      // $temp['status'] =  '1';

	      // array_push($inputdata, $temp);

	    	$sql .= " ('$user_id','$i','1'),";
	    }
	    //echo rtrim($sql,','); die;
	    $query = $this->db->simple_query(rtrim($sql,','));
		return $this->db->affected_rows();
	}
	function get_contact_setting($user_id)
	{
		$this->db->where(['user_id'=>$user_id]);
		$query  = $this->db->get('user_email_alert');

		//echo $this->db->last_query(); die;
		return  $query->result_array();
	}

	function update_setting($inputdata,$where)
	{
		$this->db->where($where);
		$this->db->update('user_email_alert',$inputdata);
		return $this->db->affected_rows();
	}

	function SndMailToExpirePlanUsers()
	{
		$sql = "SELECT user.email,user.name,user_subscription.end_date,TIMESTAMPDIFF(DAY,CURRENT_DATE,user_subscription.end_date)as days FROM user JOIN user_subscription ON user.user_id=user_subscription.user_id having days<=10 AND days>=0";

        $query = $this->db->query($sql);

		return  $query->result_array();
	}
}
