<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>LoveUndivided</title>
    <link href="<?php echo WEB_ASSETS; ?>images/favicon.ico" rel="icon">

    <!-- Bootstrap -->
    <link href="<?php echo WEB_ASSETS; ?>css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo WEB_ASSETS; ?>css/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSETS; ?>css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo WEB_ASSETS; ?>css/sweetalert.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo WEB_ASSETS; ?>css/custom.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo WEB_ASSETS; ?>css/custom-responsive.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo WEB_ASSETS; ?>css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Raleway:400,100,300,300italic,400italic,500,500italic,600,600italic,700italic,700,800%7CLora:400,400italic,700,700italic">


    <link rel="stylesheet" href="<?php echo WEB_ASSETS; ?>css/bootstrap-datepicker.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo WEB_ASSETS; ?>css/font-awesome/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo WEB_ASSETS; ?>css/imgareaselect.css">
    <link rel="stylesheet" href="<?php echo WEB_ASSETS; ?>css/flexslider.css">
    <link rel="stylesheet" type="text/css" href="<?php  echo WEB_ASSETS?>css/progress.css">
    <!-- <link rel="stylesheet" href="<?php echo WEB_ASSETS; ?>css/pgwslideshow.css"> -->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style type="text/css">      
    .alert
    {
       font-size: 12px; 
    }  
    .custom-alert
    {
      width: 250px;          
    }
    .custom-alert .alert
    {
      margin-bottom: 0px !important;
      padding: 8px !important;
    }
    .custom-alert .alert-dismissable .close,.custom-alert .alert-dismissible .close
    {
      top: -8px !important;
      right: -3px !important;
    }
    .alert-warning
    {
      background-color: #ffedeb !important;
      border-color: #ffcbc7 !important;
      color: #f44336 !important;
    }
  .close
  {
    color: #f44336 !important;
    opacity: 1 !important;
  }
  </style>  
  </head>
  <body>

     <?php if(isset($this->session->userdata('session_data')['user_id']))
            $url = BASE_URL.'dashboard';
          else
            $url = BASE_URL;
      ?>
      <div class="custom-header navbar-fixed-top">
      <div class="header-area container">  
        <!--<center>
        <a href="<?php echo $url;?>" class="logo"><img src="<?php echo WEB_ASSETS; ?>images/logo.png" align=""/></a></center>   -->
        <div class="row"> 
          <div class="col-sm-3 col-xs-12">
            <a href="<?php echo $url;?>" class="logo"><img src="<?php echo TEMPLATE_ASSETS; ?>images/logo.png" align="" class="img-reponsive"/></a>
          </div>

          <div class="col-sm-7 col-xs-12 ord2">
           <?php if(isset($this->session->userdata('session_data')['user_id'])){ ?>
            <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed text-right" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="<?php if($this->uri->segment(1)== 'activities'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>activities">Activities</a></li>
        <li class="<?php if($this->uri->segment(1)== 'matches'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>matches">Matches</a></li>
          <li class="<?php if($this->uri->segment(1)== 'chats'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>chats">Messages</a></li>
          <!-- <li class="custom-alert hidden-xs">          
          <div class="alert alert-warning alert-dismissible" role="alert" style="margin-left: 10px;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Warning! Better check yourself, you're not looking too good.
          </div>

          </li>
 -->

          
        
        <li class="dropdown visible-xs">
         <?php if(isset($this->session->userdata('session_data')['user_id'])){ ?>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profile <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo BASE_URL;?>profile">User Profile</a></li>
            <li><a href="<?php echo BASE_URL;?>general-settings">Account Settings</a></li>
            <li><a href="<?php  echo BASE_URL;?>api/User_Login/logout">Logout</a></li>
          </ul>
          <?php }  ?>
        </li> 
        <li class="visible-xs"><a href="#">Hi, Manisha</a></li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
 <?php }  ?>
          </div>

          <div class="col-xs-12 visible-xs">
            <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Warning! Better check yourself, you're not looking too good.
          </div>
          </div>

        <div class="col-sm-2 hidden-xs">
          <?php if(isset($this->session->userdata('session_data')['user_id'])){ ?>
          <div class="dropdown btn-group pull-right">
                  <!-- <a class="btn dropdown-toggle profile-icon" data-toggle="dropdown" href="#">
                      <!-- <img src="<?php echo WEB_ASSETS; ?>images/profile-icon.png" alt=""/>  -
                      <img src="<?php echo WEB_ASSETS; ?>images/homepage-user.png" alt="" height="30px" width="30px"/> <br>
                      <span class="usernm">Hi, Manisha</span>
                  </a> -->
                    <a class="btn dropdown-toggle profile-icon" data-toggle="dropdown" href="#">
                    
                    <?php 
                    if(isset($this->session->userdata('session_data')['user_image']))
                    {
                        $user_image = $this->session->userdata('session_data')['user_image'];
                      if($user_image)
                      {?>
                       <img src="<?php echo USER_IMAGES.$user_image ?>" alt="" /><br>
                      <?php } else {?><img src="<?php echo WEB_ASSETS; ?>images/homepage-user.png" alt=""/><br> <?php } ?>
                 
                     <?php } else {?><img src="<?php echo WEB_ASSETS; ?>images/homepage-user.png" alt=""/><br><?php }?>
                      <span class="usernm">Hi,<?php echo $this->session->userdata('session_data')['name']; ?> </span>
                  </a> 
                  <ul class="dropdown-menu">
                        <li><a href="<?php echo BASE_URL;?>profile">User Profile</a></li>
                         <li><a href="<?php echo BASE_URL;?>general-settings">Account Settings</a></li>
                        <li><a href="<?php  echo BASE_URL;?>api/User_Login/logout">Logout</a></li>
                  </ul>
          </div> 
         <?php }  ?>

          </div>

        </div>

        
      </div>   
      </div>
       <div class='progress123' id="progress_div">
          <div class='bar123' id='bar123'></div>
          <div class='percent' id='percent1'></div>
          <input type="hidden" id="progress_width" value="0">
      </div> 
      <div class="header-footer"></div>