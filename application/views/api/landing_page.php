<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>==== undivided ====</title>

    <!-- Bootstrap -->
    <link href="<?php echo WEB_ASSETS;?>/css/bootstrap.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php echo WEB_ASSETS;?>/css/custom.css" type="text/css" />
    <link rel="stylesheet" href=<?php echo WEB_ASSETS;?>/"css/custom-responsive.css" type="text/css" />
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="landing">

    <div class="header-landing">
    <div class="container">
    	<div class="row">
        	<div class="col-sm-6 land-logo">
    		<a href="#" class="logo"><img src="<?php echo WEB_ASSETS;?>/images/logo.png" align=""/></a>   
        	</div>
        	<div class="col-sm-6">
            <ul class="land-manu">
        		<li><a href="#">Log in</a> </li>
                <li><a href="#">Registration</a> </li>
            </ul>
        	</div>
        </div>
        </div>
    </div>
    <div class="container">
    	<div class="row">
        	<div class="col-sm-12">
            	<h1 class="land-title">We bring people together.
				<span>Love unites them...</span></h1>
            </div>
        </div>
    </div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo WEB_ASSETS;?>/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo WEB_ASSETS;?>/js/bootstrap.min.js"></script>
  </body>
</html>