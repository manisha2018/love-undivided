<?php require('home_header.php');?> 
<style type="text/css">
  .error{
    margin-right: 85%;
  }
</style>
      <!-- Page Content-->
      <main class="page-content">
        <section class="bg-images-baner section-md-100 section-80">
          <div class="shell position-r"><a href="<?php echo BASE_URL;?>"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/logo.png" alt="" width="400px"></a></div>
        </section>
        
        <section class="section-md-80 section-50 backrgound" style="padding: 40px 0px">
         <?php if($this->session->flashdata('web_flash')){ ?>
                   <div class="alert alert-danger alert-dismissible fade in" role="alert" id="message">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <?php  echo $this->session->flashdata('web_flash'); ?>
                   </div>
                <?php }?>
          <div class="shell text-left">
            <h1>Testimonials</h1>
             <div class="range range-md-reverse offset-top-60">
            <div class="container">
            <?php foreach ($get_stories as $key => $value) {?>
            <div class="col-sm-12 col-xs-12 testimonial-section">
              <div class="col-md-2 col-sm-3 col-xs-12">
                <img src="<?php echo TESTIMONIAL_PIC.$value['couple_image']; ?>" class="img-responsive center-block">
              </div>
              <div class="col-md-10 col-sm-9 col-xs-12">
                <p><?php echo $value['story_text'];?></p>
                <br>
                <p class="text-right">- <?php echo $value['couple_name'];?></p>
              </div>
            </div>
             <?php } ?>
            <!-- <div class="col-sm-12 col-xs-12 testimonial-section">  
            <div class="col-md-2 col-sm-3 col-xs-12">
                <img src="<?php echo TEMPLATE_ASSETS; ?>/images/home-05.jpg" class="img-responsive center-block">
              </div>            
              <div class="col-md-10 col-sm-9 col-xs-12">
                <p>"I joined DatingBetter.com and found the website was very easy to use. I never did anything like online dating before, but I always found meeting new singles a pain because I never had time and had difficulty in person. DatingBetter.com made it easy to meet singles on my own time and with less stress.<br>"I've met some really attractive and great girls whom I shared a lot in common with and went on several dates with. In fact, I met my current girlfriend on this site. I still come back to read the dating advice articles. Daniel Packard's articles are hilarious and great!<br>"My girlfriend and me are very grateful to DatingBeter.com because they made it easy for us to meet each other. How else would we have met?<br>"Thank You"</p>
                <br>
                <p class="text-right">- Peters</p>
              </div>              
            </div> -->
            </div>
            <div class="container text-right">
              <nav aria-label="Page navigation">
              <ul class="pagination" style="margin: 0px !important;">
              <!--<li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>-->
              <div class="pagination" style="margin: 0px !important;">
                               <?php foreach ($links as  $link) {
                                echo  $link;
                                    }?>
                                </div>
              </ul>
              </nav>
            </div>


            </div>
    
          </div>
        </section>

        <div class="container">
                <!-- RD Mailform-->
               
                <h3>Share your story with us </h3>
                <form class="testimonial-form" id="testimonial-form"  method="post" action="<?php echo BASE_URL?>api/Testimonial/submit_story" class="text-left" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="contact-name" class="form-label">Your Name</label>
                    <input id="contact-name" type="text" name="name" required="required" oninput="validateAlpha();"  class="form-control">
                  </div>

                   <div class="form-group">
                    <label for="contact-email" class="form-label">Your Email</label>
                    <input id="contact-email" type="email" name="email" required="required"  class="form-control">
                  </div>
                 

                  <div class="form-group offset-md-top-37">
                    <label for="contact-message" class="form-label">Your Message (Enter 1000 chracters only)</label>
                    <textarea id="contact-message" name="story" required="required" oninput="validateMessage();" maxlength="1000" class="form-control form-control-impressed"></textarea>
                  </div>
                   <div class="row">
                  <div class="custom-file-upload col-sm-6">
                  <div class="input-group">
                  <span class="input-group-btn">
                  <button id="file-button-browse" type="button" class="btn btn-default">
                  Browse <span class="glyphicon glyphicon-file"></span>
                  </button>
                  </span>
                  <input type="file" accept="image/gif, image/jpeg" onchange="validateFileType()" id="files-input-upload" style="display:none;height: 57px;" name="couple_image">
                  <input type="text" id="file-input-name" disabled="disabled" placeholder="File not selected" class="form-control" style="height: 57px;">
                  <span class="input-group-btn">
                  <button type="submit" class="btn btn-default" disabled="disabled" id="file-button-upload">  Upload 
                  <span class="glyphicon glyphicon-upload"></span>
                  </button>
                  </span>
                  </div>
                  </div>
                  </div>
                  <button type="submit" class="btn btn-primary text-uppercase offset-top-25">send message</button>
                </form><br><br>
              </div>

      </main>
      <!-- Page Footer-->
       <?php require('home_footer.php');?>
    <!-- Java script-->
    <!-- <script src="<?php echo TEMPLATE_ASSETS; ?>/js/core.min.js"></script>
    <script src="<?php echo TEMPLATE_ASSETS; ?>/js/script.js"></script> -->
    <script type="text/javascript">
    document.getElementById('file-button-browse').addEventListener('click', function() {
    document.getElementById('files-input-upload').click();
    });
    document.getElementById('files-input-upload').addEventListener('change', function() {
    document.getElementById('file-input-name').value = this.value;
    document.getElementById('file-button-upload').removeAttribute('disabled');
    });

     function validateAlpha(){
          var textInput = document.getElementById("contact-name").value;
          textInput = textInput.replace(/[^A-Za-z]+[\s]/g, "");
          document.getElementById("contact-name").value = textInput;
          }

      function validateMessage(){
          var textInput1 = document.getElementById("contact-message").value;
          //textInput1 = textInput1.replace(/[a-zA-Z0-9]+([\s][a-zA-Z0-9][^%$:-]+)*/, "");
          textInput1 = textInput1.replace(/[^A-Za-z0-9]+[\s]+[^%$&:,.-]/g, "");
          document.getElementById("contact-message").value = textInput1;
          }
        function validateFileType(){
        var fileName = document.getElementById("files-input-upload").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="jpg" || extFile=="jpeg" || extFile=="png" || extFile=="JPG" || extFile=="JPEG" || extFile=="PNG"){
            //TO DO
        }else{
            alert("Only jpg/jpeg and png files are allowed!");
        }   
    }
   </script>