<?php $this->load->view('api/header');?> 



    
    
    <div class="question question2 question3">
      <?php 
      foreach($questions as $value){

        $cat_id = $value['category_id'];
        $options = $value['key_option'];
       

        ?> 
        <input type="hidden" id="question_id" value="<?php echo $value['question_id']; ?>">
        <h2><span id="que_no">1.</span>&nbsp;<span id="que_name"><?php echo $value['question']; ?></span></h2><?php } ?>
        <div id="que_option">
          <?php 
          foreach ($options as $key => $value) {?>
            <div class="que_radio">
              <div class="radiobtn_holder1">
              <input id="option_<?php echo $key; ?>" class="rad_option" type="radio" name="radio_option" value="<?php echo $key; ?>">
              <label for="option"  id="que_option_<?php echo $key ;?>"><?php echo $value; ?></label>
            </div>
            </div>
          <?php 
          } ?>
        </div>
        <h2 class="text-center">How Important is this to you? </h2>
         <div class="col-md-1">
        </div>
        <div class="row">
        <div class="col-md-2">
          
          <div class="que_radio text-center">
           <div class="text-center visible-sm visible-xs">
            <p style="margin-bottom: 5px;">Not Important</p>
          </div>
            <div class="radiobtn_holder1">
            <input id="rat_option" class="rate_rad_option" type="radio" name="rat_option" value="1">
            <label for="option"  id="que_option_1">1</label>
            </div>
          </div>
          <div class="text-center hidden-sm hidden-xs">
            <p>Not Important</p>
          </div>
        </div>

        <div class="col-md-2">
          <div class="que_radio text-center">
            <div class="radiobtn_holder1">
            <input id="rat_option" class="rate_rad_option" type="radio" name="rat_option" value="2">
            <label for="option"  id="que_option_1">2</label>
            </div>
          </div>
        </div>

        <div class="col-md-2">
          <div class="que_radio text-center">
            <div class="radiobtn_holder1">
            <input id="rat_option" class="rate_rad_option" type="radio" name="rat_option" value="3">
            <label for="option"  id="que_option_1">3</label>
            </div>
          </div>
        </div>

        <div class="col-md-2">
          <div class="que_radio text-center">
            <div class="radiobtn_holder1">
            <input id="rat_option" class="rate_rad_option" type="radio" name="rat_option" value="4">
            <label for="option"  id="que_option_1">4</label>
            </div>
          </div>
        </div>

        <div class="col-md-2">
          
          <div class="que_radio text-center">
            <div class="radiobtn_holder1">
            <input id="rat_option" class="rate_rad_option" type="radio" name="rat_option" value="5">
            <label for="option"  id="que_option_1">5</label>
            </div>
          </div>
          <div class="text-center">
            <p>Very Important</p>
          </div>
        </div>

    </div>
    <div class="row">
      <div class="col-md-12"><input type="submit" name="next" value="NEXT" class="custom-btn" onclick="next_question(<?php echo $id; ?>)"></div>
      </div>
   </div>
   <div class="foot"></div>

 <?php $this->load->view('api/footer'); ?>
 <script type="text/javascript">
     var start = 0;

   function next_question(cat_id) {
    //alert(cat_id);
    var is_answered = 0;
    var is_rated = 0;
    var selected_ans = '';
    var selected_rating = '';
    var user_id = '<?php if(isset($user_id)){ echo $user_id; } ?>';

    var question_id = $('#question_id').val();

    $('.rad_option').each(function(){
      console.log($(this).is(':checked'));
      if($(this).is(':checked'))
      {
        is_answered++; 
        selected_ans = $(this).val();
      }
    });

    $('.rate_rad_option').each(function(){
     
      if($(this).is(':checked'))
      {
        is_rated++; 
        selected_rating = $(this).val();
      }
    });

    //console.log(is_answered);
    if(is_answered == 0 || is_rated == 0)
    {
      alert("Please select option.");
      return false;
    }
    start++;
    var question_no = start + 1 ;
     $.ajax({
            url:"<?php echo BASE_URL; ?>api/User_Questions/getCategoryQuestion",
            type:'POST',
            data: {cat_id:cat_id,start:start,selected_ans:selected_ans,user_id:user_id,question_id:question_id,selected_rating:selected_rating},
            success:function(data)
            {
              $('.rate_rad_option').prop('checked', false);
              //console.log()
              var obj_que = JSON.parse(data)[0];

              // console.log(JSON.parse(data).length);
              if(JSON.parse(data).length > 0) 
              {

                $('#question_id').val(obj_que.question_id);

                $('#que_name').html(obj_que.question);
                $('#que_no').html(question_no+'. ')

                //console.log(JSON.parse(JSON.stringify(obj_que.key_option)));
                var arr_option = JSON.parse(JSON.stringify(obj_que.key_option));
                $('#que_option').html('');
                //for (var i = 0; i < arr_option.length; i++) {

               // arr_option.forEach(function(data,key) {

                Object.keys(arr_option).forEach(function(key) {
                 // console.log(arr_option[i]);
                  //$('#que_option_'+i).html(arr_option[i])

                  $('#que_option').append('<div class="que_radio"><div class="radiobtn_holder1"><input id="option_'+key+'" class="rad_option" type="radio" name="field" value="'+key+'"><label for="option" id="que_option_'+key+'">&nbsp;&nbsp;'+arr_option[key]+'</label></div></div>');


                });

              }
              else
              {
                window.location = '<?php echo BASE_URL;?>question-category';
                //alert('No questions');
              }
             // console.log(obj_que.question_id);
            }
          });
   }
 </script>