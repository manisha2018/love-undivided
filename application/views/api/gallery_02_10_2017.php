<?php $this->load->view('api/header');?> 
<style type="text/css">
.fileUpload {
    /*position: relative;
    overflow: hidden;
    margin: 10px;
    width:180px; height:45px; margin:10px auto; border-radius:10px; border:2px solid; font-size:22px; text-align:center; line-height:35px;  display:block; */ padding:0; margin:0; width:100%; height:auto; border:none; background-color:transparent;
}
.fileUpload input.upload {
    /*position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);*/ padding:0; margin:0; width:calc(100% - 60px); height:100%; position:absolute; left:50%; top:50%; transform:translate(-50%,-50%); -webkit-transform:translate(-50%,-50%); opacity:0; z-index:2; cursor:pointer;
}
/*.fileUpload1 {
    position: relative;
    overflow: hidden;
    margin: 10px;
    left: 250%;
    width:180px; height:45px; margin:10px auto; border-radius:10px; border:2px solid; font-size:22px; 
    text-align:center; line-height:35px;  display:block; 
}
.fileUpload1 input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}*/
  #nxt
  {
    position:absolute; bottom:15px; left:15px; font-size:24px; color:#ee4c4c;
  } 
  .custom-file-upload .btn-default
  {
    color: #f44437 !important;
    background-color: transparent;
    border-color: #f45246 !important;
  }
    .custom-file-upload .btn-default:hover,.custom-file-upload .btn-default:focus
    {
      background-color:#f45246 !important;
      color: #fff !important;
    }
</style>
<div class="gallery_fb">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
          <div class="second-back text-center"  style="display:none;" id="loader_gif">
            <img src="<?php echo WEB_ASSETS; ?>images/ajax-loader.gif" style="margin-top: 200px;"
             >
           </div>
          <div class="second-back" id="gallery_content">
            <h1 class="title-col">More pics means more communication</h1>
                
            <section class="slider arrow-slide pic-slide">

              <ul class="pgwSlideshow">
              <?php 
              $countImg = count($images);
              $countVideo = count($videos);
              foreach ($getimages as $key => $value) {?>
                  <li>
                  <?php  if($value['upload_type']=='0'){?>
                    <img src="<?php echo USER_IMAGES.$value['gallery_image'];?>">
                  <?php } elseif($value['upload_type']=='1'){?>
                    <video width="320" height="240" controls>
                      <source src="<?php echo USER_IMAGES.$value['gallery_image']?>" type="video/mp4">
                    </video> <?php }?>
                  </li>
              <?php }?>              
              </ul>



              <!--<div id="slider" class="flexslider">
                <ul class="slides">
                  <?php  foreach ($getimages as $key => $value) {?>
                  <li>
                    <center>
                    <img src="<?php echo USER_IMAGES.$value['gallery_image'];?>">
                    </center>
                  </li>
                  <?php }?>           
                </ul>
              </div>-->

              

              <!---
              <div id="carousel" class="flexslider">
                <ul class="slides">
                <?php  foreach ($getimages as $key => $value) {?>
                  <li>
                    <img src="<?php echo USER_IMAGES.$value['gallery_image'];?>">
                  </li>
                <?php }?>                             
                </ul>
              </div>
              -->
            </section>
            <form enctype="multipart/form-data"  id="uploadimage" method="post" action="<?php echo BASE_URL;?>api/User_Login/uploadMulipleImages" style="margin-bottom: 44px;">
              <div class="container">
              <div class="col-sm-8 col-sm-push-2 custom-file-upload">
              <div class="input-group">
              <span class="input-group-btn">
              <button id="file-button-browse" type="button" class="btn btn-default" <?php

              if($countImg >= 4 && $countVideo >= 1)
              {
              echo "disabled";
              }
              ?>>

              Browse <span class="glyphicon glyphicon-file"></span>
              </button>
              </span>
              <input type="file" id="files-input-upload" style="display:none;height: 57px;" name="gallery_image[]" multiple>
              <input type="text" id="file-input-name" disabled="disabled" placeholder="File not selected" class="form-control" style="height: 57px;">
              <span class="input-group-btn">
              <button type="submit" class="btn btn-default" disabled="disabled" id="file-button-upload">
              <span class="glyphicon glyphicon-upload"></span>
              </button>
              </span>
              </div>
              </div>

              </div> 
            </form>
            <div class="col-md-12">
              <a href="<?php echo BASE_URL;?>caption" id="nxt">Skip</a>
              <a href="<?php echo BASE_URL;?>caption" class="next-page">Next</a> 
            </div>  
          </div>
        </div>
    </div>
  </div>
</div>
    
<div class="foot"></div>

<?php $this->load->view('api/footer'); ?>
    <!-- FlexSlider -->
<script defer src="<?php echo WEB_ASSETS; ?>js/jquery.flexslider.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('.pgwSlideshow').pgwSlideshow();
});

var added_image_cnt = '<?php echo $countImg;?>';
var added_video_cnt = '<?php echo $countVideo;?>';
    $(function(){

       <?php if($countImg>0) 
          {
            $remainImg = 4 - $countImg;
          }
          else
          {
            $remainImg = 4 ;

          }


       ?>
       var countIMG = <?php echo $remainImg; ?>;
       console.log(countIMG);
       //console.log(<?php echo $remainImg; ?>);

       $('input#files-input-upload').change(function(){
         var files = $(this)[0].files;
         console.log(files[0]);

         var image_cnt = 0;
         var video_cnt = 0;

        for(let i=0 ; i<files.length; i++)
        {
          console.log(files[i].type);
          let file_type = files[i].type.split('/');

          if(file_type[0]=='image')
            image_cnt++;
          else if(file_type[0]=='video')
            video_cnt++;

        }
        console.log(video_cnt);
        console.log(image_cnt);
          if((parseInt(added_image_cnt)+image_cnt) > 4 || (parseInt(added_video_cnt)+video_cnt) > 1){
             alert("You can upload only 1 video and 4 images.");
             $(this).val('');
            return false;
          }
        });

        $('.input-group-btn').click(function (event) {
            //if ($("#file-button-browse").hasClass('disabled')) {
             if(countIMG == 0)
            {
                   if( $(this).find("#file-button-browse:disabled").attr("disabled", true))
                  {
                      alert('To upload Images, you need to delete images from your profile');
                      window.location.href='<?php echo BASE_URL; ?>profile';
                  } 
                  else
                  {
                     console.log('11111');
                  }
            }

          });
    


    });
    $("#uploadimage").on('submit',(function(e) {
        e.preventDefault();
        $('#gallery_content').hide();
        $('#loader_gif').show();
      
        $.ajax({
          url: "<?php echo BASE_URL;?>api/User_Login/uploadMulipleImages", // Url to which the request is send
          type: "POST",             // Type of request to be send, called as method
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false
          success: function(data)   // A function to be called if request succeeds
          {
              console.log(data);
              if(data>4)
              {
                alert('Please Upload only Four Images');
                location.reload();
              }
              else
              {
                $('#gallery_content').show();
                $('#loader_gif').hide();
                location.reload();
              }
           
          }
        });
      }));
    // $(window).load(function(){
    //   $('#carousel').flexslider({
    //     animation: "slide",
    //     controlNav: false,
    //     animationLoop: true,
    //     slideshow: true,
    //     itemWidth: 130,
    //     prevText: "",
    //     nextText: "",
    //     itemMargin: 7,
    //     asNavFor: '#slider'
    //   });

    //   // $('#slider').flexslider({
    //   //   animation: "slide",
    //   //   controlNav: false,
    //   //   directionNav: false,
    //   //   animationLoop: true,
    //   //   slideshow: true,
    //   //   sync: "#carousel",
    //   //   start: function(slider){
    //   //   $('body').removeClass('loading');
    //   //   }
    //   // });
    // });
  </script>
  <script type="text/javascript">
// Fake file upload
document.getElementById('file-button-browse').addEventListener('click', function() {
  document.getElementById('files-input-upload').click();
});

document.getElementById('files-input-upload').addEventListener('change', function() {
  document.getElementById('file-input-name').value = this.value;
  
  document.getElementById('file-button-upload').removeAttribute('disabled');
});
</script>
  </body>
</html>