<?php $this->load->view('api/header');?>

    <div class="container">
      <div class="row">
        <div class="col-sm-12">
              <div class="second-back">
              <h1 class="title-col">Forgot Password</h1>
              <?php if($this->session->flashdata('web_flash')){ ?>
                  <div align="center" class="alert alert-danger alert-dismissible fade in" role="alert" id="message" style="width: 50%; margin-left: 25%; " >
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                  <?php  echo $this->session->flashdata('web_flash'); ?>
                  </div>
              <?php }?>
               <div class="general-area">
                       <form method="post" action="<?php echo BASE_URL;?>change-password" align="center">
                        <div class="row" style="padding-top: 50px">
                          

                          <div class="col-sm-6 col-sm-push-3 col-xs-12">
                            <div class="form-group text-left row">       
                            <div class="col-sm-6"><label>Email </label></div>
                            <div class="col-sm-6">                       
                                 <input type="text" class="contact_text" name="email" value="<?php if(isset($_GET['email'])){echo $_GET['email'];}?>" >
                               </div>
                            </div>
                             <div class="form-group text-left row"> 
                             <div class="col-sm-6"><label>New Password</label></div>
                            <div class="col-sm-6">                                 
                                <input type="password" class="contact_text" id="password" name="password">
                            </div>                 
                            </div>
                             <div class="form-group text-left row">   
                              <div class="col-sm-6"><label>Confirm Password </label></div>
                            <div class="col-sm-6">    
                                 <input type="password"  class="contact_text" id="password2" name="password2"> 
                            </div>
                          </div>

                          </div>     
                        </div>
                           
                            <div class="container" style="padding-top: 50px">
                                <input type="submit" value="Submit"  id="submit-form" class="logbt" style="text-align: center; margin: 0px auto;width:50%;"> 
                            </div>
                        </form>
              </div>
        </div>
    </div>
    
<div class="foot"></div>
<?php $this->load->view('api/footer');?>


