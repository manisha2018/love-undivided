<?php $this->load->view('api/header');?>
<div class="container">
      <div class="row">
          <div class="col-sm-12">
              <div class="second-back profile">
                <div class="left-part">

                  <?php 
                    if(count($getProfile) > 0)
                    {
                      $status = $getProfile[0]['status'];
                                        if($status == '1')
                                            $css  = 'blur1';
                                        else if($status == '2')
                                            $css  = 'blur2';
                                        else if($status == '3')
                                            $css  = 'blur3';
                                        else if($status == '' || $status == '0')
                                            $css  = 'blur1';
                                        else
                                            $css  = 'blur4';
                                        }
                                        else
                                        {
                                          $css ="";
                                        }
                                    ?>
                                        
                  <div class="pro-pic <?php echo $css ;?>"><img src="<?php echo USER_IMAGES.$getProfile[0]['user_image'];?>" alt=""/></div>
                    <h2><?php 
                   

                    echo $getProfile[0]['name']; ?></h2>
                   <!--- <div class="social">
                      <a href="#"><img src="<?php echo WEB_ASSETS;?>images/ico1.png" alt=""/></a>  
                        <a href="#"><img src="<?php echo WEB_ASSETS;?>images/ico2.png" alt=""/></a> 
                        <a href="#"><img src="<?php echo WEB_ASSETS;?>images/ico3.png" alt=""/></a>
                    </div>-->
                    <?php foreach($getProfile as $value){ ?>
                    <div class="row" style="padding: 30px 20px;">
                        <div class="col-md-12" style="text-align: left;">
                            <h3 class="basics-heading">Basics</h3><hr>
                            <h5 class="basics">RELIGION</h5>
                            <p class="basic_info"><?php if($value['religion_name']==''){echo "NA";}else{echo $value['religion_name'];}?></p>
                            <h5 class="basics">EDUCATION</h5>
                            <p class="basic_info"><?php if($value['education']=='0')
                                        {echo "Doctorate";}
                                      else if($value['education']=='1')
                                        {echo "Masters";}
                                      else if($value['education']=='2')
                                        {echo "Bachelors";}
                                      else if($value['education']=='3')
                                        {echo "Associates";}
                                      else if($value['education']=='4')
                                        {echo "Some College";}
                                      else if($value['education']=='5')
                                        {echo "High College";}
                                      else if($value['education']=='6')
                                        {echo "Did not Complete high School";}
                                      else{ echo " ";}?></p>
                             <h5 class="basics">SEX</h5>
                            <p class="basic_info"><?php if($value['gender']==0){ echo "Male";} else { echo "Female";}?></p>
                        </div>
                    </div>
                    
                    <?php } ?>
                </div>
                <div class="right-part">
                <form method="post" action="<?php echo BASE_URL;?>api/User_Login/update_user">
                    <div class="pro-box">
                        <h2>Personal Info</h2>
                        <?php foreach ($getProfile as $value){?>
                        <div class="left-box">
                            <div class="row">
                                <div class="col-md-4">
                                    <span><b>City -</b> </span>
                                </div>
                                <div class="col-md-6">
                                     <span class="edit_profile1" style="display: none" >
                                      <input type="text" class="form-control"  name="name" value="<?php echo $value['name'];?>" >
                                    </span>
                                     <span class="edit_profile2"><?php echo $value['city'];?></span>
                                </div>    
                            </div>
                             <div class="row">
                                <div class="col-md-4">
                                    <span><b>Age -</b> </span>
                                </div>
                                <div class="col-md-4">
                                     <span class="edit_profile1" style="display: none" >
                                        <input type="text" class="form-control"  name="dob" id="datepicker" value="<?php echo $value['dob'];?>" >
                                    </span>
                                    <span class="edit_profile2"><?php echo $value['age'];?></span>
                                </div>    
                             </div> 
                        </div>
                        <div class="left-box">
                            <div class="row">
                                <div class="col-md-4">
                                    <span><b>Ethnicity -</b> </span>
                                </div>
                                <div class="col-md-4">
                                      <span class="edit_profile1" style="display: none" >
                                        <input type="text" class="form-control"  name="ethnicity" value="<?php echo $value['ethnicity'];?>" >
                                    </span>
                                    <span class="edit_profile2"><?php if($value['religion_name']==''){echo "NA";}else{echo $value['ethnicity'];}?></span>
                                </div>    
                            </div>  
                            <div class="row">
                                <div class="col-md-4">
                                    <span><b>Height -</b> </span>
                                </div>
                                 <div class="col-md-4">
                                     <span class="edit_profile1" style="display: none" >
                                        <input type="text" class="form-control"  name="height" value="<?php echo $value['height'];?>" >
                                    </span>
                                     <span class="edit_profile2"><?php echo $value['height'];?></span>
                                </div>   
                            </div>
                        </div>
                     <?php } ?>
                     <div class="save">
                        <button>SAVE</button>
                    </div>
                </div>
                </form>
                    <div class="pro-box pro2">
                        <h2>About me</h2>
                        <div class="left-box">
                        <?php foreach ($getProfile as $key => $value) {?>
                            <span class="edit_caption1" style="display: none" >
                                        <textarea class="form-control" id="user_caption" maxlength="100"><?php echo $value['user_caption'];?></textarea>
                                    </span>
                                    <span class="edit_caption2"><?php echo $value['user_caption'];?></span>
                        <?php }?>
                        </div>
                    </div>
  <!-- Nav tabs --><div class="card">
                      <ul class="nav nav-tabs custom-tab" role="tablist">
                            <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><h2>Profile</h2><!-- <i class="fa fa-user" aria-hidden="true"></i> --></a></li>
                           
                            <li role="presentation"><a href="#photos" aria-controls="photos" role="tab" data-toggle="tab"><!-- <i class="fa fa-camera" aria-hidden="true"></i> --><h2>Photos</h2></a></li>
                      </ul>
 <!-- Tab panes -->
                      <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="profile">
                    <form method="post" action="<?php echo BASE_URL;?>api/User_Login/update_lifestyle">
                        <?php foreach ($getProfile as $key => $value) {?>
                        <div class="lifestyle-tabs">
                          <h3>Lifestyle&nbsp;</h3><hr>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="col-md-6">
                              <h5 class="basic-que">Do You Smoke <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                                <span class="edit_lifestyle1" style="display: none" >
                                  <input type="text" class="form-control"  name="smoke" value="<?php echo $value['smoke'];?>"  >
                                </span><span class="edit_lifestyle2">
                                <?php if($value['smoke']=='0')
                                {echo "Never";}
                                elseif($value['smoke']=='1')
                                  { echo "Socially";}
                                 elseif($value['smoke']=='2')
                                  { echo "Once a Week";}
                                 elseif($value['smoke']=='3')
                                  { echo "Few Times a Week";}
                                elseif($value['smoke']=='4')
                                  { echo "Daily";}
                                else
                                  { echo "NA";}


                                ?>
                                  
                                </span>
                            </div>
                            <div class="col-md-6">
                              <h5 class="basic-que">Do you do drugs <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                                <span class="edit_lifestyle1" style="display: none" >
                                 <select class="form-control" name="drug">
                                    <?php  
                                     if($value['drug']=='1') {?>
                                        <option value="1" selected="selected">Yes</option>
                                        <option value="2">No</option>
                                        <option value="3">Occasionally</option>
                                        <?php } else if($value['drug']=='2'){?>
                                        <option value="1" >Yes</option>
                                        <option value="2" selected="selected">No</option>
                                        <option value="3">Occasionally</option>
                                        <?php } else if($value['drug']=='3'){?>
                                        <option value="1" >Yes</option>
                                        <option value="2">No</option>
                                        <option value="3"  selected="selected">Occasionally</option>
                                        <?php } else {?>
                                        <option value="1">Yes</option>
                                        <option value="2">No</option>
                                        <option value="3">Occasionally</option>
                                        <?php }?>
                                </select>
                                </span><span class="edit_lifestyle2">
                                  <?php  if($value['drug']=='1')
                                        {echo "Yes";}
                                    else if($value['drug']=='2')
                                        {echo "No";}
                                    else if($value['drug']=='3')
                                        {echo "Occasionally";}
                                    else {echo "NA";}
                                    ?>
                                </span>
                            </div>
                            </div>
                        </div>
                         <div class="row">
                          <div class="col-md-12">
                            <div class="col-md-6">
                              <h5 class="basic-que">Do you have kids <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                                <span class="edit_lifestyle1" style="display: none" >
                                  <input type="text" class="form-control"  name="kids" value="<?php echo $value['kids'];?>"  >
                                </span><span class="edit_lifestyle2">
                                <?php  if($value['kids']=='0')
                                        {echo "I don’t have kids but I want kids some day.";}
                                    else if($value['kids']=='1')
                                        {echo "I have kids and I do want more kids";}
                                      else if($value['kids']=='2')
                                        {echo "I do not want kids";}
                                       else if($value['kids']=='3')
                                        {echo "Undecided";}
                                    else {echo "NA";}
                                    ?>
                                  
                                </span>
                            </div>
                            <div class="col-md-6">
                              <h5 class="basic-que">Do you consume alcohol <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                                <span class="edit_lifestyle1" style="display: none" >
                                 <select class="form-control" name="drink">
                                    <?php  
                                     if($value['drink']=='1') {?>
                                        <option value="1" selected="selected">Socially</option>
                                        <option value="2">No</option>
                                        <option value="3">Often</option>
                                        <?php } else if($value['drink']=='2'){?>
                                        <option value="1" >Socially</option>
                                        <option value="2" selected="selected">No</option>
                                        <option value="3">Often</option>
                                        <?php } else if($value['drink']=='3'){?>
                                        <option value="1" >Socially</option>
                                        <option value="2" >No</option>
                                        <option value="3" selected="selected">Often</option>
                                        <?php } else {?>
                                        <option value="1">Socially</option>
                                        <option value="2">No</option>
                                        <option value="3">Often</option>
                                        <?php }?>
                                </select>
                                </span><span class="edit_lifestyle2">
                                  <?php  if($value['drink']=='1')
                                        {echo "Socially";}
                                    else if($value['drink']=='2')
                                        {echo "No";}
                                      else if($value['drink']=='3')
                                        {echo "Often";}
                                    else {echo "NA";}
                                    ?>
                                </span>
                            </div>
                            </div>
                         </div>
                         <div class="row">
                          <div class="col-md-12">
                            <div class="col-md-6">
                              <h5 class="basic-que">Would you date someone with kids <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                                <span class="edit_lifestyle1" style="display: none" >
                                  <input type="text" class="form-control"  name="date_with_kids" value="<?php echo $value['date_with_kids'];?>"  >
                                </span><span class="edit_lifestyle2">
                                 <?php  if($value['date_with_kids']=='0')
                                        {echo "Yes";}
                                    else if($value['date_with_kids']=='1')
                                        {echo "No";}
                                    else {echo " NA ";}
                                    ?>
                                  
                                </span>
                            </div>
                            <div class="col-md-6">
                              <h5 class="basic-que">Do you have Pets <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                                <span class="edit_lifestyle1" style="display: none" >
                                 <select class="form-control" name="drink">
                                    <?php  
                                     if($value['pets']=='1') {?>
                                        <option value="1" selected="selected">Yes</option>
                                        <option value="2">No</option>
                                       <?php } else {?>
                                        <option value="1">Yes</option>
                                        <option value="2" selected="selected">No</option>
                                      
                                        <?php }?>
                                </select>
                                </span><span class="edit_lifestyle2">
                                  <?php  if($value['pets']=='1')
                                        {echo "Yes";}
                                       else {echo "No";}
                                    ?>
                                </span>
                            </div>
                            </div>
                         </div>
                        <?php }?>
                   
                    <div class="save3">
                        <button>SAVE</button>
                    </div>
                    </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="photos">
                          <div class="gallery">
                         <!--  <form id="uploadimage" method="post"  action="" method="post" enctype="multipart/form-data">
                            <div class="row">
                              <div class="col-md-6">
                                 <input type="file" class="upload" name="multi_images" id="multi_images"/>
                              </div>
                              <div class="col-md-6">
                                 <input type="submit" value="Upload" class="img_gallery" style="text-align: center; margin-left: 30%" /> 
                              </div>
                          </div>
                     
                          </form> -->
                           <span id="imageSuccess"></span>
                          
                            <div class="row ">
                              <?php foreach ($getImages as $key => $value) {?>
                              <div class="col-md-4">
                                <a href="<?php echo USER_IMAGES.$value['gallery_image']; ?>" data-fancybox="group"  >&nbsp;
                                <img src="<?php echo  USER_IMAGES.$value['gallery_image']; ?>"  alt=""  />
                                </a>
                              </div>
                              <?php  } ?>
                            </div>
                          </div>
                    </div>
                    </div>
                 </div>
                         
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="foot"></div>

<?php $this->load->view('api/footer');?>
<script src="<?php echo WEB_ASSETS;?>js/jquery.fancybox.js"></script>
<script type="text/javascript">
    $("[data-fancybox]").fancybox({ });
</script>
