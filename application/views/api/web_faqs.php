<?php require('home_header.php');?>
      <!-- Page Content-->
       <main class="page-content">
        <section class="bg-images-baner section-md-100 section-80">
          <div class="shell position-r"><a href="index.html"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/logo.png" alt="" width="400px"></a></div>
        </section>
        
        <section class="section-md-80 section-50 backrgound" style="padding: 40px 0px">
          <div class="shell text-left">
            <h1>FAQ's</h1>
             <div class="range range-md-reverse offset-top-60">

             <div class="container">    
             <div class="row">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <?php foreach($getFaqs as $value){ ?>
             <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="heading<?php echo $value['faq_id'];?>">
                  <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $value['faq_id'];?>" aria-expanded="true" aria-controls="collapse<?php echo $value['faq_id'];?>">
                          <i class="more-less glyphicon glyphicon-plus"></i>
                         <?php echo $value['question'];?>
                      </a>
                  </h4>
              </div>
              <div id="collapse<?php echo $value['faq_id'];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $value['faq_id'];?>" aria-expanded="true">
                  <div class="panel-body">
                       <?php echo $value['answer'];?>
                  </div>
              </div>
            </div>
            <?php } ?>
                  <!--  <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingtwo">
                          <h4 class="panel-title">
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
                                  <i class="more-less glyphicon glyphicon-minus"></i>
                                  Why political views?
                              </a>
                          </h4>
                      </div>
                      <div id="collapsetwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingtwo" aria-expanded="true">
                          <div class="panel-body">
                                Lets face it. Political opinion is a big topic right now and some of us do know and don’t exactly know how we feel about certain circulating topics. So we made it simple in our matching process. After all, it would really stink to invest all of your time into someone you are very interested in just to find out you don't agree with the same things.
                          </div>
                      </div>
                  </div> -->
              </div><!-- panel-group -->
          </div><!-- container -->           

             <div class="row text-right">
              <nav aria-label="Page navigation">
              <ul class="pagination">
              <!--<li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>-->
               <div class="pagination">
                               <?php foreach ($links as  $link) {
                                echo  $link;
                                    }?>
              </div>
              </ul>
              </nav>
            </div>
            </div>

            </div> 
          </div>
        </section>

      </main>
      <!-- Page Footer-->
     <?php require('home_footer.php');?>