<?php $this->load->view('api/header');?>
<style type="text/css">

  #profileimage{
    width: 100%;
    height: 100%;
  }
.fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
    width:180px; border-radius:10px; border:2px solid; font-size:22px; text-align:center; line-height:35px;  display:block; 
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
  
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.gallery img {
  border-radius: 5px;
  cursor: pointer;
  transition: .3s;
  position: relative;
  margin-right: 7px;
  float: left;
  display: block;
  height: 150px;
  margin-bottom: 20px;
}

.bar-main-container {
  margin: 10px auto;
  width: 100%;
  height: 50px;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
  font-family: sans-serif;
  font-weight: normal;
  font-size: 0.8em;
  color: #FFF;
}

.wrap { padding: 8px; }

.bar-percentage {
  float: left;
  background: rgba(0,0,0,0.13);
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
      padding: 9px 6px;
    width: 18%;
    height: 36px;
}

.bar-container {
  float: right;
  -webkit-border-radius: 10px;
  -moz-border-radius: 10px;
  border-radius: 10px;
  height: 10px;
  background: rgba(0,0,0,0.13);
  width: 78%;
  margin: 12px 0px;
  overflow: hidden;
}

.bar {
  float: left;
  background: #FFF;
  height: 100%;
  -webkit-border-radius: 10px 0px 0px 10px;
  -moz-border-radius: 10px 0px 0px 10px;
  border-radius: 10px 0px 0px 10px;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: alpha(opacity=100);
  -moz-opacity: 1;
  -khtml-opacity: 1;
  opacity: 1;
}

/* COLORS */
.azure   { background: #38B1CC; }
.emerald { background: #2CB299; }
.violet  { background: #8E5D9F; }
.yellow  { background: #EFC32F; }
.red     { background: #E44C41; }

.tab-pane
{
  min-height: 250px;
}
.custom-file-upload
{
  margin-bottom: 30px;
}
.custom-file-upload .btn-default
{
    color: #f44437 !important;
    background-color: transparent;
    border-color: #f45246 !important;
}
.custom-file-upload .btn-default:hover,.custom-file-upload .btn-default:focus
{
  background-color:#f45246 !important;
  color: #fff !important;
}
@media only screen and (max-width: 767px)  {
.gallery img{height:60px !important;}
}


</style>
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="second-back profile">
        <div class="left-part">
          <div class="pro-pic">
            <a href="#"  id="profileimage"><img src="<?php echo USER_IMAGES.$getProfileData[0]['user_image']; ?>" alt=""/></a>
                <a href="<?php echo BASE_URL;?>upload-pic" title="Edit Profile Picture" class="change"><img src="<?php echo WEB_ASSETS; ?>images/se1.png" alt=""/></a>
          </div>
          <h2 style="color: #f47575;"><?php echo $getProfileData[0]['name'];?></h2>
         
          <form  novalidate method="post" action="<?php echo BASE_URL;?>api/User_Login/update_basics">
          <?php foreach($getProfileData as $value){ ?>
          <div class="row" style="padding: 0px 20px;">
              <div class="col-md-12" style="text-align: left;">
                 <h3 class="basics-heading">Profile Complete</h3>
                   <div id="bar-5" class="bar-main-container red">
                <div class="wrap">
                  <div class="bar-percentage" data-percentage="<?php echo $complete_per;?>"></div>
                  <div class="bar-container">
                    <div class="bar"></div>
                  </div>
                </div>
              </div>
              <?php if(isset($cnt_que) && $cnt_que > 0) {  ?>
              <a href="<?php echo BASE_URL ;?>question-category"> Questions remaining : <?php if(isset($cnt_que)) { echo $cnt_que; } ?> </a>
              <?php
              } ?> 
                <h3 class="basics-heading">BASICS<span style="cursor: pointer;" title="Edit Basics" onclick="edit_basics(<?php echo $user_id;?>)">&nbsp;<img style="margin-top: -5px" src="<?php echo WEB_ASSETS; ?>images/edit.png" alt=""/></span></h3>
                <hr>
                <input type="hidden" class="form-control"  name="user_id" value="<?php echo $value['user_id'];?>" >
                  <h5 class="basics">RELIGION</h5>
                    <span class="edit_basic1" style="display: none" >
                                <select class="form-control" name="religion">
                                 <?php foreach ($religion  as $key => $value1) {?>
                                  <option value="<?php echo $value1['religion_id']; ?>"
                                   <?php if($getProfileData[0]['religion_id']==$value1['religion_id']){echo 'selected="selected"';} ?>>
                                  <?php echo $value1['religion_name'] ?></option>
                                
                                  <?php }?> 
                                </select>
                        </span>
                        <span class="basic_info edit_basic2">
                       <?php if($value['religion_name']==''){echo "NA";}else {echo $value['religion_name'];}  ?>
                        </span>
                   <!--  <p class="basic_info edit_basic2"><?php echo $value['religion'];?></p> -->
                  <h5 class="basics">ETHNICITY</h5>
                    <span class="edit_basic1" style="display: none" ><input type="text" class="form-control"  name="ethnicity" placeholder="Enter ethnicity" value="<?php echo $value['ethnicity'];?>" >
                    </span>
                    <p class="basic_info edit_basic2"><?php if($value['ethnicity']==''){echo "NA";}else{echo $value['ethnicity'];}?></p>
                  <h5 class="basics">EDUCATION</h5>
                   <span class="edit_basic1" style="display: none" >
                                 <select class="form-control" name="education">
                                    <?php  
                                     if($value['education']=='0') {?>
                                        <option value="0" selected="selected">Doctorate</option>
                                        <option value="1">Masters</option>
                                        <option value="2">Bachelors</option>
                                        <option value="3">Associates</option>
                                        <option value="4">Some College</option>
                                        <option value="5">High School</option>
                                        <option value="6">Did not Complete high School</option>
                                        <option value="7">Others</option>
                                        <?php } else if($value['education']=='1'){?>
                                        <option value="0" >Doctorate</option>
                                        <option value="1" selected="selected">Masters</option>
                                        <option value="2">Bachelors</option>
                                        <option value="3">Associates</option>
                                        <option value="4">Some College</option>
                                        <option value="5">High School</option>
                                        <option value="6">Did not Complete high School</option>
                                        <option value="7">Others</option>
                                        <?php } elseif($value['education']=='2'){?>
                                       <option value="0" >Doctorate</option>
                                        <option value="1">Masters</option>
                                        <option value="2" selected="selected">Bachelors</option>
                                        <option value="3">Associates</option>
                                        <option value="4">Some College</option>
                                        <option value="5">High School</option>
                                        <option value="6">Did not Complete high School</option>
                                         <option value="7">Others</option>
                                        <?php } elseif($value['education']=='3'){?>
                                       <option value="0" >Doctorate</option>
                                        <option value="1">Masters</option>
                                        <option value="2">Bachelors</option>
                                        <option value="3" selected="selected">Associates</option>
                                        <option value="4">Some College</option>
                                        <option value="5">High School</option>
                                        <option value="6">Did not Complete high School</option>
                                        <option value="7">Others</option>
                                        <?php } elseif($value['education']=='4') {?>
                                        <option value="0" >Doctorate</option>
                                        <option value="1">Masters</option>
                                        <option value="2">Bachelors</option>
                                        <option value="3">Associates</option>
                                        <option value="4" selected="selected">Some College</option>
                                        <option value="5">High School</option>
                                        <option value="6">Did not Complete high School</option>
                                         <option value="7">Others</option>
                                       <?php } elseif($value['education']=='5') {?>
                                        <option value="0" >Doctorate</option>
                                        <option value="1">Masters</option>
                                        <option value="2">Bachelors</option>
                                        <option value="3">Associates</option>
                                        <option value="4">Some College</option>
                                        <option value="5" selected="selected">High School</option>
                                        <option value="6">Did not Complete high School</option>
                                        <option value="7">Others</option>
                                        <?php } elseif($value['education']=='6') {?>
                                        <option value="0" >Doctorate</option>
                                        <option value="1">Masters</option>
                                        <option value="2">Bachelors</option>
                                        <option value="3">Associates</option>
                                        <option value="4">Some College</option>
                                        <option value="5" >High School</option>
                                        <option value="6" selected="selected">Did not Complete high School</option>
                                         <option value="7">Others</option>
                                         <?php } elseif($value['education']=='7') {?>
                                        <option value="0" >Doctorate</option>
                                        <option value="1">Masters</option>
                                        <option value="2">Bachelors</option>
                                        <option value="3">Associates</option>
                                        <option value="4">Some College</option>
                                        <option value="5" >High School</option>
                                        <option value="6" >Did not Complete high School</option>
                                         <option value="7" selected="selected">Others</option>
                                        <?php } else{?>
                                        <option value="0" >Doctorate</option>
                                        <option value="1">Masters</option>
                                        <option value="2">Bachelors</option>
                                        <option value="3">Associates</option>
                                        <option value="4">Some College</option>
                                        <option value="5" >High School</option>
                                        <option value="6" >Did not Complete high School</option>
                                        <option value="7">Others</option> 
                                        <?php }?>
                                       
                                </select>
                                </span>
                                <p class="basic_info edit_basic2"><span class="basic_info edit_basic2">
                                  <?php  if($value['education']=='0')
                                        {echo "Doctorate";}
                                      else if($value['education']=='1')
                                        {echo "Masters";}
                                      else if($value['education']=='2')
                                        {echo "Bachelors";}
                                      else if($value['education']=='3')
                                        {echo "Associates";}
                                      else if($value['education']=='4')
                                        {echo "Some College";}
                                      else if($value['education']=='5')
                                        {echo "High College";}
                                      else if($value['education']=='6')
                                        {echo "Did not Complete high School";}
                                      else if($value['education']=='7')
                                        {echo "Others";}
                                      else{ echo "NA";}
                                      
                                  
                                    ?>
                                </span></p>
                    <!-- <p class="basic_info edit_basic2"><?php echo $value['education'];?></p> -->
                  <h5 class="basics">HAIR COLOR</h5>
                    <span class="edit_basic1" style="display: none" >
                                 <select class="form-control" name="hair_color">
                                    <?php  
                                     if($value['hair_color']=='0') {?>
                                        <option value="0" selected="selected">Jet black</option>
                                        <option value="1">Off black</option>
                                        <option value="2">Dk Brown</option>
                                        <option value="3">Med Brown</option>
                                        <option value="4">Light Auburn</option>
                                        <option value="5">Med Auburn</option>
                                        <option value="6">Dark Auburn</option>
                                        <?php } else if($value['hair_color']=='1'){?>
                                       <option value="0" >Jet black</option>
                                        <option value="1" selected="selected">Off black</option>
                                        <option value="2">Dk Brown</option>
                                        <option value="3">Med Brown</option>
                                        <option value="4">Light Auburn</option>
                                        <option value="5">Med Auburn</option>
                                        <option value="6">Dark Auburn</option>
                                        <option value="6">Did not Complete high School</option>
                                        <?php } elseif($value['hair_color']=='2'){?>
                                       <option value="0" >Jet black</option>
                                        <option value="1">Off black</option>
                                        <option value="2" selected="selected">Dk Brown</option>
                                        <option value="3">Med Brown</option>
                                        <option value="4">Light Auburn</option>
                                        <option value="5">Med Auburn</option>
                                        <option value="6">Dark Auburn</option>
                                        <?php } elseif($value['hair_color']=='3'){?>
                                       <option value="0" >Jet black</option>
                                        <option value="1">Off black</option>
                                        <option value="2">Dk Brown</option>
                                        <option value="3" selected="selected">Med Brown</option>
                                        <option value="4">Light Auburn</option>
                                        <option value="5">Med Auburn</option>
                                        <option value="6">Dark Auburn</option>
                                        <?php } elseif($value['hair_color']=='4') {?>
                                        <option value="0" >Jet black</option>
                                        <option value="1">Off black</option>
                                        <option value="2">Dk Brown</option>
                                        <option value="3">Med Brown</option>
                                        <option value="4" selected="selected">Light Auburn</option>
                                        <option value="5">Med Auburn</option>
                                        <option value="6">Dark Auburn</option>
                                       <?php } elseif($value['hair_color']=='5') {?>
                                        <option value="0" >Jet black</option>
                                        <option value="1">Off black</option>
                                        <option value="2">Dk Brown</option>
                                        <option value="3">Med Brown</option>
                                        <option value="4">Light Auburn</option>
                                        <option value="5" selected="selected">Med Auburn</option>
                                        <option value="6">Dark Auburn</option>
                                        <?php } elseif($value['hair_color']=='6') {?>
                                        <option value="0" >Jet black</option>
                                        <option value="1">Off black</option>
                                        <option value="2">Dk Brown</option>
                                        <option value="3">Med Brown</option>
                                        <option value="4">Light Auburn</option>
                                        <option value="5">Med Auburn</option>
                                        <option value="6" selected="selected">Dark Auburn</option>
                                        <?php } else{?>
                                        <option value="0" >Jet black</option>
                                        <option value="1">Off black</option>
                                        <option value="2">Dk Brown</option>
                                        <option value="3">Med Brown</option>
                                        <option value="4">Light Auburn</option>
                                        <option value="5">Med Auburn</option>
                                        <option value="6" >Dark Auburn</option>
                                        <?php }?>
                                       
                                </select>
                                </span>
                                <p class="basic_info edit_basic2"><span class="basic_info edit_basic2">
                                  <?php  if($value['hair_color']=='0')
                                        {echo "Jet black";}
                                      else if($value['hair_color']=='1')
                                        {echo "Off black";}
                                      else if($value['hair_color']=='2')
                                        {echo "Dk Brown";}
                                      else if($value['hair_color']=='3')
                                        {echo "Med Brown";}
                                      else if($value['hair_color']=='4')
                                        {echo "Light Auburn";}
                                      else if($value['hair_color']=='5')
                                        {echo "Med Auburn";}
                                      else if($value['hair_color']=='6')
                                        {echo "Dark Auburn";}
                                      else{ echo "NA";}
                                      
                                  
                                    ?>
                                </span></p>
                   <!--  <p class="basic_info edit_basic2"><?php echo $value['hair_color'];?></p> -->
                  <h5 class="basics">EYE COLOR</h5>
                    <span class="edit_basic1" style="display: none" >
                                 <select class="form-control" name="eye_color">
                                    <?php  
                                     if($value['eye_color']=='0') {?>
                                        <option value="0" selected="selected">Black</option>
                                        <option value="1">Blue</option>
                                        <option value="2">Brown</option>
                                        <option value="3">Gray</option>
                                        <option value="4">Green</option>
                                        <option value="5">Hazel</option>
                                        <option value="6">Maroon</option>
                                        <option value="7">Pink</option>
                                         <option value="8">Multicolored</option>
                                        <?php } else if($value['eye_color']=='1'){?>
                                       <option value="0" >Black</option>
                                        <option value="1" selected="selected">Blue</option>
                                        <option value="2">Brown</option>
                                        <option value="3">Gray</option>
                                        <option value="4">Green</option>
                                        <option value="5">Hazel</option>
                                        <option value="6">Maroon</option>
                                        <option value="7">Pink</option>
                                         <option value="8">Multicolored</option>
                                        <?php } elseif($value['eye_color']=='2'){?>
                                       <option value="0" >Black</option>
                                        <option value="1">Blue</option>
                                        <option value="2" selected="selected">Brown</option>
                                        <option value="3">Gray</option>
                                        <option value="4">Green</option>
                                        <option value="5">Hazel</option>
                                        <option value="6">Maroon</option>
                                        <option value="7">Pink</option>
                                         <option value="8">Multicolored</option>
                                        <?php } elseif($value['eye_color']=='3'){?>
                                        <option value="0" >Black</option>
                                        <option value="1">Blue</option>
                                        <option value="2">Brown</option>
                                        <option value="3" selected="selected">Gray</option>
                                        <option value="4">Green</option>
                                        <option value="5">Hazel</option>
                                        <option value="6">Maroon</option>
                                        <option value="7">Pink</option>
                                         <option value="8">Multicolored</option>
                                        <?php } elseif($value['eye_color']=='4') {?>
                                        <option value="0" >Black</option>
                                        <option value="1">Blue</option>
                                        <option value="2">Brown</option>
                                        <option value="3">Gray</option>
                                        <option value="4" selected="selected">Green</option>
                                        <option value="5">Hazel</option>
                                        <option value="6">Maroon</option>
                                        <option value="7">Pink</option>
                                         <option value="8">Multicolored</option>
                                       <?php } elseif($value['eye_color']=='5') {?>
                                        <option value="0" >Black</option>
                                        <option value="1">Blue</option>
                                        <option value="2">Brown</option>
                                        <option value="3">Gray</option>
                                        <option value="4">Green</option>
                                        <option value="5" selected="selected">Hazel</option>
                                        <option value="6">Maroon</option>
                                        <option value="7">Pink</option>
                                         <option value="8">Multicolored</option>
                                        <?php } elseif($value['eye_color']=='6') {?>
                                        <option value="0" >Black</option>
                                        <option value="1">Blue</option>
                                        <option value="2">Brown</option>
                                        <option value="3">Gray</option>
                                        <option value="4">Green</option>
                                        <option value="5">Hazel</option>
                                        <option value="6" selected="selected">Maroon</option>
                                        <option value="7">Pink</option>
                                        <option value="8">Multicolored</option>
                                        <?php } elseif($value['eye_color']=='7') {?>
                                        <option value="0" >Black</option>
                                        <option value="1">Blue</option>
                                        <option value="2">Brown</option>
                                        <option value="3">Gray</option>
                                        <option value="4">Green</option>
                                        <option value="5">Hazel</option>
                                        <option value="6" >Maroon</option>
                                        <option value="7" selected="selected">Pink</option>
                                        <option value="8">Multicolored</option>
                                         <?php } elseif($value['eye_color']=='8') {?>
                                        <option value="0" >Black</option>
                                        <option value="1">Blue</option>
                                        <option value="2">Brown</option>
                                        <option value="3">Gray</option>
                                        <option value="4">Green</option>
                                        <option value="5">Hazel</option>
                                        <option value="6" >Maroon</option>
                                        <option value="7" >Pink</option>
                                        <option value="8" selected="selected">Multicolored</option>
                                        <?php } else{?>
                                        <option value="0" >Black</option>
                                        <option value="1">Blue</option>
                                        <option value="2">Brown</option>
                                        <option value="3">Gray</option>
                                        <option value="4">Green</option>
                                        <option value="5">Hazel</option>
                                        <option value="6">Maroon</option>
                                        <option value="7">Pink</option>
                                        <option value="8">Multicolored</option>
                                        <?php }?>
                                       
                                </select>
                                </span>
                                <p class="basic_info edit_basic2"><span class="basic_info edit_basic2">
                                  <?php  if($value['eye_color']=='0')
                                        {echo "Black";}
                                      else if($value['eye_color']=='1')
                                        {echo "Blue";}
                                      else if($value['eye_color']=='2')
                                        {echo "Brown";}
                                      else if($value['eye_color']=='3')
                                        {echo "Gray";}
                                      else if($value['eye_color']=='4')
                                        {echo "Green";}
                                      else if($value['eye_color']=='5')
                                        {echo "Hazel";}
                                      else if($value['eye_color']=='6')
                                        {echo "Maroon";}
                                       else if($value['eye_color']=='7')
                                        {echo "Pink";}
                                       else if($value['eye_color']=='8')
                                        {echo "Multicolored";}
                                      else{ echo "NA";}
                                      
                                  
                                    ?>
                                </span></p>
                   <!--  <p class="basic_info edit_basic2"><?php echo $value['eye_color'];?></p> -->
                    <div class="save2">
                      <button class="save-btn" >SAVE</button>
                      <a href="<?php echo BASE_URL;?>profile" class="save-btn"  style= "background: darkgray; color:black;" >CANCEL</a>
                     </div>
              </div>
          </div>
         <?php } ?>
         </form>
        </div>
        <div class="right-part">
         
          <form id="selfprofile-form" method="post" action="<?php echo BASE_URL;?>api/User_Login/update_user">
            <div class="pro-box">
              <h3>Personal Information <span style="cursor: pointer;" onclick="edit_user(<?php echo $user_id;?>)" title ="Edit Personal Profile"><img style="margin-top: -5px" src="<?php echo WEB_ASSETS; ?>images/edit.png" alt=""/></span></h3>
                <?php foreach ($getProfileData as $value){?>
                  <div class="left-box">
                    <div class="row">
                      <div class="col-md-12">
                        <span><b>Name </b> </span>
                         <span class="edit_profile1" style="display: none" >
                            <input type="hidden"  class="form-control"  name="user_id" value="<?php echo $value['user_id'];?>" >
                              <input type="text" maxlength="40" class="form-control" required="required" placeholder="Enter name" pattern="[a-zA-Z]+([\s][a-zA-Z]+)*" title="Enter only letters" name="name" value="<?php echo $value['name'];?>" >
                        </span>
                        <span class="edit_profile2"><?php echo $value['name'];?></span>
                      </div>
                       
                      </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span><b>Date of Birth </b> </span>
                        <span class="edit_profile1" style="display: none" >
                              <div class="input-group date" data-provide="datepicker" id="date1" data-date-start-date="-99y" data-date-end-date="-13y">
                              <input type="text" class="form-control" name="dob" id="dob" placeholder="Choose date of birth" value="<?php echo $value['dob'];?>" onchange="close_date();">
                              <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </div>
                          </div>
                        </span>
                        <span class="edit_profile2"><?php echo $value['dob'];?></span>
                      </div>   
                    </div> 
                    <div class="row">
                      <div class="col-md-12">
                        <span><b>City </b> </span>
                        <span class="edit_profile1" style="display: none" >
                               <!-- <input type="text" class="form-control"  name="city" value="<?php echo $value['city'];?>" > -->
                               <div id="pac-card"></div>
                          <input type="text" name="city"  required="required" class="form-control"  id="city" placeholder="Enter your City" value="<?php echo $value['city'];?>">
                          <div id ="map"></div>
                        </span>
                        <span class="edit_profile2"><?php echo $value['city'];?></span>
                      </div>
                          
                    </div> 
                    <div class="row">
                      <div class="col-md-12">
                        <span><b>Height </b> </span>
                         <span class="edit_profile1" style="display: none" >
                              <input type="text" placeholder="Enter height in cm e.g 150" class="form-control" required="required" onkeypress="return isNumberKey(event)" maxlength="3" name="height" id="height" value="<?php echo $value['height'];?>" >
                              <div id="result"></div>
                        </span>
                        <span class="edit_profile2"><?php if($value['height']==''){echo "NA";}else{echo $value['height'];}?></span>
                      </div>   
                    </div> 
                  </div>
                  <div class="left-box">
                    <div class="row">
                      <div class="col-md-12">
                        <span><b>Email Id </b> </span>
                         <span class="edit_profile1" style="display: none" >
                              <input type="text" class="form-control" readonly="readonly" name="email" value="<?php echo $value['email'];?>" >
                        </span>
                        <span class="edit_profile2 email1"><?php echo $value['email'];?></span>
                      </div>   
                    </div>  
                    <div class="row">
                      <div class="col-md-12">
                        <span><b>Gender </b> </span>
                        <span class="edit_profile1" style="display: none" >
                                <select class="form-control" name="gender"  required="required">
                                    <?php  
                                    if($value['gender']==0){ ?>
                                    <option value="0" selected="selected">Male</option>
                                    <option value="1">Female</option>
                                    <option value="2">Bisexual</option>
                                    <?php } else if($value['gender']==1){ ?> 
                                    <option value="0">Male</option>
                                    <option value="1" selected="selected">Female</option>
                                    <option value="2">Bisexual</option>
                                    <?php } else if($value['gender']==2){ ?>
                                    <option value="0">Male</option>
                                    <option value="1" >Female</option>
                                    <option value="2" selected="selected">Bisexual</option>
                                    <?php  } ?>
                                </select>
                        </span>
                        <span class="edit_profile2">
                        <?php if($value['gender']==0)
                                        {echo "Male";}
                                    else if($value['gender']==1)
                                        {echo "Female";} else if($value['gender']==2){ echo "Bisexual";}?>

                        </span>
                      </div>   
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                          <span><b>Mobile No  </b> </span>
                           <span class="edit_profile1" style="display: none" >
                                <input type="text" class="form-control" onkeypress="return isNumberKey(event)"  name="mob_no" placeholder="Enter Mobile Number" value="<?php echo $value['mob_no'];?>" required="required"  >
                          </span>
                          <span class="edit_profile2"><?php echo $value['mob_no'];?></span>
                      </div>   
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                          <span><b>Seeking </b> </span>
                          <span class="edit_profile1" style="display: none" >
                                <select class="form-control" name="seeking">
                                    <?php  
                                     if($value['seeking']=='1') {?>
                                        <option value="1" selected="selected">Male</option>
                                        <option value="2">Female</option>
                                        <option value="3">Bisexual</option>
                                        <?php } else if($value['seeking']=='2'){?>
                                        <option value="1" >Male</option>
                                        <option value="2" selected="selected">Female</option>
                                        <option value="3">Bisexual</option>
                                        <?php } else {?>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                        <option value="3" selected="selected">Bisexual</option>
                                        <?php }?>
                                </select>
                            </span>
                            <span class="edit_profile2">
                            <?php  if($value['seeking']=='1')
                                        {echo "Male";}
                                    else if($value['seeking']=='2')
                                        {echo "Female";}
                                    else {echo "Bisexual";}
                                    ?>
                            </span>
                    </div>
                     
                    </div>    
                  </div>
                     <?php } ?>
                     <div class="save">
                       <input class="save-btn" type="submit" value="SAVE">
                       <!-- <input class="save-btn" type="submit" style= "background: darkgray;" value="CANCEl"> -->
                       <a href="<?php echo BASE_URL;?>profile" class="save-btn"  style= "background: darkgray; color:black;" >CANCEL</a>
                    </div>
            </div>
          </form>
          <div class="pro-box pro2">
              <h3>About Me <span style="cursor: pointer;" title="Edit Caption"onclick="edit_caption(<?php echo $user_id;?>)"><img style="margin-top: -5px" src="<?php echo WEB_ASSETS; ?>images/edit.png" alt=""/></span></h3>
                <div class="left-box">
                  <?php foreach ($getProfileData as $key => $value) {?>
                  <span class="edit_caption1" style="display: none" >
                 <!--  <textarea class="form-control" id="user_caption" maxlength="100"><?php echo $value['user_caption'];?></textarea> -->
                 <textarea name="caption" required id="caption" rows="3" style="width: 100%;resize: none;" ><?php echo $value['user_caption'];?></textarea>
                    <div id="remainingChars" class="pull-right">Max limit is 100 words.</div>
                  </span><span class="edit_caption2"><?php if($value['user_caption']==''){echo "NA";}else {echo $value['user_caption'];}?></span>
                  <?php }?>
                  <div class="save1">
                    <button  class="save-btn" id="aboutMe" onclick="update_caption(<?php echo $user_id; ?>)">SAVE</button>
                    <a href="<?php echo BASE_URL;?>profile" class="save-btn"  style= "background: darkgray; color:black;" >CANCEL</a>

                  </div>
                </div>
          </div>
<!-- Nav tabs --><div class="card">
                      <ul class="nav nav-tabs custom-tab"  role="tablist">
                            <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><h2>Profile</h2><!-- i class="fa fa-user" aria-hidden="true"></i> --></a></li>
                           
                            <li role="presentation"><a href="#photos" aria-controls="photos" role="tab" data-toggle="tab"><!-- <i class="fa fa-camera" aria-hidden="true"></i> --><h2>Photos</h2></a></li>
                      </ul>
 <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="profile">
                    <form method="post" action="<?php echo BASE_URL;?>api/User_Login/update_lifestyle">
                        <?php foreach ($getProfileData as $key => $value) {?>
                        <div class="lifestyle-tabs">
                          <h3>Lifestyle&nbsp;<span style="cursor: pointer;" title="Edit Lifestyle"onclick="edit_lifestyle(<?php echo $user_id;?>)"><img style="margin-top: -5px" src="<?php echo WEB_ASSETS; ?>images/edit.png" alt=""/></span></h3><hr>
                        </div>
                        <div class="row">
                          
                            <div class="col-md-6">
                              <h5 class="basic-que">Do You Smoke <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                                 <span class="edit_lifestyle1" style="display: none" >
                                 <select class="form-control" name="smoke">
                                    <?php  
                                     if($value['smoke']=='0' || $value['smoke']== '' ) {?>
                                        <option value="0" selected="selected">Never</option>
                                        <option value="1">Socially</option>
                                        <option value="2">Once a Week</option>
                                        <option value="3">Few Times a Week</option>
                                         <option value="4">Daily</option>
                                        <?php } else if($value['smoke']=='1'){?>
                                        <option value="0">Never</option>
                                        <option value="1"  selected="selected">Socially</option>
                                        <option value="2">Once a Week</option>
                                        <option value="3">Few Times a Week</option>
                                        <option value="4">Daily</option> 
                                        <?php } elseif($value['smoke']=='2'){?>
                                        <option value="0">Never</option>
                                        <option value="1">Socially</option>
                                        <option value="2" selected="selected">Once a Week</option>
                                        <option value="3">Few Times a Week</option>
                                        <option value="4">Daily</option>
                                        <?php } elseif($value['smoke']=='3'){?>
                                        <option value="0">Never</option>
                                        <option value="1">Socially</option>
                                        <option value="2">Once a Week</option>
                                        <option value="3"  selected="selected">Few Times a Week</option>
                                        <option value="4">Daily</option>
                                        <?php } elseif($value['smoke']=='4') {?>
                                        <option value="0">Never</option>
                                        <option value="1">Socially</option>
                                        <option value="2">Once a Week</option>
                                        <option value="3">Few Times a Week</option>
                                        <option value="4" selected="selected">Daily</option>
                                        <?php } else{?>
                                        <option value="5"> </option>
                                        <?php }?>
                                       
                                </select>
                                </span><span class="edit_lifestyle2">
                                  <?php  if($value['smoke']=='0')
                                        {echo "Never";}
                                      else if($value['smoke']=='1')
                                        {echo "Socially";}
                                      else if($value['smoke']=='2')
                                        {echo "Once a Week";}
                                      else if($value['smoke']=='3')
                                        {echo "Few Times a Week";}
                                      else if($value['smoke']=='4')
                                        {echo "Daily";}
                                      else{ echo "NA";}
                                      
                                  
                                    ?>
                                </span>
                            </div>
                            <div class="col-md-6">
                              <h5 class="basic-que">Do you use drugs <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                                <span class="edit_lifestyle1" style="display: none" >
                                 <select class="form-control" name="drug">
                                    <?php  
                                     if($value['drug']=='1') {?>
                                        <option value="1" selected="selected">Yes</option>
                                        <option value="2">No</option>
                                        <option value="3">Occasionally</option>
                                        <?php } else if($value['drug']=='2'){?>
                                        <option value="1" >Yes</option>
                                        <option value="2" selected="selected">No</option>
                                        <option value="3">Occasionally</option>
                                         <?php } else if($value['drug']=='3'){?>
                                        <option value="1" >Yes</option>
                                        <option value="2">No</option>
                                        <option value="3" selected="selected">Occasionally</option>
                                        <?php } else {?>
                                        <option value="1">Yes</option>
                                        <option value="2">No</option>
                                        <option value="3">Occasionally</option>
                                        <?php }?>
                                </select>
                                </span><span class="edit_lifestyle2">
                                  <?php  if($value['drug']=='1')
                                        {echo "Yes";}
                                    else if($value['drug']=='2')
                                        {echo "No";}
                                      else if($value['drug']=='3')
                                        {echo "Occasionally";}
                                    else {echo "NA";}
                                    ?>
                                </span>
                            </div>
                            
                        </div>
                         <div class="row">
                          
                            <div class="col-md-6">
                              <h5 class="basic-que">Do you have kids <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                               <span class="edit_lifestyle1" style="display: none" >
                                 <select class="form-control" name="kids">
                                    <?php  
                                     if($value['kids']=='0') {?>
                                        <option value="0" selected="selected">I don’t have kids but I want kids some day.</option>
                                        <option value="1">I have kids and I do want more kids</option>
                                        <option value="2">I do not want kids</option>
                                        <option value="3">Undecided</option>
                                        <?php } elseif($value['kids']=='1'){?>
                                        <option value="0">I don’t have kids but I want kids some day.</option>
                                        <option value="1" selected="selected">I have kids and I do want more kids</option>
                                        <option value="2">I do not want kids</option>
                                        <option value="3">Undecided</option>
                                        <?php } elseif($value['kids']=='2'){?>
                                        <option value="0">I don’t have kids but I want kids some day.</option>
                                        <option value="1">I have kids and I do want more kids</option>
                                        <option value="2" selected="selected">I do not want kids</option>
                                        <option value="3">Undecided</option>
                                         <?php } elseif($value['kids']=='3'){?>
                                        <option value="0">I don’t have kids but I want kids some day.</option>
                                        <option value="1">I have kids and I do want more kids</option>
                                        <option value="2" >I do not want kids</option>
                                        <option value="3" selected="selected">Undecided</option>
                                        <?php } else{?>
                                        <option value="0">I don’t have kids but I want kids some day.</option>
                                        <option value="1">I have kids and I do want more kids</option>
                                        <option value="2" >I do not want kids</option>
                                        <option value="3" >Undecided</option>
                                        <?php } ?>
                                       
                                </select>
                                </span><span class="edit_lifestyle2">
                                  <?php  if($value['kids']=='0')
                                        {echo "I don’t have kids but I want kids some day.";}
                                    else if($value['kids']=='1')
                                        {echo "I have kids and I do want more kids";}
                                      else if($value['kids']=='2')
                                        {echo "I do not want kids";}
                                       else if($value['kids']=='3')
                                        {echo "Undecided";}
                                    else {echo "NA";}
                                    ?>
                                </span>
                            </div>
                            <div class="col-md-6">
                              <h5 class="basic-que">Do you consume alcohol <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                                <span class="edit_lifestyle1" style="display: none" >
                                 <select class="form-control" name="drink">
                                    <?php  
                                     if($value['drink']=='1') {?>
                                        <option value="1" selected="selected">Socially</option>
                                        <option value="2">No</option>
                                        <option value="3">Often</option>
                                        <?php } else if($value['drink']=='2'){?>
                                        <option value="1" >Socially</option>
                                        <option value="2" selected="selected">No</option>
                                        <option value="3">Often</option>
                                         <?php } else if($value['drink']=='3'){?>
                                        <option value="1" >Socially</option>
                                        <option value="2" >No</option>
                                        <option value="3" selected="selected">Often</option>
                                        <?php } else {?>
                                        <option value="1">Socially</option>
                                        <option value="2">No</option>
                                        <option value="3">Often</option>
                                        <?php }?>
                                </select>
                                </span><span class="edit_lifestyle2">
                                  <?php  if($value['drink']=='1')
                                        {echo "Socially";}
                                    else if($value['drink']=='2')
                                        {echo "No";}
                                    else if($value['drink']=='3')
                                        {echo "Often";}
                                    else {echo "NA";}
                                    ?>
                                </span>
                            </div>
                           
                         </div>
                         <div class="row">
                          
                            <div class="col-md-6">
                              <h5 class="basic-que">Would you date someone with kids <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                                 <span class="edit_lifestyle1" style="display: none" >
                                 <select class="form-control" name="date_with_kids">
                                    <?php  
                                     if($value['date_with_kids']=='0') {?>
                                        <option value="0" selected="selected">Yes</option>
                                        <option value="1">No</option> 
                                        <?php } else if($value['date_with_kids']=='1') {?>
                                        <option value="0" >Yes</option>
                                        <option value="1" selected="selected">No</option>
                                      
                                        <?php } else {?>
                                        <option value="0" >Yes</option>
                                        <option value="1" >No</option>
                                        <?php } ?>
                                        
                                        
                                </select>
                                </span><span class="edit_lifestyle2">
                                  <?php  if($value['date_with_kids']=='0')
                                        {echo "Yes";}
                                    else if($value['date_with_kids']=='1')
                                        {echo "No";}
                                    else {echo " NA ";}
                                    ?>
                                </span>
                            </div>
                            <div class="col-md-6">
                              <h5 class="basic-que">Do you have Pets <i class="fa fa-question-circle" aria-hidden="true"></i></h5>
                                <span class="edit_lifestyle1" style="display: none" >
                                 <select class="form-control" name="pets">
                                     <option value="1" <?php if($value['pets']== '1') { ?> selected <?php } ?>>Yes</option>
                                      <option value="2" <?php if($value['pets']== '2') { ?> selected <?php } ?>>No</option>
                                     
                                </select>
                                </span><span class="edit_lifestyle2">
                                  <?php  if($value['pets']=='1')
                                        {echo "Yes";}
                                       else if($value['pets']=='2')
                                        {echo "No";}
                                      else{ echo "NA";}
                                    ?>
                                </span>
                            </div>
                           
                         </div>
                        <?php }?>
                   
                    <div class="save3">
                        <button class="save-btn">SAVE</button>
                        <a href="<?php echo BASE_URL;?>profile" class="save-btn"  style= "background: darkgray; color:black;" >CANCEL</a>

                    </div>
                    </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="photos">
                          <div class="gallery">
                           <form id="uploadimage" method="post"  action="" method="post" enctype="multipart/form-data">

                            <div class="col-sm-12 custom-file-upload">
                            <div class="input-group">
                            <span class="input-group-btn">
                            <button id="file-button-browse" type="button" class="btn btn-default" " <?php $countImg=count($getImages);
                            if($countImg >= 4){echo "disabled";}?>>
                            Browse <span class="glyphicon glyphicon-file"></span>
                            </button>
                            </span>
                            <input type="file" id="files-input-upload" style="display:none;height: 57px;" name="multi_images" onchange="return fileValidation()" >
                            <input type="text" id="file-input-name" disabled="disabled" placeholder="File not selected" class="form-control" style="height: 57px;border:0px !important">
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-default" disabled="disabled" id="file-button-upload">
                            Upload <span class="glyphicon glyphicon-upload"></span>
                            </button>
                            </span>
                            </div>

                          </div>
                     
                          </form>

                            <div class="second-back text-center"  style="display:none;" id="loader_gif">
                              <img src="<?php echo WEB_ASSETS; ?>images/ajax-loader.gif" style="margin-top: 200px;">
                            </div>
                          
                            <div class="row uploaded-images">
                              <?php $countImg = count($getImages);
                              foreach ($getImages as $key => $value) {?>
                              <div class="col-sm-3 col-xs-4">
                              <center>
                             <?php  if($value['upload_type']=='0') {?>
                                <a href="<?php echo USER_IMAGES.$value['gallery_image']; ?>" data-fancybox="group"  >
                                
                                <div class="img-wrap">
                                <span class="close" onclick="deleteImage(<?php echo $value['gallery_id'] ?>);">&times;</span>
                                <img class="img-responsive" src="<?php echo  USER_IMAGES.$value['gallery_image']; ?>"  alt=""  />
                                </div>

                                  <?php } elseif ($value['upload_type']=='1') {?>
                                
                                <div class="img-wrap">
                                <span class="close" onclick="deleteImage(<?php echo $value['gallery_id'] ?>);">&times;</span>
                                  <video width="100%" controls>
                                    <source src="<?php echo USER_IMAGES.$value['gallery_image']?>" type="video/mp4">
                                  </video>
                                </div>

                                <?php }?>

                                
                                
                                </a>
                                </center>
                              </div>
                              <?php  } ?>
                            </div>
                          </div>
                    </div>
                    </div>
                 </div>
        </div>
   </div>
   </div>
   </div>
  <style type="text/css">
  .img-wrap {
    position: relative;
    display: inline-block;
   
    font-size: 0;
}
    .img-wrap .close {
    position: absolute;
    top: -12px;
    right: -12px;
    z-index: 100;
    background-color: #f44437;
    padding: 3px 2px 2px;
    color: red;
    font-weight: bold;
    cursor: pointer;
    opacity: .2;
    text-align: center;
    font-size: 38px;
    line-height: 14px;
    border-radius: 50%;
    height: 25px;
    width: 25px;
    color: #fff !important;
}

.img-wrap:hover .close {
    opacity: 1;
}

  </style>
<div class="foot"></div>
<?php $this->load->view('api/footer');?>
<script src="<?php echo WEB_ASSETS;?>js/jquery.fancybox.js"></script>
<script type="text/javascript">
function close_date()
    {
      $('.datepicker-dropdown').hide();
    }
    function edit_user(user_id) {

        $('.edit_profile1').show();
        $('.save').show();
        $('.edit_profile2').hide();
    }

    function edit_caption(user_id)
    {
        $('.edit_caption1').show();
        $('.save1').show();
        $('.edit_caption2').hide();
    }
    function update_caption(user_id)
    {
       
         var user_caption = $('#caption').val();
        $.ajax({
            type : 'post',
            url : "<?php echo BASE_URL;?>api/User_Login/update_caption_data",
            data :  {'user_id':user_id,
                      'user_caption':user_caption,
                      },
            success : function(response){
              console.log(response);

              if(response>0)
              {
                window.location.reload();
              }
              else
              {
                 window.location.reload();
              }
            }
          });
    }

    function edit_basics(user_id) {

        $('.edit_basic1').show();
        $('.save2').show();
        $('.edit_basic2').hide();
    }

    function edit_lifestyle(user_id)
    {
        $('.edit_lifestyle1').show();
        $('.save3').show();
        $('.edit_lifestyle2').hide();
    }


    // function deleteImage(img_id)
    // {
    //   //alert(img_id);
    //   if(confirm("Are you sure you want to delete this image??")==true)
    //     {
          
    //       $.ajax({
    //         url:'<?php echo BASE_URL?>api/User_Login/delete_images',
    //         datatype:'json',
    //         type:'POST',
    //         data: 'img_id='+img_id,
            
    //             success: function(response)
    //             {
    //              //alert('response'+response);
    //               if(response.trim() =='success')
    //               {
    //                 window.location.href='<?php echo BASE_URL?>profile';
    //                 //parent.slideUp('slow',function(){$this.remove();});
    //                 //$('#'+img_id).hide();
    //                // alert('Image Deleted Successfully');
    //               }else
    //               {
    //                 alert('error');
    //               }
                  
    //             },
    //             error:function(data){
    //               alert(data)
    //             }
    //         });
    //     } 
    //     // else
    //     // {
          
    //     // alert('else')
    //     // }
    //     return false;
        
    // }
     function deleteImage(img_id) {

      swal({
        title: "Are you sure?", 
        text: "Are you sure that you want to delete this media from gallery?", 
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Yes, delete it!",
        confirmButtonColor: "#ec6c62"
      }, function() {
        $.ajax(
        {
          type: "POST",
          url:"<?php echo BASE_URL;?>api/User_Login/delete_images",
          data:{'img_id':img_id},
          datatype:'json',
          success: function(data){
            //console.log(data);

            //$("#cat_"+category_id).remove();
          }
        }
        )
        .done(function(data) {

          swal("Deleted!", "Your media was deleted successfully!", "success");
            location.reload();
       
        })
        .error(function(data) {
          swal("Oops", "We couldn't connect to the server!", "error");
        });
      });
}

     
 
  document.getElementById('file-button-browse').addEventListener('click', function() {
    document.getElementById('files-input-upload').click();
  });

  document.getElementById('files-input-upload').addEventListener('change', function() {
    document.getElementById('file-input-name').value = this.value;
    
    document.getElementById('file-button-upload').removeAttribute('disabled');
  });


    
</script>

<script type="text/javascript">
   $(document).ready(function (e) {

        $('.bar-percentage[data-percentage]').each(function () {
          var progress = $(this);
          var percentage = Math.ceil($(this).attr('data-percentage'));
          $({countNum: 0}).animate({countNum: percentage}, {
            duration: 2000,
            easing:'linear',
            step: function() {
              // What todo on every count
            var pct = '';
            if(percentage == 0){
              pct = Math.floor(this.countNum) + '%';
            }else{
              pct = Math.floor(this.countNum+1) + '%';
            }
            progress.text(pct) && progress.siblings().children().css('width',pct);
            }
          });
        });

      $(function(){

       <?php if($countImg>0)
          {
            $remainImg = 4 - $countImg;
          }
          else
          {
            $remainImg = 4 ;

          }


       ?>
       var countIMG = <?php echo $remainImg; ?>;
       console.log(countIMG);
       //console.log(<?php echo $remainImg; ?>);

       // $('input#files-input-upload').change(function(){
       //   var files = $(this)[0].files;
       //    if(files.length >  <?php echo $remainImg; ?>){
       //       alert("You can only upload "+countIMG+" files");
       //       $("#file-button-upload").attr("disabled",true);
       //    }
       //  });

        // $('.input-group-btn').click(function (event) {
        //     //if ($("#file-button-browse").hasClass('disabled')) {
        //      if(countIMG == 0)
        //     {
        //            if( $(this).find("#file-button-browse:disabled").attr("disabled", true))
        //           {
        //               alert('Only 4 images you have to put in your gallery!');
        //               window.location.href='<?php echo BASE_URL; ?>profile';
        //           } 
        //           else
        //           {
        //              console.log('11111');
        //           }
        //     }

        //   });
    


    });




      $("#uploadimage").on('submit',(function(e) {
        e.preventDefault();
       // alert('hi');
       $('#loader_gif').show();
         // var uploadimage=$("#multi_images").get(0).files[0];       
        $.ajax({
                url:'<?php echo BASE_URL;?>api/User_Login/self_profile_gallery',
                type: "POST",            
                data: new FormData(this), 
                contentType: false,       
                cache: false,            
                processData:false,        
                success:function(result){
                  console.log(result);
                  if(result>4)
              {
                //alert('Please Upload only Four Images');
                swal({
                    title: "Error",
                    text: "Please Upload only Four Images",
                   // timer: 1500,
                    showConfirmButton: true,
                    type: 'error'
                });

                //location.reload();
              }
              else
              {
                  swal({
                    title: "success",
                    text: "Image Uploaded Successfully!",
                   // timer: 1500,
                    showConfirmButton: true,
                    type: 'success'
                });
             
                //location.reload();
                   $('#loader_gif').hide();
              }
                  // if(result>5)
                  // {
                  //   $('#imageSuccess').html("Image Uploaded Successfully!");
                  //   $('#imageSuccess').delay(2000).fadeOut('slow');
                  //   location.reload();
                  // }else
                  // {
                  //    $('#loader_gif').hide();
                  //    location.reload();
                  // }
                }
              });
      }));
      });

    function isNumberKey(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode;
      if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        } else {
            return true;
        }      
      }

       function fileValidation(){
    var fileInput = document.getElementById('files-input-upload');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.mov|\.avi|\.mp4|\.3gp)$/i;
    if(!allowedExtensions.exec(filePath)){
       swal({
              title: "Error",
              text: "Please upload file having extensions .jpeg/.jpg/.png/.gif only",
             // timer: 1500,
              showConfirmButton: true,
              type: 'error'
          });
       // alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    }else{
      return true;
    }
  }

</script>

<script type="text/javascript">
    $("[data-fancybox]").fancybox({ });
</script>
 <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBvC5FFDLnQCAM4RchvZ5z1_MNoYeQPBhA&libraries=places&callback=initMap" async defer></script>
<script type="text/javascript">
     function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 50.064192, lng: -130.605469},
          zoom: 3
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('city');
       

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions(
            {'country': ['us', 'pr', 'vi', 'gu', 'mp']});

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);
        });

       
      }
</script>  
  <script type='text/javascript'>
    var regex = /(\d{1,2}'\d{1,2}")|(\d{1,3}cm)/;
    
    function validate(){
        var height = document.getElementById('height').value;
        document.getElementById('result').innerHTML = regex.test(height) ? "Valid" : "Invalid"; 
    }
  </script>
