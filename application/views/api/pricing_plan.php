<?php $this->load->view('api/header');?>
            <div class="container">
                <?php if($this->session->flashdata('web_flash')){ ?>
                   <div class="alert alert-danger alert-dismissible fade in " role="alert" id="message">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <?php  echo $this->session->flashdata('web_flash'); ?>
                   </div>
                <?php }  ?>

                 <?php if($this->session->flashdata('web_flash_success')){ ?>
                   <div class="alert alert-info alert-dismissible fade in " role="alert" id="message">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <?php  echo $this->session->flashdata('web_flash_success'); ?>
                   </div>
                <?php }  ?>
                <div class="second-back">
                <h1 class="title-col"> Here’s sneak peek of your actual matches</h1>
                
                    <ul class="sneak">
                        <li><img src="<?php echo WEB_ASSETS;?>images/pro-pic.jpg" alt=""/></li>
                        <li><img src="<?php echo WEB_ASSETS;?>images/sn1.jpg" alt=""/></li>
                        <li><img src="<?php echo WEB_ASSETS;?>images/sn2.jpg" alt=""/></li>
                        
                    </ul>
                
                <h3 class="title2 title3"> Upgrade to see more matches</h3>

                    <form action="<?php echo BASE_URL;?>submit_plan" method="POST">
                        <div class="subc">
                            <div class="subc1 promocode-txt"> 
                                <span>Promotional Code</span> &nbsp;
                                <input type="text" name="promo" placeholder="Enter Code">&nbsp;<input type="submit" class="custom-btn" value="Go" style="display:inline-block;width: auto;height: 35px;line-height: 30px;">
                            </div>
                            <?php 
                            foreach ($getPlans as $key => $value) {?>
                            <div class="subc1">
                                    <ul>
                                        <li>
                                        <div class="que_radio">
                                           <label><input id="option"  type="radio" class="rad_option" name="plan" value="<?php echo $value['amount'];?>/<?php echo $value['plan_id'];?>">
                                         $<?php echo $value['price_per_month'];?> per month</label>
                                        </div>
                                        </li>
                                        <li class="pull-right"><?php echo $value['duration'];?></li>
                                        <!-- <li><a href="<?php echo BASE_URL;?>pay">Subscribe</a></li>  -->
                                    </ul>
                            </div>
                            <?php }?>
                        
                            <div class="i_agree1">
                          <div class="checkbox_holder1">
                          <input type="checkbox" name="is_unblur"  value="1" class="myCheckbox"><label>
                          <span class="terms">
                           <b>Want to skip the un-blurring process ? A onetime fee of .99 cents offers UNLIMITED unblurring of matches!!</b>
                          </span> 
                          </label>
                          </div>
                          </div>
                          
                            <input type="submit" class="custom-btn"  value="Subscribe" style="margin-bottom: 20px !important;">

                        </div>
                        <div class="clearfix"></div>
                       
                        <div class="container text-center">
                            <div class="subscribe col-sm-8 col-sm-push-2">
                               

                            </div> 

                        </div>  
                    </form>  
                </div>
            </div>
    
    <div class="foot"></div>

<?php $this->load->view('api/footer');?>