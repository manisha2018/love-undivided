<?php $this->load->view('api/header'); ?>

<link href="<?php echo WEB_ASSETS; ?>components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="<?php echo WEB_ASSETS; ?>css/jquery.awesome-cropper.css">
<style type="text/css">
.second-back
{
    padding-left: 20px;
    padding-right: 20px;
}
.upload-button {
    padding: 4px;
    border: 1px solid black;
    border-radius: 5px;
    display: block;
    float: left;
    margin-left: 160px;
}

.choose-img {
    max-width: 200px;
    max-height: 200px;
    display: block;
    margin-bottom: 20px;
}

.file-upload {
    display: none !important;
}
.select-wrapper {
  background: url(http://localhost/love-undivided/assets/frontend_assets/web_assets/images/chose-pic.jpg) no-repeat;
  background-size: cover;
  display: block;
  position: relative;
  width: 200px;
  height: 200px;
  border-radius: 50%;
  border:1px solid #aaa;
}
#image_src {
  width: 250px;
  height: 250px;
  margin-right: 100px;
  opacity: 0;
  filter: alpha(opacity=0); / IE 5-7 /
}

</style>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="second-back">
                <h1 class="title-col">Choose a profile photo</h1>
                <?php //echo $this->uri->segment(1); ?>
                 <div class="chose-pic">
              
              <!--   <span class="select-wrapper">
                    <input type="file" name="image_src" id="image_src" />
                </span>-->

                <img class="choose-img"  id="previewing"/>
                <form id="uploadimage" action="" method="post" enctype="multipart/form-data">

                <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                <input type="hidden" id="ques_img" name="ques_img"> 
                <input type="submit" value="Upload" class="custom-btn" style="text-align: center;"  />

                    <span id="imageSuccess"></span>
                    <span id="message"></span>
                      <a href="<?php echo BASE_URL;?>gallery" class="next-page" id="hideLink">Next</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="foot"></div>
    <?php $this->load->view('api/footer'); ?>
    <script src="<?php echo WEB_ASSETS; ?>components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
    <script src="<?php echo WEB_ASSETS; ?>js/jquery.awesome-cropper.js"></script> 
    <script type="text/javascript">
    $(document).ready(function (e) {


        
        $('#message').delay(2000).fadeOut('slow');
        $(document).ready(function () {
           $('#ques_img').awesomeCropper(
            { width: 150, height: 150, debug: true }
            );
        });
    

        $("#uploadimage").on('submit',(function(e) {
        e.preventDefault();
        var url = '<?php echo $this->uri->segment(1); ?>';
      
        $.ajax({
        url: "<?php echo BASE_URL;?>api/User_Login/image_upload", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        success: function(data)   // A function to be called if request succeeds
        {
            console.log(data);
            if(data>0)
            {
                //console.log($('.awesome-cropper img').attr('src')); 
                if(url=='upload-pic')
                {
                    $('#previewing').attr('src',$('.awesome-cropper img').attr('src'));
                    $('.awesome-cropper img').attr('src','');
                    $('#imageSuccess').html("Image Uploaded Successfully!");
                    window.location.href = '<?php echo BASE_URL ?>profile'; 
                    }else{
                    $('#previewing').attr('src',$('.awesome-cropper img').attr('src'));
                    $('.awesome-cropper img').attr('src','');
                    $('#imageSuccess').html("Image Uploaded Successfully!");
                    $('#hideLink').unbind('click', false);
                }
            }
       
        }
        });
        }));

        // Function to preview image after validation
        $(function() {
        $("#file").change(function() {
        $("#message").empty(); // To remove the previous error message
        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
        {
        //$('#previewing').attr('src','noimage.png');
        $("#message").html("Please Select A valid Image File");
        return false;
        }
        else
        {
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
        }
        });
        });
        function imageIsLoaded(e) {
        $("#file").css("color","green");
        $('#image_preview').css("display", "block");
        $('#previewing').attr('src', e.target.result);
        $('#previewing').attr('width', '250px');
        $('#previewing').attr('height', '230px');
        };
        });
</script>
<script type="text/javascript">
    $('#hideLink').bind('click', false);
</script>