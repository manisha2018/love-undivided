<?php $this->load->view('api/header'); ?>

<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #F8B9BD;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #f54336;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.general-area1{
  margin:0 auto; font-size:18px; text-align: center;
}
.general-area1  label{ display:inline-block; vertical-align:top; width:38%; font-weight:bold;}
</style>
<div class="container">
    	<div class="row">
        	<div class="col-sm-12">
            	<div class="second-back">
                    <h1 class="title-col">Account Settings</h1>
                    <ul class="first-menu">
                        <li><a href="<?php echo BASE_URL;?>general-settings">General</a></li>
                        <li><a href="<?php echo BASE_URL;?>billing-settings">Billing</a></li>
                        <li class="active"><a href="<?php echo BASE_URL;?>contact-settings">Notification</a></li>
                    </ul>
                    <!-- <h2 class="second-title">Contact Settings</h2> -->
                    <div class="general-area1">
                     
                          <div class="form-group">
                            <label class="basic-que">Email alert for new match?</label>
                            <label class="switch" style="width: 60px" >
                              <input type="checkbox" class="check_alert" id="check_match" <?php if(isset($arr_setts['1']) && $arr_setts['1'] == '1') { ?> checked="" <?php } ?> value="1">
                              <span class="slider round"></span>
                            </label>                 
                          </div>
                          <div class="form-group">
                            <label class="basic-que">Email alert if someone adds your profile to their favourites list? </label>
                            <label class="switch" style="width: 60px" >
                              <input type="checkbox" class="check_alert" id="check_fav" <?php if(isset($arr_setts['2']) && $arr_setts['2'] == '1') { ?> checked="" <?php } ?> value="2">
                              <span class="slider round"></span>
                            </label>                 
                          </div>
                          <div class="form-group">
                            <label class="basic-que">Email alert about upgrades and promotions?</label>
                            <label class="switch" style="width: 60px" >
                              <input type="checkbox" class="check_alert" id="check_promo" <?php if(isset($arr_setts['3']) && $arr_setts['3'] == '1') { ?> checked="" <?php } ?> value="3">
                              <span class="slider round"></span>
                            </label>                 
                          </div>
                         <!--  <div class="form-group">
                            <label class="basic-que">Should have option to change their payment and billing information?</label>
                            <label class="switch" style="width: 60px">
                              <input type="checkbox" checked id="check_bill">
                              <span class="slider round"></span>
                            </label>                 
                          </div> -->
                        </div>  
                    
                 
                </div>
            </div>
        </div>
    </div>
    
    <div class="foot"></div>
<?php $this->load->view('api/footer'); ?>
<script type="text/javascript">
  // /onclick="set_alert('1','#check_match')"
    // function set_alert(type,id)
    // {
    //   console.log(id);
    //   console.log($(id).is('checked'));
    // }

    var user_id = '<?php echo $this->session->userdata('session_data')['user_id'];?>';
    var status;
   

    //alert(status);
    $('.check_alert').change(function(){

      if($(this).prop('checked') == true)
        status = '1';
      else
        status = '0';
      console.log($(this).prop('checked'));
      $.ajax({
        url:'<?php echo BASE_URL?>api/User_Activity/change_alert',
        type:'POST',
       
        data:{user_id:user_id,alert_type:$(this).val(),status:status},
     
        success:function(result)
        {
          
        }
      });

    });
    $(function () {
        $("#submit-form").click(function () {
           
            var password = $("#password").val();
            var confirmPassword = $("#password2").val();
            if (password != confirmPassword) {
               $("#flash-messages_1").show();
                setTimeout(function() { $("#flash-messages_1").hide(); }, 5000);
                return false;
            }
            return true;
        });
    });


      $(document).ready(function(){
    $("#changepassword").submit(function(){
      //alert('change password');
      event.preventDefault();
      var formData = new FormData(this);
      var admin_id = $('#user_id').val();
      var oldpassword = $('#oldpassword').val();
      //var name = $('#name').val();
      //console.log("Hi");
       $.ajax({
                url:'<?php echo BASE_URL?>api/User_Activity/ChangePassword',
                type:'POST',
                datatype:'json',
                data:formData,
                cache:false,
              contentType: false,
              processData: false,
               success:function(result)
               {
                   console.log('result '+result);
                   if(result>0)
                   {
                      console.log('result'+result);
            $("#pass_change").show();
            setTimeout(function() { $("#pass_change").hide(); }, 4000);
            $( '#changepassword' ).each(function(){
                              this.reset();
                          });

                   }
                   else
                   {
                    $("#wrong_pass").show();
            setTimeout(function() { $("#wrong_pass").hide(); }, 4000);
            $( '#changepassword' ).each(function(){
                              this.reset();
                          });
                   }
               }
           });
    });
  });

</script>