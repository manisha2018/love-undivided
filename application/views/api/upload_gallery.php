  <?php $this->load->view('api/header');?> 
  <style type="text/css">
  .fileUpload {
      /*position: relative;
      overflow: hidden;
      margin: 10px;
      width:180px; height:45px; margin:10px auto; border-radius:10px; border:2px solid; font-size:22px; text-align:center; line-height:35px;  display:block; */ padding:0; margin:0; width:100%; height:auto; border:none; background-color:transparent;
  }
  .fileUpload input.upload {
      /*position: absolute;
      top: 0;
      right: 0;
      margin: 0;
      padding: 0;
      font-size: 20px;
      cursor: pointer;
      opacity: 0;
      filter: alpha(opacity=0);*/ padding:0; margin:0; width:calc(100% - 60px); height:100%; position:absolute; left:50%; top:50%; transform:translate(-50%,-50%); -webkit-transform:translate(-50%,-50%); opacity:0; z-index:2; cursor:pointer;
  }
  /*.fileUpload1 {
      position: relative;
      overflow: hidden;
      margin: 10px;
      left: 250%;
      width:180px; height:45px; margin:10px auto; border-radius:10px; border:2px solid; font-size:22px; 
      text-align:center; line-height:35px;  display:block; 
  }
  .fileUpload1 input.upload {
      position: absolute;
      top: 0;
      right: 0;
      margin: 0;
      padding: 0;
      font-size: 20px;
      cursor: pointer;
      opacity: 0;
      filter: alpha(opacity=0);
  }*/
    #nxt
    {
      position:absolute; bottom:15px; left:15px; font-size:24px; color:#ee4c4c;
    } 
    .custom-file-upload .btn-default
    {
      color: #f44437 !important;
      background-color: transparent;
      border-color: #f45246 !important;
    }
      .custom-file-upload .btn-default:hover,.custom-file-upload .btn-default:focus
      {
        background-color:#f45246 !important;
        color: #fff !important;
      }
  </style>
  <div class="gallery_fb">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
            <div class="second-back text-center"  style="display:none;" id="loader_gif">
              <img src="<?php echo WEB_ASSETS; ?>images/ajax-loader.gif" style="margin-top: 200px;"
               >
             </div>
            <div class="second-back" id="gallery_content">
              <h1 class="title-col">More pics means more communication</h1>
              <section class="slider">
              <div id="slider" class="flexslider">
                <ul class="slides">
                  <?php 
                    $countImg = count($images);
                    $countVideo = count($videos);
                  foreach ($getimages as $key => $value) {?>
                    <li>
                    <?php  if($value['upload_type']=='0'){?>
                      <img src="<?php echo USER_IMAGES.$value['gallery_image'];?>">
                    <?php } elseif($value['upload_type']=='1'){?>
                      <video width="100%" height="400" controls autoplay>
                      <span class="close" onclick="deleteImage(<?php echo $value['gallery_id'] ?>);">&times;</span>
                        <source src="<?php echo USER_IMAGES.$value['gallery_image']?>" type="video/mp4">
                      </video> <?php }?>
                    </li>
                  <?php  } ?>
                   
                </ul>
              </div>
              <div id="carousel" class="flexslider">
                  <ul class="slides">
                  <?php foreach ($getimages as $key => $value) {?>
                    <li>
                    <?php  if($value['upload_type']=='0'){?>
                    <span class="close" onclick="deleteImage(<?php echo $value['gallery_id'] ?>);">&times;</span>
                      <img src="<?php echo USER_IMAGES.$value['gallery_image'];?>">
                    <?php } elseif($value['upload_type']=='1'){?>
                    <span class="close" onclick="deleteImage(<?php echo $value['gallery_id'] ?>);">&times;</span>
                      <video width="90" height="90">
                        <source src="<?php echo USER_IMAGES.$value['gallery_image']?>" type="video/mp4">
                      </video> <?php }?>
                    </li>
                  <?php  } ?>
                  </ul>
                </div>
              </section>

              <form enctype="multipart/form-data"  id="uploadimage" method="post" action="<?php echo BASE_URL;?>api/User_Login/uploadMulipleImages" style="margin-bottom: 44px;">
                <div class="container">
                <div class="col-sm-8 col-sm-push-2 custom-file-upload">
                
                <br>
                <div class="input-group">
                <span class="input-group-btn">              
                <button id="file-button-browse" type="button" class="btn btn-default">

                Browse <span class="glyphicon glyphicon-file"></span>
                </button>
                </span>
                <input type="file" id="files-input-upload" style="display:none;height: 57px;" name="gallery_image[]" onchange="return fileValidation()" multiple>
                <input type="text" id="file-input-name" disabled="disabled" placeholder="File not selected" class="form-control" style="height: 57px;">
                <span class="input-group-btn">
                <button type="submit" class="btn btn-default" disabled="disabled" id="file-button-upload">
                <span class="glyphicon glyphicon-upload"></span>
                </button>
                </span>
                </div>
                </div>

                </div> 
              </form>
              <div class="col-md-12">
                <a href="<?php echo BASE_URL;?>caption" id="nxt">Skip</a>
                <a href="<?php echo BASE_URL;?>caption" class="next-page">Next</a> 
              </div>  
            </div>
          </div>
      </div>
    </div>
  </div>
      
  <div class="foot"></div>

  <?php $this->load->view('api/footer'); ?>
      <!-- FlexSlider -->
  <!-- <script defer src="<?php echo WEB_ASSETS; ?>js/jquery.flexslider.js"></script> -->


  <script type="text/javascript"> 
  var added_image_cnt = '<?php echo $countImg;?>';
var added_video_cnt = '<?php echo $countVideo;?>';
    $(function(){

       <?php if($countImg>0) 
          {
            $remainImg = 4 - $countImg;
          }
          else
          {
            $remainImg = 4 ;

          }


       ?>
       var countIMG = <?php echo $remainImg; ?>;
       //console.log(countIMG);
       //console.log(<?php echo $remainImg; ?>);

       $('input#files-input-upload').change(function(){
         var files = $(this)[0].files;
        // console.log(files[0]);

         var image_cnt = 0;
         var video_cnt = 0;

        for(let i=0 ; i<files.length; i++)
        {
         // console.log(files[i].type);
          let file_type = files[i].type.split('/');

          if(file_type[0]=='image')
            image_cnt++;
          else if(file_type[0]=='video')
            video_cnt++;

        }
       // console.log(video_cnt);
       // console.log(image_cnt);
          if((parseInt(added_image_cnt)+image_cnt) > 4 || (parseInt(added_video_cnt)+video_cnt) > 1){             
             //alert("You can upload only 1 video and 4 images.");
              swal({
                    title: "Error",
                    text: "You can upload only 1 video and 4 images!",
                   // timer: 1500,
                    showConfirmButton: true,
                    type: 'error'
                });

            $(this).val('');                        
            return false;
          }

          if(added_video_cnt==1 && added_image_cnt == 4)
          {
              $('#file-button-upload').prop('disabled', true);
          }else
          {
              $('#file-button-upload').prop('disabled', false);
          }
        });


        // $('.input-group-btn').click(function (event) {
        //     //if ($("#file-button-browse").hasClass('disabled')) {
        //      if(countIMG == 0)
        //     {
        //            if( $(this).find("#file-button-browse:disabled").attr("disabled", true))
        //           {
        //               alert('To upload Images, you need to delete images from your profile');
        //               window.location.href='<?php echo BASE_URL; ?>profile';
        //           } 
        //           else
        //           {
        //              console.log('11111');
        //           }
        //     }

        //   });
    


    });
      $("#uploadimage").on('submit',(function(e) {
          e.preventDefault();
          $('#gallery_content').hide();
          $('#loader_gif').show();
        
          $.ajax({
            url: "<?php echo BASE_URL;?>api/User_Login/uploadMulipleImages", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
                console.log(data);
              if(data>4)
              {
                //alert('Please Upload only Four Images');
                swal({
                    title: "Error",
                    text: "Please Upload only Four Images",
                   // timer: 1500,
                    showConfirmButton: true,
                    type: 'error'
                });

                location.reload();
              }
              else
              {
                $('#gallery_content').show();
             
                location.reload();
                   $('#loader_gif').hide();
              }
             
            }
          });
        }));
     
    </script>
    <script type="text/javascript">
  document.getElementById('file-button-browse').addEventListener('click', function() {
    document.getElementById('files-input-upload').click();
  });

  document.getElementById('files-input-upload').addEventListener('change', function() {
    document.getElementById('file-input-name').value = this.value;
    
    document.getElementById('file-button-upload').removeAttribute('disabled');
  });
  </script>
    <script type="text/javascript" src="<?php echo WEB_ASSETS; ?>js/slider-js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="<?php echo WEB_ASSETS; ?>js/slider-js/shCore.js"></script>
    <script type="text/javascript" src="<?php echo WEB_ASSETS; ?>js/slider-js/shBrushXml.js"></script>
    <script type="text/javascript" src="<?php echo WEB_ASSETS; ?>js/slider-js/shBrushJScript.js"></script>

    <!-- Optional FlexSlider Additions -->
    <script src="<?php echo WEB_ASSETS; ?>js/slider-js/jquery.easing.js"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/slider-js/jquery.mousewheel.js"></script> 
    <script src="<?php echo WEB_ASSETS; ?>js/slider-js/froogaloop.js"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/slider-js/jquery.fitvid.js"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/slider-js/modernizr.js"></script>

    <script type="text/javascript">    
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){

      // var player = document.getElementById('player_1');
      // $f(player).addEvent('ready', ready);

      // function addEvent(element, eventName, callback) {
      //   (element.addEventListener) ? element.addEventListener(eventName, callback, false) : element.attachEvent(eventName, callback, false);
      // }

      // function ready(player_id) {
      //   var froogaloop = $f(player_id);

      //   froogaloop.addEvent('play', function(data) {
      //     $('.flexslider').flexslider("pause");
      //   });

      //   froogaloop.addEvent('pause', function(data) {
      //     $('.flexslider').flexslider("play");
      //   });
      // }

      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: true,
        slideshow: false,
        itemWidth: 90,
        itemMargin: 8,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function(slider){
          $('body').removeClass('loading');
        }
        // before: function(slider){
        //     $f(player).api('pause');
        //   }
      });
    });
    function fileValidation(){
    var fileInput = document.getElementById('files-input-upload');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.mov|\.avi|\.mp4|\.3gp)$/i;
    if(!allowedExtensions.exec(filePath)){
       swal({
              title: "Error",
              text: "Please upload file having extensions .jpeg/.jpg/.png/.gif only",
             // timer: 1500,
              showConfirmButton: true,
              type: 'error'
          });
       // alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    }else{
      return true;
        //Image preview
        // if (fileInput.files && fileInput.files[0]) {
        //     var reader = new FileReader();
        //     reader.onload = function(e) {
        //         document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
        //     };
        //     reader.readAsDataURL(fileInput.files[0]);
        // }
    }
}


 // function deleteImage(img_id)
 //    {
 //      //alert(img_id);
 //      if(confirm("Are you sure you want to delete this image??")==true)
 //        {
          
 //          $.ajax({
 //            url:'<?php echo BASE_URL?>api/User_Login/delete_images',
 //            datatype:'json',
 //            type:'POST',
 //            data: 'img_id='+img_id,
            
 //                success: function(response)
 //                {
 //                 //alert('response'+response);
 //                  if(response.trim() =='success')
 //                  {
 //                    window.location.href='<?php echo BASE_URL?>gallery';
 //                    //parent.slideUp('slow',function(){$this.remove();});
 //                    //$('#'+img_id).hide();
 //                   // alert('Image Deleted Successfully');
 //                  }else
 //                  {
 //                    alert('error');
 //                  }
                  
 //                },
 //                error:function(data){
 //                  alert(data)
 //                }
 //            });
 //        } 
 //        // else
 //        // {
          
 //        // alert('else')
 //        // }
 //        return false;
        
 //    }

 function deleteImage(img_id) {

      swal({
        title: "Are you sure?", 
        text: "Are you sure that you want to delete this media from gallery?", 
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Yes, delete it!",
        confirmButtonColor: "#ec6c62"
      }, function() {
        $.ajax(
        {
          type: "POST",
          url:"<?php echo BASE_URL;?>api/User_Login/delete_images",
          data:{'img_id':img_id},
          datatype:'json',
          success: function(data){
            //console.log(data);

            //$("#cat_"+category_id).remove();
          }
        }
        )
        .done(function(data) {

          swal("Deleted!", "Your media was deleted successfully!", "success");
            location.reload();
       
        })
        .error(function(data) {
          swal("Oops", "We couldn't connect to the server!", "error");
        });
      });
}
    </script>

    </body>
  </html>