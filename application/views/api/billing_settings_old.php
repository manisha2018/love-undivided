<?php $this->load->view('api/header'); ?>
<style type="text/css">
/*.second-back
{
  min-height:520px;
}*/
  .upgrade {
  display: block;
    background-image: linear-gradient(-180deg, #EE9C9C 0%, #FF6A6A 100%);
    box-shadow: 0 1px 2px 0 rgba(0,0,0,0.10), inset 0 -1px 0 0 #FF6A6A;
    color: white;
    border-radius: 24px;
    border: 0;
    margin-top: 0px;
    font-size: 17px;
    font-weight: 500;
    width: 100%;
    height: 48px;
    /* line-height: 48px; */
    outline: none;
    z-index: 56;
    float: right;
    /*margin-right: 130px;*/
    padding: 12px 20px;
    cursor: pointer;
    text-align: center;
    font-family: "Raleway", sans-serif;
}

.upgrade:focus,.upgrade:hover {
  background: #FF6A6A;
  color: #fff;
}

.upgrade:active {
  background: #FF6A6A;
}
 @media only screen and (width:768px) {
  .custom-subc1
  {
    height: 60px !important;
  }
  .upgrade {
    margin-top: 0px;
    font-size: 17px;
    font-weight: 500;
    width: 100%;
    height: 48px;
    float: none;
    margin-right: 0px;
    text-align: center;
}
}
.dt-head{
  
}

@media only screen and (max-width: 767px)  {
  .custom-subc1
  {
    height: 180px !important;
  }
  .upgrade {
    margin-top: 20px;
    font-size: 17px;
    font-weight: 500;
    width: 100%;
    height: 48px;
    float: none;
    margin-right: 0px;
    text-align: center;
}
/*.upgrade {
    display: block;
    background-image: linear-gradient(-180deg, #EE9C9C 0%, #FF6A6A 100%);
    box-shadow: 0 1px 2px 0 rgba(0,0,0,0.10), inset 0 -1px 0 0 #FF6A6A;
    color: white;
    border-radius: 24px;
    border: 0;
    margin-top: 20px;
    font-size: 17px;
    text-align: center;
    font-weight: 500;
    width: 100% !important;
    height: 48px;
    line-height: 48px;
    outline: none;
    z-index: 56;
    float: none;
    margin-right: 0px;
    padding: 25px;
    cursor: pointer;
}*/
  }
</style>
<div class="container">
      <div class="row">
          <div class="col-sm-12">
              <div class="second-back">
                    <h1 class="title-col">Account Settings</h1>
                    <ul class="first-menu">
                        <li><a href="<?php echo BASE_URL;?>general-settings">General</a></li>
                        <li  class="active"><a href="<?php echo BASE_URL;?>billing-settings">Billing</a></li>
                        <li><a href="<?php echo BASE_URL;?>contact-settings">Notification</a></li>
                    </ul>
                    <!-- <h2 class="second-title">Billing</h2> -->
                    <div align="center" class="alert alert-danger alert-dismissible fade in" 
                        role="alert" id="flash-messages_1" style="display: none; width: 50%; margin-left:25%;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button><strong id="msg1">Your Password and Confirm Password Do not Match!</strong>
                    </div>
                     <div align="center" class="alert alert-danger alert-dismissible fade in" 
                         role="alert" id="pass_change" style="display: none; width: 50%; margin-left:25%;">
                         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button><strong id="msg2">Your Password is Changed!</strong>
                    </div>
                    <div align="center" class="alert alert-danger alert-dismissible fade in" 
                        role="alert" id="wrong_pass" style="display: none; width: 50%; margin-left:25%;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button><strong id="msg2">You Entered Wrong Old Password!</strong>
                    </div>
                    <input type="hidden" name="slug_name"   id="slug_name" required 
                                value="<?php echo $this->session->userdata('session_data')['slug_name']?>">
                    
                   <!-- <div class="subc">
                    <div class="subc1 promocode-txt"> 
                        <span>Promotional Code</span> &nbsp;
                        <input type="text" name="" placeholder="Enter Code">&nbsp;<input type="submit" value="Go" class="custom-btn" style="display:inline-block; width: auto;height: 35px;line-height: 30px;">
                    </div>
                    <?php foreach ($getPlans as $key => $value) {?>
                    <div class="subc1">
                          <ul>
                              <li>
                                <div class="que_radio">
                                  <input id="option" type="radio" class="rad_option" name="field" value="<?php echo $value['plan_id'];?>">
                                  <label for="option"><?php echo $value['amount'];?></label>
                                </div>
                                </li>
                                <li class="pull-right"><?php echo $value['duration'];?></li>
                               
                            </ul>
                    </div>
                    <?php }?>
                     <div class="subc1 custom-subc1" style="height: 60px"> 
                    <div class="col-md-7 col-sm-8 col-xs-12" style="line-height: 20px;padding-left:0px"><span style="opacity:1; font-size: 16px;">You have not upgraded your membership</span></div>
                    <div class="col-md-5 col-sm-4 col-xs-12"> <a href="<?php echo BASE_URL;?>pay" class="upgrade">Upgrade Now</a></div>

                     </div>
                </div> -->
                  <div class="col-sm-6 col-sm-push-3 col-md-4 col-md-push-4">
                    <?php
                    if(count($arr_user_plans)>0){ 
                      foreach ($arr_user_plans as $key => $value) {
                        
                    ?>

                        <div class="selected-pricing-plan">
                          <span class="dt-head">Current Activated Plan</span><br>
                          <span class="dt-txt">Activated on <?php echo date("F j, Y",strtotime($value['start_date']));?></span><br>
                          <span class="dt-txt">Deactivating on <?php echo date("F j, Y",strtotime($value['end_date']));?></span><br>
                          <span class="price-txt">$<?php echo $value['price_per_month'];?></span>
                          <span class="duration-txt">Duration of plan - <?php echo $value['duration'];?></span>
                        </div>
                   <?php
                      }
                    }
                    else if(count($arr_user_promo) > 0) {
                      foreach ($arr_user_promo as $key => $value) {
                        
                      ?>

                          <div class="selected-pricing-plan">
                            <span class="dt-head">Current Activated Promotional Plan</span><br>
                            <?php
                            if($value['is_free'] == '0') {?>
                            <span class="promo-txt"><?php echo $value['code']." Promocode is applied!"?></span><br><br/>

                              <span class="dt-txt">Activated on <?php echo date("F j, Y",strtotime($value['start_date']));?></span><br>
                              <span class="dt-txt">Deactivating on <?php echo date("F j, Y",strtotime($value['end_date']));?></span><br>
                               <span class="duration-txt"><?php echo $value['duration'];?> month duration.</span>

                            <?php
                            }else{?>
                              <span class="dt-txt">'Youraccountisonus' Promocode is applied.</span><br>
                               <span class="duration-txt">Unlimited Free</span>
                            <?php
                            }  ?>
                            
                           
                          </div>
                         <!--  <div style="margin-top: 10%;">
                            <a href="<?php echo BASE_URL;?>pricing-plan" class="upgrade"> Upgrade Now</a>
                          </div> -->
                          
                     <?php
                      }
                    }
                    ?>     
                   <center> <br>
                   <!--  <a href="#"><span class="fa fa-history"></span> <b>View previous transaction</b></a>    -->
                    <br><br>
                    </center>    
                    <?php
                    if(count($arr_user_plans)==0 || count($arr_user_promo) == 0 || 
                      ($arr_user_promo[0]['date_or_promoid'])=='') { ?>          
                      <a href="<?php echo BASE_URL;?>pricing-plan" class="upgrade"> Upgrade Now</a>
                    <?php 
                    }?>  
                  </div>
                  
            </div>
            
        </div>
    </div>
    
    <div class="foot"></div>
<?php $this->load->view('api/footer'); ?>
<script type="text/javascript">
    $(function () {
        $("#submit-form").click(function () {
           
            var password = $("#password").val();
            var confirmPassword = $("#password2").val();
            if (password != confirmPassword) {
               $("#flash-messages_1").show();
                setTimeout(function() { $("#flash-messages_1").hide(); }, 5000);
                return false;
            }
            return true;
        });
    });


      $(document).ready(function(){
    $("#changepassword").submit(function(){
      //alert('change password');
      event.preventDefault();
      var formData = new FormData(this);
      var admin_id = $('#user_id').val();
      var oldpassword = $('#oldpassword').val();
      //var name = $('#name').val();
      //console.log("Hi");
       $.ajax({
                url:'<?php echo BASE_URL?>api/User_Activity/ChangePassword',
                type:'POST',
                datatype:'json',
                data:formData,
                cache:false,
              contentType: false,
              processData: false,
               success:function(result)
               {
                   console.log('result '+result);
                   if(result>0)
                   {
                      console.log('result'+result);
            $("#pass_change").show();
            setTimeout(function() { $("#pass_change").hide(); }, 4000);
            $( '#changepassword' ).each(function(){
                              this.reset();
                          });

                   }
                   else
                   {
                    $("#wrong_pass").show();
            setTimeout(function() { $("#wrong_pass").hide(); }, 4000);
            $( '#changepassword' ).each(function(){
                              this.reset();
                          });
                   }
               }
           });
    });
  });

</script>