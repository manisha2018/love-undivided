<?php $this->load->view('api/header'); ?>
<style type="text/css">
</style>
<div class="container">
      <div class="row">
          <div class="col-sm-12">
              <div class="second-back">
                    <h1 class="title-col">Account Settings</h1>
                    <ul class="first-menu">
                        <li><a href="<?php echo BASE_URL;?>general-settings">General</a></li>
                        <li  class="active"><a href="<?php echo BASE_URL;?>billing-settings">Billing</a></li>
                        <li><a href="<?php echo BASE_URL;?>contact-settings">Contact</a></li>
                    </ul>
                   
                  <div class="container transaction-table table-responsive">
                      <table class="table table-bordered col-sm-8 col-sm-push-2 text-center">
                          <tr>
                            <th class="text-center">Sr.No.</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Duration</th>
                          </tr>
                          <tr>
                            <td>1</td>
                            <td>23 Sep 2017</td>
                            <td>$5.99</td>
                            <td>1 Month Plan</td>
                          </tr>
                           <tr>
                            <td>2</td>
                            <td>23 Sep 2017</td>
                            <td>$5.99</td>
                            <td>1 Month Plan</td>
                          </tr>
                           <tr>
                            <td>3</td>
                            <td>23 Sep 2017</td>
                            <td>$5.99</td>
                            <td>1 Month Plan</td>
                          </tr>
                          <tr>
                            <td>4</td>
                            <td>23 Sep 2017</td>
                            <td>$5.99</td>
                            <td>1 Month Plan</td>
                          </tr>
                          <tr>
                            <td>5</td>
                            <td>23 Sep 2017</td>
                            <td>$5.99</td>
                            <td>1 Month Plan</td>
                          </tr>
                          <tr>
                            <td>6</td>
                            <td>23 Sep 2017</td>
                            <td>$5.99</td>
                            <td>1 Month Plan</td>
                          </tr>
                      </table>
</div>
                        <div class="row">
          <center>
          <nav aria-label="Page navigation">
          <ul class="pagination">
          <!--<li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>-->
          <li class="active"><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
          </ul>
          </nav>
          </center>
          </div>
                  
                  
            </div>
            
        </div>
    </div>
    
    <div class="foot"></div>
<?php $this->load->view('api/footer'); ?>
