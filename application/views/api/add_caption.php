<?php $this->load->view('api/header'); ?>
<div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="second-back">
                <h1 class="title-col">About Me</h1>
                <div class="cap-area">
                <form action="<?php echo BASE_URL;?>api/User_Login/update_caption" method="post">

                    <div class="cap-img col-sm-3 col-xs-12">
                    <img class="center-block" src="<?php if(!empty($ProfilePic[0]['user_image'])) { echo USER_IMAGES.$ProfilePic[0]['user_image']; } else { echo WEB_ASSETS.'images/ma-ic.png'; }?>" alt=""/>
                    </div>
                    <div class="col-sm-9  col-xs-12">
                    <textarea name="caption" required="required" id="caption"></textarea>
                    <div id="remainingChars" class="pull-right">Max limit is 100 words.</div>
                    </div>
                 </div>
                 <div class="clear"></div>
                 <input type="submit" value="Next" id="captionNext" class="next-page"/>
                 </form>
                </div>
            </div>
        </div>
    </div>
    
    <div class="foot"></div>

<?php $this->load->view('api/footer'); ?>
<script type="text/javascript">
   
</script>