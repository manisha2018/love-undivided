<div class="row navbar-fixed-bottom text-center">
<div class="row" style="padding-right: 30px"><a href="#" class="pull-right footer-menu"><span class="fa fa-angle-double-up"></span> Menus </a></div>
<div class="custom-footer">
<div class="container text-center">
<ul class="nav nav-pills">
  <li role="presentation" class="active"><a href="<?php echo BASE_URL;?>">Home</a></li>
  <li role="presentation"><a href="<?php echo BASE_URL;?>About-us">About Us</a></li>
  <li role="presentation"><a href="<?php echo BASE_URL;?>Testimonials">Testimonials</a></li>
  <li role="presentation"><a href="<?php echo BASE_URL;?>Frequently-asked-questions">FAQ'S</a></li>
  <li role="presentation"><a href="<?php echo BASE_URL;?>Contact-us">Contact Us</a></li>
</ul>
</div>
</div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 <script src="<?php echo WEB_ASSETS; ?>js/jquery-1.12.4.js"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo WEB_ASSETS; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/jquery.validate.min.js"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/additional-methods.min.js"></script>
   
    <script src="<?php echo WEB_ASSETS; ?>js/jquery-ui.js"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/date.format.js"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/custom.js"></script>
    <!--  <script src="<?php echo WEB_ASSETS; ?>js/recaptcha_api.js?onload=onloadCallback&render=explicit" async defer></script> -->
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

    <script src="<?php echo WEB_ASSETS; ?>js/jquery.imgareaselect.js" type="text/javascript"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/jquery.form.js"></script>
    <script src="<?php echo WEB_ASSETS?>js/progress.js" type="text/javascript"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/moment.min.js"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/sweetalert.min.js"></script>
  <script type="text/javascript">
      $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        if(scroll>80)
        {
            $(".custom-header").addClass('custom-header-bg');
        }else
        {
            $(".custom-header").removeClass('custom-header-bg'); 
        }
        });
    </script>
<script type="text/javascript">
    $(".footer-menu").click(function(e){
        $(".custom-footer").slideToggle('500');
        e.preventDefault();
    });
</script>
  </body>
</html>