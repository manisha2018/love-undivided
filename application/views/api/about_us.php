<?php require('home_header.php');?> 
      <!-- Page Content-->
       <main class="page-content">
        <section class="bg-images-baner section-md-100 section-80">
          <div class="shell position-r"><a href="<?php echo BASE_URL;?>"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/logo.png" alt=""></a></div>
        </section>
        <section class="section-md-80 section-50 backrgound" style="padding: 40px 0px;">
          <div class="shell text-left">
            <h1>About LoveUndivided</h1>
            <div class="range range-md-reverse offset-top-28">
              <div class="cell-md-4"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/about-01.jpg" alt=""></div>
              <div class="cell-md-8 text-left">
                <!-- <h4>We are a dating group with an extensive store of experience in arranging successful relationships.</h4> 
                <p class="offset-top-10">With true love and long-lasting relationships in mind, we established our dating group to give assistance to people in search of the only love. We believe that true love finds itself, and we work to help you find your loved one.</p>
                <p class="offset-top-28">Most loners are too shy to approach the person they cherish warm feelings for. This is when a dating site comes in handy, as it allows for comfortable communication by exchanging letters or via live chats. The website of Sweet Date uses modern technologies to help our clients find the person they are looking for. With us you won't miss the chance to encounter your one and only.</p>-->
                <p class="offset-top-10">Another cool thing Love Undivided offers is the power of conversation. How many dating sites have you been on where you don't get past Hello?
                Love Undivided is different than any other dating site in that we momentarily blur out your matches profile picture! Why? It keeps things exciting while also bearing in mind privacy. Don't like the blurred out Picture?! Then keep messaging each other! After a set number of exchanged messages we slowly unblur your matches profile picture. This facilitates the power of conversation! Something that society is starting to lack since it seems to be easier to just send a text message to someone. Cool huh?.</p>
              </div>
            </div>
          </div>
        </section>
            <section class="section-md-80 section-50 backrgound" style="padding-top: 20px;">
          <div class="shell text-left">
            <h1 class="text-center text-sm-left">Mission Statement</h1>
            <div class="range range-md-reverse offset-top-28">
               <!-- <div class="cell-md-4"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/about-01.jpg" alt=""></div> -->
              <div class="cell-md-12 text-left">
                <!-- <h4>We are a dating group with an extensive store of experience in arranging successful relationships.</h4> 
                <p class="offset-top-10">With true love and long-lasting relationships in mind, we established our dating group to give assistance to people in search of the only love. We believe that true love finds itself, and we work to help you find your loved one.</p>
                <p class="offset-top-28">Most loners are too shy to approach the person they cherish warm feelings for. This is when a dating site comes in handy, as it allows for comfortable communication by exchanging letters or via live chats. The website of Sweet Date uses modern technologies to help our clients find the person they are looking for. With us you won't miss the chance to encounter your one and only.</p>-->
                <p class="offset-top-10">Our mission at LoveUndivided is to bring loving people together and NOT divide them!</br>
                  1. LoveUndivided offers users another really cool and different way to get matched with someone.</br>
                  2. LoveUndivided Is the ONLY dating website that offers this matching criteria</br>
                  3. LoveUndivided keeps it interesting with our awesome unblurring process. The more you message someone the faster their profile picture gets unblurred! If they aren’t your type, you can simply move on to your next match.</br>
                  4. LoveUndivided harnesses the POWER of Conversation!</br>
                  5. LoveUndivided gets rid of the shallow aspect of dating. Don’t judge a book by its cover! Especially in a day and age dominated by technology and texting. Looks matter to some and to some they don’t! Talk to someone before you judge them for a change!</br>
                  6. It is LoveUndivided’s upmost priority to match you with someone that will give you a lasting relationship.</br>
                  7. LoveUndivided lets you set a search distance to find your matches</br>
                  8. LoveUndivided matches you based on an array of Political topics</br>
                  9. Social media is unarguably blazing with political agenda. So let LoveUndivided match you with someone based on how closely and strongly you feel about todays hottest topics! But don’t worry! We also match you on unique compatibility questions.</br>
                  10. Bring Love together! NOT divide! Thats why, here is LoveUndivided.</br>
                  </p>
              </div>
            </div>
          </div>
        </section>
       <!--  <section class="section-bottom-90 section-md-top-80 section-md-top-0">
          <div class="shell">
            <div class="row row-no-gutter position-r">
              <div class="col-sm-6 pull-right col-md-6">
                <div class="block-2 con section-lg-top-140 inset-md-left-15">
                  <div class="baner-var-1 text-left">
                    <h1>Mission<br>Statement</h1>
                    <p class="offset-top-20">Browse our photo gallery to begin your acquaintance with our dating agency or use the services of our photographer to submit your photo.</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-sm-3 pull-right col-md-4">
                <div class="block-1 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/about-02.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 pull-right col-md-2 dn">
                <div class="block-0 emp"></div>
              </div>
              <div class="col-xs-6 col-sm-3 pull-right col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/about-03.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 pull-right col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/about-04.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 pull-right col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/about-05.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 pull-right col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/about-06.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 pull-right col-md-4 dn">
                <div class="block-0 emp"></div>
              </div>
              <div class="col-xs-6 col-sm-3 pull-right col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/about-07.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 pull-right col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/about-08.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 pull-right col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/about-09.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 pull-right col-md-2 dn">
                <div class="block-0 emp"></div>
              </div>
            </div>
          </div>
        </section> -->

        <section class="skew-1-1 section-bottom-77 position-r-1 section-top-80 section-md-top-0">
          <div class="shell offset-md-top--35 position-r">
            <h1 class="text-center text-sm-left">Top 100</h1>
            <div class="range offset-top-60 profiles position-r">
             <!--  <div class="cell-md-3 cell-sm-6"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/about-14.jpg" alt="" class="img-rounded">
                <h4 class="offset-top-16">Caroline Beek</h4>
                <div class="small text-italic">Age: 21, New York</div><a href="#" class="link offset-top-16">view info<span class="fl-budicons-launch-right164"></span></a>
              </div> -->
               <?php foreach ($added_profile as $key => $value) {?>
              <div class="cell-md-3 cell-sm-6">
               <?php   if($value['user_image']=='') {?>
                   <img src="<?php echo WEB_ASSETS;?>/images/ma-ic.png" alt="" class="img-rounded" style="height: 150px; width: 150px;"><?php }else{?>
                  <img src="<?php echo USER_IMAGES.$value['user_image']; ?>" alt="" class="img-rounded" style="height: 150px; width: 150px;"><?php } ?>
              <h4 class="offset-top-16"><?php echo $value['name'];?></h4>
                <div class="small text-italic">Age: <?php echo $value['age'];?>, <br><?php echo $value['city'];?></div><a href="<?php echo BASE_URL;?>featured-profile/<?php echo $value['slug_name'];?>" class="link offset-top-16">view info<span 
                  class="fl-budicons-launch-right164"></span></a>
                  </div>
              <?php } ?>
             
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footer-->
        <?php require('home_footer.php');?>