   <?php require('home_header.php');?>
      <!-- Page Content-->
      <main class="page-content">
        <section class="bg-images-baner section-md-100 section-80 backrgound">
          <div class="shell position-r"><a href="<?php echo BASE_URL;?>"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/logo.png" alt=""></a></div>
        </section>
        <section class="section-md-100 section-80" style="padding: 40px 0px">
          <?php if($this->session->flashdata('web_flash')){ ?>
               <div class="alert alert-danger alert-dismissible fade in" role="alert" id="message">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <?php  echo $this->session->flashdata('web_flash'); ?>
               </div>
          <?php }?>
          <div class="shell text-left">
            <h1>Contact Us</h1>
            <div class="range offset-top-30">
              <div class="cell-md-4">
                <div class="contact-info">
                  <h6>Email us with any questions, comments, or concern! Your voice counts!</h6>
                  <img src="<?php echo TEMPLATE_ASSETS; ?>/images/logo.png" alt="" style="width: 50%; height: 50%;">
                 <!--  <address>The Company Name Inc.<br>9863 - 9867 Mill Road,<br>Cambridge, MG09 99HT.</address> -->
                  <div class="list-phone">
                    <!-- <dl>
                      <dt>Freephone:</dt>
                      <dd><a href="callto:#">+1 800 559 6580</a></dd>
                    </dl> -->
                    <dl>
                      <dt>Phone Number:</dt>
                      <!-- <dd><a href="callto:#">424-341-2335</a></dd> -->
                      <dd>424-341-2335</dd>
                    </dl>
                    <!-- <dl>
                      <dt>FAX:</dt>
                      <dd><a href="callto:#">+1 800 889 9898</a></dd>
                    </dl> -->
                  </div>
                  <div class="mail">E-mail:<a href="mailto:#">Questions@LoveUndivided.com</a></div>
                </div>
              </div>
              <div class="cell-md-8">
                <!-- RD Mailform-->
                <form method="post"  id="cnt-form"  action="<?php echo BASE_URL;?>api/Contact_us/send_message" class="text-left contact-form">
                  <div class="form-group">
                    <label for="contact-name" class="form-label">Your Name</label>
                    <input id="contact-name" type="text" oninput="validateAlpha();" name="name" required="required" class="form-control">
                  </div>
                  <div class="range offset-md-top-16 offset-top-0">
                    <div class="cell-md-6">
                      <div class="form-group">
                        <label for="contact-email" class="form-label">Your Email</label>
                        <input id="contact-email" type="email" required="required" name="email" class="form-control">
                      </div>
                    </div>
                    <div class="cell-md-6 offset-top-0">
                      <div class="form-group">
                        <label for="contact-phone" class="form-label">Your Phone Number</label>
                        <input id="contact-phone" type="text" required="required" name="phone" class="form-control" onkeypress="return isNumberKey(event)">
                      </div>
                    </div>
                  </div>
                  <div class="form-group offset-md-top-37">
                    <label for="contact-message" class="form-label">Your Message (Enter 500 chracters only)</label>
                    <textarea id="contact-message" name="message" required="required" oninput="validateMessage();"  maxlength="500" class="form-control form-control-impressed"></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary text-uppercase offset-top-25">send message</button>
                </form>
              </div>
            </div>
          </div>
        </section>
        <section>
          
        </section>
      </main>
     
      <!-- Page Footer-->
        <?php require('home_footer.php');?>
     <script type="text/javascript">
      function validateAlpha(){
          var textInput = document.getElementById("contact-name").value;
          textInput = textInput.replace(/[^A-Za-z]+[\s]/g, "");
          document.getElementById("contact-name").value = textInput;
          }

      function validateMessage(){
          var textInput1 = document.getElementById("contact-message").value;
          //textInput1 = textInput1.replace(/[a-zA-Z0-9]+([\s][a-zA-Z0-9][^%$:-]+)*/, "");
          textInput1 = textInput1.replace(/[^A-Za-z0-9]+[\s]+[^%$&:,.-]/g, "");
          document.getElementById("contact-message").value = textInput1;
          }

      function isNumberKey(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode;
              if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                } else {
                    return true;
                }      
              }
     </script>