<?php $this->load->view('api/header'); ?>

<div class="container">
  <div class="row">
    <div class="col-sm-12">
    <?php if($this->session->flashdata('web_flash')){ ?>
          <div class="alert alert-danger alert-dismissible fade in" role="alert" id="message">
              <h5><b><a href="<?php echo BASE_URL;?>billing-settings">Upgrade Your Plan</a></b></h5>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <?php  echo $this->session->flashdata('web_flash'); ?>
               </div> 
          <?php }?>
      <div class="second-back">
       <!-- <ul class="first-menu first-menu2">
       
          <li class="<?php if($this->uri->segment(1)== 'activities'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>activities">Activities</a></li>
          <li class="<?php if($this->uri->segment(1)== 'matches'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>matches">Matches</a></li>
          <li class="<?php if($this->uri->segment(1)== 'chats'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>chats">Messages</a></li>
        </ul>-->

        <div class="update-area">
          <ul class="second-menu">
            <li><a href="<?php echo BASE_URL;?>activities" class="<?php if($this->uri->segment(1)== 'activities'){?> active_li <?php } ?>">All Updates</a></li>
            <li><a href="<?php echo BASE_URL;?>activities/visitors" class="<?php if($this->uri->segment(2)== 'visitors'){?> active_li <?php } ?>">Visitors</a></li>
            <li><a href="<?php echo BASE_URL;?>activities/profile-update" class="<?php if($this->uri->segment(2)== 'profile-update'){?> active_li <?php } ?>">Profile Updates</a></li>
            <li><a href="<?php echo BASE_URL;?>activities/photo-update" class="<?php if($this->uri->segment(2)== 'photo-update'){?> active_li <?php } ?>">Photo Updates </a></li>
          </ul>
          <div class="main-container">
            
            
          <ul class="list-area">
            <?php
            if(count($activities) > 0)
            {
            foreach ($activities as $key => $value) {
              $status = $value['status'];
              if($status == '1')
                  $css  = 'blur1';
              else if($status == '2')
                  $css  = 'blur2';
              else if($status == '3')
                  $css  = 'blur3';
              else if($status == '')
                  $css  = 'blur1';
              else
                  $css  = 'blur4';

              if($value['activity_type'] == '0') 
              {
                $msg = " Match Viewed Your Profile .";
                $for = " viewed you ";
              }
              else if($value['activity_type'] == '1') 
              {

                $msg = " Match Updated their Profile .";
                if($value['gender'] == '0')
                  $for = " updated his profile ";
                else
                  $for = " updated her profile ";
              }
              else if($value['activity_type'] == '2') 
              {

                $msg = " Match Updated their Photo .";
                if($value['gender'] == '0')
                  $for = " updated his Photo ";
                else
                  $for = " updated her Photo ";
              }

            ?>
              <li>
                <img src="<?php echo $value['user_image']; ?>" class="<?php echo $css;?>" alt=""/>
                <span>
                  <?php echo $msg;?>
                    <label><b><?php echo $value['name']; ?></b> <?php echo $for;?> <?php  if($value['cnt_activity']>1){  echo '('.$value['cnt_activity'].')'; }?></label>
                </span>
              </li>
            <?php
            }

          }else
          {
            ?>
            <li>
              <span>
                <label><b>No activities found . </b></label>
              </span>
            </li>
          <?php
          }

             ?>

          </ul>



          </div>
          
          <div class="row">
          <center>
          <nav aria-label="Page navigation">
          <ul class="pagination">
          <!--<li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>-->
          
          <?php
          for($i = 1 ;$i<= round(($activity_count/$limit)); $i++) { ?>

          <li class="<?php if($this->uri->segment(2) ==$i){ echo 'active'; } ?>"><a href="<?php echo BASE_URL;?>activities/<?php echo $i;?>"><?php echo $i;?></a></li>
          <!-- <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li> -->
          <?php 
          } 
          if(round(($activity_count/$limit)) > 0) {
          ?>
            <li><a href="<?php echo BASE_URL;?>activities/<?php echo round(($activity_count/$limit));?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li> 
            <?php
          }
          ?>
          </ul>
          </nav>
          </center>
          </div>
        </div>
            
      </div>
    </div>
  </div>
</div>

<div class="foot"></div>

<?php $this->load->view('api/footer'); ?>