<?php $this->load->view('api/header');?> 

    
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php if($this->session->flashdata('que_msg')){ ?>
                   <div class="alert alert-danger alert-dismissible fade in " role="alert" id="message">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <?php  echo $this->session->flashdata('que_msg'); ?>
                   </div>
                <?php }

                if($this->session->flashdata('status_update')) { ?>
                    <div class="alert alert-info alert-dismissible fade in " role="alert" id="message">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <?php  echo $this->session->flashdata('status_update'); ?>
                   </div>
                <?php 
                }?>   
                

                <div class="second-back">
               
                <div class="qadrive-butt issue-block">

                <ul class="nav nav-justified">
                <?php    
                  foreach ($arr_cat as $key => $value) {
                ?>   
                <li role="presentation"><a href="<?php echo BASE_URL;?>questions/<?php echo $value['category_id'];?>"><?php echo $value['category_name'];?>&nbsp;<span>(<?php if(isset($arr_cat_new[$value['category_id']])){ echo $arr_cat_new[$value['category_id']]['cat_cnt'] - $arr_cat_new[$value['category_id']]['ans_cnt']; }else{ echo $arr_cat_new[$value['category_id']]['ans_cnt']; } ?>)</span></a></li>
                <?php 
                } ?>

              

                </ul>


                        <!---<ul class="issues ">
                            <?php
                            foreach ($arr_cat as $key => $value) {
                            ?>   
                            <li><a href="<?php echo BASE_URL;?>questions/<?php echo $value['category_id'];?>"><?php echo $value['category_name'];?>&nbsp;(<?php if(isset($arr_cat_new[$value['category_id']])){ echo $arr_cat_new[$value['category_id']]['cat_cnt']; }else{ echo '0'; } ?>)</a></li>
                            <?php 
                            } ?>
                             </ul>--->
                            <!-- <li><a href="<?php echo BASE_URL;?>questions/2">Social Issues</a></li>
                            <li><a href="<?php echo BASE_URL;?>questions/3">Electoral Issues</a></li>
                            <li><a href="<?php echo BASE_URL;?>questions/4">Foreign Policy Issues</a></li>
                            <li><a href="<?php echo BASE_URL;?>questions/5">Healthcare & Environment</a></li>
                            <li><a href="<?php echo BASE_URL;?>questions/6">Economic & Domestic Issues</a></li>
                            <li><a href="<?php echo BASE_URL;?>questions/7">Dating Compatibility</a></li> -->
                       
                    </div>
 
                <div class="qadrive">
                    <div class="qadrive-in">
                    <div class="qadrive-pic">
                    <img src="<?php echo WEB_ASSETS; ?>images/qa.png" alt=""/>
                    </div>

                    <p>Kick start your profile so your matches can get to know you.</p>
                    
                     <a href="<?php echo BASE_URL;?>profile-pic" class="custom-btn">Share a bit about yourself</a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    </div>
    
    <div class="foot"></div>
    <script type="text/javascript">
       // window.onbeforeunload = function() { return "You work will be lost."; };
    </script>
 <?php $this->load->view('api/footer'); ?>