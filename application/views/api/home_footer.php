 <!-- Page Footer-->
      <footer class="page-foot"  style="padding: 50px 0px;background-color: #f1f1f1;">
        <div class="shell">
          <div class="range range-md-reverse">
            <div class="cell-md-4 position-r text-md-right">             
            </div>
            <div class="cell-md-4 position-r"><a href="<?php echo BASE_URL;?>"><img src="<?php echo TEMPLATE_ASSETS; ?>images/logo.png" alt=""></a></div>
            <div class="cell-md-4 text-md-left">
              <p style="margin-top: 20px">&#169;<span id="copyright-year"></span>	|	<a href="privacy.html">Privacy Policy</a>
              </p>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="<?php echo WEB_ASSETS; ?>js/jquery-1.12.4.js"></script>
    <script src="<?php echo WEB_ASSETS; ?>js/jquery.min.js"></script>
    <script src="<?php echo TEMPLATE_ASSETS; ?>/js/core.min.js"></script>
    <script src="<?php echo TEMPLATE_ASSETS; ?>/js/script.js"></script>
     <script src="<?php echo WEB_ASSETS; ?>js/jquery.validate.min.js"></script>
      <script src="<?php echo WEB_ASSETS; ?>js/custom.js"></script>
    <script type="text/javascript">
      function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
    </script>
  </body>
</html>