<?php $this->load->view('api/header');?>
<style type="text/css">
  <style type="text/css">
  {
  font-family: "Helvetica Neue", Helvetica, sans-serif;
  font-size: 15px;
  font-variant: normal;
  padding: 0;
  margin: 0;
}
form {
  width: 480px;
  margin: 20px auto;

}

label {
  position: relative;
  color: #6A7C94;
  font-weight: 400;
  height: 48px;
  line-height: 48px;
  margin-bottom: 10px;
  display: block;
}

label > span {
  float: left;
}
.field {
  background: white;
  box-sizing: border-box;
  font-weight: 400;
  border: 1px solid #CFD7DF;
  border-radius: 24px;
  color: #32315E;
  outline: none;
  height: 48px;
  line-height: 48px;
  padding: 0 20px;
  cursor: text;
  width: 76%;
  float: right;
}

.field::-webkit-input-placeholder { color: #CFD7DF; }
.field::-moz-placeholder { color: #CFD7DF; }
.field:-ms-input-placeholder { color: #CFD7DF; }

.field:focus,
.field.StripeElement--focus {
  border-color: #F99A52;
}
button {
  float: left;
  display: block;
  background-color:#f44a3d;
  box-shadow: 0 1px 2px 0 rgba(0,0,0,0.10), inset 0 -1px 0 0 #EE9C9C;
  color: white;
  border-radius: 24px;
  border: 0;
  margin-top: 20px;
  font-size: 17px;
  font-weight: 500;
  width: 100%;
  height: 48px;
  line-height: 48px;
  outline: none;
}

button:focus {
  background: #EE9C9C;
}

button:active {
  background: #E17422;
}

.outcome {
  float: left;
  width: 100%;
  padding-top: 8px;
  min-height: 20px;
  text-align: center;
}

.success, .error {
  display: none;
  font-size: 13px;
}

.success.visible, .error.visible {
  display: inline;
  margin-right: 18%;
  color: black;
  font-family: cursive;
  font-weight: bold;
  font-size: 20px;
}

.error {
  color: #EE9C9C;
}

.success {
  color: #F8B563;
}

.success .token {
  font-weight: 500;
  font-size: 13px;
}

</style>
    <div class="container">
      <div class="row">

          <div class="col-sm-12">
              <div class="second-back">
              <h1 class="title-col">Subscription</h1>
             <!--  <div class="col-sm-6">
                 <img src="<?php echo WEB_ASSETS;?>images/stripecard.jpg" alt="">
              </div>
 -->              
                <form class="payment-form">
                <label>
                  <span>Name</span>
                  <input name="cardholder-name" class="field" value="<?php if(isset($name)){ echo $name; }?>" placeholder="Enter Name" required="required" />
                </label>
                <label>
                  <span>Email</span>
    <!--               <input class="field"  type="email" id="email"  value="<?php if(isset($name)){ echo $email; }?>" required="required" placeholder="Enter Email"/> -->
                     <input class="field"  type="email" id="email"  value="<?php
                      if(isset($this->session->userdata('session_data')['email'])){ echo $this->session->userdata('session_data')['email']; }?>" readonly required="required" placeholder="Enter Email"/>
                </label>
                <label>
                  <span>Phone</span>
                  <input class="field"  type="text" required="required"  id="phone" placeholder="Enter Phone Number"/>
                </label>

                <label>
                  <span>Card</span>
                  <div id="card-element" class="field"></div>
                </label>
                <button type="submit">Pay <?php if(!empty($this->session->userdata('plan_amount'))) { echo '$ '.$this->session->userdata('plan_amount'); }?></button>
                <div class="outcome">
                  <div class="error" role="alert"></div>
                  <div class="success">
                   <span class="token"></span>
                  </div>
                </div>
                </form>
              
             </div>
        </div>
    </div>
    
    <div class="foot"></div>
<?php $this->load->view('api/footer');?>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
 var stripe = Stripe('<?php echo PUB_STRIPE_KEY;?>');
 var elements = stripe.elements();

var card = elements.create('card', {
  hidePostalCode: true,
  style: {
    base: {
      iconColor: '#F99A52',
      color: '#32315E',
      lineHeight: '48px',
      fontWeight: 400,
      fontFamily: '"Helvetica Neue", "Helvetica", sans-serif',
      fontSize: '15px',

      '::placeholder': {
        color: '#CFD7DF',
      }
    },
  }
});
card.mount('#card-element');

function setOutcome(result) {
  var successElement = document.querySelector('.success');
  var errorElement = document.querySelector('.error');
  successElement.classList.remove('visible');
  errorElement.classList.remove('visible');

  //console.log(result);
  if (result.token!=undefined && result.token.id!="") {
    // Use the token to create a charge or a customer
    // https://stripe.com/docs/charges
    successElement.querySelector('.token').textContent = result.token.id;
    let token_id = result.token.id;
    let email = $('#email').val();
    let phone = $('#phone').val();
     $.ajax({
        url: "<?php echo BASE_URL;?>api/Subscription", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: {'token_card':token_id,
          'email':email,
          'phone':phone,
          'plan_id':'<?php if(!empty($this->session->userdata('plan_id'))) { echo $this->session->userdata('plan_id'); }?>',
          'amount':'<?php if(!empty($this->session->userdata('plan_amount'))) { echo $this->session->userdata('plan_amount'); }?>',
          'is_unblur':'<?php if(!empty($this->session->userdata('is_unblur'))) { echo $this->session->userdata('is_unblur'); }?>'
        },
        dataType: "json",
        success: function(data)   // A function to be called if request succeeds
        {
          console.log('sendmail'+data);
        }
      });

      successElement.classList.add('visible');
           alert('Payment Done Successfully');
         //  window.location =  "<?php BASE_URL;?>dashboard";
         
   
  } else if (result.error) {
    errorElement.textContent = result.error.message;
    errorElement.classList.add('visible');
  }
}

card.on('change', function(event) {
  setOutcome(event);
});

document.querySelector('form').addEventListener('submit', function(e) {
  e.preventDefault();
  var form = document.querySelector('form');
  var extraDetails = {
    name: form.querySelector('input[name=cardholder-name]').value
   
  };
   if(confirm('u want to proceed?')==true)
   {
     stripe.createToken(card, extraDetails).then(setOutcome);
   }else{

      window.location =  "<?php BASE_URL;?>pay";
   }
  
});

</script>