<?php $this->load->view('api/header');?> 
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="second-back">

                <!--
                    <ul class="first-menu first-menu2">
                  
                        <li class="<?php if($this->uri->segment(1)== 'activities'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>activities">Activity</a></li>
                        <li class="<?php if($this->uri->segment(1)== 'matches'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>matches">Matches</a></li>
                        <li class="<?php if($this->uri->segment(1)== 'chats'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>chats">Messages</a></li>
                    </ul>
                -->
                <div class="match-one">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="match-left">
                                <a href="<?php echo BASE_URL?>matches" class="match_links <?php if($this->uri->segment(1)== 'matches'){?> match-button <?php } ?>">Matches</a>
                                <a href="#" class="match_links" onclick="func_filter_match($(this),'5');" >My Favorites</a>
                                <a href="#" class="match_links" onclick="func_filter_match($(this),'6');">Favorited Me</a>
                            </div>
                        </div>
                        <!-- <div class="col-sm-4">
                        <div class="match-left match-right">
                          <a href="#" class="link2"><span><img src="images/ma1.png" alt=""/></span>Match Preferences</a>                   
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="match-one match-two row" style="margin: 0px !important">
               <div class="col-sm-6 col-md-3">
                <label>View</label>
                <div class="matchin sell2">
                    <select name="filter_match" id="filter_match" onchange="func_filter_match(this,'1');">
                        <option value="1">All Macthes</option>
                        <option value="2">Messaged</option>
                        <option value="3">Not Messaged</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">                         
                <label>Age</label>
                <div class="matchin sell">
                    <select onchange="check_age(this,'1');" id="age1">
                        <?php
                        for ($i=18; $i < 99; $i++) { 
                            ?>    
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php                                         
                        } ?>


                    </select>
                </div>

                <div class="drow">
                    -
                </div>

                <div class="matchin sell">
                    <select onchange="check_age(this,'2');" id="age2"> 
                        <?php
                        for ($i=18; $i < 99; $i++) { 
                            ?>    
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php                                         
                        } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <label>Distance</label>
                <div class="matchin sell2">

                    <select name="filter_match" id="filter_match" onchange="func_filter_match(this,'4');">
                        <option value="">Any Distance</option>
                        <option value="50">upto 50 miles</option>
                        <option value="100">upto 100 miles</option>
                        <option value="200">upto 200 miles</option>

                    </select>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class=" match-right">                         
                    <div class="match-form">
                        <div class="matchin">
                            <input type="search" placeholder="Search by Name or City " onkeyup="func_filter_match(this,'3');">
                            <input type="submit" value=""/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="text-center"  style="display:none;" id="loader_gif">
          <img src="<?php echo WEB_ASSETS; ?>images/ajax-loader.gif" style="margin-top: 200px;"
          >
      </div>
      <ul class="matc-pro">
        <?php
        if(count($arr_matches) > 0) {
            foreach ($arr_matches as $key => $value) {
                $status = $value['status'];
                if($status == '1')
                    $css  = 'blur1';
                else if($status == '2')
                    $css  = 'blur2';
                else if($status == '3')
                    $css  = 'blur3';
                else if($status == '' || $status == '0')
                    $css  = 'blur1';
                else
                    $css  = 'blur4';
                ?>
                <li><center>
                    <a href="<?php echo BASE_URL;?>matched-profile/<?php echo $value['slug_name'];?>">
                        <div class="mateches-img">   
                            <img src="<?php echo $value['user_image'];?>" class="<?php echo $css;?>" alt="" />
                        </div>                            
                    </a>
                    <div class="favorite-section"><a href="#" onclick="add_fav('<?php echo $value['u2_user_id'];?>','<?php echo $value['email'];?>','<?php echo $value['name'];?>','<?php echo $value['fav_user_id'];?>');" class="add-fav" ><span id="add_fav_<?php echo $value['u2_user_id'];?>" class="fa <?php if($value['fav_user_id'] > 0){ echo'fa-heart'; } else { echo 'fa-heart-o'; } ?>"></span></a></div>
                    <span><?php echo $value['name'];?></span>
                </center>
            </li>
            <?php
        }
    } else { ?>
    <li>
        <h3>
            <?php
            if($cnt_que > 0) { ?>
            No Matches Yet..! Need to answer all questions. <a href=""> <b>Click Here </b></a>
            <?php
        }else { ?>
        No Matches Founds.
        <?php
    }   ?>
</h3>
</li>
<?php 
} ?>
                        <!-- <li>
                            <img src="images/ma-ic.png" alt=""/>
                            <span>John</span>
                        </li>
                        <li>
                            <img src="images/ma-ic.png" alt=""/>
                            <span>Daniel</span>
                        </li>
                        <li>
                            <img src="images/ma-ic.png" alt=""/>
                            <span>Joseph</span>
                        </li> -->
                    </ul>

                    <div class="row">
                      <center>
                          <nav aria-label="Page navigation">
                            <ul class="pagination">
                              <!--<li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>-->
                              <?php
                              for($i = 1 ;$i<= round(($matche_cnt/$limit)); $i++) { ?>

                              <li class="<?php if($this->uri->segment(2) ==$i){ echo 'active'; } ?>"><a href="<?php echo BASE_URL;?>matches/<?php echo $i;?>"><?php echo $i;?></a></li>
                                        <!-- <li><a href="#">2</a></li>
                                         <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li> -->
                                        <?php  } ?>
                                        <li><a href="<?php echo BASE_URL;?>matches/<?php echo round(($matche_cnt/$limit));?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li> 
                                    </ul>
                                </nav>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('api/footer'); ?>
        <script type="text/javascript">
            function add_fav(user_id,email,name,fav_user_id)
            {
                let msg = "";

                console.log($('#add_fav_'+user_id).attr('class'));
                if($('#add_fav_'+user_id).attr('class') == 'fa fa-heart')
                    msg = "remove from";
                else
                    msg = "add in";
                //add_favourite
                 swal({
                    title: "Are you sure?", 
                    text: "You want to "+msg+"  Favorites.?", 
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes!",
                    confirmButtonColor: "#ec6c62"
                }, function() {
                    $('#loader_gif').show();

                    $.ajax({
                        url: "<?php echo BASE_URL;?>api/User_matches/add_favourite",
                        method: "POST",
                        data:{fav_user_id:user_id,email:email,name:name},

                        success:function(data)
                        { 
                           //  $('#add_fav_'+user_id).toggleClass('fa-heart-o');
                          //$('#add_fav_'+user_id).toggleClass('fa-heart');
                          // console.log(data);
                        }
                    })
                    .done(function(data) {

                    $('#add_fav_'+user_id).toggleClass('fa-heart-o');
                    $('#add_fav_'+user_id).toggleClass('fa-heart');
                    $('#loader_gif').hide();

                swal("Success!", "User is "+msg+" your favorite list!", "success");
                     // location.reload();
                    })
               .error(function(data) {
                  swal("Oops", "We couldn't connect to the server!", "error");
              });
              });
            }

        // function add_fav(user_id,email,name,fav_user_id)
        // {
        //     let msg = "";

        //     console.log($('#add_fav_'+user_id).attr('class'));
        //     if($('#add_fav_'+user_id).attr('class') == 'fa fa-heart')
        //         msg = "remove";
        //     else
        //         msg = "add";
        //     //add_favourite
        //     if(confirm("Are you sure! You want to "+msg+" to Favorites."))
        //     {
        //         $.ajax({
        //             url: "<?php echo BASE_URL;?>api/User_matches/add_favourite",
        //             method: "POST",
        //             data:{fav_user_id:user_id,email:email,name:name},

        //             success:function(data)
        //             { 
        //                 //$('.matc-pro').html(html);

        //                 $('#add_fav_'+user_id).toggleClass('fa-heart-o');
        //                 $('#add_fav_'+user_id).toggleClass('fa-heart');




        //                 console.log(data);
        //             }
        //         });
        //     }
        //     else
        //     {
        //         return false;
        //     }

        // }

        function other_profile(argument) {
         alert('hii');
     }

     function check_age(obj_age,index)
     {
        let val = obj_age.value;
        let age_obj = {};
        if(index == '1')
        {

            if(val > $('#age2').val()) 
            {
                $('#age2').val(val)
                    //return false;
                }
                age_obj.age1 = $('#age1').val();
                age_obj.age2 = $('#age2').val();
                //alert('1');
                func_filter_match('','2',age_obj);


            }
            else if(index == '2')
            {
                if(val < $('#age1').val()) 
                {
                    $('#age1').val(val)
                    //return false;
                }


                age_obj.age1 = $('#age1').val();
                age_obj.age2 = $('#age2').val();
                func_filter_match('','2',age_obj);

            }


        }

        function func_filter_match(obj_filter,type,age_obj) {
            let val = obj_filter.value;
            let data = {};
            if(type == '1' || type == '3')
            {
                data = {data:val,type:type};
                console.log(data);
            }
            else if(type == '2')
            {
                data = {age1:age_obj.age1,age2:age_obj.age2,type:type};
            }
            else if(type == '4' || type == '5' || type == '6')
            {


                $('.match_links').removeClass('match-button');
                obj_filter.className = 'match-button';


                data = {data:val,type:type};
            }


            console.log(data);
            $.ajax({
                url: "<?php echo BASE_URL;?>api/User_matches/filter_matches",
                method: "POST",
                data:data,
                dataType: "html",
                success:function(html)
                { 
                    $('.matc-pro').html(html);
                    console.log(html);
                }
            });
            //alert(val);
        }
    </script>