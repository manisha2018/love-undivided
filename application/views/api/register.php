<?php $this->load->view('api/header'); ?>
  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBvC5FFDLnQCAM4RchvZ5z1_MNoYeQPBhA&libraries=places&callback=initMap" async defer></script>
<!--       <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
 -->
<style type="text/css">
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    height: 350px;
    overflow-y: auto;
}
  .city_error{
    color: red;
    font-size: 12px;
    font-weight: 600;
  }
.datepicker.datepicker-dropdown.dropdown-menu { z-index: 9999999 !important; maxDate:'0'; }
</style>
<script type="text/javascript">
     function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 50.064192, lng: -130.605469},
          zoom: 3
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('city');


        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);
        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions(
            {'country': ['us', 'pr', 'vi', 'gu', 'mp']});

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            
            //console.log('place'+place);
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            $('.city_error').show(); 
            $('#submit_form').attr('disabled',true);
            $('.city_error').html("No details available for input: '" + place.name + "'");
          //  return;
          }else{
            $('#submit_form').removeAttr('disabled');
            $('.city_error').hide();
          }
            
          // If the place has a geometry, then present it on a map.
          // if (place.geometry.viewport) {
          //   map.fitBounds(place.geometry.viewport);
          // } else {
          //   map.setCenter(place.geometry.location);
          //   map.setZoom(17);  // Why 17? Because it looks good.
          // }
          // marker.setPosition(place.geometry.location);
          // marker.setVisible(true);

          // var address = '';
          // if (place.address_components) {
          //   address = [
          //     (place.address_components[0] && place.address_components[0].short_name || ''),
          //     (place.address_components[1] && place.address_components[1].short_name || ''),
          //     (place.address_components[2] && place.address_components[2].short_name || '')
          //   ].join(' ');
          // }

          // infowindowContent.children['place-icon'].src = place.icon;
          // infowindowContent.children['place-name'].textContent = place.name;
          // infowindowContent.children['place-address'].textContent = address;
          //infowindow.open(map, marker);

        });

       
      }
</script>  
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-xs-12">
                <form id="register-form" novalidate action="<?php echo BASE_URL; ?>api/User_Login/UserRegister"  method="post">
                  <div class="register">
                  <h2>Registration</h3>
                  <?php if($this->session->flashdata('web_flash')){ ?>
                      <div class="alert alert-danger alert-dismissible fade in " role="alert" id="message"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                       <?php  echo $this->session->flashdata('web_flash'); ?>
                      </div>
                  <?php }?> 
                  <div class="form-area">
                      <div class="form-part">
                          <span><img src="<?php echo WEB_ASSETS; ?>images/user.png" alt=""/></span> <input type="text" name="name" pattern ="[a-zA-Z]+"  id="name"  placeholder="Enter your Name">
                      </div>
                      <div class="form-part">
                          <span><img src="<?php echo WEB_ASSETS; ?>images/envelope.png" alt=""/></span> <input type="text" name="email" id="email" placeholder="Enter your Email ID">
                      </div>
                      <div class="form-part">
                          <span><img src="<?php echo WEB_ASSETS; ?>images/lock.png" alt=""/></span> <input type="password" name="password" id="password"  placeholder="Create your Password">
                      </div>
                      <div class="form-part">
                          <span><img src="<?php echo WEB_ASSETS; ?>images/calender.png" alt=""/></span> 
                          <!-- <input type="text" name="dob" id=""  class="right" placeholder="Date of Birth"> -->
                          <div class="input-group date" data-provide="datepicker" id="date1" data-date-start-date="-99y" data-date-end-date="-13y">
                              <input type="text" class="form-control" name="dob" id="dob" placeholder="Choose date of birth" onchange="close_date();">
                              <div class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </div>
                          </div>
                          <!-- <div class='input-group date' id='datetimepicker6'>
                                <input type='text' id="depart" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div> -->
                      </div>
                      <div class="row">
                      <div class="col-sm-6">
                      <div class="form-part">
                          <span><img src="<?php echo WEB_ASSETS; ?>images/fa6.png" alt=""/></span>
                          <div id="pac-card">
                          <input type="text" name="city" id="city" placeholder="Enter your City">
                          <span class="city_error" style="display: none;"></span>
                          <div id ="map"></div>
                          </div>
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-part">
                          <span><img src="<?php echo WEB_ASSETS; ?>images/telephone.png" alt=""/></span> <!-- <input type="text" name="mob_no" id="mob_no" onkeypress="return isNumberKey(event)" placeholder="Enter your Mobile Number"> -->
                          <input type="text" name="mob_no" id="mob_no"  placeholder="Enter your Mobile Number" pattern="^(?:\+?\d{1,3}\s*-?)?\(?(?:\d{3})?\)?[- ]?\d{3}[- ]?\d{4}$">
                          <!-- patterns allowed for phone no
                          +1-541-754-3010
                          754-3010
                          (541) 754-3010
                           +1-541-754-3010
                          1-541-754-3010
                          001-541-754-3010
                          191 541 754 3010 -->
                      </div>
                      </div>
                      </div>

                      <div class="row">
                      <div class="col-sm-6">
                      <div class="form-part">
                           <span><img src="<?php echo WEB_ASSETS; ?>images/fa5.png" alt=""/></span>
                                  <select id="soflow-color1" name="gender">
                                          <option value="">Select Gender</option>
                                          <option value="0">Male</option>
                                          <option value="1">Female</option>
                                          <option value="2">Bisexual</option>
                                  </select>   
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-part pull-right">
                      <span><img src="<?php echo WEB_ASSETS; ?>images/search.png" alt=""/></span>
                               <select id="soflow-color" name="seeking" id="seeking">
                                  <option value="">Select a Seeking Option</option>
                                  <option value="1"> Male</option>
                                  <option value="2">Female</option>
                                  <option value="3">Bisexual</option>
                              </select>  
                      </div>  
                      </div>
                      </div>

                      <div class="form-part">
                        <div id="review_recaptcha"></div>
                      </div>
                      <div class="form-part">
                        <div class="i_agree1">
                          <div class="checkbox_holder1">
                          <input type="checkbox" name="planned_checked" class="myCheckbox"  id="planned_checked" value="1"  style="cursor: pointer;"><label>
                          <span class="terms">
                            <i>
                              I agree to the
                            </i>
                          </span> 
                          </label>
                          </div>
                          <b><a href="#" data-toggle="modal" data-target="#myModal">Terms of Service and Privacy Policy</a></b>
                          </div>
                      </div>
                      <div class="form-part">
                        <input type="submit" value="Submit" name="submit" id="submit_form" disabled="disabled"  class="custom-btn"/>
                      </div>
                      </div>
                  
                  </div>
                 
                  </div>
                
                </form>
            </div>
        </div>
    </div>
    

    <div class="foot"></div>

 <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Questions/edit_que_data" enctype="multipart/form-data" name="myForm"/>
              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title" style="color:#ff5252"><b>Terms of Service And Privacy Policy </b></h5>
                  </div>
                  <div class="modal-body" style="padding-left: 30px;padding-right: 30px;">
                  	<div class="col-sm-12">
                       <div class="form-group">
                        <h5 style="color:#666"><span class="fa fa-angle-double-right"></span> Terms of Service</h5><br>
                        <p style="font-size: 14px;line-height:27px"><?php echo $terms[0]['description'];?></p>
                      
                        <br>
                        <h5 style="border-top:1px solid #ccc; padding: 10px 0px; color:#666"><span class="fa fa-angle-double-right"></span> Privacy Policy</h5>
                        <p style="font-size: 14px;line-height:27px"><?php echo $terms[1]['description'];?></p>
                       </div>
                   </div>
                  </div>
                  <div class="modal-footer">
                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        
                        </div>
                    </div>
                  </div>
              </div>
          </form> 
      </div>
    </div>
<?php $this->load->view('api/footer'); ?>
<!--<script src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="<?php echo WEB_ASSETS;?>js/jquery.geocomplete.js"></script>
 <script type="text/javascript">
$("city").geocomplete();  // Option 1: Call on element.
$.fn.geocomplete("city"); // Option 2: Pass element as argument.
</script>

 -->
<script type="text/javascript">

//   $('.datepicker').datepicker({
//     startDate: '-3d',
//     orientation: 'bottom'
// });
  
    var review_recaptcha_widget;
    var onloadCallback = function() {
      if($('#review_recaptcha').length) {
          review_recaptcha_widget = grecaptcha.render('review_recaptcha', {
            'sitekey' : '6LdgzBMUAAAAAFvNPpSt1ssgVLcZ4XlYvWWu9SZD'
          });
      }
    };      


    function close_date()
    {
      $('.datepicker-dropdown').hide();
    }
  
    function isNumberKey(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode;
      if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        } else {
            return true;
        }      
      }

  //   $('input[type="checkbox"]').on('change', function(e){
  //    if(e.target.checked){
  //      $('#myModal').modal();
  //    }
  // });


</script>