<?php $this->load->view('api/header'); ?>


    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="second-back">
                    <h2 class="almost">Almost there!
                    <span>Love Undivided’s main goal is to bring together love seekers, NOT divide.</span>
                    </h2>
                    <h3 class="title2">HOW  WE  MATCH</h3>
                 <ul class="process">
                    <li>
                        <img src="<?php echo WEB_ASSETS;?>images/pic1.jpg" alt=" " />
                        <span>Step 1</span>
                        <p>Love Undivided takes today’s hottest issues and dumbs them down to an understandable level.</p>
                    </li>
                    
                    <li>
                        <img src="<?php echo WEB_ASSETS;?>images/pic2.jpg" alt=" " />
                        <span>Step 2</span>
                        <p>We make everything easy to understand and depending on how each  question's answer should closely match.</p>
                    </li>
                    
                    <li>
                        <img src="<?php echo WEB_ASSETS;?>images/pic3.jpg" alt=" " />
                        <span>Step 3</span>
                        <p>Love Undivided matches you based on compatibility as well.Leave the awkward politics to us so that we can ensure your first date is memorable one!
                        </p>
                    </li>
                 </ul>
                 <p class="text-right"><a href="<?php echo BASE_URL;?>pricing-plan" id="how_match" class="next-page1" style=" display: block;    margin-right: 15px;margin-bottom: 15px;">Skip</a></p>
                </div>
                
            </div>
        </div>
    </div>
    <div class="foot"></div>
<?php $this->load->view('api/footer'); ?>