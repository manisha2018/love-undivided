<?php $this->load->view('api/header');?> 

<style type="text/css">
    
    .div-scroll {
        height: 400px;
        overflow: scroll;
        padding-top: 10px;
    }
  
</style>
<div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="second-back profile chat chat2">
                <!--
                    <ul class="first-menu first-menu2">                  
                        <li class="<?php if($this->uri->segment(1)== 'activities'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>activities">Activity</a></li>
                        <li class="<?php if($this->uri->segment(1)== 'matches'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>matches">Matches</a></li>
                        <li class="<?php if($this->uri->segment(1)== 'chats'){?> active <?php } ?>"><a href="<?php echo BASE_URL;?>chats">Messages</a></li>
                    </ul>
                   --> 
                <div class="right-part">
                    
                    <div class="title-part">
                        <ul class="chat-left">
                            <li >
                                <div class="chat-pic">
                                    <img id="chat_user_pic" src="images/pro-pic.jpg" alt="" />
                                </div>
                                <label id="chat_user_name" >  </label>
                            </li>
                        </ul>
                    
                    </div>

                  <!--   <div id="loading" style="display: none;" class="text-center">
                        <img src="<?php echo WEB_ASSETS;?>images/ajax-loader.gif" >
                    </div> -->
                    <div class="div-scroll ">
                        <div class="mob-chat-1 hidden-lg hidden-md chat-history">
                        
                           
                            
                            
                        </div>
                        
                        <div id="scrollbar2" class="hidden-sm hidden-xs chat-history">
                            <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                            <div class="viewport">
                                 <div class="overview">
                                    
                                  
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>    

                    <div class="bottom-area">
                        <textarea style="resize: none" id="message-to-send" placeholder="Click here to type something. . . "></textarea>
                        
                        <div class="button-part text-center">                            
                           <img id="enter_message" style="cursor: pointer;" src="<?php echo WEB_ASSETS; ?>images/send.png" alt=""/>
                        </div>
                    </div>
                    
                    <input type="hidden" value="" name="chatwith" id="chatwith" />
 
                    <input type="hidden" value="<?php echo $user_id; ?>" name="sender_user_id" id="sender_user_id" />
                </div>
                
                <div class="left-part">
                    
                    <div class="search-area">
                        <!-- <div class="search-part">
                            <img src="images/search.jpg" alt=""/>
                            <input type="search" placeholder="Search. . . ."/>
                        </div> -->
                    </div>
                   
                    <div class="mob-chat-2 hidden-lg hidden-md">
                    
                         <ul class="chat-left list" style="height: 495px;overflow: scroll;">
                                <?php
                                if(count($arr_matches) > 0) { 
                                    foreach ($arr_matches as $key => $value) {
                                    ?>
                                    <li id="<?php echo $value['u2_user_id']; ?>#<?php echo $value['name']; ?>">
                                        <div class="chat-pic col-xs-3">
                                            <div style="border:1px solid #f44336;border-radius:50px;margin: 5px 0px;">
                                            <img src="<?php echo $value['user_image']; ?>" alt="" />
                                            <span>2</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                        <label> <?php echo $value['name']; ?></label>
                                    </div>
                                    </li>
                                <?php 
                                    }
                                } ?>
                                
                                
                            </ul>
                        
                        
                    </div>
                   
                <div id="scrollbar1" class="hidden-xs hidden-sm">
                    <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                    <div class="viewport">
                         <div class="overview">
                            
                            <ul class="chat-left list" style="height: 500px;overflow: scroll;">
                                <?php
                                if(count($arr_matches) > 0) { 
                                    foreach ($arr_matches as $key => $value) {
                                        $status = $value['status'];
                                        if($status == '1')
                                            $css  = 'blur1';
                                        else if($status == '2')
                                            $css  = 'blur2';
                                        else if($status == '3')
                                            $css  = 'blur3';
                                        else if($status == '' || $status == '0')
                                            $css  = 'blur1';
                                        else
                                            $css  = 'blur4';

                                    ?>
                                    <li class="" id="<?php echo $value['u2_user_id']; ?>#<?php echo $value['name']; ?>">
                                        <div class="chat-pic col-sm-3 col-xs-3">
                                            <div style="border:1px solid #f44336;border-radius:50px;margin: 5px 0px;">
                                            <img id="li_user_pic_<?php echo $value['u2_user_id']; ?>" class="<?php echo $css;?>" src="<?php echo $value['user_image']; ?>" alt="" />
                                           <span style="<?php if(empty($value['read_count'])) {?> display: none; <?php } ?>" id="read_cnt_<?php echo $value['u2_user_id']; ?>"><?php echo $value['read_count'];?></span>
                                           </div>
                                        </div>
                                        <div class="col-sm-9 col-xs-9">
                                        <label id="li_user_name_<?php echo $value['u2_user_id']; ?>"> <?php echo $value['name']; ?></label>
                                        </div>
                                    </li>

                                   
                                <?php 
                                    }
                                } ?>
                                
                                
                            </ul>
                            
                            
                        </div>
                    </div>
                </div>
        
                   
                </div>
                
                
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('api/footer'); ?>

    <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
    <script type="text/javascript">
        var web_assets = '<?php echo WEB_ASSETS;?>';
        var base_url = '<?php echo BASE_URL;?>';

         //check_message_match('',);
    </script>  
    <script src="<?php echo WEB_ASSETS;?>js/chat.js"></script>
    <script type="text/javascript">
         
        get_chats('<?php echo $arr_matches[0]['u2_user_id'];?>');
    </script>
