<!DOCTYPE html>
<html lang="en" class="wide wow-animation">
  <head>
    <!-- Site Title-->
    <title>Home</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="<?php echo TEMPLATE_ASSETS; ?>/images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:400,100,300,300italic,400italic,500,500italic,600,600italic,700italic,700,800%7CLora:400,400italic,700,700italic">
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSETS; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo WEB_ASSETS; ?>/css/custom.css">
     <link href="<?php echo WEB_ASSETS ?>/css/codeigniter_pagination.css" rel="stylesheet">
		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
  </head>
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->

      <header class="page-head">
        <!-- RD Navbar-->
      
          <!--  <b><a href="#">Login</a></b> &nbsp; <b><a href="#">Registration</a></b>  -->

        <!-- <div class="row">
          
          sdfjkaslksdjflksj
        </div> -->
        <div class="rd-navbar-wrap">

          <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-sidebar" data-md-layout="rd-navbar-sidebar" data-sm-auto-height="false" data-md-auto-height="false" data-lg-auto-height="false" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-sidebar" data-lg-device-layout="rd-navbar-sidebar" data-sm-stick-up-offset="50px" data-lg-stick-up-offset="150px" class="rd-navbar">
            <div class="rd-navbar-inner">

              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar" class="rd-navbar-toggle"><span></span></button>
                <!-- RD Navbar Brand-->
                <div class="rd-navbar-brand"><a href="<?php echo BASE_URL;?>" class="brand-name"><img class="header_lu_image" src="<?php echo TEMPLATE_ASSETS; ?>images/logo.png" alt=""></a></div>
              </div>
              <div class="rd-navbar-nav-wrap">
                <!-- RD Navbar Nav-->
                <ul class="rd-navbar-nav">
                  <?php
                  if(!empty($this->session->userdata('session_data')['user_id'])){ 
                    $arr_user = $this->User_Login_model->get_user_details('',$this->session->userdata('session_data')['user_id']);

                    ?>
                  <li>
                    <a style="border-bottom:1px solid #ccc;padding-bottom:10px;padding-top:10px;" href="<?php echo BASE_URL;?>dashboard">
                      <img style="border-radius: 50%" src="<?php  if(!empty($arr_user[0]['user_image'])){  echo USER_IMAGES.$arr_user[0]['user_image'];} else{ echo WEB_ASSETS.'images/homepage-user.png';}?>" alt="" height="85px"/>
                    <p style="font-weight: 300 !important">Welcome</p>
                    <span><?php echo $this->session->userdata('session_data')['name'];?></span>
                    </a>
                  </li>
                  <?php
                }?>
                  <li><a href="./">Home</a></li>
                  <li><a href="<?php echo BASE_URL;?>About-us">About us</a></li>
                  <li><a href="<?php echo BASE_URL;?>Testimonials">Testimonials</a></li>
                  <li><a href="<?php echo BASE_URL;?>Frequently-asked-questions">FAQ's</a></li>
                  <li><a href="<?php echo BASE_URL;?>Contact-us">Contact Us</a></li>
                  

                  <!-- <li><a href="services.html">Services</a>
                    <!-- RD Navbar Megamenu
                    <ul class="rd-navbar-dropdown">
                      <li><a href="#">Service 01</a></li>
                      <li><a href="#">Service 02</a></li>
                      <li><a href="#">Service 03</a></li>
                    </ul>
                  </li>
                  <li><a href="blog.html">Blog</a></li>
                  <li><a href="contacts.html">Contacts</a></li>
                  <li><a href="404.html">404</a></li> -->
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </header>