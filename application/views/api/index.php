<?php require('home_header.php');?>
      <!-- Page Content-->
      <main class="page-content">
        <!-- RD Parallax-->
        <section class="rd-parallax">

          <div data-speed="0.4" data-type="media" data-url="<?php echo TEMPLATE_ASSETS; ?>/images/home-01.jpg" class="rd-parallax-layer"></div>

       
          <div data-speed="0" data-type="html" class="rd-parallax-layer">
             
            <div class="shell section-md-top-40 section-md-bottom-220 section-80 text-center text-l">

            <a href="<?php echo BASE_URL;?>" class="lu_img"><img class="lu_logo" src="<?php echo TEMPLATE_ASSETS; ?>images/logo.png" alt=""></a>
             <?php
            if(empty($this->session->userdata('session_data')['user_id'])){ 
              ?>
              <div class="row desktop_link">
               
                 <ul class="list-inline login">
                    <li><b><a href="<?php echo BASE_URL;?>login">LOGIN</a></b></li>
                    <li><b><a href="<?php echo BASE_URL;?>register">REGISTRATION</a></b></li>
                  </ul>
               
               </div>
              <div class="row responsive_link">
                 <ul class="list-inline login">
                    <li><b><a href="<?php echo BASE_URL;?>login">LOGIN</a></b></li>
                    <li><b><a href="<?php echo BASE_URL;?>register">REGISTRATION</a></b></li>
                  </ul>
               </div>
              <?php
             }?>
              <div class="range offset-lg-top-250 offset-top-0 offset-sm-top-177">
                <div class="cell-md-6 text-center text-md-left">
                  <h1>Choose your soul mate</h1>
                  <h2 class="text-vivid-tangerine text-italic">Bring Love Together.<br>Not Divide.</h2>
                </div>
                <div class="cell-md-6"></div>
                 <div class="cell-md-3 cell-md-preffix-1 text-md-left offset-top-77"><div style="height:35px;"></div></div>
              </div>
            </div>
          </div>
        </section>
        <section class="skew-1 section-bottom-90 section-top-80 section-md-top-0 backrgound">
          <div class="shell offset-md-top--80">
            <div class="img-baner"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/home-02.png" alt=""></div>
            <div class="row row-no-gutter position-r">
              <div class="col-sm-6 col-md-6">
                <div class="block-2 con section-lg-top-190 inset-md-left-15">
                  <div class="baner text-left">
                    <h1 class="position-r">Love Stories</h1>
                     <p class="text-italic">I feel as though if you do the same things, day in and day out, you'll get the same results.What I found so refreshing about Undivided Love is that it's different than any other dating application out there-a perfectly balanced interdependent web of personality.</p><small class="text-uppercase text-bold">Lindsay and Kyle</small>
                  </div><a href="<?php echo BASE_URL; ?>Testimonials" class="link offset-sm-top-50 preffix-xs-left-60 offset-top-50">read more stories<span class="fl-budicons-launch-right164"></span></a>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="block-1 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/home-03.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-2 dn">
                <div class="block-0 emp"></div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/home-04.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/home-05.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/home-06.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/home-07.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-4 dn">
                <div class="block-0 emp"></div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/home-08.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/home-09.jpg" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="block-0 img-cont"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/lindey.png" alt=""></div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-2 dn">
                <div class="block-0 emp"></div>
              </div>
            </div>
          </div>
        </section>
        <section class="skew-2 section-md-top-10 section-md-bottom-422 section-top-80 section-bottom-120">
          <div class="shell text-left position-r">
            <h1 class="text-white">Tired of being alone?</h1>
            <ul class="list-index range offset-top-93">
              <li class="cell-md-3 cell-sm-6"><span class="list-index-counter"></span>
                <h5>Join and<br>find your love</h5>
              </li>
              <li class="cell-md-3 cell-sm-6 offset-top-77 offset-sm-top-0"><span class="list-index-counter"></span>
                <h5>add information about yourself</h5>
              </li>
              <li class="cell-md-3 cell-sm-6 offset-top-77 offset-md-top-0"><span class="list-index-counter"></span>
                <h5>Process to find your match</h5>
              </li>
              <li class="cell-md-3 cell-sm-6 offset-top-77 offset-md-top-0"><span class="list-index-counter"></span>
                <h5>get in touch and be happy</h5>
              </li>
            </ul>
          </div>
        </section>
        <section class="skew-1-1 section-lg-bottom-187 position-r-1 section-top-80 section-md-top-0 section-bottom-90">
          <div class="img-baner-var-1"><img src="<?php echo TEMPLATE_ASSETS; ?>/images/home-11.png" alt="" class="position-r"></div>
          <div class="shell offset-md-top--35">
            <div class="range profiles">
              <div class="cell-lg-9 cell-lg-preffix-3 position-r">
                <h1 class="text-center text-sm-left">Last Added Profiles</h1>
               
                 <div class="range offset-top-60">
                  <?php foreach ($added_profile as $key => $value) {?>
                  <div class="cell-md-4 cell-sm-6">
                <?php   if($value['user_image']=='') {?>
                   <img src="<?php echo WEB_ASSETS;?>/images/ma-ic.png" alt="" class="img-rounded" style="height: 150px; width: 150px;"><?php }else{?>
                  <img src="<?php echo USER_IMAGES.$value['user_image']; ?>" alt="" class="img-rounded" style="height: 150px; width: 150px;"><?php } ?>
                    <h4 class="offset-top-16"><?php echo $value['name'];?></h4>
                    <div class="small text-italic">Age: <?php echo $value['age'];?>, <br><?php echo $value['city'];?></div><!-- <a href="#" class="link offset-top-16">view info<span class="fl-budicons-launch-right164"></span></a> -->
                  </div><?php } ?>
                  </div>
              </div>
            </div>
          </div>
        </section>
        <section>
         <div class="cell-md-4 position-r text-md-left">
              <ul class="list-inline">
               <li><a href="https://www.instagram.com/love_undivided" ><i class="fa fa-instagram fa-3x"  style="color:#FF5353" aria-hidden="true"></i></a></li>
                <li><a href="https://www.facebook.com/LoveUndivided-1997886527098202/"><i class="fa fa-facebook-square fa-3x" style="color:#FF5353" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/LoveUndivided"><i class="fa fa-twitter-square fa-3x" style="color:#FF5353" aria-hidden="true"></i></a></li>
              </ul>
            </div><br>
          <div class="range range-condensed position-r">
            <!-- RD Flickr-->
            <div data-flickr-tags="tm_57746" data-photo-swipe-gallery="gallery" data-items="1" data-md-items="4" data-sm-items="2" data-lg-items="8" data-stage-padding="0" data-loop="false" data-margin="0" data-dots="true" class="flickr flickrfeed owl-carousel owl-carousel-1">
              <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item class="thumbnail">
                  <figure><img src="<?php echo TEMPLATE_ASSETS; ?>/images/_blank.png" data-image_b="src" data-title="alt" alt=""></figure></a></div>
              <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item class="thumbnail">
                  <figure><img src="<?php echo TEMPLATE_ASSETS; ?>/images/_blank.png" data-image_b="src" data-title="alt" alt=""></figure></a></div>
              <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item class="thumbnail">
                  <figure><img src="<?php echo TEMPLATE_ASSETS; ?>/images/_blank.png" data-image_b="src" data-title="alt" alt=""></figure></a></div>
              <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item class="thumbnail">
                  <figure><img src="<?php echo TEMPLATE_ASSETS; ?>/images/_blank.png" data-image_b="src" data-title="alt" alt=""></figure></a></div>
              <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item class="thumbnail">
                  <figure><img src="<?php echo TEMPLATE_ASSETS; ?>/images/_blank.png" data-image_b="src" data-title="alt" alt=""></figure></a></div>
              <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item class="thumbnail">
                  <figure><img src="<?php echo TEMPLATE_ASSETS; ?>/images/_blank.png" data-image_b="src" data-title="alt" alt=""></figure></a></div>
              <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item class="thumbnail">
                  <figure><img src="<?php echo TEMPLATE_ASSETS; ?>images/_blank.png" data-image_b="src" data-title="alt" alt=""></figure></a></div>
              <div data-type="flickr-item" class="flickr-item"><a data-image_b="href" data-photo-swipe-item class="thumbnail">
                  <figure><img src="<?php echo TEMPLATE_ASSETS; ?>/images/_blank.png" data-image_b="src" data-title="alt" alt=""></figure></a></div>
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footer-->
      <footer class="page-foot" style="padding: 50px 0px;background-color: #f1f1f1;">
        <div class="shell">
          <div class="range range-md-reverse">
            <div class="cell-md-4 position-r text-md-right">
              <!-- <ul class="list-inline">
                <li><a href="#" class="fa-instagram icon icon-sm"></a></li>
                <li><a href="#" class="fa-facebook-official icon icon-sm"></a></li>
                <li><a href="#" class="fa-twitter-square icon icon-sm"></a></li>
              </ul> -->
            </div>
            <div class="cell-md-4 position-r"><a href="<?php echo BASE_URL;?>"><img src="<?php echo TEMPLATE_ASSETS; ?>images/logo.png" alt=""></a></div>
            <div class="cell-md-4 text-md-left">
              <p style="margin-top: 20px;cursor: pointer;">&#169;<span id="copyright-year"></span>	|	
              <a href="<?php echo BASE_URL?>Privacy-policy">Privacy Policy</a>
              </p>
            </div>
            <div class="cell-md-4 text-md-right">
              <p style="margin-top: 20px;cursor: pointer;">&#169;<span id="copyright-year"></span>  | 
              <a href="<?php echo BASE_URL?>Privacy-policy">Privacy Policy</a>
              </p>
            </div>
          </div>
        </div>
      </footer>
       <!-- <a rel="nofollow" href="http://www.templatemonster.com/category/dating-web-templates/" target="_blank">Dating Website Templates at TemplateMonster.com</a> -->
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="<?php echo TEMPLATE_ASSETS; ?>js/core.min.js"></script>
    <script src="<?php echo TEMPLATE_ASSETS; ?>js/script.js"></script>
  </body>
</html>