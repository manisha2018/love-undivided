<?php $this->load->view('api/header'); ?>
<style type="text/css">
    .contact_text{
       width: 80%;
       color: darkgrey;
    }
</style>
<div class="container">
    	<div class="row">
        	<div class="col-sm-12">
            	<div class="second-back">
                    <h1 class="title-col">Account Settings</h1>
                    <ul class="first-menu">
                        <li class="active"><a href="<?php echo BASE_URL;?>general-settings">General</a></li>
                        <li><a href="<?php echo BASE_URL;?>billing-settings">Billing</a></li>
                        <li><a href="<?php echo BASE_URL;?>contact-settings">Notification</a></li>
                    </ul>
                    <!-- <h2 class="second-title">General</h2> -->
                    <div align="center" class="alert alert-danger alert-dismissible fade in" 
                        role="alert" id="flash-messages_1" style="display: none; width: 50%; margin-left:25%;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button><strong id="msg1">Your Password and Confirm Password Do not Match!</strong>
                    </div>
                     <div align="center" class="alert alert-danger alert-dismissible fade in" 
                         role="alert" id="pass_change" style="display: none; width: 50%; margin-left:25%;">
                         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button><strong id="msg2">Your Password is Changed!</strong>
                    </div>
                    <div align="center" class="alert alert-danger alert-dismissible fade in" 
                        role="alert" id="wrong_pass" style="display: none; width: 50%; margin-left:25%;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button><strong id="msg2">You Entered Wrong Old Password!</strong>
                    </div>
                    <input type="hidden" name="slug_name"   id="slug_name" required 
                                value="<?php echo $this->session->userdata('session_data')['slug_name']?>">
                                <div class="container">
                    <div class="general-area col-sm-10 col-sm-push-1 col-md-8 col-md-push-2">
                    	
                        <form id="changepassword" action="" method="post">
                            <div class="form-group row">
                            <div class="col-sm-3 col-sm-push-1">
                                <label>Email </label>
                                </div>
                                <div class="col-sm-8 col-sm-push-1">
                                 <input type="text" class="contact_text" value="<?php echo trim($this->session->userdata('session_data')['email']);?>" readonly>
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-3 col-sm-push-1">
                            	<label>Old Password </label>
                              </div>
                               <div class="col-sm-8 col-sm-push-1">
                                    <input type="password" class="contact_text" id="oldpassword" name="oldpassword">  
                                    </div>               
                            </div>
                            <div class="form-group row">
                             <div class="col-sm-3 col-sm-push-1">
                            	<label>New Password </label>
                              </div>
                              <div class="col-sm-8 col-sm-push-1">
                                 <input type="password" class="contact_text" id="password" name="password"> 
                                 </div>
                            </div>
                            <div class="form-group row">
                             <div class="col-sm-3 col-sm-push-1">
                                <label>Confirm Password </label>
                                </div>
                                 <div class="col-sm-8 col-sm-push-1">
                                 <input type="password"  class="contact_text" id="password2" name="password2"> 
                                 </div>
                            </div>
                            <div class="form-group col-sm-12">
                            <center>
                            <input  type="submit" value="Submit" id="submit-form" class="custom-btn" style="width: 83%;    margin-left: -5%;"> 
                            </center>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="foot"></div>
<?php $this->load->view('api/footer'); ?>
<script type="text/javascript">
    $(function () {
        $("#submit-form").click(function () {
           
            var password = $("#password").val();
            var confirmPassword = $("#password2").val();
            if (password != confirmPassword) {
               $("#flash-messages_1").show();
                setTimeout(function() { $("#flash-messages_1").hide(); }, 5000);
                return false;
            }
            return true;
        });
    });


      $(document).ready(function(){
    $("#changepassword").submit(function(){
      //alert('change password');
      event.preventDefault();
      var formData = new FormData(this);
      var admin_id = $('#user_id').val();
      var oldpassword = $('#oldpassword').val();
      //var name = $('#name').val();
      //console.log("Hi");
       $.ajax({
                url:'<?php echo BASE_URL?>api/User_Activity/ChangePassword',
                type:'POST',
                datatype:'json',
                data:formData,
                cache:false,
              contentType: false,
              processData: false,
               success:function(result)
               {
                   console.log('result '+result);
                   if(result>0)
                   {
                      console.log('result'+result);
            $("#pass_change").show();
            setTimeout(function() { $("#pass_change").hide(); }, 4000);
            $( '#changepassword' ).each(function(){
                              this.reset();
                          });

                   }
                   else
                   {
                    $("#wrong_pass").show();
            setTimeout(function() { $("#wrong_pass").hide(); }, 4000);
            $( '#changepassword' ).each(function(){
                              this.reset();
                          });
                   }
               }
           });
    });
  });

</script>