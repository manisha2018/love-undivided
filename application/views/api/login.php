<?php $this->load->view('api/header');?> 

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="log col-sm-8 col-sm-push-2 col-md-4 col-md-push-4 ">
				<h2>Members login here</h2>
				<!-- href="<?php echo htmlspecialchars($loginUrl); ?>" -->
				<a  onclick="facebookLogin();"><img style="cursor: pointer;" src="<?php echo WEB_ASSETS; ?>images/face1.png" alt=""/></a>     
				<h5>Or login below</h5> 
				<?php if($this->session->flashdata('web_flash')){ ?>
				<div class="alert alert-danger alert-dismissible fade in " role="alert" id="message">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<?php  echo $this->session->flashdata('web_flash'); ?>
				</div>
				<?php }?> 
				<div class="log-form">
					<form method="post" action="<?php echo BASE_URL;?>api/User_Login/UserLogin">
						<div class="form-group">
							<label>Email Address:</label>
							<div class="log-in">
								<input type="text" placeholder="Email Address" name="email" id="email" value="<?php if(isset($_COOKIE['username'])){ echo $_COOKIE['username'];} ?>" />
								<span><img src="<?php echo WEB_ASSETS; ?>images/envelope.png" alt=""/></span> 
								
							</div>
						</div>
					</div>   
					<div class="log-form">
						<div class="form-group">
							<label>Password:</label>
							<div class="log-in">
							 <input type="password" placeholder="Password" name="password" id="password" value="<?php if(isset($_COOKIE['password'])){ echo $_COOKIE['password'];} ?>" /><span><img src="<?php echo WEB_ASSETS; ?>images/lock.png" alt=""/></span>             
						 </div>
					 </div>
					 <div class="col-sm-6" style="padding-left: 0px">
						<a href="#" data-target="#pwdModal" data-toggle="modal">forgot password?</a>  
					</div>        
					<div class="keep col-sm-6"><br>
													<!-- <label><input type="checkbox" name="remember_me" id="remember_me" 
													onchange="checkCookie($('#email').val(),$('#password').val())" value="1"> Keep me logged in</label> -->
												</div>             
											</div>
											<input type="submit" value="Log In" class="logbt"/>
										</form>
										
										<div class="notmem">Not a member? <a href="<?php echo BASE_URL;?>register">Register Now</a></div>
										
									</div>
								</div>
							</div>
						</div>
						
						
						<div class="foot"></div>
						<!-- Modal for Forgot Pasword -->
						<div id="pwdModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
							 <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>api/User_Login/forgot_password" enctype="multipart/form-data"/>
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										<h1 class="text-center">What's My Password?</h1>
									</div>
									<div class="modal-body">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-body">
													<div class="text-center">
														
														<p>If you have forgotten your password you can reset it here.</p>
														<div class="panel-body">
															<fieldset>
																<div class="form-group">
																	<input class="form-control input-lg" placeholder="E-mail Address" name="email" type="email">
																</div>
																<input class="custom-btn" value="Send My Password" type="submit">
															</fieldset>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<div class="col-md-12">
											<button class="custom-gray-btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
										</div>  
									</div>
								</div>
							</form>
								</div>
						</div>

					<!--FB modal-->

					<div id="pwdModal1" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
							
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									</div>
									<div class="modal-body">
										<div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-body">
													<div class="text-center">
														
														<p>Enter Your Email Address</p>
														<div class="panel-body">
															<fieldset>
																<div class="form-group">
																<input type="hidden" name="facebook_id" id="facebook_id" value=""/>
								<input type="hidden" name="facebook_name" id="facebook_name" value=""/>
								<input type="hidden" name="facebook_first_name" id="facebook_first_name" value=""/>
								<input type="hidden" name="facebook_last_name" id="facebook_last_name" value=""/>
								<input type="hidden" name="facebook_gender" id="facebook_gender" value=""/>
								<input type="hidden" name="facebook_profile" id="facebook_profile" value=""/>
																	<input class="form-control input-lg" placeholder="E-mail Address" name="facebook_email" id="facebook_email" type="email">
																</div>
																<input class="custom-btn" value="Send My Password" onclick="myFunction()" type="submit">
															</fieldset>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<div class="col-md-12">
											<button class="custom-gray-btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
										</div>  
									</div>
								</div>
						
								</div>
						</div>
				<?php $this->load->view('api/footer');?> 
				<script type="text/javascript">
					function setCookie(cname, cvalue, exdays) {
	 //alert(cvalue);
	 var d = new Date();
	 d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	 var expires = "expires="+d.toUTCString();
	 document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
 }

 function getCookie(cname) {
 // alert(cname);
 var name = cname + "=";
 var ca = document.cookie.split(';');
 for(var i = 0; i < ca.length; i++) {
	var c = ca[i];
	while (c.charAt(0) == ' ') {
		c = c.substring(1);
	}
	if (c.indexOf(name) == 0) {
		return c.substring(name.length, c.length);
	}
}
return "";
}

function checkCookie(username1,password1) {
 
	// alert(password1);
	if (username1 != "" && username1 != null ||  password1 != "" && password1 != null ) {
		setCookie("username", username1, 365);
		setCookie("password", password1, 365);
		
	}
}
var username = getCookie("username");
var password = getCookie("password");
if (username != "" || password != "") {
			 // alert("Welcome again " + username);
			 $('#email').val(username);
			 $('#password').val(password);
		 } 

		 window.fbAsyncInit = function() {
			FB.init({
				appId            : '239091213260075',
				autoLogAppEvents : true,
				xfbml            : true,
				version          : 'v2.11'
			});
		};

		(function(d, s, id){
		 var js, fjs = d.getElementsByTagName(s)[0];
		 if (d.getElementById(id)) {return;}
		 js = d.createElement(s); js.id = id;
		 js.src = "https://connect.facebook.net/en_US/sdk.js";
		 fjs.parentNode.insertBefore(js, fjs);
	 }(document, 'script', 'facebook-jssdk'));

		function facebookLogin()
	  {
				FB.login(function(response) {
					if (response.authResponse) 
					{

					  FB.api('/me?fields=name,first_name,last_name,email,link,gender,locale,picture', function(response) {
					  	//console.log(response);
						if(response.error==undefined)
						{
									if(response.hasOwnProperty("email"))
									{
											$("#fbModal").modal("hide");
											$.ajax({
												url:'<?php echo BASE_URL?>api/User_Login/userFacebookLogin',
												type:'POST',
												datatype:'json',
												data:JSON.stringify(response),
												cache:false,
												contentType: false,
												processData: false,
												success:function(result)
												{
													console.log(result);
													if(result == 0)
													  {
													   window.location.href = "<?php echo BASE_URL;?>activities";
															//console.log('redirect to activity');
														}else if(result == 1)
														{
															window.location.href = "<?php echo BASE_URL;?>question-category";
															 //console.log('redirect to ques catgory');
														}else
														{
															window.location.href = "<?php echo BASE_URL;?>question-category";
															//console.log('redirect to question');
														}

											  }

											});
										
									} 
									else
									{

				              console.log(response.id);
			                	$('#facebook_id').val(response.id);
			                	$('#facebook_email').val();
			                	$('#facebook_name').val(response.name);
			              		$('#facebook_first_name').val(response.first_name);
											  $('#facebook_last_name').val(response.last_name);
												$('#facebook_gender').val(response.gender);
												$('#facebook_profile').val(response.picture.data.url);
												$("#pwdModal1").modal("show");

									}
						}
						else
						{
						console.log('error');
						}
					  });
				  } 
				  else 
				  {
					  console.log('User cancelled login or did not fully authorize.');
				  }
			 });
	  }



		function myFunction() {
			//console.log('myFunction');
			if(confirm("are u want to login?"))
			{
				console.log('myFunction');
		 var facebook_id = $('#facebook_id').val();
		 var facebook_name = $('#facebook_name').val();
		 var facebook_first_name = $('#facebook_first_name').val();
		 var facebook_last_name = $('#facebook_last_name').val();
		 var facebook_gender = $('#facebook_gender').val();
		 var facebook_profile = $('#facebook_profile').val();
		 var facebook_email = $('#facebook_email').val();
		
	var fb_data = {id:facebook_id,
								email:facebook_email,
								name:facebook_name,
								gender:facebook_first_name,
								facebook_last_name:facebook_last_name,
								facebook_profile:facebook_profile,

							};
		//console.log(fb_data);
				$.ajax({
				        url:'<?php echo BASE_URL?>api/User_Login/userFacebookLogin',
				        type:'POST',
				        datatype:'json',
				        data:JSON.stringify(fb_data),
				        cache:false,
				      contentType: false,
				      processData: false,
				     success:function(result){
				       console.log(result);
												if(result == 0)
													  {
													   window.location.href = "<?php echo BASE_URL;?>activities";
															//console.log('redirect to activity');
														}else if(result == 1)
														{
															window.location.href = "<?php echo BASE_URL;?>question-category";
															 //console.log('redirect to ques catgory');
														}else
														{
															window.location.href = "<?php echo BASE_URL;?>question-category";
															//console.log('redirect to question');
														}

				        }
							});
		       
			}
		}
		</script>