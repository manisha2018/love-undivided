<?php $this->load->view('admin/header');?>
<div class="right_col" role="main">
 <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
   <div class="title_left">
     <h3>Pricing Plan</h3>
     <?php if($this->session->flashdata('admin_flash')){ ?>
     <div class="alert alert-success alert-dismissible fade in" role="alert" id="message" style="width: 50%; text-align: center; margin-left: 18%;">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <?php  echo $this->session->flashdata('admin_flash'); ?>
    </div>
    <?php }?>
  </div>
  <div class="x_panel">
    <div class="x_content">
     <div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search">
      <div class="input-group" style="float: right;">
        <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#add_plan" >Add Pricing Plan
        </button>
      </div> 
    </div>

    <table id="datatable" class="table table-striped table-bordered dataTable">
      <thead>
        <tr>
          <th>Serial No</th>
          <th>Pricing Plan Name</th>
          <th>Price per month</th>
          <th>Duration</th>
          <th class="no-sort" style="text-align: center;">Action</th>
        </tr>
      </thead>
      <tbody>
       <?php
       $index=1;
       foreach ($pricing as $value){ 
        $plan_id = $value['plan_id'];
        ?>
        <tr id="plan_<?php echo $value['plan_id']; ?>">
          <th scope="row"><?php echo $index++; ?></th>
          <td><?php echo $value['plan_name']?></td>
          <td><?php echo $value['price_per_month']?></td>
          <td><?php echo $value['duration']?></td>
          <td class="center" style="text-align: center;">
            <a href="" class="edit btn btn-sm btn-default"  data-toggle="modal"   data-target="#myModal" data-id="<?php echo $value['plan_id'];?>" title="Edit Category"><i class="fa fa-pencil"></i></a>
            <a class="delete btn btn-sm btn-danger"  onclick="deleteplan(<?php echo $value['plan_id'];?>);" title="Delete Category"><i class="fa fa-trash-o"></i></a>
          </td> 
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
</div>
</div>
</div>
</div>

<div id="add_plan" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Pricing_plan/insert_plan" enctype="multipart/form-data"  id="myform2"  onsubmit="return validate(this)"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Pricing Plan</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <input  class="form-control" type="hidden" name="plan_id" id="plan_id">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Plan Name</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" class="form-control" id="plan_name" name="plan_name" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Price per month</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" class="form-control allownumericwithdecimal" id="price_per_month" name="price_per_month" required> <span style="color: red;font-size: 12px;">Numeric values only allowed (With Decimal Point)</span>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Duration</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" class="form-control" id="duration" name="duration" required>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-9 col-sm-9 col-xs-12">
            <button type="button" id="reset" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </form> 
</div>
</div><!--END Create new Job Category Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Pricing_plan/edit_plan_data" enctype="multipart/form-data" onsubmit="return validatePlan(this)"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Pricing Plan</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Plan Name</label>

          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
           <input  class="form-control" type="hidden" name="plan_id" id="plan_id">
           <input  class="form-control" type="text" name="plan_name" id="plan_name">
         </div>
       </div>
       <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Price per month</label>

        <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
          <input  class="form-control allownumericwithdecimal" type="text" name="price_per_month" id="price_per_month">
          <span style="color: red;font-size: 12px;">Numeric values only allowed (With Decimal Point)</span>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Duration</label>

        <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
          <input  class="form-control" type="text" name="duration" id="duration">

        </div>
      </div>
    </div>
    <div class="modal-footer">
      <div class="form-group">
        <div class="col-md-9 col-sm-9 col-xs-12">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Submit</button>
          
        </div>
      </div>
    </div>
  </div>
</form> 
</div>
</div><!--END Create new Job Category Modal -->



<?php $this->load->view('admin/footer');?>
<script type="text/javascript">
$(document).ready(function() {
    $("#reset").click(function() {
      $(':input','#myform2').val("");    
    });
});
 $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
              event.preventDefault();
            }
          });
 function validate(info){
  var plan_name = document.getElementById('plan_name').value;
  var price_per_month = document.getElementById('price_per_month').value;
  var duration = document.getElementById('duration').value;
  if((jQuery.trim( plan_name )).length==0 ||
   info.plan_name.value == null){
    alert("Plan Name is mandatory, you cannot leave this field empty");
  return(false);
}
else if((jQuery.trim( price_per_month )).length==0 ||
 info.price_per_month.value == null)
{
 alert("price_per_month is mandatory, you cannot leave this field empty");
 return(false);
}
else if((jQuery.trim( duration )).length==0 ||
 info.duration.value == null)
{
 alert("Duration is mandatory, you cannot leave this field empty");
 return(false);
}
else{
  return(true);
}
}
$(document).ready(function() {
  $('#datatable').DataTable( {
    "paging":   true,
   // "ordering": true,
   "info":     false,
   "order": [],
   "columnDefs": [ {
    "targets"  : 'no-sort',
    "orderable": false,
  }],
  language: {
    searchPlaceholder: 'Search records'
  }
} );
} );
</script>
<script type="text/javascript">
 function deleteplan(plan_id) {

  swal({
    title: "Are you sure?", 
    text: "Are you sure that you want to delete this  Pricing plan?", 
    type: "warning",
    showCancelButton: true,
    closeOnConfirm: false,
    confirmButtonText: "Yes, delete it!",
    confirmButtonColor: "#ec6c62"
  }, function() {
    $.ajax(
    {
      type: "POST",
      url:"<?php echo BASE_URL;?>admin/Pricing_plan/delete_plan",
      data:{'plan_id':plan_id},
      datatype:'json',
      success: function(data){
        console.log(data);
        $("#plan_"+plan_id).remove();
      }
    }
    )
    .done(function(data) {

      swal("Deleted!", "Your Plan was successfully Delete!", "success");
      location.reload();
    })
    .error(function(data) {
      swal("Oops", "We couldn't connect to the server!", "error");
    });
  });
}

</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myModal').on('show.bs.modal', function (e) {
      var plan_id = $(e.relatedTarget).data('id');
       // alert(plan_id);
       $.ajax({
        type : 'post',
        url : "<?php echo BASE_URL;?>admin/Pricing_plan/get_plan_data",
            data :  'plan_id='+ plan_id, //Pass $id
            success : function(data){
              var obj = $.parseJSON(data);
              //console.log(obj);
            $('.fetched-data #plan_name').val(obj['plan_name']);//Show fetched data from database
            $('.fetched-data #price_per_month').val(obj['price_per_month']);
            $('.fetched-data #duration').val(obj['duration']);
            $('.fetched-data #plan_id').val(obj['plan_id']);
          }
        });
     });
  });

  function validatePlan(info){
  var plan_name = document.getElementById('plan_name').value;
  var price_per_month = document.getElementById('price_per_month').value;
  var duration = document.getElementById('duration').value;
  if(info.plan_name.value!=null && info.plan_name.value!=0 && info.plan_name.value!='' && (jQuery.trim( plan_name )).length==0 && info.price_per_month.value!=null && info.price_per_month.value!=0 && info.price_per_month.value!='' && (jQuery.trim( price_per_month )).length==0 && info.duration.value!=null && info.duration.value!=0 && info.duration.value!='' && (jQuery.trim( duration )).length==0){
    //alert("Plan Name is mandatory, you cannot leave this field empty");
    return(true);
		}
		
		else{
		 alert("Please fill all fields!");
		  return(false);
		}
}
</script>



<script type="text/javascript">
  $('#message').delay(2000).fadeOut('slow');

</script>