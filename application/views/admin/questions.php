<?php $this->load->view('admin/header');?>
<div class="right_col" role="main">
 <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12 paragraph">
   <div class="title_left">
     <h3>Questions</h3>
   </div>
   <?php if($this->session->flashdata('admin_flash')){ ?>
   <div class="alert alert-success alert-dismissible fade in" role="alert" id="message" style="width: 50%; text-align: center; margin-left: 18%;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php  echo $this->session->flashdata('admin_flash'); ?>
  </div>
  <?php }?>
  <div class="x_panel">
    <div class="x_content">
     <div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search">
      <div class="input-group" style="float: right;">
        <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#add_question" >Add Question
        </button>
      </div> 
    </div>
    
    
    <table  class="table table-striped table-bordered dataTable" id="datatable">
      <thead>
        <tr>
          <th>Serial No</th>
          <th>Question</th>
          <th>Category</th>
          <th>Option Group</th>
          <th class="no-sort">Action</th>
        </tr>
      </thead>
      <tbody>
       <?php
       $index=1;
       foreach ($question as $value){ 
        $que_id = $value['question_id'];
        ?>
        <tr id="que_<?php echo $value['question_id']; ?>">
          <th scope="row"><?php echo $index++; ?></th>
          <td><?php echo $value['question']?></td>
          <td><?php echo $value['category_name']?></td>
          <td><?php echo $value['option_group_name']?></td>
          <td class="center" style="text-align: center;">
            <a href="" class="edit btn btn-sm btn-default"  data-toggle="modal"   data-target="#myModal" data-id="<?php echo $value['question_id'];?>" title="Edit Category"><i class="fa fa-pencil"></i></a>
            <a class="delete btn btn-sm btn-danger"  onclick="deleteque(<?php echo $value['question_id'];?>);" title="Delete Category"><i class="fa fa-trash-o"></i></a>
          </td> 
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
</div>
</div>
</div>
</div>


<div id="add_question" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Questions/insert_question" enctype="multipart/form-data" id="myform2"  onsubmit="return validate(this)"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Question</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Question</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <textarea id="question" rows="7" cols="5" style="resize: none;" name="question" class="form-control"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Category</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <select class="form-control" id="category" name="category">
              <?php foreach($category as $value){?> 
              <option value="<?php echo $value['category_id'];?>"><?php echo $value['category_name']; ?></option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Option Group</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <?php foreach ($option_group as $key => $value) {?>
            <input type="radio"  id="option_group" name="option_group" required value="<?php echo $value['option_group_id'];?>"><?php echo $value['option_group_name']?><br>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-9 col-sm-9 col-xs-12">
           
            <button type="button" id="reset" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Submit</button>
            
          </div>
        </div>
      </div>
    </div>
  </form> 
</div>
</div><!--END Create new Job Category Modal -->


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Questions/edit_que_data"  enctype="multipart/form-data" name="myForm" onsubmit="return validateQue(this)"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Question</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Question</label>
          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
            <input  class="form-control" type="hidden" name="question_id" id="question_id">
            <textarea id="question" name="question"  class="form-control"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Category</label>
          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
            <select id="category" name="category" class="form-control">
              <?php foreach($category as $value){?>
              <option value="<?php echo $value['category_id']; ?>"><?php echo $value['category_name'];?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Option Group</label>
          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
            <?php foreach ($option_group as $key => $value) {?>
            <input class="option_group" type="radio"  id="<?php echo $value['option_group_id'];?>" name="option_group" required value="<?php echo $value['option_group_id'];?>" checked="checked"><?php echo $value['option_group_name']?><br>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-9 col-sm-9 col-xs-12">
            <button type="reset" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </form> 
</div>
</div><!--END Create new Job Category Modal -->

<?php $this->load->view('admin/footer');?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#reset").click(function() {
      $('#question','#myform2').val("");
      $('#option_group','#myform2').val("");
       // $('#category','#myform2').val("");
     });
  });
  function validate(info){
    var str = document.getElementById('question').value;
    var str2 =  document.getElementsByClassName('option_group').value;
    if((jQuery.trim( str )).length==0 ||
     info.question.value == null){
      alert("Question is mandatory, you cannot leave this field empty");
    return(false);
  }
    // else if((jQuery.trim( str2 )).length==0 )
    // {
    //   alert("Option is mandatory, you cannot leave this field empty");

    // }
    else{
      return(true);
    }
  }
  $(document).ready(function() {
    $('#datatable').DataTable( {
      "paging":   true,
       // "ordering": true,
       "info":     false,
       "info":     false,
       "order": [],
       "columnDefs": [ {
        "targets"  : 'no-sort',
        "orderable": false,
      }],
      language: {
        searchPlaceholder: 'Search records'
      }
    } );
  } );
</script>
<script type="text/javascript">
 function deleteque(question_id) {
   
  swal({
    title: "Are you sure?", 
    text: "Are you sure that you want to delete this Question?", 
    type: "warning",
    showCancelButton: true,
    closeOnConfirm: false,
    confirmButtonText: "Yes, delete it!",
    confirmButtonColor: "#ec6c62"
  }, function() {
    $.ajax(
    {
      type: "POST",
      url:"<?php echo BASE_URL;?>admin/Questions/delete_question",
      data:{'question_id':question_id},
      datatype:'json',
      success: function(data){
        console.log(data);
        $("#que_"+question_id).remove();
      }
    }
    )
    .done(function(data) {

      swal("Deleted!", "Your Question was successfully Delete!", "success");
      location.reload();
    })
    .error(function(data) {
      swal("Oops", "We couldn't connect to the server!", "error");
    });
  });
}

</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myModal').on('show.bs.modal', function (e) {
      var question_id = $(e.relatedTarget).data('id');
      var option = $('input[name=option_group]:checked', '#myForm').val()
       // alert(category_id);
       $.ajax({
        type : 'post',
        url : "<?php echo BASE_URL;?>admin/Questions/get_que_data",
            data :  'question_id='+ question_id, //Pass $id
            success : function(data){
              var obj = $.parseJSON(data);
              //console.log(obj);
            $('.fetched-data #question').val(obj['question']);//Show fetched data from database
            $('.fetched-data #question_id').val(obj['question_id']);
            $('.fetched-data #category').val(obj['category_id']);
            $('.fetched-data #'+obj['option_group_id']).prop('checked',true);
             //console.log( $('.fetched-data #'+obj['option_group_id']));
             
           }
         });
     });
  });

  function validateQue(info){
    var str = document.getElementById('question').value;
    var str2 =  document.getElementsByClassName('option_group').value;
    if(info.question.value!=null && info.question.value!=0 && info.question.value!='' && (jQuery.trim( str )).length==0)
    {
           return(true);
    }
    else
    {
       alert("Question is mandatory, you cannot leave this field empty");
      return(false);
    }
  }
</script>



<script type="text/javascript">
  $('#message').delay(2000).fadeOut('slow');

</script>