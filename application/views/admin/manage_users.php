<?php $this->load->view('admin/header');?>
<div class="right_col" role="main">
	<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 paragraph">
	    <div class="title_left">
           <h3>Users List</h3>
       </div>
        <div align="center" class="alert alert-success alert-dismissible fade in" role="alert" id="flash-messages_1" style="display: none;width: 50%; text-align: center; margin-left: 18%;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button><strong id="msg1">Profile is Featured!</strong>
        </div>
        <div align="center" class="alert alert-danger alert-dismissible fade in" role="alert" id="flash-messages_2" style="display: none;width: 50%; text-align: center; margin-left: 18%;">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button><strong id="msg1">Profile is removed from Featured List!!</strong>
        </div>
        <div class="x_panel">
          <div class="x_content">
                      <table id="datatable" class="table table-striped table-bordered dataTable">
                        <thead>
                          <tr>
                            <th>Serial No</th>
                            <th>Name</th>
                            <th>Email-ID</th>
                            <th>City</th>
                            <th>Phone No</th>
                            <th>Gender</th>
                            <th>Religion</th>
                            <th>Edcucation</th>
                            <th class="no-sort">User Image</th>
                            <th class="no-sort">Featured User <?php echo "Count ".$featurecount; ?></th>
                            <th class="no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php 
                           $index = 1;
                           foreach ($users as $value){?>
                            <tr id="user_<?php echo $value['user_id']; ?>">
                              <th scope="row"><?php echo $index++;?></th>
                                <td><?php echo $value['name']?></td>
                                <td><?php echo $value['email']?></td>
                                <td><?php echo $value['city']?></td>
                                <td><?php echo $value['mob_no']?></td>
                                <td><?php 
                                     if($value['gender']=='0')
                                     {
                                      echo "Male";
                                     }elseif($value['gender']=='1')
                                     {
                                      echo "Female";
                                     }else
                                     {echo"Bisexual";}?>
                                </td>
                                <td><?php if($value['religion_name']==''){echo "NA";}else {echo $value['religion_name'];}  ?></td>
                                <td><?php if($value['education']=='0'){echo "Doctorate";}
                                        elseif ($value['education']=='1') { echo "Masters";}
                                        elseif ($value['education']=='2') { echo "Bachleors";}
                                        elseif ($value['education']=='3') { echo "Associates";}
                                        elseif ($value['education']=='4') { echo "Some College";}
                                        elseif ($value['education']=='5') { echo "High College";}
                                        elseif ($value['education']=='6') { echo "Did not complete high school";} 
                                        elseif ($value['education']=='7') { echo "Others";} else{echo "NA";}?></td>
                                <td><img src="<?php echo USER_IMAGES.$value['user_image']; ?>" alt="" style="height: 60px; width: 60px;"/></td>
                                <td><?php if($value['featured_user']==0){ ?>  <a class="edit btn btn-sm btn-default" style="margin-left: 40%;" title="Feature Profile" onclick="featured_user(<?php echo $value['user_id'];?>,1)"><i class="fa fa-star-o"></i></a> <?php } else { ?> <a class="edit btn btn-sm btn-danger" style="margin-left: 40%;" title="Unfeature Profile"onclick="featured_user(<?php echo $value['user_id'];?>,0)"><i class="fa fa-star"></i></a><?php } ?></td>
                                 <td class="center" style="text-align: center;">
                                    <a class="edit btn btn-sm btn-default"  data-toggle="modal"   data-target="#viewuser" data-id="<?php echo $value['user_id'];?>" title="View User"><i class="fa fa-eye"></i></a>
                                    <a class="delete btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-basic" onclick="deleteuser(<?php echo $value['user_id'];?>);" title="Delete User"><i class="fa fa-trash-o"></i></a>
                                </td> 
                            <?php }?>
                            </tr>
                         </tbody>
                      </table>
          </div>
       </div>
	  </div>
</div>
</div>
<?php $this->load->view('admin/footer');?>
<div id="viewuser" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Categories/edit_cat_data" enctype="multipart/form-data"/>
              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Billing Plan</h4>
                  </div>
                        <div class="form-group" id="noplan">
                            <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
                              <b style="color: red"><div class="no_plan"></div></b>
                            </div>
                       </div>
                  <div class="modal-body">
                       <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Plan Name</label>
                            <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
                               <input  class="form-control" type="hidden" name="user_id" id="user_id">
                               <input  class="form-control" type="text" name="plan_name" id="plan_name" readonly="readonly">
                            </div>
                       </div>
                       <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Duration</label>
                            <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
                               <input  class="form-control" type="text" name="duration" id="duration" readonly="readonly">
                            </div>
                       </div>
                       <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Amount</label>
                            <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
                               <input  class="form-control" type="text" name="amount" id="amount" readonly="readonly">
                            </div>
                       </div>
                    </div>
                  <div class="modal-footer">
                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12" align="center">
                       
                        <button type="button"  class="btn btn-danger" data-dismiss="modal">Close</button>
                        <!-- <button type="submit" class="btn btn-success">Submit</button> -->
                          
                        </div>
                    </div>
                  </div>
              </div>
          </form> 
      </div>
    </div><!--END Create new Job Category Modal -->
<script type="text/javascript">
  $(document).ready(function() {
    $('#datatable').DataTable( {
        "paging":   true,
        //"ordering": true,
        "info":     false,
        "order": [],
        "columnDefs": [ {
        "targets"  : 'no-sort',
        "orderable": false,
        }],
         language: {
        searchPlaceholder: 'Search records'
      }
  
    } );
} );
</script>
<script type="text/javascript">
   function deleteuser(user_id) {
   
    swal({
      title: "Are you sure?", 
      text: "Are you sure that you want to delete this User?", 
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      confirmButtonText: "Yes, delete it!",
      confirmButtonColor: "#ec6c62"
    }, function() {
        $.ajax(
                {
                    type: "POST",
                    url:"<?php echo BASE_URL;?>admin/Manage_users/delete_user",
                    data:{'user_id':user_id},
                    datatype:'json',
                    success: function(data){
                      //console.log(data);
                        $("#user_"+user_id).remove();
                    }
                }
        )
      .done(function(data) {

        swal("Deleted!", "Your User was successfully Delete!", "success");
        location.reload();
      })
      .error(function(data) {
        swal("Oops", "We couldn't connect to the server!", "error");
      });
    });
  }

</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#viewuser').on('show.bs.modal', function (e) {
        var user_id = $(e.relatedTarget).data('id');
       // alert(category_id);
        $.ajax({
            type : 'post',
            url : "<?php echo BASE_URL;?>admin/Manage_users/get_usersplan_data",
            data :  'user_id='+ user_id, //Pass $id
            success : function(data){
              var obj = $.parseJSON(data);
            
              console.log(obj['status']);
            $('.fetched-data #plan_name').val(obj['plan_name']);//Show fetched data from database
            $('.fetched-data #duration').val(obj['duration']);
            $('.fetched-data #amount').val(obj['amount']);
            if(obj['status']==0)
            {
              $('#noplan').show();
              $('.no_plan').html(obj['response']);

            }

          
            }
        });
     });
});

function featured_user(user_id,val) {

    if(val==0)
    {
      swal({
        title: "Are you sure?", 
        text: "Are you sure you want to remove this profile from featured list??", 
        type: "warning",
         timer: 5000,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Yes!",
        confirmButtonColor: "#ec6c62"
      }, function() {
        $.ajax(
        {
          type: "POST",
          url:"<?php echo BASE_URL;?>admin/Manage_users/feature_user",
         data: { 'val': val,
                   'user_id':user_id,},
          datatype:'json',
          success: function(data){
            //console.log(data);

            //$("#cat_"+category_id).remove();
          }
        }
        )
        .done(function(data) {

          swal("Success!", "User removed from featured list!", "success");
           // location.reload();
           setTimeout(function() {
            window.location.reload();
          }, 3000);

       
        })
        .error(function(data) {
          swal("Oops", "We couldn't connect to the server!", "error");
        });
      });

    }
    else if(val==1)
    {
     swal({
        title: "Are you sure?", 
        text: "Are you sure you want to featured this profile??", 
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
         timer: 5000,
        confirmButtonText: "Yes!",
        confirmButtonColor: "#ec6c62"
      }, function() {
        $.ajax(
        {
          type: "POST",
          url:"<?php echo BASE_URL;?>admin/Manage_users/feature_user",
          data: { 'val': val,
                   'user_id':user_id,},
          datatype:'json',
          success: function(data){
            //console.log(data);

            //$("#cat_"+category_id).remove();
          }
        }
        )
        .done(function(data) {

          swal("Success!", "User Added in featured list!", "success");
           setTimeout(function() {
            window.location.reload();
          }, 3000);
            //location.reload();
       
        })
        .error(function(data) {
          swal("Oops", "We couldn't connect to the server!", "error");
        });
      });
    }

}

</script>
