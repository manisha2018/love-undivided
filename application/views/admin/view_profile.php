  <?php $this->load->view('admin/header');?>

  <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Admin Profile</h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <!-- Display  Messages -->
            <div class="alert alert-info alert-dismissible fade in" role="alert" id="pass_change" style="display: none;">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong id="msg2">Your Password is Changed!</strong>
            </div>
            <div class="alert alert-info alert-dismissible fade in" role="alert" id="wrong_pass" style="display: none;">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong id="msg2">You Entered Wrong Old Password!</strong>
            </div>
            <div class="alert alert-info alert-dismissible fade in" role="alert" id="flash-messages_1" style="display: none;">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong id="msg2">You Profile is Updated!</strong>
            </div>
            <div class="alert alert-info alert-dismissible fade in" role="alert" id="flash-messages_2" style="display: none;">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong id="msg2">You Profile is Not Updated!</strong>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Change Profile</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
             
              <?php //print_r($_SESSION); ?>
                  <form class="form-horizontal form-label-left" method="post" action="" enctype="multipart/form-data"/ id="profile_form">
                    <div class="form-group row">
                      <input type="hidden"  required="required" class="form-control col-md-7 col-xs-12" id="userid" name="userid" value="<?php  echo $this->session->userdata('admin_session_data')['userid']; ?>">

                        <label class="control-label col-md-4" for="user-name"> Name <span class="required">*</span>
                        </label>
                        <div class="col-md-8">
                          <input type="text" required="required" class="form-control col-md-7 col-xs-12" id="user_name" name="user_name" oninput="validateAlpha();"  value="<?php  echo $this->session->userdata('admin_session_data')['user_name']; ?>">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-md-4" for="user_email">Username (Email Id)<span class="required"> *</span>
                        </label>
                        <div class="col-md-8">
                          <input type="text" id="user_email" readonly="readonly" name="user_email" required="required" class="form-control col-md-7 col-xs-12" value="<?php  echo $this->session->userdata('admin_session_data')['user_email'];?>">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="control-label col-md-4" for="phone_no">Phone No<span class="required"> *</span>
                        </label>
                        <div class="col-md-8">
                          <input type="text" onkeypress="return isNumberKey(event);" id="phone_no" name="phone_no" required="required" class="form-control col-md-7 col-xs-12" value="<?php  echo $this->session->userdata('admin_session_data')['phone_no'];?>">
                        </div>
                      </div>
                     <!--   <div class="form-group row">
                        <label class="control-label col-md-4" for="user_profile">Profile Picture    </label>
                        <div class="col-md-8">
                          <input type="file" id="user_profile" name="user_profile"  class="form-control col-md-7 col-xs-12">
                        </div>
                      </div> -->
                       <div class="form-group row">
                    
                        <div class="col-md-8">
                          <button type="submit" id="profile" class="btn btn-danger" data-dismiss="modal">Submit</button>
                        </div>
                      </div>
                   </form>
                   </div>
                </div>
              </div>


              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Change Password</h2>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <form class="form-horizontal form-label-left" method="post" action="" enctype="multipart/form-data"/ id="changepassword">
                     <div class="form-group row">
                       <input type="hidden"  required="required" class="form-control col-md-7 col-xs-12" id="userid" name="userid" value="<?php  echo $this->session->userdata('admin_session_data')['userid']; ?>">
                        <label class="control-label col-md-4" for="first-name"> Old Password <span class="required">*</span>
                        </label>
                        <div class="col-md-8">
                          <input type="password" required="required" class="form-control col-md-7 col-xs-12" id="oldpassword" name="oldpassword">
                        </div>
                     </div>
                     <div class="form-group row">
                        <label class="control-label col-md-4" for="first-name">Password <span class="required">*</span>
                        </label>
                        <div class="col-md-8">
                          <input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-4" for="first-name"> Confirm Password <span class="required">*</span>
                        </label>
                        <div class="col-md-8">
                          <input type="password" id="password2" name="password2" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group row">
                    
                        <div class="col-md-8">
                          <button type="submit" id="submit-form"  class="btn btn-danger" data-dismiss="modal">Submit</button>
                        </div>
                    </div>
                    </form>
                  </div>
                </div>
              </div>

             

           
            </div>
          </div>
        </div>
<?php $this->load->view('admin/footer');?>
<script type="text/javascript">
$(document).ready(function(){ 
$("#user_name").keydown(function(event) {
    if (event.keyCode == 32) {
        event.preventDefault();
    }
});
});
    function validateAlpha(){
          var textInput = document.getElementById("user_name").value;
          textInput = textInput.replace(/[^A-Za-z]+[\s]/g, "");
          if(document.getElementById("user_name").value = textInput)
          {
          	$('#profile').removeAttr('disabled');
          	return true;
          }else{
          	$('#profile').attr('disabled',true);
          	return false;
          }
          }

 	function isNumberKey(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode;
              if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
              	  $('#profile').attr('disabled',true);
                    return false;
                } else {
                		 $('#profile').removeAttr('disabled');
                    return true;
                }      
              }
$(document).ready(function(){
    $("#profile_form").submit(function(){
        //alert("Submitted");
         event.preventDefault();
         var formData = new FormData(this);//construct set of key value pairs and representing form field and values
             var user_name = $('#user_name').val();
             var user_email = $('#user_email').val();
            
            //console.log(user_name);

             //var user_profile=$("#user_profile").get(0).files[0];  
               $.ajax({
                url:'<?php echo BASE_URL?>admin/Login/update_admin_profile',
                type:'POST',
                datatype:'json',
                data:formData,
                cache:false,
              contentType: false,
              processData: false,
             success:function(result){
                 console.log(result);
              //  $("#msg").html("Profile Updated");
                  if(result=='1')
                  {
                   //alert('profile update');
            $("#flash-messages_1").show();
            setTimeout(function() { 
            	$("#flash-messages_1").hide();
            	location.reload();
            	 }, 4000);
            
                  }else
                  {
            //alert('profile not update');
            $("#flash-messages_2").show();
            setTimeout(function() { $("#flash-messages_2").hide();   location.reload(); }, 4000);
                     

                  }

                }

        });  
    });
});
</script>
<script type="text/javascript">
    $(function () {
        $("#submit-form").click(function () {
            var password = $("#password").val();
            var confirmPassword = $("#password2").val();
            if (password != confirmPassword) {
               // alert("Password and Confirm Password do not match.");
                swal({
                    title: "Error",
                    text: "Password and Confirm Password do not match!",
                   // timer: 1500,
                    showConfirmButton: true,
                    type: 'error'
                });

                return false;
            }
            return true;
        });
    });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#changepassword").submit(function(){
      //alert('change password');
      event.preventDefault();
      var formData = new FormData(this);
      var admin_id = $('#userid').val();
      var oldpassword = $('#oldpassword').val();
      var name = $('#user_name').val();
      //console.log("Hi");
       $.ajax({
                url:'<?php echo BASE_URL?>admin/Login/changepassword',
                type:'POST',
                datatype:'json',
                data:formData,
                cache:false,
              contentType: false,
              processData: false,
               success:function(result)
               {
                  // console.log(result);
                   if(result>0)
                   {
                   
            $("#pass_change").show();
            setTimeout(function() { $("#pass_change").hide(); }, 4000);
            $( '#changepassword' ).each(function(){
                              this.reset();
                          });

                   }
                   else
                   {
                    $("#wrong_pass").show();
            setTimeout(function() { $("#wrong_pass").hide(); }, 4000);
            $( '#changepassword' ).each(function(){
                              this.reset();
                          });
                   }
               }
           });
    });
  });

  	   
        // function Validate() {
        //     var regex = new RegExp("^[0-9?=.*!@#$%^&*+]+$");
        //    // var name_patt = new RegExp("/^[a-zA-Z]+$/");
        //     var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        //     if (!regex.test(document.getElementById("phone_no").value)) {
        //         event.preventDefault();
        //         //alert('phone_no');
        //         return true;
        //     }else{
        //     	return false;
        //     }
            
        // }       

</script>