<?php
if(get_cookie('user_email'))
{
  //echo"hi";
  $a=$_COOKIE['user_email'];
  echo "<script>
var obj = $.parseJSON($a);
alert(obj);
  </script>";
}
echo get_cookie('user_email');
//die;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo ADMIN_ASSETS; ?>img/favicon.ico" type="image/x-icon">
     <title><?php echo SITE_NAME; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo ADMIN_ASSETS."css/bootstrap.min.css"?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo ADMIN_ASSETS."css/font-awesome.min.css"?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo ADMIN_ASSETS."css/nprogress.css"?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo ADMIN_ASSETS."css/animate.min.css"?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo ADMIN_ASSETS."css/custom.min.css"?>" rel="stylesheet">
  </head>
  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
        <!-- <center><h1>Love Undivided</h1></center> -->
       <a href="<?php echo BASE_URL; ?>"><img src="<?php echo TEMPLATE_ASSETS?>/images/logo.png" alt="" style="width: 100%;""></a>
          <section class="login_content">
          <?php if($this->session->flashdata('admin_flash')){ ?>
           
             <div class="alert alert-success alert-dismissible fade in" role="alert" id="message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true" style="display: none;">×</span>
                    </button>
                    <?php  echo $this->session->flashdata('admin_flash'); ?>
                  </div>
            <?php }?>
            <form method="post" action="<?php echo BASE_URL;?>admin/Login/admin_login">
              <h1>Login</h1>
              <div>
              <!--  <input type="hidden" name="check_input" value="1"/> -->
                <input type="email" class="form-control" placeholder="Username" name="user_email"  id="user_email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="user_password"  id="user_password" required="" />
              </div>
               <!-- <div style="float: left;">
              <input type="checkbox"  name="checkbox"  id="checkbox" value="1" />&nbsp;Remember me
              </div> -->
              <br>
              <div>
               <button type="submit" class="btn btn-default">Log In</button></div>
               <a href="#" data-toggle="modal" data-target="#adminmail" ><strong> Forgot Password? </strong></a>
               <!--  <?php echo BASE_URL;?>admin/Login/admin_forgot_password" ?> -->
             

              <div class="clearfix"></div>

              <div class="separator">
               <div>
                  
                  <p>©2017 All Rights Reserved. Love Undivided </p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
    <div id="adminmail" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <form class="form-horizontal form-label-left" method="post"  action="<?php echo BASE_URL;?>admin/Login/forgot_password" enctype="multipart/form-data"/>
              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Send Mail</h4>
                  </div>
                  <div class="modal-body">
                     <!--  <div class="alert alert-success alert-dismissible fade in" role="alert" id="message_error1" >
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    
                  </div> -->
                       <div class="form-group">
                          <label class="control-label col-md-4 col-sm- col-xs-12 has-feedback ">Enter Your mail Address</label>
                          <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="email" class="form-control has-feedback-left" name="user_email"  required><span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                           
                          </div>
                      </div>
                      
                  </div>
                  <div class="modal-footer">
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-success">Sent</button> 
                        </div>
                    </div>
                  </div>
              </div>
          </form> 
      </div>
    </div><!--END Forgot Password for User Modal -->
    

    
   

    <!-- jQuery -->
    <script src="<?php echo ADMIN_ASSETS."js/jquery.min.js"?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo ADMIN_ASSETS."js/bootstrap.min.js"?>"></script>
    <!-- FastClick -->
    <script src="<?php echo ADMIN_ASSETS."js/fastclick.js"?>"></script>
    <!-- NProgress -->
    <script src="<?php echo ADMIN_ASSETS."js/nprogress.js"?>"></script>

    <script src="<?php echo ADMIN_ASSETS."js/pnotify.js"?>"></script>
    <script src="<?php echo ADMIN_ASSETS."js/pnotify.buttons.js"?>"></script>
    <script src="<?php echo ADMIN_ASSETS."js/pnotify.nonblock.js"?>"></script>
    <script>
      $('#message').delay(2000).fadeOut('slow');
      $("#checuserkmail").click(function(){

     
      var user_email=document.getElementById('user_email1').value;
      
      $.ajax({
          type: "POST",
          url:'Login/get_user_details',
          datatype:'json',
          data:{"user_email":user_email},
          success: function(data) {
            document.getElementById("message_error").style.display="block";
           if(data)
           {
            document.getElementById("message_error").innerHTML = "Check your Mail ";
           }else{
            document.getElementById("message_error").innerHTML = "Email not exist";
           }
         }
     });
      });
       $('#message_error').delay(2000).fadeOut('slow');
       $("#checkadminmail").click(function(){

     
      var user_email=document.getElementById('user_email2').value;
      
      $.ajax({
          type: "POST",
          url:'Login/get_admin_details',
          datatype:'json',
          data:{"user_email":user_email},
          success: function(data) {
            document.getElementById("message_error1").style.display="block";
           if(data)
           {
            document.getElementById("message_error1").innerHTML = "Check your Mail ";
           }else{
            document.getElementById("message_error1").innerHTML = "Email not exist";
           }
         }
     });
      });
       $('#message_error1').delay(2000).fadeOut('slow');
    </script>
  
    
  </body>
  </html>
