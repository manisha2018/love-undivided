<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo ADMIN_ASSETS; ?>img/logo.png">
     <title><?php echo SITE_NAME; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo ADMIN_ASSETS."css/bootstrap.min.css"?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo ADMIN_ASSETS."css/font-awesome.min.css"?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo ADMIN_ASSETS."css/nprogress.css"?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo ADMIN_ASSETS."css/animate.min.css"?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo ADMIN_ASSETS."css/custom.min.css"?>" rel="stylesheet">
  </head>
  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
        <center><h1>Love Undivided</h1></center>
       
          <section class="login_content">
          <?php if($this->session->flashdata('admin_flash')){ ?>
           
             <div class="alert alert-success alert-dismissible fade in" role="alert" id="message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <?php  echo $this->session->flashdata('admin_flash'); ?>
                  </div>
            <?php }?>
            <form method="post" action="<?php echo BASE_URL;?>admin/Login/change_pass">
              <h1>Forgot Password</h1>
               
              <div>
                 <input type="text" class="form-control" placeholder="Username" name="user_email"  id="user_email" value="<?php if(isset($_GET['user_email'])){echo $_GET['user_email'];}?>">
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="user_password"  id="user_password" required="required" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password"  id="confirm_password" required="required"/>
              </div>
              <div>
               <button type="submit" class="btn btn-default" onclick="return Validate()">Submit</button>
              </div>
               
            </form>
          </section>
        </div>
      </div>
    </div>
    
    <!-- jQuery -->
    <script src="<?php echo ADMIN_ASSETS."js/jquery.min.js"?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo ADMIN_ASSETS."js/bootstrap.min.js"?>"></script>
    <!-- FastClick -->
    <script src="<?php echo ADMIN_ASSETS."js/fastclick.js"?>"></script>
    <!-- NProgress -->
    <script src="<?php echo ADMIN_ASSETS."js/nprogress.js"?>"></script>

    <script src="<?php echo ADMIN_ASSETS."js/pnotify.js"?>"></script>
    <script src="<?php echo ADMIN_ASSETS."js/pnotify.buttons.js"?>"></script>
    <script src="<?php echo ADMIN_ASSETS."js/pnotify.nonblock.js"?>"></script>
    <script>
      $('#message').delay(2000).fadeOut('slow');
       function Validate() {
        var password = document.getElementById("user_password").value;
        var confirmPassword = document.getElementById("confirm_password").value;
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    }
    </script>
   
  </body>
  </html>
