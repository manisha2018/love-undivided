<?php $this->load->view('admin/header');?>
<div class="right_col" role="main">
 <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
   <div class="title_left">
     <h3>Terms And Conditions</h3>
   </div>
   <?php if($this->session->flashdata('admin_flash')){ ?>
   <div class="alert alert-success alert-dismissible fade in" role="alert" id="message" style="width: 50%; text-align: center; margin-left: 18%;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php  echo $this->session->flashdata('admin_flash'); ?>
  </div>
  <?php }?>
  <div class="x_panel">
    <div class="x_content">
      <div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search">
        <div class="input-group" style="float: right;">
          <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#add_terms" >Add Terms Conditions and Privacy Policy
          </button>
        </div> 
      </div>

      <table id="datatable" class="table table-striped table-bordered dataTable">
        <thead>
          <tr>
            <th>Serial No</th>
            <th>Name</th>
            <th>Description</th>
            <!--  <th>Duration</th> -->
            <th class="no-sort">Action</th>
          </tr>
        </thead>
        <tbody>
         <?php
         $index=1;
         foreach ($getTermsConditions as $value){ 
          $term_cond_id = $value['term_cond_id'];
          ?>
          <tr id="term_<?php echo $value['term_cond_id']; ?>">
            <th scope="row"><?php echo $index++; ?></th>
            <td><?php echo $value['name']?></td>
            <td><?php echo $value['description']?></td>
            <!-- <td><?php echo $value['duration']?></td> -->
            <td class="center" style="text-align: center;">
              <a href="" class="edit btn btn-sm btn-default"  data-toggle="modal"   data-target="#myModal" data-id="<?php echo $value['term_cond_id'];?>" title="Edit Conditions"><i class="fa fa-pencil"></i></a>
              <a class="delete btn btn-sm btn-danger"  onclick="deleteTermsCond(<?php echo $value['term_cond_id'];?>);" title="Delete Conditions"><i class="fa fa-trash-o"></i></a>
            </td> 
          </tr>
          <?php }?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=g6iradqrc3uphnkw5fq5toa9o02ahdlucazem1m1u1s7tn2i'></script> 
<!--  <script src="<?php// echo ASSETS?>global/js/pages/tinymce.min.js?apiKey=g6iradqrc3uphnkw5fq5toa9o02ahdlucazem1m1u1s7tn2i'"></script> -->
<script>
  tinymce.init({
    selector: '#description',
    plugins: "autoresize",
    autoresize_max_height: 500
  });
   tinymce.init({
    selector: '#description1',
    plugins: "autoresize",
    autoresize_max_height: 200
  });
</script>
<div id="add_terms" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Terms_Conditions/insert_terms" id="myform2" onsubmit="return validate(this)"  enctype="multipart/form-data"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Terms Condtions</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <input  class="form-control" type="hidden" name="plan_id" id="plan_id">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Enter Title</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" class="form-control" id="name" name="name" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Enter Description</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <!--   <input type="text" class="form-control" id="description" name="description" required> -->
            <textarea id="description" style="resize: none;" name="description" style="width: 100%;"></textarea>
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-9 col-sm-9 col-xs-12">
            <button type="button" id="reset" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Submit</button>
            
          </div>
        </div>
      </div>
    </div>
  </form> 
</div>
</div><!--END Create new Job Category Modal -->

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Terms_Conditions/edit_terms" enctype="multipart/form-data" name="myForm" onsubmit="return validateTerms(this)"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Terms And Conditions</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Name</label>
          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
            <input  class="form-control" type="hidden" name="term_cond_id" id="term_cond_id">
            <input type="text" name="name" id="name" class="form-control">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Description</label>
          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
            <textarea id="description1" style="resize: none;" name="description1" style="width: 100%;"></textarea>
         </div>
       </div>
       
     </div>
     <div class="modal-footer">
      <div class="form-group">
        <div class="col-md-9 col-sm-9 col-xs-12">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </div>
    </div>
  </div>
</form> 
</div>
</div>


<?php $this->load->view('admin/footer');?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#reset").click(function() {
      $(':input','#myform2').val("");
       $('#description','#myform2').val("");    
    });
  });
  function validate(info){
    var name = document.getElementById('name').value;
    var description = document.getElementById('description').value;
    if((jQuery.trim( name )).length==0 ||
     info.name.value == null){
      alert("Title is mandatory, you cannot leave this field empty");
    return(false);
  } else if((jQuery.trim( description )).length==0 ||
   info.description.value == null)
  {
   alert("Description is mandatory, you cannot leave this field empty");
   return(false);
 }
 else{
  return(true);
}
}
$(document).ready(function() {
  $('#datatable').DataTable( {
    "paging":   true,
        //"ordering": true,
        "info":     false,
        "order": [],
        "columnDefs": [ {
          "targets"  : 'no-sort',
          "orderable": false,
        }],
        language: {
          searchPlaceholder: 'Search records'
        }
      } );
} );
</script>
<script type="text/javascript">
 function deleteTermsCond(term_cond_id) {

  swal({
    title: "Are you sure?", 
    text: "Are you sure that you want to delete this Terms Conditions", 
    type: "warning",
    showCancelButton: true,
    closeOnConfirm: false,
    confirmButtonText: "Yes, delete it!",
    confirmButtonColor: "#ec6c62"
  }, function() {
    $.ajax(
    {
      type: "POST",
      url:"<?php echo BASE_URL;?>admin/Terms_Conditions/delete_terms",
      data:{'term_cond_id':term_cond_id},
      datatype:'json',
      success: function(data){
        console.log(data);
        $("#term_"+term_cond_id).remove();
      }
    }
    )
    .done(function(data) {

      swal("Deleted!", "Your Terms and Conditions was successfully Delete!", "success");
      location.reload();
    })
    .error(function(data) {
      swal("Oops", "We couldn't connect to the server!", "error");
    });
  });
}

</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myModal').on('show.bs.modal', function (e) {
      var term_cond_id = $(e.relatedTarget).data('id');
        //alert(term_cond_id);
        $.ajax({
          type : 'post',
          url : "<?php echo BASE_URL;?>admin/Terms_Conditions/get_terms_data",
            data :  'term_cond_id='+ term_cond_id, //Pass $id
            success : function(data){
              var obj = $.parseJSON(data);
              //console.log(obj);
            $('.fetched-data #name').val(obj['name']);//Show fetched data from database
           // $('.fetched-data #description1').val(obj['description']);
           tinymce.get('description1').setContent(obj['description']);
            $('.fetched-data #term_cond_id').val(obj['term_cond_id']);
          }
        });
      });
  });

  function validateTerms(info){
  var name = document.getElementById('name').value;
  var description1 = tinyMCE.get('description1').getContent();
 // console.log(info.name.value);
  // console.log(description1);

  //var duration = document.getElementById('duration').value;
  if(info.name.value!=null && info.name.value!=0 && info.name.value!='' && (jQuery.trim( name )).length==0 && description1!=''){
    //alert("Plan Name is mandatory, you cannot leave this field empty");
    return(true);
    }
    
    else{
     alert("Please fill all fields!");
      return(false);
    }
}
</script> 



<script type="text/javascript">
  $('#message').delay(5000).fadeOut('slow');

</script>