<?php $this->load->view('admin/header');?>
<div class="right_col" role="main">
	<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="title_left">
           <h3>Testimonials</h3>
       </div>
        <div align="center" class="alert alert-info alert-dismissible fade in" role="alert" id="flash-messages_1" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button><strong id="msg1">Testimonial Story is approved!</strong>
        </div>
        <div align="center" class="alert alert-danger alert-dismissible fade in" role="alert" id="flash-messages_2" style="display: none;">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
              </button><strong id="msg1">Testimonial Story is not approved!</strong>
        </div>
       <div class="x_panel">
          <div class="x_content">
                      <table id="datatable" class="table table-striped table-bordered dataTable">
                        <thead>
                          <tr>
                            <th>Serial No</th>
                            <th>Name</th>
                            <th>Success Story</th>
                            <th class="no-sort">Couple Image</th>
                             <th>Approval</th>
                            <th class="no-sort">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php 
                           $index = 1;
                           foreach ($getstories as $value){
                            $testmonial_id =  $value['testimonial_id']; ?>
                            <tr id="story_<?php echo $value['testimonial_id']; ?>">
                              <th scope="row"><?php echo $index++;?></th>
                                <td><?php echo $value['couple_name']?></td>
                                <td><?php echo $value['story_text']?></td>
                                <td><img src="<?php echo TESTIMONIAL_PIC.$value['couple_image']; ?>" alt="" style="height: 60px; width: 60px;"/></td>
                                <td><?php if($value['is_approve']=='0') {?>
                                <button class="btn btn-success" onclick="is_approve(<?php echo $testmonial_id;?>,1)">Accept</button>
                              <?php }else{?>
                                <button class="btn btn-danger" onclick="is_approve(<?php echo $testmonial_id;?>,0)">Reject</button>
                                <?php }?></td>
                                 <td class="center" style="text-align: center;">
                                    <a href="" class="edit btn btn-sm btn-default"  data-toggle="modal"   data-target="#myModal" data-id="<?php echo $value['testimonial_id'];?>" title="Edit Testimonial"><i class="fa fa-pencil"></i></a>
                                    <a class="delete btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-basic" onclick="deletestory(<?php echo $value['testimonial_id'];?>);" title="Delete Story"><i class="fa fa-trash-o"></i></a>
                                </td> 
                            <?php }?>
                            </tr>
                         </tbody>
                      </table>
          </div>
       </div>
	  </div>
</div>
</div>
<?php $this->load->view('admin/footer');?>
<div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Testimonial/edit_testimonial" enctype="multipart/form-data" name="myForm"/>
              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Testimonial</h4>
                  </div>
                  <div class="modal-body">
                       <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Couple Name</label>
                          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
                            <input  class="form-control" type="hidden" name="testimonial_id" id="testimonial_id">
                            <input type="text" name="couple_name" id="couple_name" class="form-control">
                          </div>
                       </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Success Story</label>
                          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
                         <textarea id="success_story" name="success_story" rows="20" cols="6" class="form-control"></textarea>
                          </div>
                       </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Couple Image</label>
                          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
                          <input type="file" name="couple_image" id="couple_image" class="form-control">
                          </div>
                       </div>
                      
                    </div>
                  <div class="modal-footer">
                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12">
                         <button type="button" id="reset" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                        
                        </div>
                    </div>
                  </div>
              </div>
          </form> 
      </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#datatable').DataTable( {
        "paging":   true,
        //"ordering": true,
        "info":     false,
         "order": [],
        "columnDefs": [ {
          "targets"  : 'no-sort',
          "orderable": false,
        }],
        language: {
          searchPlaceholder: 'Search records'
        }
    } );
} );
</script>
<script type="text/javascript">
   function deletestory(testimonial_id) {
   
    swal({
      title: "Are you sure?", 
      text: "Are you sure that you want to delete this story?", 
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      confirmButtonText: "Yes, delete it!",
      confirmButtonColor: "#ec6c62"
    }, function() {
        $.ajax(
                {
                    type: "POST",
                    url:"<?php echo BASE_URL;?>admin/Testimonial/delete_story",
                    data:{'testimonial_id':testimonial_id},
                    datatype:'json',
                    success: function(data){
                      //console.log(data);
                        $("#story_"+testimonial_id).remove();
                    }
                }
        )
      .done(function(data) {

        swal("Deleted!", "Your Story was successfully Delete!", "success");
        location.reload();
      })
      .error(function(data) {
        swal("Oops", "We couldn't connect to the server!", "error");
      });
    });
  }

</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myModal').on('show.bs.modal', function (e) {
        var testimonial_id = $(e.relatedTarget).data('id');
        //alert(term_cond_id);
        $.ajax({
            type : 'post',
            url : "<?php echo BASE_URL;?>admin/Testimonial/get_testimonial_data",
            data :  'testimonial_id='+ testimonial_id, //Pass $id
            success : function(data){
              var obj = $.parseJSON(data);
              console.log(data);
            $('.fetched-data #couple_name').val(obj['couple_name']);//Show fetched data from database
            $('.fetched-data #success_story').val(obj['story_text']);
            $('.fetched-data #testimonial_id').val(obj['testimonial_id']);
            }
        });
     });
});

  function is_approve(testmonial_id,val) {
// alert(testmonial_id);
// alert(val);
    $.ajax({
            type : 'post',
            url : "<?php echo BASE_URL;?>admin/Testimonial/is_approve_story",
            data : { 'val': val,
                    'testimonial_id':testmonial_id,},
             //Pass $id
            success : function(data){
              var obj = $.parseJSON(data);
              console.log(data);
              if(data>0)
              {
                //  $("#flash-messages_1").show();
                // setTimeout(function() { $("#flash-messages_1").hide(); }, 8000);
                location.reload();
              }else
              {
                // $("#flash-messages_2").show();
                // setTimeout(function() { $("#flash-messages_2").hide(); }, 8000);
                 location.reload();
              }
            }
          });

  }
</script>
