<?php $this->load->view('admin/header');?>
<div class="right_col" role="main">
	<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="title_left">
           <h3>Matches</h3>
       </div>
       <div class="x_panel">
          <div class="x_content">
                      <table id="datatable" class="table table-striped table-bordered dataTable">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Username</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php 
                           $index = 1;
                           foreach ($userlist as $value){?>
                            <tr id="user_<?php echo $value['user_id']; ?>">
                              <th scope="row"><?php echo $index++;?></th>
                                <td><?php echo $value['name']?></td>
                               
                                 <td class="center" style="text-align: center;">
                                    <a class="edit btn btn-sm btn-default"  href="<?php echo BASE_URL?>/admin/Matches/getIndividualMatches/<?php echo $value['user_id'];?>" title="View User"><i class="fa fa-eye"></i></a>
                                    <a class="delete btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-basic" onclick="deleteuser(<?php echo $value['user_id'];?>);" title="Delete User"><i class="fa fa-trash-o"></i></a>
                                </td> 
                            <?php }?>
                            </tr>
                         </tbody>
                      </table>
          </div>
       </div>
	  </div>
</div>
</div>
<?php $this->load->view('admin/footer');?>
<script type="text/javascript">
  $(document).ready(function() {
    $('#datatable').DataTable( {
        "paging":   true,
        "ordering": true,
        "info":     false
    } );
} );
</script>
<script type="text/javascript">
   function deleteuser(user_id) {
   
    swal({
      title: "Are you sure?", 
      text: "Are you sure that you want to delete this User?", 
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      confirmButtonText: "Yes, delete it!",
      confirmButtonColor: "#ec6c62"
    }, function() {
        $.ajax(
                {
                    type: "POST",
                    url:"<?php echo BASE_URL;?>admin/Matches/delete_user",
                    data:{'user_id':user_id},
                    datatype:'json',
                    success: function(data){
                      //console.log(data);
                        $("#user_"+user_id).remove();
                    }
                }
        )
      .done(function(data) {

        swal("Deleted!", "Your User was successfully Delete!", "success");

      })
      .error(function(data) {
        swal("Oops", "We couldn't connect to the server!", "error");
      });
    });
  }

</script>