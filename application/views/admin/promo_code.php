<?php $this->load->view('admin/header');?>
<div class="right_col" role="main">
	<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
     <div class="title_left">
       <h3>Coupan Code</h3>
     </div>
     <?php if($this->session->flashdata('admin_flash')){ ?>
     <div class="alert alert-success alert-dismissible fade in" role="alert" id="message" style="width: 50%; text-align: center; margin-left: 18%;">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <?php  echo $this->session->flashdata('admin_flash'); ?>
    </div>
    <?php }?>
    <div class="x_panel">
      <div class="x_content">
        <div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search">
          <div class="input-group" style="float: right;">
            <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#add_code" >Add Promo Code
            </button>
          </div> 
        </div>
        
        <table id="datatable" class="table table-striped table-bordered dataTable">
          <thead>
            <tr>
              <th>Serial No</th>
              <th>Coupan Code</th>
              <th>Duration</th>
              <th class="no-sort" style="text-align: center;">Action</th>
            </tr>
          </thead>
          <tbody>
           <?php 
           $index = 1;
           foreach ($getpromocode as $value){?>
           <tr id="promo_<?php echo $value['promo_id']; ?>">
            <th scope="row"><?php echo $index++;?></th>
            <td><?php echo $value['code']?></td>
            <td><?php if($value['is_free']==1){echo "Completely Free";}else{echo $value['duration']." Month";}?></td>

            <td class="center" style="text-align: center;">
              <a href="" class="edit btn btn-sm btn-default"  data-toggle="modal"   data-target="#myModal" data-id="<?php echo $value['promo_id'];?>" title="Edit Conditions"><i class="fa fa-pencil"></i></a>
              <a class="delete btn btn-sm btn-danger"  onclick="deletepromocode(<?php echo $value['promo_id'];?>);" title="Delete Promo code"><i class="fa fa-trash-o"></i></a>
            </td> 
            <?php }?>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
<?php $this->load->view('admin/footer');?>
<div id="add_code" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Promo_code/insert_code" id="myform2" enctype="multipart/form-data" onsubmit="return validate(this)"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Promo Code</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <!-- <input  class="form-control" type="hidden" name="plan_id" id="plan_id"> -->
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Enter Coupan Code</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" class="form-control" id="code" placeholder="Enter Promo Code" name="code" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Enter Duration</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
           <input type="number" class="form-control" id="duration" name="duration" style="width: 50%;" min="1" placeholder="Select Duration" />Per Month
         </div>
       </div>
       <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback"></label>
        <div class="col-md-9 col-sm-9 col-xs-12">
          <input type="checkbox"  id="is_free" name="is_free" value="">&nbsp;Click checkbox if you want to free above code.
        </div>
      </div>

    </div>
    <div class="modal-footer">
      <div class="form-group">
        <div class="col-md-9 col-sm-9 col-xs-12">
          <button type="button" id="reset" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </div>
    </div>
  </div>
</form> 
</div>
</div><!--END Create new Job Category Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Promo_code/edit_promocode" id="myform2" enctype="multipart/form-data" onsubmit="return validatePromo(this)" name="myForm"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Promo Code</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Code</label>
          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
            <input  class="form-control" type="hidden" name="promo_id" id="promo_id">
            <input type="text" name="code" id="code" class="form-control" placeholder="Enter Code">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Duration</label>
          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
           <input type="number" class="form-control" id="duration" name="duration" style="width: 50%;" min="1" placeholder="Select Duration" />Per Month
         </div>
       </div>
       <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback"></label>
        <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">

          <input type="checkbox"  id="is_free1" name="is_free1" value="<?php  ?>"/>&nbsp;Click checkbox if you want to free above code.
        </div>
      </div>

    </div>
    <div class="modal-footer">
      <div class="form-group">
        <div class="col-md-9 col-sm-9 col-xs-12">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Submit</button>
          
        </div>
      </div>
    </div>
  </div>
</form> 
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $("#reset").click(function() {
      $(':input','#myform2').val("");    
    });
});
  function validate(info){
    var code = document.getElementById('code').value;
    var duration = document.getElementById('duration').value;
    if((jQuery.trim( code )).length==0 ||
     info.code.value == null){
      alert("Promo Code is mandatory, you cannot leave this field empty");
    return(false);
  } else if((jQuery.trim( duration )).length==0 ||
   info.duration.value == null)
  {
   alert("Duration is mandatory, you cannot leave this field empty");
   return(false);
 }
 else{
  return(true);
}
}
$(document).ready(function() {
  $('#datatable').DataTable( {
    "paging":   true,
        //"ordering": true,
        "info":     false,
        "order": [],
        "columnDefs": [ {
          "targets"  : 'no-sort',
          "orderable": false,
        }],
        language: {
          searchPlaceholder: 'Search records'
        }
      } );
} );
</script>
<script type="text/javascript">
 function deletepromocode(promo_id) {

  swal({
    title: "Are you sure?", 
    text: "Are you sure that you want to delete this promo code?", 
    type: "warning",
    showCancelButton: true,
    closeOnConfirm: false,
    confirmButtonText: "Yes, delete it!",
    confirmButtonColor: "#ec6c62"
  }, function() {
    $.ajax(
    {
      type: "POST",
      url:"<?php echo BASE_URL;?>admin/Promo_code/deletepromocode",
      data:{'promo_id':promo_id},
      datatype:'json',
      success: function(data){
                      //console.log(data);
                      $("#promo_"+promo_id).remove();
                    }
                  }
                  )
    .done(function(data) {

      swal("Deleted!", "Your Promo code was successfully Delete!", "success");
      location.reload();
    })
    .error(function(data) {
      swal("Oops", "We couldn't connect to the server!", "error");
    });
  });
}

</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myModal').on('show.bs.modal', function (e) {
      var promo_id = $(e.relatedTarget).data('id');
        //alert(term_cond_id);
        $.ajax({
          type : 'post',
          url : "<?php echo BASE_URL;?>admin/Promo_code/get_promo_data",
            data :  'promo_id='+ promo_id, //Pass $id
            success : function(data){
              var obj = $.parseJSON(data);
              console.log(obj['is_free']);
            $('.fetched-data #code').val(obj['code']);//Show fetched data from database
            $('.fetched-data #duration').val(obj['duration']);
            $('.fetched-data #promo_id').val(obj['promo_id']);
            // $('#is_free').prop('checked', obj['is_free'] == 1);
            $('.fetched-data #is_free1').val(obj['is_free']);
            if(obj['is_free']=="1" || obj['is_free']==1)
            {
               // console.log('hi');
               $("#is_free1").prop('checked', true); 

             }else
             {
                //console.log('bye');
                $('#is_free1').prop('checked',false);


              }
            }
          });
      });
  });
  function validatePromo(info){
    var code = document.getElementById('code').value;
    var duration = document.getElementById('duration').value;
    if(info.code.value!=null && info.code.value!=0 && info.code.value!='' && (jQuery.trim( code )).length==0 && info.duration.value!=null && info.duration.value!=0 && info.duration.value!='' && (jQuery.trim( duration )).length==0){
      
    return(true);
  } 
 else{
  alert('Please fill all details!')
  return(false);
}
}
</script> 

