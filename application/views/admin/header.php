<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?php echo ADMIN_ASSETS; ?>img/logo123.png">
	<title><?php echo SITE_NAME; ?> </title>

	<!-- Bootstrap -->
	<link href="<?php echo ADMIN_ASSETS."css/bootstrap.min.css"?>" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo ADMIN_ASSETS."css/font-awesome.min.css"?>" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo ADMIN_ASSETS."css/nprogress.css"?>" rel="stylesheet">
	<!-- Animate.css -->
	<link href="<?php echo ADMIN_ASSETS."css/animate.min.css"?>" rel="stylesheet">

	<!-- iCheck -->
	<link href="<?php echo ADMIN_ASSETS."css/green.css"?>" rel="stylesheet">
	<!-- bootstrap-progressbar -->
	<link href="<?php echo ADMIN_ASSETS."css/prettify.min.css"?>" rel="stylesheet">
	<!-- Datatables -->
	
	<link href="<?php echo ADMIN_ASSETS."css/dataTables.bootstrap.min.css"?>" rel="stylesheet">
	<link href="<?php echo ADMIN_ASSETS."css/buttons.bootstrap.min.css"?>" rel="stylesheet">
	<link href="<?php echo ADMIN_ASSETS."css/fixedHeader.bootstrap.min.css"?>" rel="stylesheet">
	<link href="<?php echo ADMIN_ASSETS."css/responsive.bootstrap.min.css"?>" rel="stylesheet">
	<link href="<?php echo ADMIN_ASSETS."css/scroller.bootstrap.min.css"?>" rel="stylesheet">


	<link href="<?php echo ADMIN_ASSETS."css/bootstrap-progressbar-3.3.4.min.css"?>" rel="stylesheet">
	<!-- JQVMap -->
	<link href="<?php echo ADMIN_ASSETS."css/jqvmap.min.css"?>" rel="stylesheet"/>
	<!-- bootstrap-daterangepicker -->


	<!-- Custom Theme Style -->
	<link href="<?php echo ADMIN_ASSETS."css/switchery.min.css"?>" rel="stylesheet">
	<link href="<?php echo ADMIN_ASSETS."css/starrr.css"?>" rel="stylesheet">
	<link href="<?php echo ADMIN_ASSETS."css/select2.min.css"?>" rel="stylesheet">

	<link href="<?php echo ADMIN_ASSETS."css/daterangepicker.css"?>" rel="stylesheet">
	<link href="<?php echo ADMIN_ASSETS."css/custom.min.css"?>" rel="stylesheet">
	<!-- <link href="<?php echo ADMIN_ASSETS."css/custom.css"?>" rel="stylesheet"> -->
	<link href="<?php echo ADMIN_ASSETS."css/sweetalert.css"?>" rel="stylesheet">
	
</head>

<style type="text/css">
	
	.nav li.current-page a {
	    color:red ;
	}
</style>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="" class="site_title"><img src="<?php echo ADMIN_ASSETS; ?>img/icon/logo1.png"><!-- <span><?php //echo SITE_NAME; ?></span> --></a>
					</div>

					<div class="clearfix"></div>

					<!-- menu profile quick info -->
					<!-- <div class="profile">
						<div class="profile_pic">
							
							<img src="<?php //echo ADMIN_ASSETS.'img/images/default_image.png'?>" alt="..." class="img-rounded profile_img" style="width: 100%;">

						</div>

					</div> -->
					<!-- /menu profile quick info -->
					<br />
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_setion">

							<!-- <?php if($this->session->userdata('user_email')=='admin@admin.com'){ 
									echo "<span>Welcome</span>";
									echo "<h2>Admin</h2>"; 
									}else {
										echo "<span>Welcome</span>";
										echo "<h2>";?> 
											<?php echo $this->session->userdata('user_name');?>
									<?php echo "</h2>";
									} ?> -->
							<ul class="nav side-menu">
								<?php 
					                   // echo '<pre>';
					                   // print_r($leftmenu);die();
								foreach ($leftmenu as $key => $value) 
							   { ?>
								<li <?php if($active == '0'){?> class='active' <?php  }?> >
									<a href="
										<?php if($value['module_link']!=''){ echo BASE_URL.$value['module_link']; } else echo "#";?>">
										<i class="<?php echo $value['icon'];?>"></i>
										<?php echo  $value['module_name'];?>
										
									</a>
									<?php $submodule = $this->Module_model->get_sub_modules($value['module_id']);
									if(count($submodule)>0){?>
										<ul class="nav child_menu">
											<?php foreach ($submodule as $key => $value)
											{ ?>
												<li>
												<a href="<?php echo BASE_URL.$value['module_link'];?>"><?php echo $value['module_name'];?></a>
												</li>
											<?php }?>
										</ul>
									<?php } ?>
								</li>
							<?php } ?>

							</ul>
						</div>
					</div>
					<!-- /sidebar menu -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class="">
								<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								Hi,
									        <!-- <img src="<?php 
                if($this->session->userdata('admin_session_data')['user_profile']!="")
                { echo ADMIN_PROFILE.$this->session->userdata('admin_session_data')['user_profile']; }
              else
              {
                echo ADMIN_PROFILE.'default.png';
              }?>" style="height: 40px; width: 40px;" onerror="this.onerror=null;" alt="...">  -->
									<!-- <img src="<?php echo $this->session->userdata('user_profile');?>" onerror="this.onerror=null;this.src='<?php //echo ADMIN_ASSETS.'images/default_image.png';?>';" alt="..." > --><?php echo $this->session->userdata('admin_session_data')['user_name'] ?>
									<span class=" fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu pull-right">
									<li><a href="<?php echo BASE_URL;?>admin/Login/getProfile">Profile</a></li>
									<li>
									 <a href="<?php echo BASE_URL;?>admin/Login/admin_logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
									</li>
									
								</ul>
							</li>


						</ul>
					</nav>
				</div>
			</div>
        	<!-- /top navigation -->