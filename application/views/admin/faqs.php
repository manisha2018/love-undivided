<?php $this->load->view('admin/header');?>
<div class="right_col" role="main">
	<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 paragraph">
     <div class="title_left">
       <h3>FAQs</h3>
     </div>
     <?php if($this->session->flashdata('admin_flash')){ ?>
        <div class="alert alert-success alert-dismissible fade in" role="alert" id="message" style="width: 50%; text-align: center; margin-left: 18%;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <?php  echo $this->session->flashdata('admin_flash'); ?>
        </div>
        <?php }?>
     <div class="x_panel">
      <div class="x_content">
        <div class="col-md-3 col-sm-3 col-xs-12 form-group pull-right top_search">
          <div class="input-group" style="float: right;">
            <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#add_faqs" >Add FAQs
            </button>
          </div> 
        </div>
       
        <table id="datatable" class="table table-striped table-bordered dataTable">
          <thead>
            <tr>
              <th>Serial No</th>
              <th>Question</th>
              <th>Answer</th>
              <!--  <th>Duration</th> -->
              <th class="no-sort">Action</th>
            </tr>
          </thead>
          <tbody>
           <?php
           $index=1;
           foreach ($getFaqs as $value){ 
            $faq_id = $value['faq_id'];?>
            <tr id="faq_<?php echo $value['faq_id']; ?>">
             <th scope="row"><?php echo $index++; ?></th>
             <td><?php echo $value['question']?></td>
             <td><?php echo $value['answer']?></td>
             <!-- <td><?php echo $value['duration']?></td> -->
             <td class="center" style="text-align: center;">
               <a href="" class="edit btn btn-sm btn-default"  data-toggle="modal"   data-target="#myModal" data-id="<?php echo $value['faq_id'];?>" title="Edit faq"><i class="fa fa-pencil"></i></a>
               <a class="delete btn btn-sm btn-danger"  onclick="deleteFaq(<?php echo $value['faq_id'];?>);" title="Delete Faq"><i class="fa fa-trash-o"></i></a>
             </td> 
           </tr>
           <?php }?>
         </tbody>
       </table>
     </div>
   </div>
 </div>
</div>
</div>

<div id="add_faqs" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/FAQs/insert_faqs" id="myform2" onsubmit="return validate(this)" enctype="multipart/form-data"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add FAQs</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input  class="form-control" type="hidden" name="faq_id" id="faq_id">
            <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Enter Question</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
             <textarea id="question" rows="4" cols="4" name="question" style="width: 100%; resize: none;"></textarea>
           </div>
         </div>
         <div class="form-group">
           <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Enter Answer</label>
           <div class="col-md-9 col-sm-9 col-xs-12">
             <textarea id="answer" rows="4" cols="4" name="answer" style="width: 100%; resize: none;"></textarea>
           </div>
         </div>
       </div>
       <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-9 col-sm-9 col-xs-12">
            <button type="button" id="reset" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </form> 
</div>
</div>
<!--END Create new Job Category Modal -->

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/FAQs/edit_faqs" enctype="multipart/form-data" name="myForm"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Faqs</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
           <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Question</label>
           <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
            <input  class="form-control" type="hidden" name="faq_id" id="faq_id">
            <textarea id="question" name="question"  class="form-control"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Answer</label>
          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
           <textarea id="answer" name="answer"  class="form-control"></textarea>
         </div>
       </div>

     </div>
     <div class="modal-footer">
      <div class="form-group">
        <div class="col-md-9 col-sm-9 col-xs-12">
          <button type="button" id="reset" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </div>
    </div>
  </div>
</form> 
</div>
</div>
<!--END Create new Job Category Modal -->

<?php $this->load->view('admin/footer');?>
<script type="text/javascript">
 $(document).ready(function() {
    $("#reset").click(function() {
      $(':input','#myform2').val("");
       //$('#description','#myform2').val("");    
    });
  });
  function validate(info){
    var question = document.getElementById('question').value;
    var answer = document.getElementById('answer').value;
    if((jQuery.trim( question )).length==0 ||
     info.question.value == null){
      alert("Question is mandatory, you cannot leave this field empty");
    return(false);
  } else if((jQuery.trim( answer )).length==0 ||
   info.answer.value == null)
  {
   alert("Answer is mandatory, you cannot leave this field empty");
   return(false);
 }
 else{
  return(true);
}
}
$(document).ready(function() {
  $('#datatable').DataTable( {
    "paging":   true,
        //"ordering": true,
        "info":     false,
        "order": [],
        "columnDefs": [ {
          "targets"  : 'no-sort',
          "orderable": false,
        }],
        language: {
          searchPlaceholder: 'Search records'
        }
      } );
} );
</script>
<script type="text/javascript">
 function deleteFaq(faq_id) {

  swal({
    title: "Are you sure?", 
    text: "Are you sure that you want to delete this Faq?", 
    type: "warning",
    showCancelButton: true,
    closeOnConfirm: false,
    confirmButtonText: "Yes, delete it!",
    confirmButtonColor: "#ec6c62"
  }, function() {
    $.ajax(
    {
      type: "POST",
      url:"<?php echo BASE_URL;?>admin/FAQs/delete_faqs",
      data:{'faq_id':faq_id},
      datatype:'json',
      success: function(data){
        console.log(data);
        $("#faq_"+faq_id).remove();
      }
    }
    )
    .done(function(data) {

      swal("Deleted!", "Your FAQs was successfully Delete!", "success");
      location.reload();
    })
    .error(function(data) {
      swal("Oops", "We couldn't connect to the server!", "error");
    });
  });
}

</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myModal').on('show.bs.modal', function (e) {
      var faq_id = $(e.relatedTarget).data('id');
        //alert(term_cond_id);
        $.ajax({
          type : 'post',
          url : "<?php echo BASE_URL;?>admin/FAQs/get_faqs_data",
            data :  'faq_id='+ faq_id, //Pass $id
            success : function(data){
              var obj = $.parseJSON(data);
              //console.log(obj);
            $('.fetched-data #question').val(obj['question']);//Show fetched data from database
            $('.fetched-data #answer').val(obj['answer']);
            $('.fetched-data #faq_id').val(obj['faq_id']);
          }
        });
      });
  });
</script> 



<script type="text/javascript">
  $('#message').delay(5000).fadeOut('slow');

</script>