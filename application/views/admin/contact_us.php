<?php $this->load->view('admin/header');?>
<div class="right_col" role="main">
 <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
   <div class="title_left">
     <h3>Contact Us Details</h3>
   </div>
   <?php if($this->session->flashdata('admin_flash')){ ?>
   <div class="alert alert-success alert-dismissible fade in" role="alert" id="message" style="width: 50%; text-align: center; margin-left: 18%;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php  echo $this->session->flashdata('admin_flash'); ?>
  </div>
  <?php }?>
  <div class="x_panel">
    <div class="x_content">

      <table id="datatable" class="table table-striped table-bordered dataTable">
        <thead>
          <tr>
            <th>Serial No</th>
            <th>Username</th>
            <th>Email</th>
            <th>Subject</th>
            <th>Content</th>
            <th class="no-sort" style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
         <?php
         $index=1;
         foreach ($getcontacts as $value){ 
          $contact_id = $value['contact_us_id'];
          ?>
          <tr id="contact_<?php echo $value['contact_us_id']; ?>">
            <th scope="row"><?php echo $index++; ?></th>
            <td><?php echo $value['name']?></td>
            <td><?php echo $value['email']?></td>
            <td><?php echo $value['subject']?></td>
            <td><?php echo $value['content']?></td>
            <td class="center" style="text-align: center;">
              <!-- <a href="" class="edit btn btn-sm btn-default"  data-toggle="modal"   data-target="#myModal" data-id="<?php echo $value['contact_us_id'];?>" title="Edit Category"><i class="fa fa-pencil"></i></a> -->
              <a class="delete btn btn-sm btn-danger"  onclick="deletecontact(<?php echo $value['contact_us_id'];?>);" title="Delete Contact"><i class="fa fa-trash-o"></i></a>
            </td> 
          </tr>
          <?php }?>
        </tbody> 
      </table> 
    </div>
  </div>
</div>
</div>
</div>

<div id="add_category" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Categories/insert_category" enctype="multipart/form-data"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Category</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Category Name</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" class="form-control" id="category_name" name="category_name" required>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-9 col-sm-9 col-xs-12">

            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Submit</button>

          </div>
        </div>
      </div>
    </div>
  </form> 
</div>
</div><!--END Create new Job Category Modal -->

<div id="edit_category" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Categories/insert_category" enctype="multipart/form-data"/>

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Category</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Category Name</label>
          <div class="col-md-9 col-sm-9 col-xs-12">
            <input type="text" class="form-control" id="category_name" name="category_name" required>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-9 col-sm-9 col-xs-12">

            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Submit</button>

          </div>
        </div>
      </div>
    </div>
  </form> 
</div>
</div><!--END Create new Job Category Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="<?php echo BASE_URL;?>admin/Categories/edit_cat_data" enctype="multipart/form-data"/>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Category</h4>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12 has-feedback">Category Name</label>

          <div class="col-md-9 col-sm-9 col-xs-12 fetched-data">
           <input  class="form-control" type="hidden" name="category_id" id="category_id">
           <input  class="form-control" type="text" name="category_name" id="category_name">

         </div>
       </div>
     </div>
     <div class="modal-footer">
      <div class="form-group">
        <div class="col-md-9 col-sm-9 col-xs-12">

          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Submit</button>

        </div>
      </div>
    </div>
  </div>
</form> 
</div>
</div><!--END Create new Job Category Modal -->

<?php $this->load->view('admin/footer');?>
<script type="text/javascript">
 $(document).ready(function() {
    $('#datatable').DataTable( {
      "paging":   true,
     // "ordering": true,
      "info":     false,
        "order": [],
        "columnDefs": [ {
        "targets"  : 'no-sort',
        "orderable": false,
        }],
         language: {
        searchPlaceholder: 'Search records'
      },
       
    } );
  } );
</script>
<script type="text/javascript">
 function deletecontact(contact_us_id) {

  swal({
    title: "Are you sure?", 
    text: "Are you sure that you want to delete this Contact?", 
    type: "warning",
    showCancelButton: true,
    closeOnConfirm: false,
    confirmButtonText: "Yes, delete it!",
    confirmButtonColor: "#ec6c62"
  }, function() {
    $.ajax(
    {
      type: "POST",
      url:"<?php echo BASE_URL;?>admin/Contact_us/delete_contact",
      data:{'contact_us_id':contact_us_id},
      datatype:'json',
      success: function(data){
        console.log(data);
        $("#contact_"+contact_us_id).remove();
      }
    }
    )
    .done(function(data) {

      swal("Deleted!", "Your Contact was successfully Delete!", "success");
      location.reload();
    })
    .error(function(data) {
      swal("Oops", "We couldn't connect to the server!", "error");
    });
  });
}

</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myModal').on('show.bs.modal', function (e) {
      var category_id = $(e.relatedTarget).data('id');
       // alert(category_id);
       $.ajax({
        type : 'post',
        url : "<?php echo BASE_URL;?>admin/Categories/get_cat_data",
            data :  'category_id='+ category_id, //Pass $id
            success : function(data){
              var obj = $.parseJSON(data);
              console.log(obj);
            $('.fetched-data #category_name').val(obj['category_name']);//Show fetched data from database
            $('.fetched-data #category_id').val(obj['category_id']);
            
          }
        });
     });
  });
</script>



<script type="text/javascript">
  $('#message').delay(2000).fadeOut('slow');

</script>