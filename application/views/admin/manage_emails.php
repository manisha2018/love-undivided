<?php $this->load->view('admin/header');?>
<div class="right_col" role="main">
  <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="title_left">
           <h3>Send Email</h3>
       </div>
      <?php if($this->session->flashdata('admin_flash')){ ?>
          <div class="alert alert-info alert-dismissible fade in" role="alert" id="message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <?php  echo $this->session->flashdata('admin_flash'); ?>
          </div>
      <?php }?>
       <div class="x_panel">
          	<form role="form" class="form-validation" action="<?php echo BASE_URL?>admin/Bulk_Emails/send_mail" method="post" enctype="multipart/form-data" onsubmit="return validate(this)" > 
                     <div class="form-group row"><label for="email subject" class="col-sm-2 form-control-label">To</label>
                          <div class="col-sm-10"> 
                          <select multiple="multiple" class="form-control" id="email_tags" name="email_tags[]" data-placeholder="Recipient" style="width: 100%;">
                                         <option></option>
                          </select>
                         </div>
                    </div>

                    <div class="form-group row"><label for="email subject" class="col-sm-2 form-control-label">Subject</label>
                          <div class="col-sm-10"> 
                          <input type="text" class="form-control" name="email_subject" id="email_subject" placeholder="Enter Subject" required="required"/> 
                         </div>
                    </div>
                     <div class="form-group row"><label for="email subject" class="col-sm-2 form-control-label">Coupon Code</label>
                          <div class="col-sm-10"> 
                          <!-- <input type="text" class="form-control" name="email_subject" id="email_subject" placeholder="Enter Subject" required="required"/>  -->
                          <select id="coupan_code" name="coupan_code" class="form-control">
                          <option value="">Select Coupon Code</option>
                          <?php foreach ($coupan_code as $key => $value) {?>
                           <option value="<?php echo $value['code']; ?>"><?php echo $value['code'];?></option>
                          <?php } ?>
                           
                          </select>
                         </div>
                    </div>
                    <div class="form-group row"><label for="email body" class="col-sm-2 form-control-label">Content</label>
                          <div class="col-sm-10"><textarea name="email_body"  class="form-control" id="email_body" required="required" >[name]</textarea>
                          </div>
                    </div>
                    <div class="form-group row" id="check"><label for="Multiple Users" class="col-sm-2  form-control-label">Multiple Users</label>
                          <div class="col-sm-10">
	                      <input type="checkbox" class="myCheckbox" id="all_users" name="all_users" value="1" />
	                            Please Select Checkbox For Sending Mail to All Users
                    	   </div>		
                    </div>
                    <div class="form-group row" id="region" ><label for="Select Region" class="col-sm-2 form-control-label" >Select Region</label>
                        	<div class="col-sm-10">
                        	<div id="pac-card"></div>
	                        <input type="text" name="city" id="city" onblur="checkTextField(this);" class="form-control" placeholder="Enter Region"/>
	                        <div id ="map"></div>
                            </div>
                    </div>
                    <div class="form-group row" id="age" ><label for="Select Region" class="col-sm-2 form-control-label" >Select Age</label>
                          <div class="col-sm-10">
                              <select onchange="check_age(this,'1');" id="age1" name="age1">
                              <option value="1">Select Age</option>
                                        <?php 
                                        for ($i=18; $i < 99; $i++) { 
                                        ?>    
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                        <?php                                         
                                        } ?>
                              </select> - 
                              <select onchange="check_age(this,'2');" id="age2" name="age2"> 
                                <option value="1">Select Age</option>
                                        <?php
                                        for ($i=18; $i < 99; $i++) { 
                                        ?>    
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                        <?php                                         
                                        } ?>
                              </select>
                          </div>
                    </div>
                   <hr>
                   <div style="color: red"><b>Note</b>: Use [promo_code] in body of message while sending any promotional code.</div>
                    <div class="text-center  m-t-20">
                     <button type="button" id="reset" class="btn btn-danger" data-dismiss="modal">Close</button>
                     <button type="submit" class="btn btn-success">Submit</button>
                      <!--   <button type="submit" class="btn btn-embossed btn-success" id="btnSubmit">Submit</button>
                        <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button> -->
                    </div>
                    <hr>
            </form>
       </div>
	</div>
  </div>
</div>
<?php $this->load->view('admin/footer');?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvC5FFDLnQCAM4RchvZ5z1_MNoYeQPBhA&libraries=places&callback=initMap" async defer></script>
<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=g6iradqrc3uphnkw5fq5toa9o02ahdlucazem1m1u1s7tn2i'></script> 

<script type="text/javascript">
 function validate(info){
    var email_subject = document.getElementById('email_subject').value;
    var coupan_code = document.getElementById('coupan_code').value;
    if((jQuery.trim( email_subject )).length==0 ||
     info.email_subject.value == null){
      alert("Email Subject is mandatory, you cannot leave this field empty!");
    return(false);
  } else if((jQuery.trim( coupan_code )).length==0 ||
   info.coupan_code.value == null)
  {
   alert("Coupon Code is mandatory, you have to choose one of the code!");
   return(false);
 }
 else{
  return(true);
}
}
	function check_age(obj_age,index)
        {
            let val = obj_age.value;
            let age_obj = {};
            if(index == '1')
            {
               
                if(val > $('#age2').val()) 
                {
                    $('#age2').val(val)
                    //return false;
                }
                age_obj.age1 = $('#age1').val();
                age_obj.age2 = $('#age2').val();
                //alert('1');
                func_filter_match('','2',age_obj);


            }
            else if(index == '2')
            {
                if(val < $('#age1').val()) 
                {
                    $('#age1').val(val)
                    //return false;
                }
               

                age_obj.age1 = $('#age1').val();
                age_obj.age2 = $('#age2').val();
                func_filter_match('','2',age_obj);

            }


        }

  function func_filter_match(obj_filter,type,age_obj) {
            let val = obj_filter.value;
            let data = {};
            if(type == '1' || type == '3')
            {
                data = {data:val,type:type};
                console.log(data);
            }
            else if(type == '2')
            {
                data = {age1:age_obj.age1,age2:age_obj.age2,type:type};
            }
           


            $.ajax({
                url: "<?php echo BASE_URL;?>api/User_matches/filter_matches",
                method: "POST",
                data:data,
                dataType: "html",
                success:function(html)
                { 
                    $('.matc-pro').html(html);
                  console.log(html);
                }
            });
            //alert(val);
        }

  function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 50.064192, lng: -130.605469},
          zoom: 3
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('city');
       

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions(
            {'country': ['us', 'pr', 'vi', 'gu', 'mp']});

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);
        });

       
      }

      tinymce.init({
    selector: '#email_body'
  });

$(document).on('click', '.myCheckbox', function () {
    var target = $(this).data('target');    
    if ($(this).is(':checked')) {
      //alert("hi");
      $("#region").hide();
      $("#age").hide();

    }
    else {
         $("#region").show();
         $("#age").show();
    }
});

function checkTextField(field) {

  //var val = document.getElementById("age").value;
 if ( $("#city").val().trim().length === 0 ) 
  {
    $('#age').show();
    $('#check').show();
  }else
  {
    $('#age').hide();
    $('#check').hide();
  }
}
$("#email_tags").change(function(){
  var email=$('#email_tags option:last-child').val();
  //alert(email);
  var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if($.trim($('#email_tags').val())=='')
    {
      $('#region').show();
      $('#check').show();
      $('#age').show();
      $('#all_users').show();
    }
    else if($.trim(email).match(pattern))
    {
      
      $('#region').hide();
      $('#check').hide();
      $('#age').hide();
      $('#all_users').hide();
      return true;
   }else
   {
      $('#region').hide();
      $('#all_users').hide();
      $('#age').hide();
      alert("Please Enter Valid Email Address!");
   }


});
</script>
<script type="text/javascript">
    $("#email_tags").select2({
       tags: true
    })

</script>

