<?php
class Subscription extends CI_Controller 
{

    public function __construct() 
    {
        parent:: __construct();
        $this->load->model('api/Subscription_model');
        $this->load->model('api/User_Login_model');
       // $this->load->helper('utility_helper');
        if(empty($this->session->userdata('session_data')['user_id'])){
          redirect(BASE_URL.'login');
        }

        require_once('vendor/autoload.php');
        \Stripe\Stripe::setApiKey(STRIPE_KEY);

    }

    public function index()
    {
        $token_card = $this->input->post('token_card');

        $plan_id = $this->input->post('plan_id');
        $is_unblur = $this->input->post('is_unblur');
        $amount = $this->input->post('amount') * 100;
        // echo $token_card;
        // die();
        $user_id = $this->session->userdata('session_data')['user_id'];
       
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $result = \Stripe\Customer::create(array(
            "email"=>$email,
            "description" => "Customer for ".$email,
            "source" =>  $token_card // obtained with Stripe.js
        ));
        
        if(!empty($result->id))
        {
            $data = array('stripe_id'=>$result->id);
            $this->User_Login_model->update_user($user_id,$data);

            
            \Stripe\Charge::create(array(
              "amount" => $amount,
              "currency" => "usd",
              'customer' => $result->id,
              "description" => "Charge for natalie.davis@example.com"
            ));
        }   
       // print_r($result);

        // $input_data = array(
        //         'user_id' => $user_id,
        //         'end_date' => 
        //     );
        $sql ="SELECT user_id FROM `user_subscription` where user_id = '$user_id'";
        $userExist = $this->Subscription_model->checkUserExist($sql);
        //print_r($userExist);die
        if($userExist>0)
        {
           $update =  $this->Subscription_model->updateSubscription($user_id,$plan_id);
           if($update>0)
           {

                $getplans = $this->Subscription_model->getPlans($plan_id);
                $plan = $getplans[0]['amount'];
                $duration = $getplans[0]['duration_month'];
                $message = '<b><i>Hello '.$name.',<br/>Thank You for subscribing to the '.$plan.'. Your plan will expire in '.$duration.'<br><br> Along with contact email for any questions or concerns <b>Questions@LoveUndivided.com</b>';
                   $msg['message_array'] = array($message);
                    //print_r($msg);
                    //$template = $this->load->view('api/email_template',$msg,true);
                    $to =  $email;
                    $subject = 'payment done successfully';   
                   $send =  sendMail($to,$subject,$template);
                   echo $send;
        
            
           }

        }else
        {
            $add = $this->Subscription_model->add_subscription($user_id,$plan_id);
            if($a>0)
            {
                $getplans = $this->Subscription_model->getPlans($plan_id);
                $plan = $getplans[0]['amount'];
                $duration = $getplans[0]['duration_month'];
                $message = '<b><i>Hello '.$name.',<br/>Thank You for subscribing to the '.$plan.'. Your plan will expire in '.$duration.'<br><br> Along with contact email for any questions or concerns <b>Questions@LoveUndivided.com</b>';
                   $msg['message_array'] = array($message);
                    //print_r($msg);
                    //$template = $this->load->view('api/email_template',$msg,true);
                    $to =  $email;
                    $subject = 'payment done successfully';   
                   $send =  sendMail($to,$subject,$template);
                   echo $send;
        
            }
        }

       
        

        // if($is_unblur == 1)
        //     $this->User_Login_model->update_user($user_id,['is_unblur_active'=>'1']);

        // echo $result;

    }
}