
app.controller("LoginController", ["$scope", "$location", "$window", "authenticationSvc","$http","GooglePlus","TheLinkedin",
  function ($scope, $location, $window, authenticationSvc,$http,GooglePlus,TheLinkedin) {
    $scope.userInfo = null;
    var Base_url="http://bizmoapps.com/ydbg/api/";

      

    $scope.singIn = function () {
        authenticationSvc.login($scope.email, $scope.password,$scope.type='custom')
            .then(function (result) {
                $scope.userInfo = result;
                $.amaran({
                            'message'   :'Login Successfully!!',
                            'position'  :'top left'
                        });
                 $location.path("/Web-notification/");
            },function (error) {
                //$window.alert("Invalid credentials");
                $.amaran({
                                  'message'   :'Invalid credentials ',
                                  'position'  :'top left'
                            });
                console.log(error);
            });
    };

    $scope.glogin = function (user) {
       authenticationSvc.Slogin(user,$scope.type='googleplus')
            .then(function (result) {
                $scope.userInfo = result;
                $.amaran({
                            'message'   :'Login Successfully!!',
                            'position'  :'top left'
                        });
                 $location.path("/Web-notification/");
            },function (error) {
                //$window.alert("Invalid credentials");
                $.amaran({
                                  'message'   :'Invalid credentials ',
                                  'position'  :'top left'
                            });
                console.log(error);
            });
    };

    $scope.fbLogin = function (user) {

      if (!user.email)
      {
       $('#myModal').modal('show'); 

        authenticationSvc.fLogin(user,$scope.femail,$scope.type='facebook')
            .then(function (result) {
                $scope.userInfo = result;
                $.amaran({
                            'message'   :'Login Successfully!!',
                            'position'  :'top left'
                        });
                 $location.path("/Web-notification/");
            },function (error) {
                //$window.alert("Invalid credentials");
                $.amaran({
                                  'message'   :'Invalid credentials ',
                                  'position'  :'top left'
                            });
                console.log(error);
            });

      }else{

       authenticationSvc.fLogin(user,$scope.type='facebook')
            .then(function (result) {
                $scope.userInfo = result;
                $.amaran({
                            'message'   :'Login Successfully!!',
                            'position'  :'top left'
                        });
                 $location.path("/Web-notification/");
            },function (error) {
                //$window.alert("Invalid credentials");
                $.amaran({
                                  'message'   :'Invalid credentials ',
                                  'position'  :'top left'
                            });
                console.log(error);
            });
      }

    }


    $scope.linkedin = function () {
        TheLinkedin.login().then(function(response){
               console.log(response);
          $scope.linked_In(response);
        });
      };
    $scope.linked_In=function(user){
       authenticationSvc.linkedin(user,$scope.type='linked_in')
            .then(function (result) {
                $scope.userInfo = result;
                $.amaran({
                            'message'   :'Login Successfully!!',
                            'position'  :'top left'
                        });
                 $location.path("/Web-notification/");
            },function (error) {
                //$window.alert("Invalid credentials");
                $.amaran({
                                  'message'   :'Invalid credentials ',
                                  'position'  :'top left'
                            });
                console.log(error);
            });
    }

    $scope.google = function () {
        GooglePlus.login().then(function (authResult) {
            // console.log(authResult);
               GooglePlus.getUser().then(function (user) {


                console.log(user);
                 $scope.glogin(user)
            });
        }, function (err) {
            console.log(err);
        });
    };

    $scope.fb=function(){
      FB.login(function(response) {
        if (response.authResponse) {
         
         FB.api('/me?fields=id,first_name,last_name,email', function(response) {
          $scope.fbLogin(response);
          console.log(response);
           
         }),{'scope':'email'});
        } else {
         console.log('User cancelled login or did not fully authorize.');
        }
    });
    }


    $scope.SingUp = function (user) {
           // use $.param jQuery function to serialize data from JSON 
            var data = $.param({

                   user_fname:user.user_fname,
                  user_lname:user.user_lname, 
                  user_email:user.user_email,
                  user_company:user.user_company,
                  user_phone:user.user_phone,
                   user_password:user.user_password
            });
            $http.post(Base_url+'Login/register', data)
            .success(function (data, status, headers, config) 
            {
               $.amaran({
                                  'message'   :'User Registered Successfully!!',
                                  'position'  :'top left'
                            });
                
                   $location.path("/login");
                   $scope.user=data;
            })
            .error(function (data) {
                $scope.ResponseDetails = "Data: " + data +
                    "<hr />status: " + status ;
            });
        };

         $scope.for_password=function(user){
          var data = $.param({user_email:user.email });
            $http.post(Base_url+'Login/forgot_password', data)
            .success(function (data, status, headers, config) 
            {
              if (data.error) {
                $.amaran({
                                  'message'   :data.error,
                                  'position'  :'top left'
                            });
                 
              }else{
                  $.amaran({
                                  'message'   :data,
                                  'position'  :'top left'
                            });
              }                
            })
            .error(function (data) {
                $.amaran({
                                  'message'   :data.error,
                                  'position'  :'top left'
                            });
                 
            });
         }
    $scope.cancel = function () {
        $scope.userName = "";
        $scope.password = "";
    }; 
}]);



app.controller("HomeController", ["$scope", "$location", "authenticationSvc", "auth","$http",
  function ($scope, $location, authenticationSvc, auth,$http) 
{
  $scope.userprofile = null;
  var Base_url="http://bizmoapps.com/ydbg/api/";
    $scope.userInfo = auth;
   
    
     
    $scope.hide=true;
    $scope.Logout = function () 
    {
        authenticationSvc.logout()
            .then(function (result) {
                $scope.userInfo = null;
                $location.path("/login");
                $.amaran({
                                  'message'   :'Logout Successfully!!',
                                  'position'  :'top left'
                            });
            }, function (error) {
                console.log(error);
            });
    };
    $scope.lifestyle=[
                        { title: 'Vegetarian', id: 1 },
                        { title: 'Runner', id: 2 },
                        { title: 'Yogini', id: 3 },
                        { title: 'Vegan', id: 4 },
                        { title: 'Vegetaria', id: 5 },
                        { title: 'Runne', id: 6 },
                        { title: 'Yogin', id: 7 },
                        { title: 'Vega' , id: 8 }
                      ];

    $scope.feed=function(uid,cat_id)
    {
          var data = $.param({ user_id:uid, category_id:cat_id});
          
            $http.post(Base_url+'Feeds/get_watching_feeds', data)
            .success(function (data, status, headers, config) 
            {
              if (data.error==undefined) {  
              $scope.feeds=data;
              $scope.cat_id=cat_id;
            }else
            {
              alert("no feed found");
            }
            })
            .error(function(data, status,response) { 
            $scope.error=data.error;
             $scope.cat_id=cat_id;
             });
      }

      $scope.is_inspire=function(user_id,parent_id,is_inspire)
      {
         var data = $.param({user_id:user_id  ,parent_id: parent_id, parent_type:'0' });
            if (is_inspire=='0') 
            {
                 $http.post(Base_url+'Feeds/add_inspire_feeds', data)
                .success(function (data, status, headers, config) 
                {
                  $scope.feed(user_id,'0');
                })
                .error(function(data, status,response) { 

                $scope.error=data.error;

                 });
            }
            else
            {
              $http.post(Base_url+'Feeds/remove_inspire_feeds', data)
              .success(function (data) 
              {
                $scope.feed(user_id,'0');
              })
              .error(function(data, status,response) { 

              $scope.error=data.error;
               });
            }
      }
      $scope.Myfeed=function(x)
      {
             var data = $.param({profile_id: x});
                  $http.post(Base_url+'Feeds/get_post_feed', data)
                  .success(function (data) 
                  {
                    $scope.myfeed=data;
                    // alert("no feed found");
                  })
                  .error(function(data, status,response) { 
                     alert("no feed found");
                  $scope.error='No Feeds Found';

                  });
        }
        $scope.Usr_profile = function (x) 
        {
                 // use $.param jQuery function to serialize data from JSON 
                
                  var data = $.param({profile_id: x ,user_id:x});
                  $http.post(Base_url+'Athlete/get_athletes', data)
                  .success(function (data, status, headers, config) 
                  {
                    $scope.prof=data;
                  })
                  .error(function(data, status,response) { 
                  console.log(response);
                   });
        }
        $scope.Player_profile = function (profile_id) 
        {
          authenticationSvc.p_profile(profile_id)
            
                 // use $.param jQuery function to serialize data from JSON 
                  // var data = $.param({profile_id: profile_id ,user_id:'0'});
                  // $http.post(Base_url+'Athlete/get_athletes', data)
                  // .success(function (data) 
                  // {
                  //   $scope.hide=false;
                  //   $scope.Pl_profile=data;
                  
                  // }).
                  // error(function (error){ 
                  // console.log(error);
                  //  });
        }
        $scope.player = function (user_id) 
        {
                 // use $.param jQuery function to serialize data from JSON 
                  var data = $.param({user_id: user_id, profile_id: 0});
                  $http.post(Base_url+'Athlete/get_athletes', data)
                  .success(function (data, status, headers, config) 
                  {
                    $scope.players=data;
                  })
                  .error(function(data, status,response) { 
                  console.log(response);
                   });
        }
        $scope.open = function (uid,parent_id) 
        {
                $scope.showModal=true;
                $scope.uid=uid;
                $scope.parent_id=parent_id;
                 var data = $.param({ user_id:uid, ff_parent_id: parent_id,ff_parent_type:'0'});

                  $http.post(Base_url+'Feeds/get_feedbacks', data)
                  .success(function (data, status, headers, config) 
                  {
                    if (data.error==undefined) {  
                    $scope.comments=data.result;
                  }
                  else{
                    alert("no feed found");
                  }

                  })
                  .error(function(data, status,response) { 

                  $scope.error=data.error;

                   });
        };
        $scope.post = function (uid,parent_id) 
        {
                 var data = $.param({user_id: uid, ff_parent_id: parent_id,ff_parent_type:'0',ff_txt:$scope.post_data});
                  $http.post(Base_url+'Feeds/add_feedback', data)
                  .success(function (data, status, headers) 
                  {
                    $scope.postdata=data;
                    $scope.feed(uid,'0');
                      $scope.cancel();
                  })
                  .error(function(data, status,response) { 
                  console.log(response);
                   });
               };
               $scope.cancel = function() {
                $scope.showModal = false;
        };
             $scope.play_bc = function (user_id) 
        {
            var week = 1;
            var year = 2017;
             // use $.param jQuery function to serialize data from JSON 
              var data = $.param({user_id: 1, week_no: week , week_year: year});
              
              $http.post(Base_url+'Balance_chart/get_balance_chart_data', data)
              .success(function (data, status, headers) 
              {
                $scope.CurrentDate = new Date();
                $scope.play_bc=data.total_habits;
                $scope.play=data;
              })
              .error(function(data, status,response) { 
                console.log(response);
              });
        }
}]);

app.controller("UserController", ["$scope", "$location", "authenticationSvc", "auth","$http",
  function ($scope, $location, authenticationSvc, auth,$http) 
{
   var Base_url="http://bizmoapps.com/ydbg/api/";

  $scope.userprofile=authenticationSvc.getUserProfile();
  //$window.localStorage["userprofile"] = JSON.stringify(authenticationSvc.getUserProfile());

  $scope.Myfeed=function(x)
      {
             var data = $.param({profile_id: x});
                  $http.post(Base_url+'Feeds/get_post_feed', data)
                  .success(function (data) 
                  {
                    $scope.myfeed=data;
                    

                  })
                  .error(function(data, status,response) { 

                  $scope.error=data;
                  alert("no feed found");

                  });
        }

            $scope.Logout = function () 
    {
        authenticationSvc.logout()
            .then(function (result) {
                $scope.userInfo = null;
                $location.path("/login");
                $.amaran({
                                  'message'   :'Logout Successfully!!',
                                  'position'  :'top left'
                            });
            }, function (error) {
                console.log(error);
            });
    }
    $scope.feed=function(uid,cat_id)
    {
          var data = $.param({ user_id:uid, category_id:cat_id});
          
            $http.post(Base_url+'Feeds/get_watching_feeds', data)
            .success(function (data, status, headers, config) 
            {
              if (data.error==undefined) {  
              $scope.feeds=data;
              $scope.cat_id=cat_id;
            }else
            {
              alert("no feed found");
            }
            })
            .error(function(data, status,response) { 
            $scope.error=data.error;
             $scope.cat_id=cat_id;
             });
      }

      $scope.is_inspire=function(user_id,parent_id,is_inspire)
      {
         var data = $.param({user_id:user_id  ,parent_id: parent_id, parent_type:'0' });
            if (is_inspire=='0') 
            {
                 $http.post(Base_url+'Feeds/add_inspire_feeds', data)
                .success(function (data, status, headers, config) 
                {
                  $scope.feed(user_id,'0');
                })
                .error(function(data, status,response) { 

                $scope.error=data.error;

                 });
            }
            else
            {
              $http.post(Base_url+'Feeds/remove_inspire_feeds', data)
              .success(function (data) 
              {
                $scope.feed(user_id,'0');
              })
              .error(function(data, status,response) { 

              $scope.error=data.error;
               });
            }
      }
      // $scope.Myfeed=function(x)
      // {
      //        var data = $.param({profile_id: x});
      //             $http.post(Base_url+'Feeds/get_post_feed', data)
      //             .success(function (data) 

      //             {
      //               if (data.user_id) {  
      //               $scope.myfeed=data;
      //             }else{

      //               alert("no feed found");
      //             }

      //             })
      //             .error(function(data, status,response) { 

      //             $scope.error=data.error;
      //             alert("no feed found");

      //             });
      //   }

         $scope.open = function (uid,parent_id) 
        {
                $scope.showModal=true;
                $scope.uid=uid;
                $scope.parent_id=parent_id;
                 var data = $.param({ user_id:uid, ff_parent_id: parent_id,ff_parent_type:'0'});

                  $http.post(Base_url+'Feeds/get_feedbacks', data)
                  .success(function (data, status, headers, config) 
                  {
                    if (data.error==undefined) {  
                    $scope.comments=data.result;
                  }
                  else{
                    alert("no feed found");
                  }

                  })
                  .error(function(data, status,response) { 

                  $scope.error=data.error;

                   });
        };
        $scope.post = function (uid,parent_id) 
        {
                 var data = $.param({user_id: uid, ff_parent_id: parent_id,ff_parent_type:'0',ff_txt:$scope.post_data});
                  $http.post(Base_url+'Feeds/add_feedback', data)
                  .success(function (data, status, headers) 
                  {
                    $scope.postdata=data;
                    $scope.feed(uid,'0');
                      $scope.cancel();
                  })
                  .error(function(data, status,response) { 
                  console.log(response);
                   });
               };
               $scope.cancel = function() {
                $scope.showModal = false;
        };

}]);

// angular.module('ui.bootstrap.demo', ['ui.bootstrap']);




