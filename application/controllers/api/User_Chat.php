<?php
class User_Chat extends CI_Controller 
{

    public function __construct() 
    {
        parent:: __construct();
        $this->load->model('api/User_Match_model');
        $this->load->model('api/User_Que_model');
       	$this->load->helper('utility_helper');
      	require_once('vendor/autoload.php');

      if(empty($this->session->userdata('session_data')))
      {
        redirect(BASE_URL.'login');
      }

      if(check_subscription($this->session->userdata('session_data')['user_id']))
      {
        //print_r($this->uri->segment(1)); die;
        if($this->uri->segment(1) != 'billing-settings')
          redirect(BASE_URL.'billing-settings');

      }
      $this->cnt_que = $this->User_Que_model->check_answered_question($this->session->userdata('session_data')['user_id']);
      // //print_r($cnt_que); die;
      if($this->cnt_que > 0)
        redirect(BASE_URL. 'question-category');
      
    }


	public function index()
  {
    
    $data['user_id'] = $user_id = $this->session->userdata('session_data')['user_id'];
    $data['seeking'] = $seeking = $this->session->userdata('session_data')['seeking'];

    $data['arr_matches'] = $this->User_Match_model->get_matches($user_id,$seeking);
    //print_r($data['arr_matches']); die;
   
    $this->load->view('api/chat',$data);
  }

  public function update_match_msg()
  {
    $user_id = $this->input->post('user_id');
    $user_match_id = $this->input->post('user_match_id');
    $status = $this->input->post('status');

    $where['user_id'] = $user_id;
    $where['user_match_id'] = $user_match_id;

    $input_data2['status'] = $status;

    
    $input_data['user_id'] = $user_id;
    $input_data['user_match_id'] = $user_match_id;
    $input_data['status'] = $status;

    $result = $this->User_Match_model->insert_match_msg($input_data);

    //echo $result; die;
    if($result <= 0 )
      $result = $this->User_Match_model->update_match_msg($where,$input_data2);

    if($result > 0 )
      echo '1';
    else
      echo '0';
  }

  public function make_read_chat()
  {
    // $user_id = $this->input->post('user_id');
    // $user_match_id = $this->input->post('user_match_id');

    $where['user_id'] = $this->input->post('user_id');
    $where['user_match_id'] = $this->input->post('user_match_id');
    $input_data['read_count'] = 0;
    
    $result = $this->User_Match_model->update_match_msg($where,$input_data);
    if($result > 0 )
      echo '1';
    else
      echo '0';
  }

  public function update_read_msg()
  {
    $user_id = $this->input->post('user_id');
    $user_match_id = $this->input->post('user_match_id');
    
    $read_count = $this->input->post('read_count');

    
    
    // $where['user_match_id'] = $user_match_id;
    // $where['user_id'] = $user_id;

    

 
    $result = $this->User_Match_model->make_read_count($user_id,$user_match_id);

    if($result > 0 )
      echo '1';
    else
      echo '0';
  }




}