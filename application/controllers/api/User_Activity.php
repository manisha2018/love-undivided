<?php
class User_Activity extends CI_Controller 
{

  public $cnt_que;
  public function __construct() 
  {
    parent:: __construct();
    $this->load->model('api/User_Login_model');
    $this->load->model('api/User_Que_model');
    $this->load->model('api/User_Match_model');
    $this->load->model('api/User_Activity_model');
    $this->load->model('api/Subscription_model');

    $this->load->helper('utility_helper');
      //require_once('vendor/autoload.php');


    if(empty($this->session->userdata('session_data')['user_id']))
    {
     redirect(BASE_URL.'login');
   }
   else
   {
     $this->cnt_que = $this->User_Que_model->check_answered_question($this->session->userdata('session_data')['user_id']);
   }

      // if($this->uri->segment(1)!='pay' && $this->uri->segment(1)!='pricing-plan' && $this->uri->segment(1)!='submit_plan'  && check_subscription($this->session->userdata('session_data')['user_id']))
      // {
      //   //print_r($this->uri->segment(1)); die;
      //   if($this->uri->segment(1) != 'billing-settings')
      //     redirect(BASE_URL.'billing-settings');

      // }


      // //print_r($cnt_que); die;
      // if($this->cnt_que > 0)
      //   redirect(BASE_URL. 'question-category');

 }


 public function user_activity()
 {
  $data['id'] = $this->session->userdata('session_data')['user_id'];
   //  $arr_user = $this->User_Activity_model->get_user_visits();
  $data['cnt_que'] = $this->cnt_que;

  $data['limit'] = $limit = 2;
  $page = ($this->uri->segment(2)>1) ? ($this->uri->segment(2)-1)*$limit: 0;
  // echo $page; die;
  if($this->cnt_que > 0)
    redirect(BASE_URL. 'question-category');
        if(check_subscription($this->session->userdata('session_data')['user_id']))
        {
         redirect(BASE_URL.'billing-settings');
       }

      if($this->uri->segment(2) == 'visitors')
        $type = '0';
      else if($this->uri->segment(2) == 'profile-update')
        $type = '1';
      else if($this->uri->segment(2) == 'photo-update')
        $type = '2';
      else
        $type = '';
      $expire= check_expiration($this->session->userdata('session_data')['user_id']);
      if($expire[0]['promo_id']!=4 && $expire[0]['is_free']=='0')
      {
        if($expire[0]['days'] >= 1 && $expire[0]['days'] < 10) 
        {
              //echo 'between 1 - 7 days';
          $this->session->set_flashdata('web_flash','Your plan will expired in '.$expire[0]['days'].'!');
        } elseif($expire[0]['days'] == 0) 
        {
              //echo 'deadline';
          $expire[0]['days']='today';
          $this->session->set_flashdata('web_flash','Your plan will expired in '.$expire[0]['days'].'!');

        } elseif($expire[0]['days']<=0)//-days
        {
           /// when plan expires
         redirect(BASE_URL.'billing-settings');
        }else{
         //+days
                   

        }

      }else{
        
      }

       $data['activity_count'] = $this->User_Activity_model->get_activity_count($data['id'],$type);
      // echo $data['activity_count']; die;
      //$data['update_activities'] = $this->User_Activity_model->get_user_update_activities($data['id'],$type);
      $data['activities'] = $this->User_Activity_model->get_user_activities($data['id'],$type,$page,$limit);

      foreach ($data['activities'] as $key => $value) {
        if($value['activity_id'] == '' || $value['activity_id'] == 0)
          unset($data['activities'][$key]);
      }

    

     $this->load->view('api/activity',$data);
}
public function submit_plan()
{
  $user_id = $this->session->userdata('session_data')['user_id'];
  $promo = $this->input->post('promo');


  $plan = explode('/',$this->input->post('plan'));
  $is_unblur = $this->input->post('is_unblur');

  if(empty($plan[0]) && empty($promo))
  {
    $this->session->set_flashdata('web_flash','You need to select a plan or promo code');
    redirect(BASE_URL.'pricing-plan');
  }


  if(!empty($promo))
  {
     $sql ="SELECT user_id FROM `user_subscription` where user_id = '$user_id'";
     $userExist = $this->Subscription_model->checkUserExist($sql);
     $sql2 = "SELECT promo_id,code,STRCMP(BINARY code,'$promo') as status from promo_code HAVING status = 0";
     $promoExist = $this->User_Activity_model->getQueryResult($sql2);  //checking promo code is exist in database or not
   
     if(!empty($promoExist))
     {
        if($userExist>0)
          {
            $updatePromo = $this->Subscription_model->updatePromo($user_id,$promo);
            if($updatePromo>0)
            {
               $this->session->set_flashdata('web_flash_success','You have succussfully redeemed your promo code!');
               redirect(BASE_URL.'pricing-plan');
            }else
            {
               $this->session->set_flashdata('web_flash','Something Went Wrong! Enter valid Promo code!');
               redirect(BASE_URL.'pricing-plan');
            }
          }else
          {
              $chekPromo = $this->Subscription_model->check_promo($user_id,$promo);
              if($check_promo>0)
              {
                   $this->session->set_flashdata('web_flash_success','You have succussfully redeemed your promo code!');
                  redirect(BASE_URL.'pricing-plan');
             }else
             {
                  $this->session->set_flashdata('web_flash','Something Went Wrong! Enter valid Promo code!');
                  redirect(BASE_URL.'pricing-plan');
             }
      
          }
     }else
     {
          $this->session->set_flashdata('web_flash','Please Enter Valid Promo Code!');
          redirect(BASE_URL.'pricing-plan');
     }
     
   
  }
  else
  {

    if(!empty($is_unblur))
    {
      $plan_amount = $plan[0] + 0.99;
    }
    else
    {
      $plan_amount = $plan[0]; 
    }

    if(isset($plan[1]))
      $plan_id = $plan[1];
    else
      $plan_id = 0;

      //print_r($plan_id); die;
    $this->session->set_userdata('plan_amount',$plan_amount);
    $this->session->set_userdata('plan_id',$plan_id);
    $this->session->set_userdata('is_unblur',$is_unblur);

      //print_r($plan); die;


    redirect(BASE_URL.'pay');
  }
}
public function Pricing_Plan()
{
  $data['getPlans'] = $this->User_Login_model->getPricingPlans();

  $check_subscription = check_subscription($this->session->userdata('session_data')['user_id']);
      if($check_subscription['data']=='')
      {
        $this->load->view('api/pricing_plan',$data);
      }
      else
     {
        redirect(BASE_URL. 'dashboard');
     }

    // print_r($data);die;

}
public function payment_view()
{
  $data['plan'] = $this->uri->segment(2);
   // print_r($this->session->userdata()); die;
   // $data['name'] = $this->session->userdata('session_data')['name'];
  $data['email'] = $this->session->userdata('session_data')['email'];
  $this->load->view('api/stripe',$data);
}

public function other_user_profile()
{
  $slug= $this->uri->segment(2);

  if($this->cnt_que > 0)
    redirect(BASE_URL. 'question-category');

  $user_id = $this->session->userdata('session_data')['user_id'];
  $data['getProfile'] = $this->User_Login_model->getProfile($slug,$user_id);
    //print_r( $data['getProfile']); die;
  $data['getImages'] = $this->User_Login_model->getImages($slug);

  if(count($data['getProfile']) > 0 && $data['getProfile'][0]['user_id'] != $user_id)
  {
    $input_activity = array( 
      'user_id'=>$user_id,
      'activity_type'=>'0',
      'match_id'=>$data['getProfile'][0]['user_id'],
      );

    $this->User_Match_model->create_activity($input_activity);
  }
    // print_r($data);die;
  if( $data['getProfile'])
  {
    $this->load->view('api/other_profile',$data);
  }
}


public function general_settings()
{
  $this->load->view('api/general_settings');
}
public function billing_settings()
{
  $data['arr_user_plans'] = $this->User_Login_model->getUserActivePlan($this->session->userdata('session_data')['user_id']);
  $data['arr_user_promo'] = $this->User_Login_model->getUserActivePromo($this->session->userdata('session_data')['user_id']);
  // echo"<pre>";
  // print_r( $data['arr_user_plans']);
  // print_r( $data['arr_user_promo'][0]);
  //  die;
  $this->load->view('api/billing_settings',$data);
}
public function contact_settings()
{

  $this->User_Activity_model->add_contact_setting($this->session->userdata('session_data')['user_id']);
  $arr_settings = $this->User_Activity_model->get_contact_setting($this->session->userdata('session_data')['user_id']);

    //print_r($arr_settings); die;
  $arr_setts = [];
  foreach ($arr_settings as $key => $value) {
      // $arr_setts[$value['alert_type']] = [];
      // $temp=[];
      // $temp['status'] = $value['status'];

    $arr_setts[$value['alert_type']]= $value['status'];
  }
    // echo '<pre>';
    // print_r($arr_setts['2']); die;
  $data['arr_setts'] = $arr_setts;
  $this->load->view('api/contact_settings',$data);
}

public function change_alert()
{
  $where['user_id'] = $this->input->post('user_id');
  $where['alert_type'] = $this->input->post('alert_type');
  $input['status'] = $this->input->post('status');


  $this->User_Activity_model->update_setting($input,$where);

}

public function ChangePassword()
{

  $oldpassword=$this->input->post('oldpassword');

  $password=createHashAndSalt($this->input->post('password'));

  $slug=$this->session->userdata('session_data')['slug_name'];
  $user_id = $this->session->userdata('session_data')['user_id'];
  $userdata = $this->User_Login_model->getProfile($slug);
            //print_r($userdata);die;
  if (count($userdata) > 0) 
  {
    $userdata = $userdata[0];

    if (verifyPasswordHash($oldpassword, $userdata['password']) == TRUE) 
    {
      $edit_pass=array('password'=>$password);
      $val=$this->User_Login_model->updateProfilePic($edit_pass,$user_id);
      echo $val;



    } else 
    {

      echo "fail";
    }
  }
}

public function featuredProfile()
{
  $slug= $this->uri->segment(2);
  $user_id = $this->session->userdata('session_data')['user_id'];
  $data['getProfile'] = $this->User_Login_model->getProfile($slug,$user_id);
   $data['getImages'] = $this->User_Login_model->getImages($slug);

  if( $data['getProfile'])
  {
    $this->load->view('api/featured_profile',$data);
  }



}

public function SndMailToExpirePlanUsers()
{
 $users = $this->User_Activity_model->SndMailToExpirePlanUsers();
  print_r($users);
  die;
 $email=array();
 $mesg=array();
 foreach ($users as $key => $value) {

  $msg="Hello ".$value['name'].",<br/><br/><br/>Your Plan will expired in ".$value['days']." days!.Please <a href=".BASE_URL."billing-settings>click here</a> to Upgrade Plan!<br/><br/><br/>Thank You,<br>LoveUndivided Team.<br>";
  array_push($email, $value['email']);
  array_push($mesg, $msg);
}
$to=$email;
$message = $mesg;
sendMail($to,"Upgrade Plan",$message);

}


}