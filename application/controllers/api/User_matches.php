<?php
class User_matches extends CI_Controller 
{
    public $cnt_que;

    public function __construct() 
    {
        parent:: __construct();
        $this->load->model('api/User_Match_model');
        $this->load->helper('utility_helper');
        $this->load->model('api/User_Que_model');
        require_once('vendor/autoload.php');

        if(empty($this->session->userdata('session_data')['user_id'])){
          redirect(BASE_URL.'login');
        }

        if(check_subscription($this->session->userdata('session_data')['user_id']))
        {
            //print_r($this->uri->segment(1)); die;
            if($this->uri->segment(1) != 'billing-settings')
              redirect(BASE_URL.'billing-settings');

        }

        $this->cnt_que = $this->User_Que_model->check_answered_question($this->session->userdata('session_data')['user_id']);
        // //print_r($cnt_que); die;
        if($this->cnt_que > 0)
            redirect(BASE_URL. 'question-category');
    }


    public function get_matches()
    {
        //echo $this->uri->segment(1); die;

        $data['limit'] = $limit = 2;

        $page = ($this->uri->segment(2)>1) ? ($this->uri->segment(2)-1)*$limit: 0;

        $data['id'] = $user_id = $this->session->userdata('session_data')['user_id'];
        $seeking = $this->session->userdata('session_data')['seeking'];
        $data['arr_matches'] = $this->User_Match_model->get_matches($user_id,$seeking,"1","","","","",$page,$limit);

        $data['matche_cnt'] = $this->User_Match_model->get_match_count($user_id,$seeking);
        $data['cnt_que'] = $this->cnt_que;


        //print_r($data['arr_matches']); die;


        $this->load->view('api/matches',$data);
    }


  

  public function filter_matches()
  {
    $type = $this->input->post('type');
    $data = $this->input->post('data');
    $age1 = $this->input->post('age1');
    $age2 = $this->input->post('age2');

    if($type == 5)
        $arr_matches = $this->User_Match_model->get_favourites($this->session->userdata('session_data')['user_id']);
    else if($type == 6)
        $arr_matches = $this->User_Match_model->get_favourites($this->session->userdata('session_data')['user_id'],'1');
    else
        $arr_matches = $this->User_Match_model->filter_matches($type,$data,$age1,$age2);
    //print_r($arr_matches); die;
    $html = '';

    if(count($arr_matches) > 0) {
        foreach ($arr_matches as $key => $value) {
            $status = $value['status'];
            if($status == '1')
                $css  = 'blur1';
            else if($status == '2')
                $css  = 'blur2';
            else if($status == '3')
                $css  = 'blur3';
            else if($status == '0')
                $css  = 'blur1';
            else
                $css  = 'blur4';

            if($value['fav_user_id'] > 0)
                $css2 = 'fa-heart'; 
            else  
                $css2 = 'fa-heart-o'; 
            
           
            $html .='<li><center><a href="'.BASE_URL.'matched-profile/'.$value['slug_name'].'"><div class="mateches-img"> <img src="'.$value['user_image'].'" class="'.$css.'" alt="" /></div> </a>
            <div class="favorite-section"><a href="#" onclick="add_fav('.$value['u2_user_id'].');" class="add-fav" ><span id="add_fav_'.$value['u2_user_id'].'" class="fa '.$css2.'"></span></a></div>

            <span onclick="add_fav('.$value['u2_user_id'].');">'.$value['name'].'</span></center></li>';
    
        }
    } else { 
       $html .='<li><h3>No Matches Found!</h3></li>';
 
    } 
      echo $html;
  }

  public function add_favourite()
  {
    $arr_fav['frm_user_id'] = $this->session->userdata('session_data')['user_id'];
    $arr_fav['to_user_id'] = $this->input->post('fav_user_id');
    $name = $this->input->post('name');
    $email = $this->input->post('email');

    $result = $this->User_Match_model->add_favourite($arr_fav);

    $to =  $email;
    $subject = 'Favourites LoveUndivided';
    $message = '<span style="font color:red"><i>
    Hi <b>'.$name.'</b></i></span>,
    <br/>'.$this->session->userdata('session_data')['name'].' added you as Favourites Match . <a href="'.BASE_URL.'"> Clear here </a> to Login in to Love Undivided to know more about your Match.
    <br>Along with contact email for any questions or concerns <b>Questions@LoveUndivided.com</b>';
    $msg['message_array'] = array($message);
                //print_r($msg);
    $template = $this->load->view('api/email_template',$msg,true);
    $send=sendMail($to,$subject,$template);

    echo $result;

  }
}