<?php
class User_Questions extends CI_Controller 
{

    public $cnt_que;
    public function __construct() 
    {
        parent:: __construct();
        $this->load->model('api/User_Que_model');
        $this->load->model('api/User_Login_model');
        $this->load->helper('utility_helper');
        $this->load->library('pagination');
        $this->load->library('pagination');


        if( $this->session->userdata('session_data') == '' )
        {
         redirect(BASE_URL.'api/User_Questions/getCategoryQue');
        }

        //$this->cnt_que = $this->User_Que_model->check_answered_question($this->session->userdata('session_data')['user_id']);
      
       

        // if(empty($this->session->userdata('session_data')))
        // {
        //   redirect(BASE_URL.'login');
        // }

        // if(check_subscription($this->session->userdata('session_data')['user_id']))
        // {
        //   //print_r($this->uri->segment(1)); die;
        //   if($this->uri->segment(1) != 'billing-settings' && $this->cnt_que == 0)
        //     redirect(BASE_URL.'billing-settings');
          

        // }
    

    }

    public  function que_category() {

      //echo $this->session->userdata('user_status'); die;
      if(empty($this->session->userdata('session_data')['user_id']) ){
        redirect(BASE_URL);
      }

      if($this->session->userdata('user_status') == '0')
        $this->session->set_flashdata('status_update', 'An email has sent to your registered email id. Please verify your email is for further procedure');

      $user_id = $this->session->userdata('session_data')['user_id'];
      $arr_questions=$this->User_Que_model->Get_Questions('','',$user_id);

      $data['arr_cat'] =  $arr_cat = $this->User_Que_model->get_que_cat();

      $arr_cnt_que_cat = $this->User_Que_model->count_que_cat($user_id);


      $arr_cat_new = [];
      $cat_que_counter = 0;
      if(count($arr_cnt_que_cat) > 0)
      {
        foreach ($arr_cnt_que_cat as $key => $value) {
          $arr_cat_new[$value['category_id']] = $value;
        }
      }
      else
      {
        $this->User_Login_model->update_user($user_id,['is_que_answered'=>'1']);
      }

      if(count($arr_cnt_que_cat) > 0)
      {
        $data['arr_cat_new'] = $arr_cat_new;
      }



      $this->load->view('api/que_category',$data);
    }

    public function getCategoryQue()
    {

      if(empty($this->session->userdata('session_data')['user_id']) || $this->session->userdata('user_status') == '0'){
        redirect(BASE_URL.'api/User_Login/logout');
      }
      $id = $this->uri->segment(2);
      //print_r($this->session->userdata('session_data')); die;
      if ($this->session->userdata('session_data')!=null) 
        {
          
            $start = 0;
            $data['id'] = $id;
            $data['user_id'] = $user_id = $this->session->userdata('session_data')['user_id'];
            $arr_questions=$this->User_Que_model->Get_Questions($start,$id,$user_id);

            if(count($arr_questions) == 0) {
               
              $this->User_Que_model->add_que_rating($user_id);
             // echo $this->db->last_query(); die;
      
              $this->session->set_flashdata('que_msg', 'No Questions left ..!');
              redirect(BASE_URL.'question-category');
            }


            $arr_option1 = explode(',',$arr_questions[0]['options']);
            $arr_option2 = explode(',',$arr_questions[0]['options_id']);

            $arr_options = array_combine($arr_option2, $arr_option1);
            $arr_questions[0]['key_option'] = $arr_options;

            $data['questions'] = $arr_questions;
            // echo '<pre>';
            // print_r($data); die;
           $this->load->view('api/questions',$data);

        

        }

    }

    public function getCategoryQuestion() 
    {

      
      $cat_id = $this->input->post('cat_id');
      $start = $this->input->post('start');
      $selected_ans = $this->input->post('selected_ans');
      $que_id = $this->input->post('question_id');
      $user_id = $this->input->post('user_id');

      $input_data['user_id'] = $user_id;
      $input_data['question_id'] = $que_id;
      $input_data['option_id'] = $selected_ans;
      $input_data['que_rating'] = $this->input->post('selected_rating');
      // print_r($input_data); die;
      $result = $this->User_Que_model->submit_answer($input_data);



     // $result = $this->User_Que_model->submit_cat_rating($user_id);

      $arr_questions = $this->User_Que_model->Get_Questions($start,$cat_id,$user_id);

      if(count($arr_questions) > 0)
      {
        $arr_option1 = explode(',',$arr_questions[0]['options']);
        $arr_option2 = explode(',',$arr_questions[0]['options_id']);

        $arr_options = array_combine($arr_option2, $arr_option1);
        $arr_questions[0]['key_option'] = $arr_options;
      }
      else
      {
        $this->User_Que_model->add_que_rating($user_id);
        //echo $this->db->last_query(); die;
      }


      echo json_encode($arr_questions);
    }

}

    