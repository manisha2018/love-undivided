<?php
class FAQs extends CI_Controller 
{

    public function __construct() 
    {
        parent:: __construct();
        $this->load->model('api/User_Login_model');
        $this->load->helper('utility_helper');
        require_once('vendor/autoload.php');
        $this->load->model('api/User_Que_model');
        $this->load->model('api/FAQs_model');
        $this->load->helper('cookie');


        // if ($this->session->userdata('session_data')!=null) 
        // {
        //   redirect(BASE_URL.'question-category');
        // }
        //phpinfo(); die;

    }

    public function getFaqs()
    {
        $limit=5;
        $config["base_url"] = BASE_URL."api/FAQs/getFaqs";
        $config['per_page']=5;
        $total_row = $this->FAQs_model->record_count();
        $config['total_rows']=$total_row;
        $this->pagination->initialize($config);
        $config["uri_segment"] =4;
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data['getFaqs']=$this->FAQs_model->getFaqs($config['per_page'], $page);
        $links = $this->pagination->create_links();
        $data["links"] = explode('&nbsp;',$links );
      //$data['getFaqs'] = $this->FAQs_model->getFaqs();
        $this->load->view('api/web_faqs',$data);
    }

}