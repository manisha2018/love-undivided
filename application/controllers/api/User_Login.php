<?php
class User_Login extends CI_Controller 
{

    public function __construct() 
    {
        parent:: __construct();
        $this->load->model('api/User_Login_model');
       	$this->load->helper('utility_helper');
      	require_once('vendor/autoload.php');
        $this->load->model('api/User_Que_model');
        $this->load->model('api/User_Match_model');
        $this->load->model('api/User_Activity_model');



        // if ($this->session->userdata('session_data')!=null) 
        // {
        //   redirect(BASE_URL.'question-category');
        // }
        //phpinfo(); die;

      // if($this->uri->segment(1)!='login' && $this->uri->segment(1)!='register' && check_subscription($this->session->userdata('session_data')['user_id']))
      // {
      //   //print_r($this->uri->segment(1)); die;
      //   if($this->uri->segment(1) != 'billing-settings')
      //     redirect(BASE_URL.'billing-settings');

      // }

    }

    public function new_match()
    {
      $data['arr_matches'] = $this->User_Match_model->get_new_matches(86,2);
    } 
  	
  	public function LoginView()
  	{
         $fb = new \Facebook\Facebook([
            'app_id' => '239091213260075',
            'app_secret' => '617d76881ba4e747ee4b8241a055e649',
            'default_graph_version' => 'v2.9',
            //'default_access_token' => '{access-token}', // optional
          ]);
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email']; // Optional permissions
        $data['loginUrl'] = $helper->getLoginUrl(BASE_URL."dashboard", $permissions);
        $this->load->view('api/login',$data);



  	}

    public function UserLogin()
    { 
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if($email =='' && $password=='')
        {
          $this->session->set_flashdata('web_flash', 'Email ID and Password fields are Mandatory!');
          redirect(BASE_URL.'login');
        }
        elseif ($email=='') {
          $this->session->set_flashdata('web_flash', 'Email ID  field is Mandatory!');
          redirect(BASE_URL.'login');
        }
        elseif ($password=='') {
         $this->session->set_flashdata('web_flash', 'Password field is Mandatory!');
          redirect(BASE_URL.'login');
        }
        elseif($email!='' && $password!='')
        {
          $userdata = $this->User_Login_model->getRecords($email);
          //
          //print_r($userdata);
           if (count($userdata) > 0) 
          {
              $userdata = $userdata[0];
              $user_id = $userdata['user_id'];

                  if (verifyPasswordHash($password, $userdata['password']) == TRUE && $userdata['user_status'] == '1') 
                  {
                       
                          $newdata = array(
                              'user_id' => $userdata['user_id'],
                              'name' => $userdata['name'],
                              'email' => $userdata['email'],
                              'gender'=> $userdata['gender'],
                              'seeking' => $userdata['seeking'],
                              'slug_name' => $userdata['slug_name'],
                               'user_image' =>$userdata['user_image']
                             
                              
                          );
                          $this->session->set_userdata('session_data',$newdata);
                          $this->session->set_userdata('user_status','1');

                          $cnt_que = $this->User_Que_model->check_answered_question($user_id);
                          //print_r($cnt_que); die;

                          if($cnt_que==0)
                            redirect(BASE_URL. 'activities');
                          else 
                            redirect(BASE_URL. 'question-category');
                   
                  } else 
                  {

                      $this->session->set_flashdata('web_flash', 'Password do not match!!! Try again');
                      redirect(BASE_URL. "login");
                  }
          } 
          else
           {
              $this->session->set_flashdata('web_flash', 'Email do not match!!! Try again');
              redirect(BASE_URL . "login");
           }
        }
    }

    public function RegisterView()
    {
        $data['terms'] = $this->User_Login_model->getTermsConditions();
        $this->load->view('api/register',$data);
    }

    public function UserRegister()
    {

      $email = $this->input->post('email');
      $from = $this->input->post('dob');
      $to   = new DateTime('today');
      $age = date_diff(date_create($this->input->post('dob')), date_create('today'))->y; 
      $slug_name = create_slug($this->input->post('name'),'user','slug_name');
      $city = $this->input->post('city');
      $formattedAddr = str_replace(' ','+',$city);

       if($city!='')
          {
            $location=file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".$formattedAddr."&sensor=false");
            
          }
          $address=json_decode($location);
          $lat=isset($address->results[0]->geometry->location->lat)?$address->results[0]->geometry->location->lat:0;
          $lng=isset($address->results[0]->geometry->location->lng)?$address->results[0]->geometry->location->lng:0;
          $lat!=''? $lat : $lat='';
          $lng!=''? $lng : $lng='';

        $userdata = array(
                          'name'=> $this->input->post('name'),
                          'slug_name'=> $slug_name,
                          'email'=> $this->input->post('email'),
                          'password'=> createHashAndSalt($this->input->post('password')),
                          'dob'=> $this->input->post('dob'),
                          'age' => $age,
                          'city'=> $this->input->post('city'),
                          'mob_no'=> $this->input->post('mob_no'),
                          'gender'=> $this->input->post('gender'),
                          'seeking'=> $this->input->post('seeking'),
                          'lat'=>$lat,
                          'lng'=>$lng
                          );

        
        $checkuser = $this->User_Login_model->checkuseravailable($email);
        $data['email'] = $email;
        //print_r($checkuser);die;
        if($checkuser==1)
        {
          //print_r($this->session->userdata('session_data')); die;
          //redirect(BASE_URL.'question-category');
          //$this->load->view('api/que_category',$data);
           $this->session->set_flashdata('web_flash', 'Email ID is already Exist!');
           redirect(BASE_URL. 'register');
        }
        elseif ($checkuser==0) 
        {
            $insert_user = $this->User_Login_model->insert_users($userdata);
            $session = array(
                              'user_id' => $insert_user,
                              'email' => $this->input->post('email'),
                              'name' => $this->input->post('name'),
                              'gender' =>$this->input->post('gender'),
                              'seeking'=>$this->input->post('seeking'),
                              'slug_name' => $slug_name

                          );
       $this->session->set_userdata('session_data',$session);
       $this->session->set_userdata('user_status','0');

      //echo $insert_user;die;
       //print_r($this->session->userdata('session_data')); die;
              // if($insert_user>0)
              // {
              //   $to =  $email;
              //   $subject = 'Welcome to LoveUndivided';
              //   $message = '<span style="font color:red"><b><i>
              //   Hello <b>'.$this->input->post('name').'</b>,
              //   <br/>
              //   Thank you for registering</i></b>...</span><br>
              //   <a href="'.BASE_URL.'api/User_Login/activate_account?key='.base64_encode($insert_user).'">Click here</a> to to verify your email and activate your Love undivided account.
              //   <br><br> Along with contact email for any questions or concerns <b>Questions@LoveUndivided.com</b>';
              //   $send=sendMail($to,$subject,$message);

              //   redirect(BASE_URL.'question-category');
              // }
        $name =  $this->input->post('name');
        $message = '<span style="font color:red"><b><i>
                Hello '.$name.',
                <br/>
                Thank you for registering</i></b>...</span><br>
                <a href="'.BASE_URL.'api/User_Login/activate_account?key='.base64_encode($insert_user).'">Click here</a> to to verify your email and activate your Love undivided account.
                <br><br> Along with contact email for any questions or concerns <b>Questions@LoveUndivided.com</b>';
               $msg['message_array'] = array($message);
                //print_r($msg);
                $template = $this->load->view('api/email_template',$msg,true);
              if($insert_user>0)
              {
                $to =  $email;
                $subject = 'Welcome to LoveUndivided';
           
                $send=sendMail($to,$subject,$template);

                redirect(BASE_URL.'question-category');
              }
        }
                    
      

    }

  public function activate_account()
  {
    $user_id = base64_decode($this->input->get('key'));

    $data_arr['user_status'] = '1';

    $updated_user = $this->User_Login_model->update_user($user_id,$data_arr);

    $this->session->set_userdata('user_status','1');

    echo '<p>Your email verified successfully and your Love Undivided account activated ! <a href="'.BASE_URL.'login"> Click here </a>to login</p>';

  }

  public function View_Profile()
  {
      //print_r($this->session->userdata('session_data')); die;
      $user_id = $this->session->userdata('session_data')['user_id'];
      $slug_name = $this->session->userdata('session_data')['slug_name'];
      $data['cnt_que'] = $cnt_que = $this->User_Que_model->check_answered_question($user_id);
      // echo $this->db->last_query(); die;

      // if($cnt_que > 0)
      // {
      //   $this->session->set_flashdata('que_msg', 'Please answer all questions !');
      //   redirect(BASE_URL. 'question-category');
      // }

      //print_r( $cnt_que ); die;



      $data['getProfileData'] = $arr_user = $this->User_Login_model->getProfile($slug_name);

      $total_que = 0;

      if(isset($arr_user[0]['cnt_all_question']))
        $total_que = $arr_user[0]['cnt_all_question'];
      
      $temp = $total_que - $cnt_que;

      if($temp > 0)
        $que_per = $temp/ $total_que * 100;
      else
        $que_per = 0;

      $arr_keys =  array_keys($arr_user[0]); 

      $cnt = 0;

      $fields = ['name','email','city','mob_no','dob','age','seeking','user_caption','user_image','height','hair_color','eye_color','smoke','drug','drink','kids','pets','date_with_kids','religion_id','ethnicity','education'];
      //echo '<pre>';
      foreach ($arr_keys as $key => $value) {
        if(in_array($value, $fields) && !empty($arr_user[0][$value]) && $arr_user[0][$value] != "" )
        {
          //if($arr_user[0][$value] == 'pets')

            $cnt++;
          //echo $arr_user[0][$value].'<br>';
        }else if(in_array($value, $fields))
        {
         // echo $value.'<br>';
        }
        
      }
      //die;
      $data['complete_per'] = ((round(($cnt)/count($fields) * 100) ) + $que_per) / 2;
      
      //print_r($data['getProfileData']); die;
     
      $data['religion'] = $this->User_Login_model->getReligion();
      $data['getImages'] = $this->User_Login_model->getImages($user_id);
      $data['user_id'] = $user_id;
      //print_r( $data['getProfileData']);die;
      $this->load->view('api/self_profile',$data);
  }

    public function update_user()
    {
      //print_r($this->input->post());die;
     // $slug_name = $this->session->userdata('session_data')['slug_name'];
      $user_id = $this->session->userdata('session_data')['user_id'];

      $slug_name = create_slug($this->input->post('name'),'user','slug_name',$user_id);

      //echo $slug_name; die;

      
      $data_arr = array('name'=>$this->input->post('name'),
                        'slug_name'=>$slug_name,
                        'dob'=>$this->input->post('dob'),
                        'email' => $this->input->post('email'),
                        'city'=>$this->input->post('city'),
                        'height'=>$this->input->post('height'),
                        'gender'=>$this->input->post('gender'),
                        'seeking'=>$this->input->post('seeking'),
                        'mob_no'=>$this->input->post('mob_no')
                        );
      $updated_user = $this->User_Login_model->update_user($user_id,$data_arr);

      $newdata = array(
                              'user_id' => $user_id,
                              'name' => $this->input->post('name'),
                              'email' => $this->input->post('email'),
                              'gender'=> $this->input->post('gender'),
                              'seeking' => $this->input->post('seeking'),
                              'slug_name' => $slug_name
                             
                              
                          );
      $this->session->set_userdata('session_data',$newdata);
      $this->session->set_userdata('user_status','1');
      // if($updated_user>0)
      // {
      //    redirect(BASE_URL . "profile/",$slug_name);
      // }else
      // {
      //   redirect(BASE_URL . "profile/");
      // }
      redirect(BASE_URL . "profile/");
    }

    
    public function update_caption_data()
    {
        $user_id = $this->input->post('user_id');
        $user_caption = $this->input->post('user_caption');
        $data_arr = array('user_caption'=>$user_caption);
        $update_caption  = $this->User_Login_model->update_caption_data($user_id,$data_arr);
        echo json_encode($update_caption);
    }

    public function profile_pic()
    {
      if(empty($this->session->userdata('session_data')['user_id']!=null)){
         redirect(BASE_URL);
      }
      $data['user_id']= $user_id = $this->session->userdata('session_data')['user_id'];


      // $fields = 'user_image';
      // $where['user_id'] = $user_id;
      // if($this->User_Login_model->check_user($fields,$where))
      // {
      //   redirect(BASE_URL.'gallery');
      // }
      // $cnt_que = $this->User_Que_model->check_answered_question($user_id);
      //   //print_r($cnt_que); die;
      //   if($cnt_que > 0)
      //   {
      //     $this->session->set_flashdata('que_msg', 'Please answer all questions !');
      //     redirect(BASE_URL. 'question-category');
      //   }


      $this->load->view('api/choose_profile_pic',$data);
    }

    public function uploadGallery()
    {
      $user_id =  $this->session->userdata('session_data')['user_id'];
      // $cnt_que = $this->User_Que_model->check_answered_question($user_id);
      
      // if($cnt_que > 0)
      // {
      //   $this->session->set_flashdata('que_msg', 'Please answer all questions !');
      //   redirect(BASE_URL. 'question-category');
      // }
      if(empty($this->session->userdata('session_data')['user_id']!=null))
      {
       redirect(BASE_URL);
      }
      
      $data['getimages'] = $this->User_Login_model->getImages($user_id);
      $data['images'] = $this->User_Login_model->getOnlyimages($user_id);
      $data['videos'] = $this->User_Login_model->getOnlyvideos($user_id);
        // echo"<pre>";
        // print_r($data);die;
      if(count($data['images']) > 4 && count($data['videos']) > 1)
        redirect(BASE_URL.'caption');
      //print_r( count($data['videos']));die;
      $this->load->view('api/upload_gallery',$data);
     
    }

   

    public function image_upload()
    {
      $user_id = $this->input->post('user_id');
      //echo "string".$user_id;

      if($this->input->post("ques_img") !="")
      {

        $data = $this->input->post("ques_img");
        $data = str_replace('data:image/png;base64,', '', $data);

        $data = str_replace(' ', '+', $data);

        $data = base64_decode($data);

        $file_name = rand() . '.png';

        $file='uploads/user_images/' .$file_name;

        //print_r($file);

        $success = file_put_contents($file, $data);

        $data = base64_decode($data); 

        $source_img =  @imagecreatefromstring(file_get_contents($data));

        $rotated_img = @imagerotate(file_get_contents($source_img), 90, 0);

        $file1 = 'uploads/user_images/'. rand(). '.png';

        $imageSave = @imagejpeg(file_get_contents($source_img),$file1, 10);
        if( $file!=""){         
          $user_data["user_image"] = $file_name;
          $newdata = array(
                              'user_id' => $this->session->userdata('session_data')['user_id'],
                              'name' => $this->session->userdata('session_data')['name'],
                              'email' => $this->session->userdata('session_data')['email'],
                              'gender'=> $this->session->userdata('session_data')['gender'],
                              'seeking' => $this->session->userdata('session_data')['seeking'],
                              'slug_name' => $this->session->userdata('session_data')['slug_name'],
                              'user_image' => $user_data["user_image"]
                             
                              
                          );
                          $this->session->set_userdata('session_data',$newdata);
        }

        $updatePic = $this->User_Login_model->updateProfilePic($user_data,$user_id);
        echo json_encode($updatePic);

      } 


      // if(isset($_FILES["user_image"]["type"]))
      //   {
          
      //         if($_FILES['user_image']['size']>0 && $_FILES['user_image']['error']==0)
      //         {
      //           $file_ext = substr(strrchr($_FILES['user_image']['name'],'.'),1);
      //           $file_path = 'uploads/user_images/';
                 
      //           $user_image = upload_image($file_path,'user_image','profile_');

      //           // $image = new Imagick(USER_IMAGES.$user_image);
      //           // $image->blurImage(5,10);
      //           // imagejpeg($image,"./$file_path".$_FILES['user_image']['name'].'blur3.'.$file_ext);

      //           // $image = new Imagick(USER_IMAGES.$user_image);
      //           // $image->blurImage(5,7);
      //           // imagejpeg($image,"./$file_path".$_FILES['user_image']['name'].'blur2.'.$file_ext);

      //           // $image = new Imagick(USER_IMAGES.$user_image);
      //           // $image->blurImage(5,2);
      //           // imagejpeg($image,"./$file_path".$_FILES['user_image']['name'].'blur1.'.$file_ext);

      //           $user_data['user_image']=$user_image;

      //           // $user_data['user_image_blur3']=$_FILES['user_image']['name'].'blur3.'.$file_ext;
      //           // $user_data['user_image_blur2']=$_FILES['user_image']['name'].'blur2.'.$file_ext;
      //           // $user_data['user_image_blur1']=$_FILES['user_image']['name'].'blur1.'.$file_ext;
      //         }

            
      //   }
    }

     public function uploadMulipleImages()
    {
        //print_r($_FILES);die;
        $user_id = $this->session->userdata('session_data')['user_id'];
        $getExistingImages = $this->User_Login_model->getImages($user_id);
        $imageCount = count($getExistingImages);
        $gallery_image='';
        $number_of_files = sizeof($_FILES['gallery_image']['tmp_name']);
        $files = $_FILES['gallery_image'];

        $errors = array();
        $filesCount = count($_FILES['gallery_image']['name']);
        
            $number_of_files = sizeof($_FILES['gallery_image']['tmp_name']);
            $files = $_FILES['gallery_image'];
            $errors = array();

            $filesCount = count($_FILES['gallery_image']['name']);

            // echo $filesCount;
            // echo '<pre>';
            //  print_r($_FILES);die;
            if($filesCount<=5)
            {
              $uploadType='';
              for($i = 0; $i < $filesCount; $i++)
              {
                  $_FILES['gallery_image']['name'] = $files['name'][$i];
                  $_FILES['gallery_image']['type'] = $files['type'][$i];
                  $_FILES['gallery_image']['tmp_name'] = $files['tmp_name'][$i];
                  $_FILES['gallery_image']['error'] = $files['error'][$i];
                  $_FILES['gallery_image']['size'] = $files['size'][$i];
                  $uploadPath = 'uploads/user_images/';
                  $config['upload_path'] = $uploadPath;
                  $img = ['image/jpg','image/jpeg','image/png','image/gif'];
                  $video=['video/mov','video/avi','video/mp4','video/3gp'];
              
              //print_r($_FILES['gallery_image']['type']);die;
                 if(in_array($_FILES['gallery_image']['type'], $img))
                 {
                 // echo "img";
                  $uploadType='0';

                 }elseif(in_array($_FILES['gallery_image']['type'], $video))
                 {
                 // echo "video";
                  $uploadType = '1';
                 }
                // print_r($_FILES['gallery_image']['type']);die;
                  $config['allowed_types'] = '*';
                  $name=time();
                  $file_name=$name.strrchr($_FILES['gallery_image']['name'],'.');
                  $config['file_name']=$file_name; 
                  $this->load->library('upload', $config);
                  $this->upload->initialize($config);
                 
                  if($this->upload->do_upload('gallery_image'))
                  {
                      $fileData = $this->upload->data();
                      $uploadData[$i]['file_name'] = $fileData['file_name'];
                      $uploadData[$i]['file_type'] = $uploadType;

                  }else
                  {
                   echo $this->upload->display_errors();
                  }
                  
              }

              if(!empty($uploadData))
              {
               
               // print_r($data);die;
                $addimage = $this->User_Login_model->addImages($uploadData,$user_id);
                $getimages = $this->User_Login_model->getImages($user_id);
              }
              else
                $getimages = '';
                echo json_encode($getimages);
            }else
            {
              echo json_encode($filesCount);
            }
       // }
        
    }

    public function AddCaption()
    {
      $user_id =  $this->session->userdata('session_data')['user_id'];
      // $cnt_que = $this->User_Que_model->check_answered_question($user_id);
      

      $fields = 'user_caption';
      $where['user_id'] = $user_id;
      if($this->User_Login_model->check_user($fields,$where))
      {
        redirect(BASE_URL.'how-we-match');
      }
      
      // if($cnt_que > 0)
      // {
      //   $this->session->set_flashdata('que_msg', 'Please answer all questions !');
      //   redirect(BASE_URL. 'question-category');
      // }
      $slug = $this->session->userdata('session_data')['slug_name'];
      //echo $slug;
      $data['ProfilePic'] = $this->User_Login_model->getProfile($slug);
      //print_r($data);die;
      $this->load->view('api/add_caption',$data);
    }
     
    public function update_caption()
    {
      $user_id = $this->session->userdata('session_data')['user_id'];
      $data_arr = array('user_caption' => $this->input->post('caption'));
      $update_caption = $this->User_Login_model->updateCaption($user_id,$data_arr);
        if($update_caption>0)
        {
          redirect(BASE_URL.'how-we-match');
          
        }
        else
        {
          redirect(BASE_URL.'how-we-match');
        }
    }

    public function how_we_match()
    {
      $this->load->view('api/how_we_match');
      //header("Refresh: 10; URL=".BASE_URL."pricing-plan");
      //window.onbeforeunload = function() { return "You work will be lost."; };

    }

    public function update_basics()
    {
      $user_id = $this->session->userdata('session_data')['user_id'];
      $data_arr = array(
                        'religion_id'=>$this->input->post('religion'),
                        'ethnicity'=>$this->input->post('ethnicity'),
                        'hair_color'=>$this->input->post('hair_color'),
                        'eye_color'=>$this->input->post('eye_color'),
                        'education'=>$this->input->post('education')
                        
                        );
    //  print_r($data_arr);die;
      $updated_user = $this->User_Login_model->update_user($user_id,$data_arr);
      if($updated_user>0)
      {
         redirect(BASE_URL . "profile/");
      }else
      {
        redirect(BASE_URL . "profile/");
      }

    }

    public function update_lifestyle()
    {
      $user_id = $this->session->userdata('session_data')['user_id'];
      $data_arr = array(
                        'smoke'=>$this->input->post('smoke'),
                        'kids'=>$this->input->post('kids'),
                        'drug'=>$this->input->post('drug'),
                        'drink'=>$this->input->post('drink'),
                        'pets'=>$this->input->post('pets'),
                        'date_with_kids'=>$this->input->post('date_with_kids')
                        
                        );
    //  print_r($data_arr);die;
      $updated_user = $this->User_Login_model->update_user($user_id,$data_arr);
      if($updated_user>0)
      {
         redirect(BASE_URL . "profile/");
      }else
      {
        redirect(BASE_URL . "profile/");
      }
    }
   

    public function self_profile_gallery()
    {
 //      $user_id = $this->session->userdata('session_data')['user_id'];
 //     //echo json_encode($user_id);die;
 //      if(isset($_FILES["multi_images"]["type"]))
 //        {
          
 //              if($_FILES['multi_images']['size']>0 && $_FILES['multi_images']['error']==0)
 //              {
                 
 //                 $user_image = upload_image('uploads/user_images/','multi_images','img_');
 //                 $user_data['gallery_image']=$user_image;
 //                 $user_data['user_id']=$user_id;
 //              }
 // //print_r($user_data);
 //              $Gallery = $this->User_Login_model->GalleryPic($user_data);
 //               echo json_encode($Gallery);

 //        }

        $user_id = $this->session->userdata('session_data')['user_id'];
        $getExistingImages = $this->User_Login_model->getImages($user_id);
        $imageCount = count($getExistingImages);
        $multi_images='';
        
        //$files = $_FILES['gallery_image'];

        $errors = array();
        //$filesCount = count($_FILES['gallery_image']['name']);
        
            $number_of_files = sizeof($_FILES['multi_images']['tmp_name']);
           // $files = $_FILES['multi_images'];
            $errors = array();

            //$filesCount = count($_FILES['multi_images']['name']);

            // echo $filesCount;
            // echo '<pre>';
            //  print_r($_FILES);die;
            if($imageCount<=4)
            {
              $uploadType='';
               // $_FILES['multi_images']['name'] = $files['name'][$i];
                  // $_FILES['multi_images']['type'] = $files['type'][$i];
                  // $_FILES['multi_images']['tmp_name'] = $files['tmp_name'][$i];
                  // $_FILES['multi_images']['error'] = $files['error'][$i];
                  // $_FILES['multi_images']['size'] = $files['size'][$i];
                  $uploadPath = 'uploads/user_images/';
                  $config['upload_path'] = $uploadPath;
                  $img = ['image/jpg','image/jpeg','image/png','image/gif'];
                  $video=['video/mov','video/avi','video/mp4','video/3gp'];
              
                //print_r($_FILES['multi_images']['type']);die;
                 if(in_array($_FILES['multi_images']['type'], $img))
                 {
                 // echo "img";
                  $uploadType='0';

                 }elseif(in_array($_FILES['multi_images']['type'], $video))
                 {
                  //echo "video";
                  $uploadType = '1';
                 }
                // print_r($_FILES['multi_images']['type']);die;
                  $config['allowed_types'] = '*';
                  $name=time();
                  $file_name=$name.strrchr($_FILES['multi_images']['name'],'.');
                  $config['file_name']=$file_name; 
                  $this->load->library('upload', $config);
                  $this->upload->initialize($config);
                 
                  if($this->upload->do_upload('multi_images'))
                  {
                      $fileData = $this->upload->data();
                      $uploadData[0]['file_name'] = $fileData['file_name'];
                      $uploadData[0]['file_type'] = $uploadType;

                  }else
                  {
                   echo $this->upload->display_errors();
                  }
                  
              

              if(!empty($uploadData))
              {
               
               // print_r($data);die;
                $addimage = $this->User_Login_model->addImages($uploadData,$user_id);
                $getimages = $this->User_Login_model->getImages($user_id);
              }
              else
                $getimages = '';
                echo json_encode($getimages);
            }else
            {
              echo json_encode($filesCount);
            }
     
    }

    public function forgot_password()
    {
            $email=$this->input->post('email');
            $user_email=$this->User_Login_model->getRecords($email);
            $name = $user_email[0]['name'];
            if($user_email[0]['email']==$email)
            {
                
                $body =  'Hello '.$name.',<br>To update your password click <a href='.BASE_URL.'update-password?email='.$email.'>here</a><br>If you have any questions, please email Questions@LoveUndivided.com.<br><br><b>Best Regards,<br>LoveUndivided Team</b><br>';
                $msg['message_array'] = array($body);
                $template = $this->load->view('api/email_template',$msg,true); 
                $to=$email;
                if(sendMail($to,"Reset Password ",$template))
                {                   
                    $this->session->set_flashdata('web_flash', 'Please Check Your Email!');
                    redirect(BASE_URL."login");
                }
                else
                {
                  $this->session->set_flashdata('web_flash', 'Email Not Sent ');
                  redirect(BASE_URL."login");
                }
            }
            else
            {
                $this->session->set_flashdata('web_flash', 'You Entered Wrong Email ID! ');
                redirect(BASE_URL."login");
            }
    }

    public function update_password()
    {
        $email=$this->input->get('email');
        $this->load->view('api/forgot_password',$email);
             
    }

    public function change_pass()
    {
          $user_email =  $this->input->post('email'); 
          $new_password =  $this->input->post('password');
          $confirm_password =  $this->input->post('password2');
          $userdata  =  $this->User_Login_model->getRecords($user_email);
          $name =   $userdata[0]['name'];
          if(count($userdata)>0)
          {
               if($new_password==$confirm_password)
               {
                  $user_password  =  createHashAndSalt($new_password);
                  $userdata = $userdata[0];
                  $inputArray['password']  = $user_password;

                  $data=array();
                  $data['password'] = $user_password;
                  $result=$this->User_Login_model->update_user_password($user_email,$data);
                   
                    if($result)
                    {
                       $to =  $user_email;
                       $subject = 'Password Update';
                       $message = 'Hello '.$name.',<br>Your new password is:-'.$new_password.'<br>If you have any questions, please email <b>Questions@LoveUndivided.com.</b><br><br>Best Regards,<br>LoveUndivided Team.';
                         $msg['message_array'] = array($message);
                         $template = $this->load->view('api/email_template',$msg,true);
                       $send=sendMail($to,$subject,$template);
                       $this->session->set_flashdata('web_flash', 'Your Password is Updated Successfully!');
                       redirect(BASE_URL."login");
                    }
                    else
                    {
                      
                     $this->session->set_flashdata('web_flash', 'Password Not Updated');
                     redirect(BASE_URL."login");
                    }
                  

               }
               else
               {
                
                 $this->session->set_flashdata('web_flash', 'Password do not match!!! Try again');
                 redirect(BASE_URL."api/User_Login/update_password?email=$user_email");
               }
              
          }
          else
          {
            $this->session->set_flashdata('web_flash', 'Email ID not exist!!! Try again');
            redirect(BASE_URL."login");
          }
    }

    public  function delete_images()
    {
      $gallery_id = $this->input->post('img_id');
      $val=$this->User_Login_model->delete_images($gallery_id);
      if($val)
      {
          echo "success";
      }else
      {
          echo "fail";

      }
    }

    public function userFacebookLogin()
    {
      $data = json_decode(file_get_contents('php://input'), true);
     // print_r($data);
      $fb_profile='';
       if($data['picture'])
       {
        $fb_profile = $data['picture']['data']['url'];
       }else if($data['facebook_profile'])
       {
        $fb_profile = $data['facebook_profile'];
       }
      $email = $data['email'];
      $slug_name = create_slug($data['name'],'user','slug_name');
      $gender = $data['gender'];
      if($gender == 'male'){$gen = '0';}elseif ($gender == 'female') {
      $gen = '1';}else{$gen = '2';}
      $fb_data  = array('name'=>$data['name'],
                        'email'=>$data['email'],
                        'fb_id'=>$data['id'],
                        'gender'=>$gen,
                        'login_type'=>'1',
                        'slug_name'=>$slug_name,
                        'user_status'=>'1',
                        'user_image'=>$fb_profile);
      $checkuser = $this->User_Login_model->checkuseravailable($email);
          //print_r($fb_data);
          //print_r($checkuser);die;
        if($checkuser==1)
        {
           $userdata = $this->User_Login_model->getRecords($email);
           if (count($userdata) > 0) 
          {
              $userdata = $userdata[0];
              $user_id = $userdata['user_id'];

                  if($userdata['user_status'] == '1') 
                  {
                       
                          $newdata = array(
                              'user_id' => $userdata['user_id'],
                              'name' => $userdata['name'],
                              'email' => $userdata['email'],
                              'gender'=> $userdata['gender'],
                              'seeking' => $userdata['seeking'],
                              'slug_name' => $userdata['slug_name'],
                              'user_image' =>$userdata['user_image']
                             
                              
                          );
                          $this->session->set_userdata('session_data',$newdata);
                          $this->session->set_userdata('user_status','1');

                          $cnt_que = $this->User_Que_model->check_answered_question($user_id);
                         // echo json_encode($cnt_que);
                          //print_r($cnt_que); die;

                          if($cnt_que==0)
                            echo json_encode(0);
                            //redirect(BASE_URL. 'activities');
                          else 
                            echo json_encode(1);
                            //redirect(BASE_URL. 'question-category');
                   
                  } else 
                  {

                      // $this->session->set_flashdata('web_flash', 'Password do not match!!! Try again');
                      // redirect(BASE_URL. "login");
                  }
          } 
        }
        elseif ($checkuser==0) 
        {
          //print_r($fb_data);
            $insert_user = $this->User_Login_model->insert_users($fb_data);
            $session = array(
                              'user_id' => $insert_user,
                              'email' => $data['email'],
                              'name' => $data['name'],
                              'gender' =>$gen,
                              'user_status' =>'1',
                              //'seeking'=>$this->input->post('seeking'),
                              'slug_name' => $slug_name

                          );
           $this->session->set_userdata('session_data',$session);
            echo json_encode($insert_user);
          // $this->session->set_userdata('user_status','0');
     
        }

      echo json_encode($data);
    }

public function SndMailToExpirePlanUsers()
{
 $users = $this->User_Activity_model->SndMailToExpirePlanUsers();
 if(!empty($users))
 {
     $email=array();
     $mesg=array();
     foreach ($users as $key => $value) {
     $msg="Hello ".$value['name'].",<br/><br/><br/>Your Plan will expired in ".$value['days']." days!.Please <a href=".BASE_URL."billing-settings>click here</a> to Upgrade Plan!<br/><br/><br/>Thank You,<br>LoveUndivided Team.<br>";
     array_push($email, $value['email']);
     array_push($mesg, $msg);
     }
  $to=$email;
  $message = $mesg;
  sendMail($to,"Upgrade Plan",$message);
 }else
 {
  echo json_encode('empty data');
 }


}
    public function logout()
    {
      $this->session->unset_userdata($newdata);
      $this->session->sess_destroy();
      $this->session->set_flashdata('admin_flash', 'Logout sucessfully');
      redirect(BASE_URL);
    }


}