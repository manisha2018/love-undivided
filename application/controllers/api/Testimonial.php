<?php
class Testimonial extends CI_Controller 
{

    public function __construct() 
    {
        parent:: __construct();
        $this->load->model('api/User_Login_model');
        $this->load->helper('utility_helper');
        require_once('vendor/autoload.php');
        $this->load->model('api/User_Que_model');
        $this->load->model('api/testimonial_model');
        $this->load->helper('cookie');


        // if ($this->session->userdata('session_data')!=null) 
        // {
        //   redirect(BASE_URL.'question-category');
        // }
        //phpinfo(); die;

    }

    public function getstories()
    {
            $limit=5;
            $config["base_url"] = BASE_URL."Testimonials";
            $config['per_page']=10;
            $total_row = $this->testimonial_model->record_count();
            $config['total_rows']=$total_row;
            $this->pagination->initialize($config);
            $config["uri_segment"] =4;
            $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $data['get_stories']=$this->testimonial_model->getstories($config['per_page'], $page);
            $links = $this->pagination->create_links();
            $data["links"] = explode('&nbsp;',$links );
           
           
            //$data['get_stories'] = $this->testimonial_model->getStories();
            $this->load->view('api/testimonials',$data);
    }

    public function submit_story()
    {
        $name = $this->input->post('name');
        $email = $this->input->post('email'); 
        $story = $this->input->post('story');

        $story_data = array('couple_name'=>$name,
                             'story_text'=>$story);
     
     $img_arr = array('image/jpeg','image/jpg','image/png');
     if(in_array($_FILES['couple_image']['type'], $img_arr))
     {
        $couple_image='';
          if($_FILES['couple_image']['size']>0 && $_FILES['couple_image']['error']==0)
                {
                   $couple_image = upload_image('uploads/testimonials/','couple_image','couple_');
                   $story_data['couple_image']=$couple_image;
                }
                //print_r($_FILES['couple_image']['error']);die;
        $res = $this->testimonial_model->submit_story($story_data);
        $body =  '<b>Hello,<br><br><br> New testimonial has been added.To read this in more details please click : <a href='.BASE_URL.'admin/login>here</a><br>'; 
        $to = "manisha.bansode@eeshana.com";
       if(sendMail($to,"New Testimonial ",$body))
       
        {
            $this->session->set_flashdata('web_flash', 'Your Story Added Successfully');
            redirect(BASE_URL."Testimonials");
        }else
        {
            $this->session->set_flashdata('web_flash', 'Your Story not Added ');
            redirect(BASE_URL."Testimonials");
        }
    }else
    {
        $this->session->set_flashdata('web_flash', 'Only jpg/jpeg and png files are allowed!');
        redirect(BASE_URL."Testimonials");
    }
    }




}