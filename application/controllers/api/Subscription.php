<?php
class Subscription extends CI_Controller 
{

    public function __construct() 
    {
        parent:: __construct();
        $this->load->model('api/Subscription_model');
        $this->load->model('api/User_Login_model');
       // $this->load->helper('utility_helper');
        // if(empty($this->session->userdata('session_data')['user_id'])){
        //   redirect(BASE_URL.'login');
        // }

        require_once('vendor/autoload.php');
        \Stripe\Stripe::setApiKey(STRIPE_KEY);

    }

    public function index()
    {
        $token_card = $this->input->post('token_card');
        $plan_id = $this->input->post('plan_id');
        $is_unblur = $this->input->post('is_unblur');
        $amount = $this->input->post('amount') * 100;
        $user_id = $this->session->userdata('session_data')['user_id'];
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        try{
                      $charge = \Stripe\Charge::create(array(
                      "amount" => $amount,
                      "currency" => "usd",
                      "description" => "LU charges",
                      "source" => $token_card,
                    ));
                      if($charge->paid == true)
                      {
                        $json_success = array();
                        $json_success['status'] = 1;
                        $sql ="SELECT user_id FROM `user_subscription` where user_id = '$user_id'";
                        $userExist = $this->Subscription_model->checkUserExist($sql);
                        print_r($userExist);
                        //echo json_encode($json_success);
                      }else
                      {
                        $json_fail = array();
                        $json_fail['status'] = 0;
                        echo json_encode($json_fail);
                      }
                    
                }
                catch(Exception $e)
                {
                    echo json_encode($e->getMessage());
                }
    }
}