<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() 
    {
        parent:: __construct();
        $this->load->model('api/User_Match_model');
        $this->load->model('api/User_Login_model');
        $this->load->model('api/User_Activity_model');

        $this->load->helpers('utility_helper');
    }
	
	public function index()
	{
		//echo "abbas";
		$sql = "SELECT * FROM `user` WHERE featured_user ='1' ORDER by user_id DESC";
		$data['added_profile'] = $this->User_Activity_model->getQueryResult($sql);
		$this->load->view('api/index',$data);		
			
	}

	public function about_us()
	{
		$sql = "SELECT * FROM `user` WHERE featured_user ='1' ORDER by user_id DESC";
		$data['added_profile'] = $this->User_Activity_model->getQueryResult($sql);
		$this->load->view('api/about_us',$data);
	}

	public function contact_us()
	{
		$this->load->view('api/contact_us');
	}
	public function privacy_policy()
	{
		$this->load->view('api/privacy_policy');
	}

	public function send_visit_alert()
	{
		$arr_user = $this->User_Activity_model->get_user_visits();

		$email = [];
		$msg = [];
		foreach ($arr_user as $key => $value) {


			array_push($email, $value['email']);
	        array_push($msg," Hi ".$value['name']." <br> Your Match ".$value['visit_by_name']." has visited your profile ".$value['cnt']." time. Login to your Love Undivided Account to check out more about it. " );
			
		}

		// print_r($email);
		// print_r($msg);

		sendMail($email,"Love Undivided - Visitors",$msg);

		//print_r($arr_user); die;
	}

	public function SndMailToExpirePlanUsers()
	{
	   $users = $this->User_Activity_model->SndMailToExpirePlanUsers();
	    // print_r($users);
	   $email=array();
	   $mesg=array();
	   foreach ($users as $key => $value) {
	    if($value['days']==0 || $value['days']=='0')
        {$value['days']='today';}
      else{$value['days'] = $value['days'].' days' ;}
	    $msg="Hello ".$value['name'].",<br/><br/><br/>Your Plan will expired in ".$value['days']."!.Please <a href=".BASE_URL."billing-settings>click here</a> to Upgrade Plan!<br/><br/><br/>Thank You,<br>LoveUndivided Team.<br>";
	    array_push($email, $value['email']);
	    array_push($mesg, $msg);
	   }
	   $to=$email;
	   $message = $mesg;
	   if(sendMail($to,"Upgrade Plan",$message))
	   {
	   	echo json_encode('Mail Sent');
	   }else
	   {
	   	echo json_encode('Something Went Wrong! Mail not Sent');
	   }

	}
}
