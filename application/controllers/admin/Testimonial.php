<?php
class Testimonial extends CI_Controller 
{
	public function __construct()
	    {
	              parent::__construct();
	              $this->load->model('admin/Module_model');
	              $this->load->model('admin/Users_model');
	              $this->load->model('admin/Testimonials_model');
	              $this->load->helper('utility_helper');
	              $this->load->helper('url');	
				  
		

	    }

	public function manage_Testimonials()
	{
		
		if($this->session->userdata('admin_session_data')!=null)
		{
			$data['leftmenu'] = $this->Module_model->get_modules();
			$data['active'] = 2;
			$data['getstories'] = $this->Testimonials_model->getstories();
			//print_r($data['users']);die;
			$this->load->view('admin/testimonial',$data);
		}else{
			 redirect(BASE_URL.'admin/Login');
		}
	}

	public function delete_story()
	{
		$id = $this->input->post('testimonial_id');
        $delete_test = $this->Testimonials_model->delete_story($id);
	}

	public function is_approve_story()
	{
		$val = $this->input->post('val');
		$testimonial_id = $this->input->post('testimonial_id');
		$updateStory = $this->Testimonials_model->updateStory($testimonial_id,$val);
		echo $updateStory;
	}
	public function get_testimonial_data()
	  	{
		    $id=$this->input->post('testimonial_id');
		    $data = $this->Testimonials_model->get_testimonial_data($id)[0];
		    echo json_encode($data);

	  	}
	public function edit_testimonial()
		 {
		 	$id=$this->input->post('testimonial_id');
		 	$couple_existing_image=$this->Testimonials_model->get_testimonial_data($id);
				$imgvar=$_SERVER['DOCUMENT_ROOT']."/love-undivided/uploads/testimonials/".$couple_existing_image[0]['couple_image']; 
		   
		    $edit_testimonial_data = array(
		        'couple_name'=>$this->input->post('couple_name'),
		        'story_text'=>$this->input->post('success_story')
		        );
		     $couple_image='';
		    if($_FILES['couple_image']['size']>0 && $_FILES['couple_image']['error']==0)
		        {
		           $couple_image = upload_image('uploads/testimonials/','couple_image','couple_');
		           $edit_testimonial_data['couple_image']=$couple_image;
		        }
		    $edit_story = $this->Testimonials_model->edit_story($id,$edit_testimonial_data);
		    if($couple_image!='')
	            	unlink($imgvar);
	            
		    if($edit_story>0)
		          {
		            $this->session->set_flashdata('admin_flash', 'Data Updated Successfully');
		            redirect(BASE_URL.'admin/Testimonial/manage_Testimonials');
		          }
		          else
		          {
		            $this->session->set_flashdata('admin_flash', 'Data Not Updated');
		            redirect(BASE_URL.'admin/Testimonial/manage_Testimonials');
		          }
		 }




}