<?php
class Matches extends CI_Controller 
{
	public function __construct()
	    {
	              parent::__construct();
	              $this->load->model('admin/Module_model');
	              $this->load->model('admin/Matches_model');
	              $this->load->helper('utility_helper');
	              $this->load->helper('url');	
			
		if($this->session->userdata('admin_session_data') == '')
          {
           redirect(BASE_URL.'admin/Login');
          }

	    }

	public function getMatches()
	{
		if($this->session->userdata('admin_session_data')!=null)
				{
					$data['leftmenu'] = $this->Module_model->get_modules();
					$data['active'] = 6;
					$data['userlist'] = $this->Matches_model->getUserList();
					//print_r($data['category']);die;
					$this->load->view('admin/matches',$data);
				}
				else
			    {
			       redirect(BASE_URL.'admin/Login');
			    }
	}

	public function getIndividualMatches()
	{
		if($this->session->userdata('admin_session_data')!=null)
				{
					$data['leftmenu'] = $this->Module_model->get_modules();
					$data['active'] = 6;
					$id = $this->uri->segment(4);
					//$data['userlist'] = $this->Matches_model->getUserList();
					//print_r($data['category']);die;
					$this->load->view('admin/individual_matches',$data);
				}
	}

	public function delete_user()
	{
		$id = $this->input->post('user_id');
	    $delete_user = $this->Matches_model->delete_user($id);
	 
	}









}

