<?php
class Categories extends CI_Controller 
{
	public function __construct()
	    {
	              parent::__construct();
	              $this->load->model('admin/Module_model');
	              $this->load->model('admin/Category_model');
	              $this->load->helper('utility_helper');
	              $this->load->helper('url');	
				  
		if($this->session->userdata('admin_session_data') == '')
          {
           redirect(BASE_URL.'admin/Login');
          }

	    }


	    public function getCategory()
	    {
	    	if($this->session->userdata('admin_session_data')!=null)
				{
					$data['leftmenu'] = $this->Module_model->get_modules();
					$data['active'] = 3;
					$data['category'] = $this->Category_model->getCategory();
					//print_r($data['category']);die;
					$this->load->view('admin/category',$data);
				}else
			    {
			       redirect(BASE_URL.'admin/Login');
			    }
			    	
	    }

	    public function insert_category()
	    {
	    	$category_name = $this->input->post('category_name');
	    	if(ctype_space($category_name))
			{
				$this->session->set_flashdata('error_flash', 'Please enter category!');
				redirect(BASE_URL.'admin/Categories/getCategory');
			}else
			{
				$cat = array('category_name'=>$category_name);
		    	$insert =  $this->Category_model->insert_category($cat);
		    	if($insert>0)
		    	{
		    		  $this->session->set_flashdata('admin_flash', 'Category inserted Successfully');
		    		  redirect(BASE_URL.'admin/Categories/getCategory');
		    	}
			}
	    	
	    }


	    public function delete_category()
	    {
	    	 $id = $this->input->post('category_id');
	    	 $delete_cat = $this->Category_model->delete_category($id);

	    }

	    public function get_cat_data()
	    {
	    	 $id=$this->input->post('category_id');
	    	 $data = $this->Category_model->get_cat_data($id)[0];

	    	 echo json_encode($data);
	    }

	    public function edit_cat_data()
	    {
	    	 $id=$this->input->post('category_id');
	    	 $cat_name=$this->input->post('category_name');
	    	 $edit_cat = $this->Category_model->edit_cat_data($id,$cat_name);
	    	 if($edit_cat>0)
	    		{
		    		$this->session->set_flashdata('admin_flash', 'Category Updated Successfully');
		    		redirect(BASE_URL.'admin/Categories/getCategory');
	    		}
	    		else
	    		{
		    		$this->session->set_flashdata('admin_flash', 'Category Not Updated');
		    		redirect(BASE_URL.'admin/Categories/getCategory');
	    		}
	    }
	/**
  * CRUD operation For Categories
  * return type-
  * created on -14th June 2017;
  * updated on -
  * created by - Manisha Bansode;
  */

}