<?php
class Promo_code extends CI_Controller 
{
	public function __construct()
	    {
	              parent::__construct();
	              $this->load->model('admin/Module_model');
	              $this->load->model('admin/Users_model');
	              $this->load->model('admin/PromoCode_model');
	              $this->load->helper('utility_helper');
	              $this->load->helper('url');	
				  
		

	    }

	public function manage_promo_code()
	{
		
		if($this->session->userdata('admin_session_data')!=null)
		{
			$data['leftmenu'] = $this->Module_model->get_modules();
			$data['active'] = 2;
			$data['getpromocode'] = $this->PromoCode_model->getpromocode();
			//print_r($data['users']);die;
			$this->load->view('admin/promo_code',$data);
		}
		else
	    {
	       redirect(BASE_URL.'admin/Login');
	    }
	}

	public function insert_code()
  		{
//print_r($this->input->post());die;
  			$is_free = $this->input->post('is_free');
  			if(isset($_POST['is_free']))
  			{
  				 $code_data = array(
	                      'code'=>$this->input->post('code'),
	                      'is_free'=>1
	                      
	                      );
  				
  			}else
  			{
  				$code_data = array(
	                      'code'=>$this->input->post('code'),
	                      'is_free'=>0,
	                      'duration'=>$this->input->post('duration')
	                      
	                      );
  				
  			}
	    //print_r($code_data);die; 

	       $insert =  $this->PromoCode_model->insert_code($code_data);
	       //echo $insert;die;
	       if($insert>0)
	       {
	        $this->session->set_flashdata('admin_flash', 'Data inserted Successfully!');
	        redirect(BASE_URL.'admin/Promo_code/manage_promo_code');
	       }
	       else
	       {
	        $this->session->set_flashdata('admin_flash', 'Data Not Inserted!');
	        redirect(BASE_URL.'admin/Promo_code/manage_promo_code');
	       }

		}
		public function deletepromocode()
  		{
		     $id = $this->input->post('promo_id');
		     $deletepromocode = $this->PromoCode_model->deletepromocode($id);
  		}

  		public function get_promo_data()
	  	{
		    $id=$this->input->post('promo_id');
		    $data = $this->PromoCode_model->get_promo_data($id)[0];
		    echo json_encode($data);

	  	}
	  	public function edit_promocode()
		 {
		 	//print_r($this->input->post());die;
			    $id=$this->input->post('promo_id');

			    if(isset($_POST['is_free1']))
	  			{
	  				 $edit_code_data = array(
		                      'code'=>$this->input->post('code'),
		                      'is_free'=>1
		                      
		                      );
	  				
	  			}else
	  			{
	  				$edit_code_data = array(
		                      'code'=>$this->input->post('code'),
		                      'is_free'=>0,
		                      'duration'=>$this->input->post('duration')
		                      
		                      );
	  				
	  			}
			    
			    //print_r($edit_code_data);die;
			    $edit_code = $this->PromoCode_model->edit_code($id,$edit_code_data);
			    if($edit_code>0)
			          {
			            $this->session->set_flashdata('admin_flash', 'Data Updated Successfully');
			            redirect(BASE_URL.'admin/Promo_code/manage_promo_code');
			          }
			          else
			          {
			            $this->session->set_flashdata('admin_flash', 'Data Not Updated');
			            redirect(BASE_URL.'admin/Promo_code/manage_promo_code');
			          }
		 }



}