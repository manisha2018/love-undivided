<?php
class Terms_Conditions extends CI_Controller 
{

	//echo "hieee";
	public function __construct()
	    {
	              parent::__construct();
	              $this->load->model('admin/Module_model');
	              $this->load->model('admin/Terms_Conditions_model');
	              $this->load->helper('utility_helper');
	              $this->load->helper('url');	
				  
		if($this->session->userdata('admin_session_data') == '')
          {
           redirect(BASE_URL.'admin/Login');
          }

	    }


	    public function getTermsConditions()
	    {
	    	if($this->session->userdata('admin_session_data')!=null)
				{
					$data['leftmenu'] = $this->Module_model->get_modules();
					$data['active'] = 7;
					$data['getTermsConditions'] = $this->Terms_Conditions_model->getTermsConditions();
					//print_r($data['category']);die;
					$this->load->view('admin/terms_conditions',$data);
				}
				else
				{
			 		redirect(BASE_URL.'admin/Login');
				}
			    	
	    }

	    public function insert_terms()
  		{

	      $terms_data = array(
	                      'name'=>$this->input->post('name'),
	                      'description'=>$this->input->post('description')
	                      
	                      );

	       $insert =  $this->Terms_Conditions_model->insert_terms($terms_data);
	       //echo $insert;die;
	       if($insert>0)
	       {
	        $this->session->set_flashdata('admin_flash', 'Data inserted Successfully!');
	        redirect(BASE_URL.'admin/Terms_Conditions/getTermsConditions');
	       }
	       else
	       {
	        $this->session->set_flashdata('admin_flash', 'Data Not Inserted!');
	        redirect(BASE_URL.'admin/Terms_Conditions/getTermsConditions');
	       }

		}

		public function delete_terms()
  		{
		     $id = $this->input->post('term_cond_id');
		     $delete_terms = $this->Terms_Conditions_model->delete_terms($id);
  		}
  		public function get_terms_data()
	  	{
		    $id=$this->input->post('term_cond_id');
		    $data = $this->Terms_Conditions_model->get_terms_data($id)[0];
		    echo json_encode($data);

	  	}
	  	public function edit_terms()
		 {
		    $id=$this->input->post('term_cond_id');
		    $edit_term_data = array(
		        'name'=>$this->input->post('name'),
		        'description'=>$this->input->post('description1')
		        );
		    $edit_terms = $this->Terms_Conditions_model->edit_terms($id,$edit_term_data);
		    if($edit_terms>0)
		          {
		            $this->session->set_flashdata('admin_flash', 'Data Updated Successfully');
		            redirect(BASE_URL.'admin/Terms_Conditions/getTermsConditions');
		          }
		          else
		          {
		            $this->session->set_flashdata('admin_flash', 'Data Not Updated');
		            redirect(BASE_URL.'admin/Terms_Conditions/getTermsConditions');
		          }
		 }

}