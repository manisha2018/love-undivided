<?php
class Contact_us extends CI_Controller 
{
	public function __construct()
	    {
	              parent::__construct();
	              $this->load->model('admin/Module_model');
	              $this->load->model('admin/Contact_us_model');
	              $this->load->helper('utility_helper');
	              $this->load->helper('url');	
				  
		if($this->session->userdata('admin_session_data') == '')
          {
           redirect(BASE_URL.'admin/Login');
          }

	    }


	    public function getContacts()
	    {
	    	if($this->session->userdata('admin_session_data')!=null)
				{
					$data['leftmenu'] = $this->Module_model->get_modules();
					$data['active'] = 7;
					$data['getcontacts'] = $this->Contact_us_model->getContacts();
					//print_r($data['category']);die;
					$this->load->view('admin/contact_us',$data);
				}else
			    {
			       redirect(BASE_URL.'admin/Login');
			    }
			    	
	    }
	    public function delete_contact()
	    {
	    	 $id = $this->input->post('contact_us_id');
	    	 $delete_contact = $this->Contact_us_model->delete_contact($id);

	    }




}