<?php
class Pricing_plan extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('admin/Pricing_model');
        $this->load->model('admin/Module_model');
        $this->load->helper('utility_helper');
        $this->load->helper('url');

        if($this->session->userdata('admin_session_data') == '')
          {
           redirect(BASE_URL.'admin/Login');
          }
    }

    public function getPricingPlan()
    {
    	if($this->session->userdata('admin_session_data')!=null)
				{
					$data['leftmenu'] = $this->Module_model->get_modules();
					$data['active'] = 6;
					$data['pricing'] = $this->Pricing_model->getPricingPlan();
					//print_r($data['pricing']);die;
					$this->load->view('admin/pricing_plan',$data);
				}else
        {
           redirect(BASE_URL.'admin/Login');
        }
    }

    public function insert_plan()
    {
        $plan_name = $this->input->post('plan_name');
        $price_per_month = $this->input->post('price_per_month');
        $duration = $this->input->post('duration');
        $plan = array('plan_name'=>$this->input->post('plan_name'),
                       'price_per_month'=>$this->input->post('price_per_month'),
                       'duration'=>$this->input->post('duration')
                     );
        $insert =  $this->Pricing_model->insert_plan($plan);
        if($insert>0)
        {
              $this->session->set_flashdata('admin_flash', 'Plan inserted Successfully');
              redirect(BASE_URL.'admin/Pricing_plan/getPricingPlan');
        }
    }

    public function delete_plan()
    {
        $id = $this->input->post('plan_id');
        $delete_cat = $this->Pricing_model->delete_plan($id);
    }
    public function get_plan_data()
        {
             $id=$this->input->post('plan_id');
             $data = $this->Pricing_model->get_plan_data($id)[0];
             echo json_encode($data);
        }

    public function edit_plan_data()
    {
        $id=$this->input->post('plan_id');
        $cat_name=$this->input->post('category_name');
        $plan_data = array('plan_name'=>$this->input->post('plan_name'),
                       'price_per_month'=>$this->input->post('price_per_month'),
                       'duration'=>$this->input->post('duration')
                     );

        $edit_plan = $this->Pricing_model->edit_plan_data($id,$plan_data);
        if($edit_plan>0)
            {
                $this->session->set_flashdata('admin_flash', 'Plan Updated Successfully');
                redirect(BASE_URL.'admin/Pricing_plan/getPricingPlan');
            }
            else
            {
                $this->session->set_flashdata('admin_flash', 'Plan Not Updated');
                redirect(BASE_URL.'admin/Pricing_plan/getPricingPlan');
            }
    }

      /**
  * CRUD operation for Pricing Plan
  * return type-
  * created on -19th June 2017;
  * updated on -
  * created by - Manisha Bansode;
  */

}