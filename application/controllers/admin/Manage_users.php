<?php
class Manage_users extends CI_Controller 
{
	public function __construct()
	    {
	              parent::__construct();
	              $this->load->model('admin/Module_model');
	              $this->load->model('admin/Users_model');
	              $this->load->helper('utility_helper');
	              $this->load->helper('url');	
				  
		if($this->session->userdata('admin_session_data') == '')
          {
           redirect(BASE_URL.'admin/Login');
          }

	    }

	public function getUsers()
	{
		
		if($this->session->userdata('admin_session_data')!=null)
		{
			$data['leftmenu'] = $this->Module_model->get_modules();
			$data['active'] = 2;
			$data['users'] = $this->Users_model->getUsers();
			$sql ="SELECT COUNT(featured_user) as featurecount FROM user WHERE featured_user='1'";
			$res = $this->Users_model->getQueryResult($sql);
			$featurecount = $res[0]['featurecount'];
			$data['featurecount'] = $res[0]['featurecount'];
			//echo"<pre>";
			//print_r($data['users']);die;
			$this->load->view('admin/manage_users',$data);
		}else{
			 redirect(BASE_URL.'admin/Login');
		}
	}

	public function delete_user()
	{
		$id = $this->input->post('user_id');
	    $delete_user = $this->Users_model->delete_user($id);
	 
	}

		/**
  * CRUD operation for Users
  * return type-
  * created on -14th June 2017;
  * updated on -
  * created by - Manisha Bansode;
  */

		public function get_usersplan_data()
		{
			$id=$this->input->post('user_id');
			$userplan = $this->Users_model->get_usersplan_data($id);
			if(empty($userplan))
			{
				$json_noplan = array();
				$json_noplan['status'] = 0;
				$json_noplan['response']="No Any Subscription Plan";
				echo json_encode($json_noplan);
			}else
			{
				echo json_encode($userplan[0]);
			}
			
		}
		/**
  * Billing plan of users
  * return type-
  * created on -28th June 2017;
  * updated on -
  * created by - Manisha Bansode;
  */


		public function feature_user()
		{
			$user_id = $this->input->post('user_id');
			$val = $this->input->post('val');
			$sql ="SELECT COUNT(featured_user) as featurecount FROM user WHERE featured_user='1'";
			$res = $this->Users_model->getQueryResult($sql);
			$featurecount = $res[0]['featurecount'];
			if($val==0)
			{
				$updateFeature = $this->Users_model->feature_user($user_id,$val);
			    echo $updateFeature;
			}else
			{
				if($featurecount>=9)
				{
					echo "false";
				}else
				{
					$updateFeature = $this->Users_model->feature_user($user_id,$val);
			    	echo $updateFeature;
				}
			}
			
			
			
		}
}