<?php
class FAQs extends CI_Controller 
{
	public function __construct()
	    {
	              parent::__construct();
	              $this->load->model('admin/Module_model');
	              $this->load->model('admin/Users_model');
	              $this->load->model('admin/FAQs_model');
	              $this->load->helper('utility_helper');
	              $this->load->helper('url');	
				  
		

	    }

	public function manage_Faqs()
	{
		
		if($this->session->userdata('admin_session_data')!=null)
		{
			$data['leftmenu'] = $this->Module_model->get_modules();
			$data['active'] = 2;
			$data['getFaqs'] = $this->FAQs_model->getFaqs();
			//print_r($data['users']);die;
			$this->load->view('admin/faqs',$data);
		}else
    {
       redirect(BASE_URL.'admin/Login');
    }
	}

	public function insert_faqs()
    {
        $question = $this->input->post('question');
        $answer = $this->input->post('answer');
        $faqs = array('question'=>$question,
                       'answer'=>$answer
                       
                     );
        $insert =  $this->FAQs_model->insert_faqs($faqs);
        if($insert>0)
        {
              $this->session->set_flashdata('admin_flash', 'Question inserted Successfully');
              redirect(BASE_URL.'admin/FAQs/manage_Faqs');
        }
    }

    public function get_faqs_data()
    {
             $id=$this->input->post('faq_id');
             $data = $this->FAQs_model->get_faqs_data($id)[0];
             echo json_encode($data);
    }

    public function edit_faqs()
    {
    	$id=$this->input->post('faq_id');
        $faq_data = array('question'=>$this->input->post('question'),
                          'answer'=>$this->input->post('answer')
                     	 );

        $edit_faqs = $this->FAQs_model->edit_faqs($id,$faq_data);
        if($edit_faqs>0)
            {
                $this->session->set_flashdata('admin_flash', 'Question Updated Successfully');
                redirect(BASE_URL.'admin/FAQs/manage_Faqs');
            }
            else
            {
                $this->session->set_flashdata('admin_flash', 'Question Not Updated');
                redirect(BASE_URL.'admin/FAQs/manage_Faqs');
            }
    }

    public function delete_faqs()
    {
        $id = $this->input->post('faq_id');
        $delete_faq = $this->FAQs_model->delete_faqs($id);
    }





}