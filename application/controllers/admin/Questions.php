<?php
class Questions extends CI_Controller 
{
  function __construct()
  {
    parent::__construct();
   
    $this->load->helper('url'); 
    $this->load->model('admin/Module_model');
    $this->load->model('admin/Question_model');
    $this->load->model('admin/Category_model');
   
    if($this->session->userdata('admin_session_data') == '')
          {
           redirect(BASE_URL.'admin/Login');
          }
    
  }


  public function getQuestions()
  {
    if($this->session->userdata('admin_session_data')!='')
      {
        $data['leftmenu'] = $this->Module_model->get_modules();
        $data['active'] = "1";
        $data['category'] = $this->Category_model->getCategory();
        $data['option_group'] = $this->Question_model->getOptionGroup();
        $data['question'] = $this->Question_model->getQuestions();
        //print_r($data['question']);die;
        $this->load->view('admin/questions',$data);
    }
    else
    {
       redirect(BASE_URL.'admin/Login');
    }
    
  }

  public function insert_question()
  {

      $que_data = array(
                      'question'=>$this->input->post('question'),
                      'option_group_id'=>$this->input->post('option_group'),
                      'category_id'=>$this->input->post('category')
                      );

       $insert =  $this->Question_model->insert_question($que_data);
       //echo $insert;die;
       if($insert>0)
       {
        $this->session->set_flashdata('admin_flash', 'Question inserted Successfully!');
        redirect(BASE_URL.'admin/Questions/getQuestions');
       }
       else
       {
        $this->session->set_flashdata('admin_flash', 'Question Not Inserted!');
        redirect(BASE_URL.'admin/Questions/getQuestions');
       }


  }

  public function delete_question()
  {
     $id = $this->input->post('question_id');
     $delete_cat = $this->Question_model->delete_question($id);
  }

  public function get_que_data()
  {
    $id=$this->input->post('question_id');
    $data = $this->Question_model->get_que_data($id)[0];
    echo json_encode($data);

  }
  /**
  * CRUD operation For Questions
  * return type-
  * created on -20th June 2017;
  * updated on -
  * created by - Manisha Bansode;
  */

  public function edit_que_data()
  {
    $id=$this->input->post('question_id');
  
    $edit_que_data = array(
      'question'=>$this->input->post('question'),
      'category_id'=>$this->input->post('category'),
      'option_group_id'=>$this->input->post('option_group')
      );
    $edit_que = $this->Question_model->edit_que_data($id,$edit_que_data);
      if($edit_que>0)
          {
            $this->session->set_flashdata('admin_flash', 'Question Updated Successfully');
            redirect(BASE_URL.'admin/Questions/getQuestions');
          }
          else
          {
            $this->session->set_flashdata('admin_flash', 'Question Not Updated');
            redirect(BASE_URL.'admin/Questions/getQuestions');
          }
  }









}