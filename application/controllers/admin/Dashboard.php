<?php
class Dashboard extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		//$this->load->model('admin/Dashboard_model');
		$this->load->helper('url');	
		$this->load->model('admin/Module_model');
		if($this->session->userdata('admin_session_data') == '')
          {
           redirect(BASE_URL.'admin/Login');
          }
		
	}


	public function index()
	{
		if($this->session->userdata('admin_session_data')!='')
  		{
  			$data['leftmenu'] = $this->Module_model->get_modules();
  				$data['active'] = "1";
  			//print_r($data['leftmenu']);die;
			$this->load->view('admin/dashboard',$data);
		}else
		{
			 redirect(BASE_URL.'admin/Login');
		}
		
	}


	/**
  * Dashboard
  * return type-
  * created on -13th June 2017;
  * updated on -
  * created by - Manisha Bansode;
  */








}