<?php
class Login extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('admin/Login_model');
        $this->load->model('admin/Module_model');
        $this->load->helper('utility_helper');
        $this->load->helper('url');
        $this->load->helper('cookie');
    }
	

    public function index()
    {
            $this->load->view('admin/login');
    }

    public function admin_login()
    {
        $user_email = $this->input->post('user_email'); 
        $user_password = $this->input->post('user_password');
        $remember = $this->input->post('checkbox');

        $userdata  = $this->Login_model->get_user_details($user_email);
       // print_r($userdata);die();
        if(count($userdata)>0)
        {
            $userdata = $userdata[0];
            // if(isset($remember))
            // {
            //   $a=array('user_email'=>$user_email,'user_password'=>$user_password);
            //   $b= json_encode($a);
            //   $cookie1= array(
            //           'name'   => 'user_email',
            //           'value'  => $b,
            //            'expire' => '86500',
            //       );
               
            //     $this->input->set_cookie($cookie1);
            //   }

  
          

            if(verifyPasswordHash($user_password,$userdata['user_password']) == TRUE)
            {
                $newdata = array(
                    'userid'  => $userdata['userid'],
                    'user_name'  => $userdata['user_name'],
                    'user_email' => $userdata['user_email'],
                    'phone_no' => $userdata['phone_no'],
                    'user_profile'  => $userdata['user_profile']
                );
               
                $this->session->set_userdata('admin_session_data',$newdata);
               // print_r($newdata);die;
                $this->session->set_flashdata('admin_flash', 'Logged in Successfully');
                redirect(BASE_URL."admin/Dashboard");
            }
            else
            {
                $this->session->set_flashdata('admin_flash', 'Password do not match!!! Try again');
                redirect(BASE_URL."admin/Login");
            }
        }
        else
        {
            $this->session->set_flashdata('admin_flash', 'Email do not match!!! Try again');
            redirect(BASE_URL."admin/Login");
        }
        
    }
    public function admin_logout()
    {
         
           $this->session->unset_userdata($newdata);
           $this->session->sess_destroy();
           $this->session->set_flashdata('admin_flash', 'Logout sucessfully');
           redirect(BASE_URL.'admin/Login');
       
    }

      /**
  * Login And logout
  * created on -13th June 2017;
  * updated on -
  * created by - Manisha Bansode;
  */
    public function getProfile()
    {
        if($this->session->userdata('admin_session_data')!='')
        {
          
            $data['leftmenu'] = $this->Module_model->get_modules();
            $data['active'] = "1";
            
            $this->load->view('admin/view_profile',$data);
        }

    }

    public function update_admin_profile()
    {
       
        // $user_profile='';
        //   if($_FILES['user_profile']['size']>0 && $_FILES['user_profile']['error']==0)
        //         {
        //            $user_profile = upload_image('uploads/admin_profile/','user_profile','user_profile_');

        //         }

        $admin_data=array
            (
                'user_name'=>$_POST['user_name'], 
                'user_email'=>$_POST['user_email'],
                //'user_profile'=>$user_profile,
                'phone_no'=>$_POST['phone_no'],
                'userid'=>$_POST['userid']
            );

        // if($user_profile!='')
        //     {
        //         $admin_data['user_profile']=$user_profile;
        //     }
        $admin_id=$this->input->post('userid');
        $admin_existing_image=$this->Login_model->getRecords($admin_id);
        $profile=$this->Login_model->updateRecord($admin_data,$admin_id);       
        // if(isset($admin_existing_image[0]['user_profile']))
        // {
        //     $imgvar=$_SERVER['DOCUMENT_ROOT']."/love_undivided/uploads/admin_profile/".$admin_existing_image[0]['user_profile']; 

        //     $imgvar1=$admin_existing_image[0]['user_profile'];
        //     if(!empty($imgvar1))
        //     {
        //         unlink($imgvar);
        //     } 
        // }
                $newdata = array
            (
                 'userid'=>$_POST['userid'],
                'user_name' => $_POST['user_name'],
                'user_email' => $_POST['user_email'],
                'phone_no' => $_POST['phone_no'],
                //'user_profile' =>$user_profile
                
            );
        //print_r($_SESSION);
        $this->session->set_userdata('admin_session_data',$newdata);
        echo $profile;

        
    }

    public function changepassword()
    {
        $user_name=$this->input->post('user_name');
        $oldpassword=$this->input->post('oldpassword');
        $password=createHashAndSalt($this->input->post('password'));
        $admin_id=$this->input->post('userid');
        $userdata = $this->Login_model->getRecords($admin_id);

        if (count($userdata) > 0) 
            {
                  $userdata = $userdata[0];

                if (verifyPasswordHash($oldpassword, $userdata['user_password']) == TRUE) 
                    {
                        $edit_pass=array('user_password'=>$password);
                        $val=$this->Login_model->updateRecord( $edit_pass,$admin_id);
                        echo $val;
                         //echo "success";
                   
                     
                    } else 
                    {

                        echo "fail";
                    }
            }
    }
   
    public function forgot_password()
    {
      $user_email = $this->input->post('user_email');
      $admin_email=$this->Login_model->getAdminRecords($user_email);
            //echo "<pre>";print_r($admin_email); die;
            if($admin_email[0]['user_email']==$user_email)
            {
                //echo"send mail"; die;
            $body =  '<b>Hello,<br><br><br>To update your password click</b> <a href='.BASE_URL.'admin/Login/update_password?user_email='.$user_email.'>here</a> <br><br><br><b>If you have any questions, please email <span style="color:red;">Questions@LoveUndivided.com.</span><br><br><br>Best,<br><br><br></b>'; 

           $to=$user_email;
          //$to =array($mailids);
          if(sendMail($to,"Update Password ",$body))
            {                   
            $this->session->set_flashdata('admin_flash', 'Please Check Your Email!');
            redirect(BASE_URL."admin/Login");
            }else
            {
            $this->session->set_flashdata('admin_flash', 'Email Not Sent ');
            redirect(BASE_URL."admin/Login");
            }


            }else
            {
                $this->session->set_flashdata('admin_flash', 'You Entered Wrong Email ID! ');
                redirect(BASE_URL."admin/Login");
            }

    }

    public function update_password()
    {
        $user_email=$this->input->get('user_email');
        $this->load->view('admin/forgot_password',$user_email);
             
    }
       
     public function change_pass()
    {
          $user_email =  $this->input->post('user_email'); 
          $new_password =  $this->input->post('user_password');
          $confirm_password =  $this->input->post('confirm_password');


          $userdata  =  $this->Login_model->getAdminRecords($user_email);
         // print_r($userdata);die();
          if(count($userdata)>0)
          {
               if($new_password==$confirm_password)
               {

                  $user_password  =  createHashAndSalt($new_password);
                  $userdata = $userdata[0];
                  $inputArray['user_password']  = $user_password;

                  $data=array();
                  $data['user_email']=$user_email ;
                  $data['user_password'] = $user_password;
                 
                  $result=$this->Login_model->update_admin_pass($user_email,$user_password);
                  //print_r($result);die;
                    if($result)
                    {

                       $to =  $user_email;
                       $subject = 'Password Update';
                       $message = '<b>Hello,<br><br><br>Your new password is:-'.$new_password.'<br><br><br>If you have any questions, please email <span style="color:red;">Questions@LoveUndivided.com.</span><br><br><br>Best,<br><br><br></b>';
                       $send=sendMail($to,$subject,$message);
                       $this->session->set_flashdata('admin_flash', 'Your Password is Updated Successfully!');
                          redirect(BASE_URL."admin/Login");
                    }
                    else
                    {
                     $this->session->set_flashdata('admin_flash', 'Your Password is Not Updated');
                     redirect(BASE_URL."admin/Login");
                    }
                  

               }
               else
               {
                 $this->session->set_flashdata('admin_flash', 'Password do not match!!! Try again');
                 redirect(BASE_URL."admin/Login/update_password?username=$user_email");
                }
              
          }
          else
          {
           $this->session->set_flashdata('admin_flash', 'Email ID not exist!!! Try again');
            redirect(BASE_URL."admin/Login");
          }
    }


  /**
  * Basic Functionality(change admin profile,chnage password etc)
  * created on -16th June 2017;
  * updated on -
  * created by - Manisha Bansode;
  */






}