<?php
class Bulk_Emails extends CI_Controller 
{
	public function __construct()
	    {
	              parent::__construct();
	              $this->load->model('admin/Module_model');
	              $this->load->model('admin/Users_model');
	              $this->load->helper('utility_helper');
	              $this->load->helper('url');	
				  
		

	    }

	public function manage_email()
	{
		
		if($this->session->userdata('admin_session_data')!=null)
		{
			$data['leftmenu'] = $this->Module_model->get_modules();
			$data['active'] = 2;
			$data['users'] = $this->Users_model->getUsers();
			$query="SELECT * FROM `promo_code` ";
			$data['coupan_code'] = $this->Users_model->getQueryResult($query);
			//print_r($data['users']);die;
			$this->load->view('admin/manage_emails',$data);
		}else
	    {
	       redirect(BASE_URL.'admin/Login');
	    }
	}

	public function send_mail()
	{
		//print_r($this->input->post());die;
		$email_title = $this->input->post('email_title');
		$coupan_code = $this->input->post('coupan_code');
		$email_subject = $this->input->post('email_subject');
		$email_body = $this->input->post('email_body');
		$all_users = $this->input->post('all_users');
		$region = $this->input->post('city');
		$age1 = $this->input->post('age1');
		$age2 = $this->input->post('age2');
		$email_tags = $this->input->post('email_tags');

		if($all_users==1)
			{
				$getUsers = $this->Users_model->getUsers();
				$email=array();
				$msg=array();
		    	foreach ($getUsers as $key => $value) 
		    	{
		    		if($coupan_code!="")
						{
							$msg1 = array("[name]","[promo_code]");
							$msg2 = array($value['name'],$coupan_code,$email_body);
							$succ_msg = str_replace($msg1, $msg2,$email_body);
						}else
						{
							$msg1 = array("[name]");
							$msg2 = array($value['name'],$email_body);
							$succ_msg = str_replace($msg1, $msg2,$email_body);
						}
	          		array_push($email, $value['email']);
	          		array_push($msg,$succ_msg);
		    	}
		    	$username = implode(',', $email);
		      	$to=$email;
		      	$message = $msg;
		      	if(sendMail($to,$email_subject,$message))
		      	{
		      		$this->session->set_flashdata('admin_flash', 'Email Sent Sucessfully');
		      		redirect(BASE_URL.'admin/Bulk_Emails/manage_email');
		      	}else
		      	{
		      		$this->session->set_flashdata('admin_flash', 'Email Not Sent');
		      		redirect(BASE_URL.'admin/Bulk_Emails/manage_email');
		      	}
			
			}
		else
			{
					if(!empty($region))
					{
						$query = "SELECT * FROM user WHERE city LIKE '$region%'";
						$res = $this->Users_model->getQueryResult($query);
						$email=array();
						$msg=array();
						foreach ($res as $key => $value) {
							if($coupan_code!="")
							{
								$msg1 = array("[name]","[promo_code]");
								$msg2 = array($value['name'],$coupan_code,$email_body);
								$succ_msg = str_replace($msg1, $msg2,$email_body);
							}else
							{
								$msg1 = array("[name]");
								$msg2 = array($value['name'],$email_body);
								$succ_msg = str_replace($msg1, $msg2,$email_body);
							}
							
			          		array_push($email, $value['email']);
			          		array_push($msg,$succ_msg);
						}
						$username = implode(',', $email);
				      	$to=$email;
				      	$message = $msg;
				      	if(sendMail($to,$email_subject,$message))
				      	{
				      		$this->session->set_flashdata('admin_flash', 'Email Sent Sucessfully');
				      		redirect(BASE_URL.'admin/Bulk_Emails/manage_email');
				      	}else
				      	{
				      		$this->session->set_flashdata('admin_flash', 'Email Not Sent');
				      		redirect(BASE_URL.'admin/Bulk_Emails/manage_email');
				      	}
						

					}
					elseif($age1!='' || $age2!='')
					{
						//echo $age1; echo $age2;
						//die;
						if($age1!= 1 || $age2!= 1)
						{
							$query = "SELECT * FROM `user` WHERE age  BETWEEN $age1 and $age2";
							$res = $this->Users_model->getQueryResult($query);
							$email=array();
							$msg=array();
							foreach ($res as $key => $value) {
								if($coupan_code!="")
								{
									$msg1 = array("[name]","[promo_code]");
									$msg2 = array($value['name'],$coupan_code,$email_body);
									$succ_msg = str_replace($msg1, $msg2,$email_body);
								}else
								{
									$msg1 = array("[name]");
									$msg2 = array($value['name'],$email_body);
									$succ_msg = str_replace($msg1, $msg2,$email_body);
								}
				          		array_push($email, $value['email']);
				          		array_push($msg,$succ_msg);
							}
							$username = implode(',', $email);
					      	$to=$email;
					      	$message = $msg;
					      	// print_r($to);
					      	// print_r($message);die;
					      	if(sendMail($to,$email_subject,$message))
					      	{
					      		$this->session->set_flashdata('admin_flash', 'Email Sent Sucessfully');
					      		redirect(BASE_URL.'admin/Bulk_Emails/manage_email');
					      	}else
					      	{
					      		$this->session->set_flashdata('admin_flash', 'Email Not Sent');
					      		redirect(BASE_URL.'admin/Bulk_Emails/manage_email');
					      	}
						}
						elseif(is_array($email_tags))
						{
							//print_r($this->input->post());die;
							$multiple_users = $this->input->post('email_tags');
							if($coupan_code!="")
							{
								$msg1 = array("[promo_code]");
								$msg2 = array($coupan_code,$email_body);
								$msg = str_replace($msg1, $msg2,$email_body);
							}else
							{
								$msg = $email_body;
							}
							
							$email_id = implode(",", $multiple_users);
							$to = $multiple_users;
							if(sendMail($to,$email_subject,$msg))
						      	{
						      		$this->session->set_flashdata('admin_flash', 'Email Sent Sucessfully');
						      		redirect(BASE_URL.'admin/Bulk_Emails/manage_email');
						      	}else
						      	{
						      		$this->session->set_flashdata('admin_flash', 'Email Not Sent');
						      		redirect(BASE_URL.'admin/Bulk_Emails/manage_email');
						      	}
								


						}else
						{
							$this->session->set_flashdata('admin_flash', 'Please choose one of filter Age Range or Region!');
					      	redirect(BASE_URL.'admin/Bulk_Emails/manage_email');
							
						}
					
						
					}

			}
			
	
			
		//print_r($this->input->post());
		
			
		



		
		
		 


		
	}


}